<?php
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
defined('BASEPATH') OR exit('No direct script access allowed');

class Cro extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		print_r($this->session->userdata());
	}

	public function cro_login(){
		$decode = explode(";",base64_decode($this->input->post('uuid')));
		$session = array("email" => $decode[0],
			"password" => "",
			"nama" => $decode[1],
			"uuid" => $decode[3],
			"kategori" => $decode[4],
			"cro" => "true",
			"logged_in" => true);

		$this->session->set_userdata($session);
		redirect(base_url('home'));
	}
}
