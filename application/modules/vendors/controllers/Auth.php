<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	//$halaman_vendor_api="http://localhost/api-yeps/auth/login_vendor";
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->session->sess_destroy();
		header("location: ".base_url(''));
	}

	public function logout(){
		if(@$this->session->userdata('cro') == 'true'){
			$this->session->sess_destroy();
			header("location: https://localhost/beta15/dashboard/data_vendor");
		}
		else{
			$this->session->sess_destroy();
			header("location: ".base_url(''));	
		}
	}

	public function login(){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->config->item('api_http')."/auth/login_vendor",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "email=".$this->input->post('email'),
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/x-www-form-urlencoded"
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			$data = json_decode($response);
			
			if(is_object($data)){
				echo $response;
			}
			else{
				$session = array("email" => $data[0]->email,
					"uuid" => $data[0]->uuid,
					"logged_in" => true);
					
				$this->session->set_userdata($session);
				unset($data[0]->uuid);
				unset($data[0]->email);
				echo json_encode($data);	
			}
		}
	}
}
