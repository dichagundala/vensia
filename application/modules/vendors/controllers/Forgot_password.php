<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot_password extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('lupa-password');
	}

	public function newpass($encode)
	{
		if(@$encode == ""){
			header("location: ".base_url(''));
		}
		else{
			$hasil_encode = explode(';', base64_decode($encode));
			if(count($hasil_encode) != 2){
				header("location: ".base_url(''));		
			}
			else{
				//$this->session->set_userdata('email', $hasil_encode[0]);
				//$this->session->set_userdata('uuid', $hasil_encode[1]);
				//redirect(base_url('/forgot_password/new_pass'), 'refresh');
				//$this->new_pass($hasil_encode[0], $hasil_encode[1]);
				$data['email'] = $hasil_encode[0];
				$data['uuid'] = $hasil_encode[1];
				$this->load->view('new-password', $data);
			}
			
		}
	}

	/*public function new_pass(){
		$data['email'] = $this->session->userdata('email');
		$data['uuid'] = $this->session->userdata('uuid');
		$this->load->view('new-password', $data);
	}*/
}
