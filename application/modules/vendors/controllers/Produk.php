<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Produk extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model("produka");
	}
	public function index()
	{
		$data['id'] = 3;
		$this->load->view('produk', $data);
	}

	public function tipe($uuid){
		$data = array();

		$id_product = $this->produka->get_id_product($uuid);

		$array_dummy = array();
		$data_json['result'] = array();
		$result = array();
		foreach($id_product as $key1){
			$a = array();
			$b = array();
			$result = $this->produka->get_produk_by_uuid($key1['uuid']);
			foreach($result as $key2){
				array_push($a, $key2['nama']);
				array_push($b, $key2['value']);
			}
			$array_dummy = array_combine($a, $b);
			array_push($data_json['result'], array("uuid" => $key1['uuid'], "parameter" => $array_dummy));

		}

		for ($i=1; $i < 12; $i++) { 

		}
		$data['kategori'] = $kategori;
		$data['produk'] = $data_json;
		echo json_encode($data);
	}

	public function edit($id, $uuid){
		$data['id'] = $id;
		$data['uuid'] = $uuid;
		$this->load->view('produk-edit', $data);
	}

}
