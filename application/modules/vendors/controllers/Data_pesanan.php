<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Data_pesanan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Produka");
	}
	 public function index()
	{
		$data['id'] = 8;
		$this->load->view('data-pesanan', $data);
	}

	public function get_detail_belanja($uuid){
		
		$data['detail'] = $this->Produka->get_transaksi($uuid);
		$data['detail_add'] = $this->Produka->get_transaksi_add($uuid);
	
		$this->load->view('data-lengkap-belanja', $data);
	}

	
}
