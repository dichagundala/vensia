<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if(@$_GET['sess'] != ''){
			$encode = $_GET['sess'];
			$decode = explode(';',base64_decode($encode));
			$data['email'] = $decode[0];
			$data['namaDepan'] = $decode[1];
			$data['secureUrl'] = $decode[2];
			$data['uuid'] = $decode[3];
			$data['kategori'] = $decode[4];
		}
		else {
			$data = array();
		}
		$this->load->view('welcome_message', $data);
	}
}
