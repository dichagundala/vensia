<?php $this->load->view('header'); ?>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.css">
<?php $this->load->view('sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header sty-one">
    <h1>Porfolio Vendor</h1>
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li><i class="fa fa-angle-right"></i> Pengaturan Toko</li>&nbsp;
      <li><i class="fa fa-angle-right"></i> <a href="<?php echo base_url('vendors/portfolio')?>">Portfolio</a></li>&nbsp;
      <li><i class="fa fa-angle-right"></i> Foto Album</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-lg-12">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" id="tambahFoto">
          <i class="fa fa-image"></i>&nbsp; Tambah Foto
        </button>
        <button type="button" class="btn btn-danger" id="hapusSemuaFoto">
          <i class="fa fa-trash"></i>&nbsp; Hapus Semua Foto
        </button>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="border-bottom: 5px solid #0073b7; border-top: 3px solid #0073b7">
          <div class="modal-header">
            <h5 class="modal-title" style="font-weight: bold" id="myModalLongTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form id="modalFoto" method="POST" action="#">
            <div class="modal-body">
              <div class="form-group">
                <label>Judul Foto</label>
                <input type="text" class="form-control" placeholder="Masukkan Judul Foto" name="ket_foto" id="ket_foto" required="required">
              </div>
              <div class="form-group">
                <label>Upload Foto</label>
                <input type="file" class="form-control dropify-event" id="foto" placeholder="Masukkan Deskripsi Foto">
                <input type="hidden" id="foto_hidden" name="public_id">
              </div>
              <input type="hidden" name="uuid_album" value="<?php echo $uuid_album; ?>">
              <div id="uploadFotoProgress">*foto sedang di upload</div>
              <div id="uploadFotoDone">*foto selesai di upload</div>
            </div>
            <div class="modal-footer">
              <button type="reset" class="btn btn-secondary" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="row m-t-3">
      <div class="col-lg-12">
        <div class="card card-outline">
          <div class="card-header bg-blue">
            <h5 class="text-white m-b-0">Data Foto Album</h5> 
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Foto</th>
                    <th>Judul Kegiatan Foto</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </section>
  <!-- /.content --> 
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('footer'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() { 
    var readySubmit = false;
    $('body').delegate('#tambahFoto', 'click', function(event) {
      $("#uploadFotoProgress").hide();
      $("#uploadFotoDone").hide();
      $('form#modalFoto').trigger('reset');
      var drEvent1 = $("#foto.dropify-event").dropify();

      drEvent1.on('dropify.beforeClear', function(event, element){
        return true;
      });

      drEvent1.on('dropify.afterClear', function(event, element){
        $("#foto_hidden").val('');
      });

      drEvent1.on('dropify.errors', function(event, element){
        console.log('Has Errors');
      });
      $("button.dropify-clear").trigger('click'); 
      $('form#modalFoto').attr('action', '<?= $this->config->item('api_http'); ?>/vendors/add_album_foto');
      $("#myModalLongTitle").html("Tambah Album");
      $('#myModal').modal('show');
      return false;
    });

    $('body').delegate('#editFoto', 'click', function(event) {
      $("#uploadFotoProgress").hide();
      $("#uploadFotoDone").hide();
      $('form#modalFoto').trigger('reset');
      $.ajax({
        url: '<?= $this->config->item('api_http'); ?>/vendors/get_album_foto',
        type: 'POST',
        data: {
          uuid : $(this).data('uuid')
        },
        dataType: 'json',
        success: function(result){
          var drEvent1 = $("#foto.dropify-event").dropify({
            defaultFile: 'https://res.cloudinary.com/yepsindo/image/upload/' + result[0].public_id + '.jpg',
            messages: {
              'default': 'Drag and drop a file here or click',
              'replace': 'Drag and drop or click to replace',
              'remove':  'Remove',
              'error':   'Ooops, something wrong happended.'
            },
            error: {
              'fileSize': 'The file size is too big ({{ value }} max).',
              'minWidth': 'The image width is too small ({{ value }}}px min).',
              'maxWidth': 'The image width is too big ({{ value }}}px max).',
              'minHeight': 'The image height is too small ({{ value }}}px min).',
              'maxHeight': 'The image height is too big ({{ value }}px max).',
              'imageFormat': 'The image format is not allowed ({{ value }} only).'
            }
          });

          drEvent1.on('dropify.beforeClear', function(event, element){
            return true;
          });

          drEvent1.on('dropify.afterClear', function(event, element){
            $("#foto_hidden").val('');
          });

          drEvent1.on('dropify.errors', function(event, element){
            console.log('Has Errors');
          });
          $("#ket_foto").val(result[0].ket_foto); 
          $("#foto_hidden").val(result[0].public_id);
          readySubmit = true;
        }
      });
      $("button.dropify-clear").trigger('click'); 
      $('form#modalFoto').attr('action', '<?= $this->config->item('api_http'); ?>/vendors/edit_album_foto');
      $("#myModalLongTitle").html("Ubah Foto");
      $('<input />').attr('type', 'hidden').attr('id', 'uuid').attr('name', "uuid").attr('value', $(this).data('uuid')).appendTo('form#modalFoto');
      $('#myModal').modal('show');
      return false;
    });

    $("form#modalFoto").submit(function(e){
      e.preventDefault();
      if(readySubmit){
        $.post($(this).attr('action'), $(this).serialize(), function(result){
          result = JSON.parse(result);
          swal({
            title: result.status,
            text: result.message,
            type: result.status
          }).then((result) => {
            location.reload();
          });
        });
      }
      else{
        swal("Info", "Foto masih diupload, sabar ya.", "info");
      }
    });

    $.ajax({
      url: '<?= $this->config->item('api_http'); ?>/vendors/get_album_foto_all',
      type: 'POST',
      data: {
        uuid_album : '<?php echo $uuid_album; ?>'
      },
      dataType: 'json',
      success: function(result){
        var no = 1;
        for(var i = 0; i < result.length; i++){
          var template = '<tr><td>'+no+'</td><td><img src="https://res.cloudinary.com/yepsindo/image/upload/q_30/'+result[i].public_id+'.jpg" width="60" height="60"/></td><td>'+result[i].ket_foto+'</td><td><button id="editFoto" data-uuid="'+result[i].uuid+'" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></button>&nbsp;<button id="hapusFoto" data-uuid="'+result[i].uuid+'" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button></td></tr>';
          no++;
          $("#example1 tbody").append(template);
        }
        $('#example1').DataTable();
      }
    });

    $('body').delegate('#hapusFoto', 'click', function(event) {
      swal({
        title: "Hapus Foto",
        text: "Anda yakin ingin menghapus foto ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Ya, saya yakin!',
        cancelButtonText: "Tidak, batalkan!",
      }).then((result) => {
        if(result.value){
          $.ajax({
            url: '<?= $this->config->item('api_http'); ?>/vendors/delete_album_foto',
            type: 'POST',
            data: {
              uuid : $(this).data('uuid')
            },
            dataType: 'json',
            success: function(result){
              swal({
                title: result.status,
                text: result.message,
                type: result.status
              }).then((result) => {
                location.reload();
              });
            }
          });
        }
      });
    });

    $('body').delegate('#hapusSemuaFoto', 'click', function(event) {
      swal({
        title: "Hapus Semua Foto",
        text: "Anda yakin ingin menghapus semua foto dialbum ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Ya, saya yakin!',
        cancelButtonText: "Tidak, batalkan!",
      }).then((result) => {
        if(result.value){
          $.ajax({
            url: '<?= $this->config->item('api_http'); ?>/vendors/delete_album_foto_all',
            type: 'POST',
            data: {
              uuid_album : '<?php echo $uuid_album; ?>'
            },
            dataType: 'json',
            success: function(result){
              swal({
                title: result.status,
                text: result.message,
                type: result.status
              }).then((result) => {
                location.reload();
              });
            }
          });
        }
      });
    });

    $("#foto").on("change", function(e){ 
      var file = this.files[0];
      var form = new FormData();
      readySubmit = false;
      $("#uploadFotoProgress").show();
      $("#uploadFotoDone").hide();
      form.append('image', file);
      $.ajax({
        url : "<?= $this->config->item('api_http'); ?>/vendors/cloudinary",
        type: "POST",
        cache: false,
        contentType: false,
        processData: false,
        data : form,
        success: function(response){
          data = JSON.parse(response);
          $("#foto_hidden").val(data.public_id);
          readySubmit = true;
          $("#uploadFotoProgress").hide();
          $("#uploadFotoDone").show();
          console.log("Uploaded");
        }
      });
    });
  });

</script>