<?php
if(!$this->session->userdata("logged_in")){
  header("location: ".base_url(''));
}

if( !isset($_SERVER['HTTPS']) && $_SERVER['HTTP_HOST'] != 'localhost') {
  $url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
  header("Location: ".'https://'.$url); 
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YEPS - Dashboard Vendor</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- v4.0.0 -->
  <link rel="stylesheet" href="<?php echo base_url('dist/bootstrap/css/bootstrap.min.css'); ?>">

  <!-- Favicon -->
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('dist/img/favicon-16x16.png'); ?>">

  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('dist/css2/style.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('dist/css2/custom.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('dist/css/font-awesome/css/font-awesome.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('dist/css/et-line-font/et-line-font.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('dist/css/themify-icons/themify-icons.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('dist/css/simple-lineicon/simple-line-icons.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('dist/plugins/dropify/dropify.min.css'); ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('dist/plugins/datatables/css/dataTables.bootstrap.min.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('dist/plugins/gmaps/examples.css'); ?>" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/introjs.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body class="skin-blue sidebar-mini">
  <div class="wrapper boxed-wrapper">
    <header class="main-header"> 
      <!-- Logo --> 
      <a href="<?php echo base_url('vendors/home'); ?>" class="logo blue-bg"> 
        <!-- mini logo for sidebar mini 50x50 pixels --> 
        <span class="logo-mini"><img src="<?php echo base_url('dist/img/logo-n.png'); ?>" alt="" style="width: 40px"></span> 
        <!-- logo for regular state and mobile devices --> 
        <span class="logo-lg"><img src="<?php echo base_url('dist/img/logo.png'); ?>" alt="" style="width: 150px"></span> </a> 
        <!-- Header Navbar -->
        <nav class="navbar blue-bg navbar-static-top"> 
          <!-- Sidebar toggle button-->
          <ul class="nav navbar-nav pull-left">
            <li><a class="sidebar-toggle" data-toggle="push-menu" href="#"></a> </li>
          </ul>
          <div class="pull-left search-box">
            <!-- <form action="#" method="get" class="search-form">
              <div class="input-group">
                <input name="search" class="form-control" placeholder="" type="text">
                <span class="input-group-btn">
                  <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i> </button>
                </span></div>
              </form> -->
              <!-- search form --> </div>
              <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                  <!-- Messages -->
                  <!-- <li class="dropdown messages-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-envelope-o"></i>
                    <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                  </a>
                  <ul class="dropdown-menu">
                    <li class="header">You have 4 new messages</li>
                    <li>
                      <ul class="menu">
                        <li><a href="#">
                          <div class="pull-left"><img src="<?php echo base_url('dist/img/img1.jpg'); ?>" class="img-circle" alt="User Image"> <span class="profile-status online pull-right"></span></div>
                          <h4>Alex C. Patton</h4>
                          <p>I've finished it! See you so...</p>
                          <p><span class="time">9:30 AM</span></p>
                        </a></li>
                        <li><a href="#">
                          <div class="pull-left"><img src="<?php echo base_url('dist/img/img3.jpg'); ?>" class="img-circle" alt="User Image"> <span class="profile-status offline pull-right"></span></div>
                          <h4>Nikolaj S. Henriksen</h4>
                          <p>I've finished it! See you so...</p>
                          <p><span class="time">10:15 AM</span></p>
                        </a></li>
                        <li><a href="#">
                          <div class="pull-left"><img src="<?php echo base_url('dist/img/img2.jpg'); ?>" class="img-circle" alt="User Image"> <span class="profile-status away pull-right"></span></div>
                          <h4>Kasper S. Jessen</h4>
                          <p>I've finished it! See you so...</p>
                          <p><span class="time">8:45 AM</span></p>
                        </a></li>
                        <li><a href="#">
                          <div class="pull-left"><img src="<?php echo base_url('dist/img/img4.jpg'); ?>" class="img-circle" alt="User Image"> <span class="profile-status busy pull-right"></span></div>
                          <h4>Florence S. Kasper</h4>
                          <p>I've finished it! See you so...</p>
                          <p><span class="time">12:15 AM</span></p>
                        </a></li>
                      </ul>
                    </li>
                    <li class="footer"><a href="#">View All Messages</a></li>
                  </ul>
                </li> -->
                <!-- Notifications  -->
                <li class="dropdown messages-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fas fa-bell"></i>
                  <div class="notify"><span class="pull-right badge bg-red" style="font-size: 9px;" id="total_notif"></span></div>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Notifications</li>
                  <li>
                    <ul class="menu"  style="height: auto;max-height: 400px;overflow-x: hidden;">
                      <!-- <li><a href="#">
                        <div class="pull-left icon-circle blue"><i class="fa fa-coffee"></i></div>
                        <h4>Nikolaj S. Henriksen</h4>
                        <p>I've finished it! See you so...</p>
                        <p><span class="time">1:30 AM</span></p>
                      </a></li>
                      <li><a href="#">
                        <div class="pull-left icon-circle green"><i class="fa fa-paperclip"></i></div>
                        <h4>Kasper S. Jessen</h4>
                        <p>I've finished it! See you so...</p>
                        <p><span class="time">9:30 AM</span></p>
                      </a></li>
                      <li><a href="#">
                        <div class="pull-left icon-circle yellow"><i class="fa  fa-plane"></i></div>
                        <h4>Florence S. Kasper</h4>
                        <p>I've finished it! See you so...</p>
                        <p><span class="time">11:10 AM</span></p>
                      </a></li> -->
                    </ul>
                  </li>
                  <!--  <li class="footer"><a href="#">Check all Notifications</a></li> -->
                </ul>
              </li>
              <!-- User Account  -->
              <?php 
              if(@$this->session->userdata('cro') == 'true'){
                ?>
                <a href="<?php echo base_url('vendors/auth/logout'); ?>" class="btn btn-info">Kembali ke Data Vendor CRO</a>
                <?php
              }
              else{
                ?>
                <li class="dropdown user user-menu p-ph-res"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                  <img class="user-image" id="image_vendor_header" alt="User Image"> <span class="hidden-xs" id="nama_vendor"></span> </a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url('vendors/profil'); ?>"><i class="icon-profile-male"></i> My Profile</a></li>
                  <!-- <li><a href="#"><i class="icon-wallet"></i> My Balance</a></li>
                  <li><a href="#"><i class="icon-envelope"></i> Inbox</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="#"><i class="icon-gears"></i> Account Setting</a></li> -->
                  <li role="separator" class="divider"></li>
                  <li><a href="<?php echo base_url(''); ?>"><i class="fa fa-power-off"></i> Back To Home</a></li>
                </ul>
              </li>
              <?php
            }
            ?>
          </ul>
        </div>
      </nav>
    </header>