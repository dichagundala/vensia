<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header sty-one">
    <h1>Request Edit Data Ketegori dan Alamat</h1>
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li><i class="fa fa-angle-right"></i> Profil</li>&nbsp;
      <li><i class="fa fa-angle-right"></i> Edit Kategori</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-lg-12 m-b-3">
        <div class="card bg-yellow">
          <div class="card-body">
            <div class="info-widget-text row">
              <div class="col-sm-12 pull-left"><i class="fa fa-info-circle"></i>&nbsp; Untuk melakukan perubahan kategori dan alamat silahkan mengisi form dibawah ini.</div>
              <!-- Nanti akan dihubungi oleh tim YEPS Indonesia -->
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline">
          <div class="card-header bg-blue">
            <h5 class="text-white m-b-0">Form Request Edit Kategori</h5>
          </div>
          <div class="card-body">
            <i class="fa fa-list margin-r-5"></i> Kategori
            <p class="text-muted" style="margin-top: 5px" id="kategori_edit_request"></p>
            <hr>
            <form>
              <div class="form-group">
                <small id="kategoriHelp" class="form-text text-muted" style="font-size: 14px; margin-bottom: 10px">Kategori bisa di pilih lebih dari 1</small>
                <div class="form-row">
                  <div class="col-md-6">
                    <div class="checkbox">
                      <label><input type="checkbox" value="Venue" name="kategori[]">&nbsp;Venue</label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox" value="Photography" name="kategori[]">&nbsp;Photography</label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox" value="Wardrobe" name="kategori[]">&nbsp;Wardrobe</label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox" value="Decoration" name="kategori[]">&nbsp;Decoration</label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox" value="Souvenir" name="kategori[]">&nbsp;Souvenir</label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox" value="Catering" name="kategori[]">&nbsp;Catering</label>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="checkbox">
                      <label><input type="checkbox" value="Make Up" name="kategori[]">&nbsp;Make Up</label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox" value="Invitation" name="kategori[]">&nbsp;Invitation</label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox" value="Entertainment" name="kategori[]">&nbsp;Entertainment</label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox" value="Transportation" name="kategori[]">&nbsp;Transportation</label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox" value="Event Cake" name="kategori[]">&nbsp;Event Cake</label>
                    </div>
                    <div class="checkbox">
                      <label><input type="checkbox" value="Videography" name="kategori[]">&nbsp;Videography</label>
                    </div>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-success">Kirim Request</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="row m-t-3">
      <div class="col-lg-12">
        <div class="card card-outline">
          <div class="card-header bg-blue">
            <h5 class="text-white m-b-0">Form Request Edit Alamat</h5>
          </div>
          <div class="card-body">
            <i class="fas fa-map-marker-alt margin-r-5"></i> Alamat Sekarang
            <p class="text-muted" style="margin-top: 5px" id="lokasi_edit_request"></p>
            <hr>
            <form>
              <div class="form-group col-md-12">
                <label for="inputAddress">Alamat <strong style="color: red;">*</strong></label>
                <textarea class="form-control" id="address" name="address" placeholder="Alamat" rows="3" required="required"></textarea>
              </div>
              <div class="form-row col-md-12">
                <div class="form-group col-md-3">
                  <label for="inputProvinsi">Provinsi <strong style="color: red;">*</strong></label>
                  <select id="inputProvinsi" class="form-control" required="required">
                  </select>
                  <input type="hidden" id="hiddenProvinsi" name="provinsi">
                </div>
                <div class="form-group col-md-3">
                  <label for="inputKabupaten">Kabupaten <strong style="color: red;">*</strong></label>
                  <select id="inputKabupaten" class="form-control" required="required">
                  </select>
                  <input type="hidden" id="hiddenKabupaten" name="kabupaten">
                </div>
                <div class="form-group col-md-3">
                  <label for="inputKecamatan">Kecamatan <strong style="color: red;">*</strong></label>
                  <select id="inputKecamatan" class="form-control" required="required">
                  </select>
                  <input type="hidden" id="hiddenKecamatan" name="kecamatan">
                </div>
                <div class="form-group col-md-3">
                  <label for="inputKelurahan">Kelurahan <strong style="color: red;">*</strong></label>
                  <select id="inputKelurahan" class="form-control" required="required">
                  </select>
                  <input type="hidden" id="hiddenKelurahan" name="kelurahan">
                </div>
              </div>
              <button type="submit" class="btn btn-success">Kirim Request</button>
            </form>
          </div>
        </div>
      </div>
    </div>

  </section>
  <!-- /.content --> 
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('footer'); ?>

<script type="text/javascript">
  $.ajax({
    url: '<?= $this->config->item('api_http'); ?>/vendors/user/' + '<?php echo $this->session->userdata('uuid'); ?>',
    type: 'GET',
    dataType: 'json',
    success: function(result){
      $("#kategori_edit_request").html(result[0].kategori);
      var str = result[0].kategori;
      var temp = new Array();
      temp = str.split(", ");
      for (i=0; i!=temp.length;i++) {
        var checkbox = $("input[type='checkbox'][value='"+temp[i]+"']");
        checkbox.attr("checked","checked");
      }
      $("#lokasi_edit_request").append(result[0].addressVendor + ', ' + result[0].vKelurahan + ', ' + result[0].vKecamatan + ', ' + result[0].vKabupaten + ', ' + result[0].vProvinsi);
    }
  });

  $(document).ready(function() { 
    $("#inputProvinsi").append('<option value="">Pilih</option>'); 
    $("#inputKabupaten").html(''); 
    $("#inputKecamatan").html(''); 
    $("#inputKelurahan").html(''); 
    $("#inputKabupaten").append('<option value="">Pilih</option>'); 
    $("#inputKecamatan").append('<option value="">Pilih</option>'); 
    $("#inputKelurahan").append('<option value="">Pilih</option>'); 
    url = '<?= $this->config->item('api_http'); ?>/vendors/provinsi'; 
    $.ajax({ url: url, 
      type: 'GET', 
      dataType: 'json', 
      success: function(result) { 
        for (var i = 0; i < result.length; i++) 
          $("#inputProvinsi").append('<option value="' + result[i].id_prov + '">' + result[i].nama + '</option>'); 
      } 
    }); 
  }); 
  $("#inputProvinsi").change(function(){ 
    var id_prov = $("#inputProvinsi").val(); 
    $("#hiddenProvinsi").val($( "#inputProvinsi option:selected" ).text());
    var url = '<?= $this->config->item('api_http'); ?>/vendors/kabupatens?id_prov=' + id_prov; 
    $("#inputKabupaten").html(''); $("#inputKecamatan").html(''); 
    $("#inputKelurahan").html(''); $("#inputKabupaten").append('<option value="">Pilih</option>'); 
    $("#inputKecamatan").append('<option value="">Pilih</option>'); 
    $("#inputKelurahan").append('<option value="">Pilih</option>'); 
    $.ajax({ url : url, 
      type: 'GET', 
      dataType : 'json', 
      success : function(result){ 
        for(var i = 0; i < result.length; i++) 
          $("#inputKabupaten").append('<option value="'+ result[i].id_kab +'">' + result[i].nama + '</option>'); 
      } 
    });  
  }); 
  $("#inputKabupaten").change(function(){ 
    var id_kab = $("#inputKabupaten").val(); 
    $("#hiddenKabupaten").val($( "#inputKabupaten option:selected" ).text());
    var url = '<?= $this->config->item('api_http'); ?>/vendors/kecamatan?id_kab=' + id_kab; 
    $("#inputKecamatan").html(''); $("#inputKelurahan").html(''); 
    $("#inputKecamatan").append('<option value="">Pilih</option>'); 
    $("#inputKelurahan").append('<option value="">Pilih</option>'); 
    $.ajax({ url : url, 
      type: 'GET', 
      dataType : 'json', 
      success : function(result){ 
        for(var i = 0; i < result.length; i++) 
          $("#inputKecamatan").append('<option value="'+ result[i].id_kec +'">' + result[i].nama + '</option>'); 
      } 
    });  
  }); 
  $("#inputKecamatan").change(function(){ 
    var id_kec = $("#inputKecamatan").val(); 
    $("#hiddenKecamatan").val($( "#inputKecamatan option:selected" ).text());
    var url = '<?= $this->config->item('api_http'); ?>/vendors/kelurahan?id_kec=' + id_kec; $("#inputKelurahan").html(''); 
    $("#inputKelurahan").append('<option value="">Pilih</option>'); 
    $.ajax({ url : url, 
      type: 'GET', 
      dataType : 'json', 
      success : function(result){ 
        for(var i = 0; i < result.length; i++) 
          $("#inputKelurahan").append('<option value="'+ result[i].id_kel +'">' + result[i].nama + '</option>'); 
      } 
    });  
  });
  $("#inputKelurahan").change(function(){
    $("#hiddenKelurahan").val($( "#inputKelurahan option:selected" ).text());
  }); 
</script>