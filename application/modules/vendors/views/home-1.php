<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header sty-one">
    <h1>Vendor Dashboard</h1>
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li><i class="fa fa-angle-right"></i> Vendor Dashboard 2</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-lg-12 m-b-3">
        <div class="card">
            <div class="card-body">
              <div class="info-widget-text row">
                <div class="col-sm-12 pull-left"><i class="fa fa-info-circle"></i>&nbsp; Fitur ini masih dalam tahap Pengembangan</div>
              </div>
            </div>
          </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-3">
        <div class="tile-progress tile-pink">
          <div class="tile-header">
            <h5>Pesanan Bulan ini</h5>
            <h3>40</h3>
          </div>
          <div class="tile-progressbar"> <span data-fill="80%" style="width: 80%;"></span> </div>
          <div class="tile-footer">
            <!-- <h4> <span class="pct-counter">8</span> di Minggu ini </h4> -->
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="tile-progress tile-red">
          <div class="tile-header">
            <h5>Pesanan di Terima</h5>
            <h3>85</h3>
          </div>
          <div class="tile-progressbar"> <span data-fill="80%" style="width: 80%;"></span> </div>
          <div class="tile-footer">
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="tile-progress tile-cyan">
          <div class="tile-header">
            <h5>Pesanan di Tolak</h5>
            <h3>15</h3>
          </div>
          <div class="tile-progressbar"> <span data-fill="80%" style="width: 80%;"></span> </div>
          <div class="tile-footer">
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="tile-progress tile-aqua">
          <div class="tile-header">
            <h5>Total Pesanan</h5>
            <h3>100</h3>
          </div>
          <div class="tile-progressbar"> <span data-fill="80%" style="width: 80%;"></span> </div>
          <div class="tile-footer">
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->


    <div class="row">
      <div class="col-xl-8 m-b-3">
        <div class="box box-widget widget-user-2">
          <div class="widget-user-header bg-yellow">
            <h5 class="widget-user-desc" style="margin-left: 0px;">Jumlah Produk</h5>
          </div>
          <div class="box-footer no-padding">
            <ul class="nav nav-stacked">
              <li><a href="#">Venue <span class="pull-right badge bg-blue">15</span></a></li>
              <li><a href="#">Photography <span class="pull-right badge bg-aqua">25</span></a></li>
              <li><a href="#">Wardrobe <span class="pull-right badge bg-green">30</span></a></li>
              <li><a href="#">Decoration <span class="pull-right badge bg-red">150</span></a></li>
              <li><a href="#">Souvenir <span class="pull-right badge bg-green">30</span></a></li>
              <li><a href="#">Videography <span class="pull-right badge bg-aqua">25</span></a></li>
              <li><a href="#">Entertainment <span class="pull-right badge bg-green">120</span></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-xl-4 m-b-3">
        <div class="box-gradient">
          <div class="card-body text-white">
            <h5>Total Pendapatan</h5>
            <h2>Rp. 100.000.000</h2>
            <h5>Oktober 2018</h5>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->

    
  </section>
  <!-- /.content --> 
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('footer'); ?>


