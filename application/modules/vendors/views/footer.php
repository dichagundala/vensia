 <footer class="main-footer">
 	<div class="pull-right hidden-xs">Version 1.0</div>
 	Copyright &copy; <?php echo date('Y') == '2018' ? '2018' : '2018 - '.date('Y'); ?> VENSIA. All rights reserved.</footer>
 </div>
 <!-- ./wrapper --> 

 <!-- jQuery 3 --> 
 <script src="<?php echo base_url('dist/js/jquery.min.js'); ?>"></script> 

 <!-- v4.0.0-alpha.6 --> 
 <script src="<?php echo base_url('dist/bootstrap/js/bootstrap.min.js'); ?>"></script> 
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/intro.min.js"></script>

 <!-- template --> 
 <script src="<?php echo base_url('dist/js/adminkit.js'); ?>"></script>
 <!-- <script src="https://maps.google.com/maps/api/js?key=AIzaSyCUBL-6KdclGJ2a_UpmB2LXvq7VOcPT7K4&amp;sensor=true"></script> 
 <script src="<?php echo base_url('dist/plugins/gmaps/gmaps.int.js'); ?>"></script>
 <script src="<?php echo base_url('dist/plugins/gmaps/gmaps.js'); ?>"></script> -->

 <script src="<?php echo base_url('dist/plugins/datatables/jquery.dataTables.min.js'); ?>"></script> 
 <script src="<?php echo base_url('dist/plugins/datatables/dataTables.bootstrap.min.js'); ?>"></script>  
 <script src="<?php echo base_url('dist/plugins/dropify/dropify.min.js'); ?>"></script> 
 <script type="text/javascript">


 	$("#nama_vendor").html(localStorage.getItem("namaVendor"));
 	$("#email_vendor").html('<?php echo $this->session->userdata('email'); ?>');
 	$("#image_vendor").attr('src', localStorage.getItem("url") == "" ? 'https://res.cloudinary.com/yepsindo/image/upload/v1536898735/profile.jpg' : localStorage.getItem("url"));
 	$("#image_vendor_side").attr('src', localStorage.getItem("url") == "" ? 'https://res.cloudinary.com/yepsindo/image/upload/v1536898735/profile.jpg' : localStorage.getItem("url"));
 	$("#image_vendor_header").attr('src', localStorage.getItem("url") == "" ? 'https://res.cloudinary.com/yepsindo/image/upload/v1536898735/profile.jpg' : localStorage.getItem("url"));

 	$.post("http://localhost/api15/vendors/produk_order", "uuid=<?php echo $this->session->userdata('uuid'); ?>", function(data, status) {
 		data = JSON.parse(data);
 		$("#total_notif").html(data.response.total);
 		$("#total_notif_2").html(data.response.total);

 		if(data.response.total == 0){
 			var template = '<li><div style="text-align: center;">Tidak ada notifikasi</div></li>';
 			//alert(template);
			 $('.menu').append(template);
 		}
		//var no2 = 1;
		//var response = data.response;
 		for(var i = 0; i < data.response.total; i++){
 			var template = '<li><a href="<?php echo base_url('vendors/data_pesanan');?>"><div class="pull-left icon-circle red"><i class="icon-lightbulb"></i></div><h4 style="font-size:10px;">'+data.response.data[i].kode_invoice+'</h4><p class="product-height">'+data.response.data[i].nama_jasa+'</p><p>Jumlah pesanan : '+data.response.data[i].qty+'</p><p><span class="time">Booking date : '+indonesia_date(data.response.data[i].date_order) + ' ' + data.response.data[i].time_order+'</span></p></a></li>';
 			//var template = '<tr><td style="text-align:center;">'+no2+'</td><td>'+data.response.data[i].kode_invoice+'</td><td>'+data.response.data[i].nama_jasa+'</td><td>'+indonesia_date(data.response.data[i].date_order) + ' Jam ' + data.response.data[i].time_order +'</td><td style="text-align:center;">'+data.response.data[i].qty+'</td><td>Rp. '+ convertRupiah(data.response.data[i].harga_promo == 0 ? data.response.data[i].harga_normal : data.response.data[i].harga_promo) +'</td><td style="text-align: center;">'+(data.response.data[i].status == 'ACCEPT' || data.response.data[i].status == 'accept' || data.response.data[i].status == 'reject' || data.response.data[i].status == 'REJECT' ? data.response.data[i].status : '<button data-uuid="'+data.response.data[i].uuid+'" class="btn btn-success btn-sm accept">Terima</button>&nbsp;<button data-uuid="'+data.response.data[i].uuid+'" class="btn btn-danger btn-sm reject">Tolak</button>')+'</td></tr>';
			 //no2++;
			 //alert(template);
			// $('.menu').append(template);
			$('.menu').append(template);
 		}

 	});

 	var month = ['', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', ' Agustus', 'September', 'Oktober', 'November', 'Desember'];

 	function indonesia_date(date = '', time = true){
 		if(time == true){
 			d = date.split(' ');
 			date = d[0];
 		}

 		d = date.split('-');

 		date = d[2]+' '+month[parseInt(d[1])]+' '+d[0];

 		return date;
 	}
 </script>
</body>
</html>