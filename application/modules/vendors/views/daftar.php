<?php ?>

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from html.tonatheme.com/2018/bari-landing/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Jul 2018 07:33:36 GMT -->
<head>
	<meta charset="UTF-8">
	<title>Vendor YEPS Registration</title>
	<link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('asset/css/global.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('asset/css/element.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('asset/css/style.css'); ?>">

	<!-- Fav Icons -->
	<link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.png'); ?>" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url('assets/images/favicon.png'); ?>" type="image/x-icon">

</head>
<body>

	<!-- Modal -->
	<div class="modal fade" id="exampleModalLong" tabindex="100" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Syarat Ketentuan</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<iframe src="https://yepsindonesia.com/syarat_ketentuan/two" style="width: 100%; height: 400px"></iframe> 
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- end:Modal -->

	<div class="preloader"></div>
	<div class="page-wrapper">

		<header class="main-header" id="home">
			<div class="container-fluid">
				<div class="pull-left">
					<div class="logo"><a href="#"><img src="<?php echo base_url('assets/images/logo.png'); ?>" alt="" style="width: 220px"></a></div>
				</div>
				<div class="pull-right">
					<div class="menu">
						<ul class="scroll-nav">
							<li>&nbsp;</li>
						</ul>
					</div>
					<div class="link-btn"><a href="https://yepsindonesia.com" class="theme-btn btn-style-one"  target="_blank">YEPS Indonesia</a></div>
				</div>
			</div>
		</header>


		<section class="banner" style="background-image: url(<?php echo base_url('assets/images/bg-header.jpg');?>);">
			<div class="container">
				<!-- <h2>Bergabunglah di YEPS Indonesia</h2> -->
			</div>
			<div class="parallax-container">
				<div class="slide1"><img src="<?php echo base_url('assets/images/b1.png'); ?>" alt=""></div>
				<!-- <div class="slide2"><img src="<?php echo base_url('assets/images/b2.png'); ?>" alt=""></div> -->
				<div class="slide3"><img src="<?php echo base_url('assets/images/b3.png'); ?>" alt=""></div>
				<div class="slide4"><img src="<?php echo base_url('assets/images/b4.png'); ?>" alt=""></div>
				<div class="slide5"><img src="<?php echo base_url('assets/images/b5.png'); ?>" alt=""></div>
			</div>
		</section>

		<section class="main-demo" id="demo">
			<div class="container">
				<div class="text-center">
					<div class="sec-title centered">
						<h2>Pendaftaran Vendor</h2>
						<div class="text">Silahkan masukkan data Vendor</div>
					</div>
				</div>
				
				<div class="row">
					<div class="container">
						<form method="POST" id="register-form" action="#" enctype="multipart/form-data">
							<strong style="color: red;">*</strong> Wajib di isi
							<div class="form-group col-md-12">
							<input type="hidden" class="form-control" id="uuid" name="uuid" value="<?php echo $this->session->userdata('uuid');?>" placeholder="uuid" required="required">
								<label for="inputNameVendor">Nama Vendor <strong style="color: red;">*</strong></label>
								<input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama Vendor" required="required">
							</div>
							<div class="form-group col-md-12">
								<label for="inputNameVendor">Nama Pemilik / PT <strong style="color: red;">*</strong></label>
								<input type="text" class="form-control" id="nama_pemilik" name="nama_pemilik" placeholder="Masukkan Nama Pemilik Vendor" required="required">
							</div>
							<div class="form-group col-md-12">
								<label for="inputNameVendor">Deskripsi Vendor <strong style="color: red;">*</strong></label>
								<textarea class="form-control" id="deskripsi" name="deskripsi" rows="3" placeholder="Deskripsi Vendor" required="required"></textarea>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="inputEmail">Email <strong style="color: red;">*</strong></label>
									<input type="email" class="form-control" id="email" value="<?php echo $this->session->userdata('email');?>" name="email" placeholder="Email" required="required" readonly>
								</div>
								<div class="form-group col-md-6">
									<label for="inputTelepon">Telepon <strong style="color: red;">*</strong></label>
									<input type="text" class="form-control" id="tel" name="telp" placeholder="Telepon" required="required">
								</div>
							</div>
							<div class="form-group col-md-12">
								<label for="inputNameVendor">No. NPWP <strong style="color: red;">*</strong></label>
								<input type="text" class="form-control" id="npwp" name="npwp" placeholder="Masukkan Nomor NPWP" required="required">
							</div>
							<div class="form-group col-md-12">
								<label for="inputAddress">Alamat <strong style="color: red;">*</strong></label>
								<textarea class="form-control" id="address" name="address" placeholder="Alamat" rows="3" required="required"></textarea>
							</div>							
							<div class="form-row">
								<div class="form-group col-md-3">
									<label for="inputProvinsi">Provinsi <strong style="color: red;">*</strong></label>
									<select id="inputProvinsi" class="form-control" required="required">
									</select>
									<input type="hidden" id="hiddenProvinsi" name="provinsi">
								</div>
								<div class="form-group col-md-3">
									<label for="inputKabupaten">Kabupaten <strong style="color: red;">*</strong></label>
									<select id="inputKabupaten" class="form-control" required="required">
									</select>
									<input type="hidden" id="hiddenKabupaten" name="kabupaten">
								</div>
								<div class="form-group col-md-3">
									<label for="inputKecamatan">Kecamatan <strong style="color: red;">*</strong></label>
									<select id="inputKecamatan" class="form-control" required="required">
									</select>
									<input type="hidden" id="hiddenKecamatan" name="kecamatan">
								</div>
								<div class="form-group col-md-3">
									<label for="inputKelurahan">Kelurahan <strong style="color: red;">*</strong></label>
									<select id="inputKelurahan" class="form-control" required="required">
									</select>
									<input type="hidden" id="hiddenKelurahan" name="kelurahan">
								</div>
							</div>
							<div class="form-group col-md-12">
								<label for="inputLine">Photo</label>
								<input type="file" class="form-control" id="inputFoto" name="image">
								<small id="photoHelp" class="form-text text-muted">Upload Photo. Maksimal size 5mb</small>
							</div>
							<div class="form-group col-md-12">
								<label for="inputLine">Kategori Vendor</label><br>
								<small id="kategoriHelp" class="form-text text-muted">Kategori bisa di pilih lebih dari 1</small>
								<div class="form-row options">
									<div class="col-md-6">
										<div class="checkbox">
											<label><input type="checkbox" value="Venue" name="kategori[]" required>&nbsp;Venue</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Photography" name="kategori[]" required>&nbsp;Photography</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Wardrobe" name="kategori[]" required>&nbsp;Wardrobe</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Decoration" name="kategori[]" required>&nbsp;Decoration</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Souvenir" name="kategori[]" required>&nbsp;Souvenir</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Catering" name="kategori[]" required>&nbsp;Catering</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Make Up" name="kategori[]" required>&nbsp;Make Up</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="checkbox">
											<label><input type="checkbox" value="Invitation" name="kategori[]" required>&nbsp;Invitation</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Entertainment" name="kategori[]" required>&nbsp;Entertainment</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Transportation" name="kategori[]" required>&nbsp;Transportation</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Event Cake" name="kategori[]" required>&nbsp;Event Cake</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Videography" name="kategori[]" required>&nbsp;Videography</label>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group col-md-12">
								<label for="inputWebsite">Website</label>
								<input type="text" class="form-control" id="inputWebsite" name="website" placeholder="Masukkan Url Website">
							</div>
							<div class="form-row">
								<div class="form-group col-md-3">
									<label for="inputWebsite">Whatsapp</label>
									<input type="text" class="form-control" id="inputWhatsapp" name="whatsapp" placeholder="Masukkan Nomor Whatsapp">
								</div>
								<div class="form-group col-md-3">
									<label for="inputFacebook">Facebook</label>
									<input type="text" class="form-control" id="inputFacebook" name="facebook" placeholder="Masukkan username facebook">
								</div>
								<div class="form-group col-md-3">
									<label for="inputInstagram">Instagram</label>
									<input type="text" class="form-control" id="inputInstagram" name="instagram" placeholder="Masukkan username Instagram">
								</div>
								<div class="form-group col-md-3">
									<label for="inputLine">Line</label>
									<input type="text" class="form-control" id="inputLine" name="line" placeholder="Masukkan username Line">
								</div>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
							</div>
							<!-- <div class="form-group">
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
									Launch demo modal
								</button>
							</div> -->
							<div class="form-group col-md-12">
								<center>
									<div style="background-color: #fafafa; padding: 20px; border: 1px solid #eeeeee;
									border-radius: 4px;
									box-shadow: 0 1px 3px 0 rgba(0,0,0,.12);">
									<p>Dengan mengklik Daftar, Anda telah menyetujui segala <a data-toggle="modal" data-target="#exampleModalLong">Syarat dan Ketentuan</a> yang telah dibuat oleh Kami. <br>Anda akan menerima notifikasi Email dari YEPS Indonesia.</p>
								</div>
							</center>
						</div>
						<div class="form-group col-md-12">
							<input type="submit" class="btn btn-primary" name="daftar" value="Daftar">&nbsp;<input type="reset" class="btn btn-danger" name="batal" value="Batal">
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>

	<!-- Footer -->
	<footer id="footer" class="footer-light">
		<div class="footer-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<center>
							<!-- Footer widget area 1 -->
							<div class="widget clearfix widget-contact-us" style="background-image: url('<?php echo base_url('asset/images/world-map-dark.png'); ?>'); background-position: 30% 70px; background-repeat: no-repeat">
								<img style="max-height: 80px; margin-bottom: 20px" src="<?php echo base_url('asset/images/logo-p.png'); ?>">
								<ul class="list-icon">
									<li>
										<i class="fa fa-map-marker"></i>&nbsp;Ariobimo Sentral Lantai 4. Jl. H. R. Rasuna Said Kuningan Timur, Kecamatan Setiabudi, 
										Jakarta Selatan, DKI Jakarta 12950
									</li>
									<li><i class="fa fa-phone"></i> (123) 456-7890 </li>
									<li><i class="fa fa-envelope"></i> <a href="halo@yepsindonesia.com">halo@yepsindonesia.com</a>
									</li>
								</ul>
								<!-- Social icons -->
								<div class="social-icons social-icons-border float-left m-t-20">
									<!-- <ul>
										<li class="social-instagram"><a href="https://www.instagram.com/yepsindonesia/"><i class="fa fa-instagram"></i></a></li>
										<li class="social-facebook"><a href="https://www.facebook.com/yepsindo/"><i class="fa fa-facebook"></i></a></li>
										<li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li class="social-youtube"><a href="#"><i class="fa fa-youtube"></i></a></li>
									</ul> -->
								</div>
								<!-- end: Social icons -->
							</div>
							<!-- end: Footer widget area 1 -->
						</center>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- end: Footer -->

	<!-- Scroll Top Button -->
	<button class="scroll-top scroll-to-target" data-target="html">
		<span class="fa fa-angle-up"></span>
	</button>

</div>


<script src="<?php echo base_url('asset/js/jquery.js');?>"></script>
<script src="<?php echo base_url('asset/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('asset/js/pagenav.js'); ?>"></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js'></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/utils/Draggable.min.js'></script>
<script src="<?php echo base_url('asset/js/mousemoveparallax.js');?>"></script>
<script src="<?php echo base_url('asset/js/owl.js');?>"></script>
<script src="<?php echo base_url('asset/js/script.js');?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.11/dist/sweetalert2.all.min.js"></script>
<script type="text/javascript">
$(function(){
    var requiredCheckboxes = $('.options :checkbox[required]');
    requiredCheckboxes.change(function(){
        if(requiredCheckboxes.is(':checked')) {
            requiredCheckboxes.removeAttr('required');
        } else {
            requiredCheckboxes.attr('required', 'required');
        }
    });
});
</script>
<script type="text/javascript">
	$("#register-form").submit(function(e) {
		e.preventDefault();    
		var formData = new FormData(this);
		
		$.ajax({
			url: '<?= $this->config->item('api_http'); ?>/vendors/user',
			type: 'POST',
			data: formData,
			beforeSend  : function(){
                $("#submit").prop("disabled", true);
                $("#loading").show();
                $("#wrapper").css('opacity','0.5');  
            },
			success: function (data) {
				console.log(data);					
				if(data.status == 'success'){
				swal({
					type : "success",
					text : data.message,
					}).then( function(e){
						window.location.href = '<?= base_url(''); ?>';
					});
				} else {
					swal({
						type : "info",
						text : "Pendaftaran gagal silahkan, ulangi lagi!",
					}).then( function(e){
						
						location.reload();
					});
				}
			},
			cache: false,
			contentType: false,
			processData: false
		});
	});

	$(document).ready(function() { 
		$("#inputProvinsi").append('<option value="">Pilih</option>'); 
		$("#inputKabupaten").html(''); 
		$("#inputKecamatan").html(''); 
		$("#inputKelurahan").html(''); 
		$("#inputKabupaten").append('<option value="">Pilih</option>'); 
		$("#inputKecamatan").append('<option value="">Pilih</option>'); 
		$("#inputKelurahan").append('<option value="">Pilih</option>'); 
		url = '<?= $this->config->item('api_http'); ?>/vendors/provinsi'; 
		$.ajax({ url: url, 
			type: 'GET', 
			dataType: 'json', 
			success: function(result) { 
				for (var i = 0; i < result.length; i++) 
					$("#inputProvinsi").append('<option value="' + result[i].id_prov + '">' + result[i].nama + '</option>'); 
			} 
		}); 
	}); 
	$("#inputProvinsi").change(function(){ 
		var id_prov = $("#inputProvinsi").val(); 
		$("#hiddenProvinsi").val($( "#inputProvinsi option:selected" ).text());
		var url = '<?= $this->config->item('api_http'); ?>/vendors/kabupatens?id_prov=' + id_prov; 
		$("#inputKabupaten").html(''); $("#inputKecamatan").html(''); 
		$("#inputKelurahan").html(''); $("#inputKabupaten").append('<option value="">Pilih</option>'); 
		$("#inputKecamatan").append('<option value="">Pilih</option>'); 
		$("#inputKelurahan").append('<option value="">Pilih</option>'); 
		$.ajax({ url : url, 
			type: 'GET', 
			dataType : 'json', 
			success : function(result){ 
				for(var i = 0; i < result.length; i++) 
					$("#inputKabupaten").append('<option value="'+ result[i].id_kab +'">' + result[i].nama + '</option>'); 
			} 
		});  
	}); 
	$("#inputKabupaten").change(function(){ 
		var id_kab = $("#inputKabupaten").val(); 
		$("#hiddenKabupaten").val($( "#inputKabupaten option:selected" ).text());
		var url = '<?= $this->config->item('api_http'); ?>/vendors/kecamatan?id_kab=' + id_kab; 
		$("#inputKecamatan").html(''); $("#inputKelurahan").html(''); 
		$("#inputKecamatan").append('<option value="">Pilih</option>'); 
		$("#inputKelurahan").append('<option value="">Pilih</option>'); 
		$.ajax({ url : url, 
			type: 'GET', 
			dataType : 'json', 
			success : function(result){ 
				for(var i = 0; i < result.length; i++) 
					$("#inputKecamatan").append('<option value="'+ result[i].id_kec +'">' + result[i].nama + '</option>'); 
			} 
		});  
	}); 
	$("#inputKecamatan").change(function(){ 
		var id_kec = $("#inputKecamatan").val(); 
		$("#hiddenKecamatan").val($( "#inputKecamatan option:selected" ).text());
		var url = '<?= $this->config->item('api_http'); ?>/vendors/kelurahan?id_kec=' + id_kec; $("#inputKelurahan").html(''); 
		$("#inputKelurahan").append('<option value="">Pilih</option>'); 
		$.ajax({ url : url, 
			type: 'GET', 
			dataType : 'json', 
			success : function(result){ 
				for(var i = 0; i < result.length; i++) 
					$("#inputKelurahan").append('<option value="'+ result[i].id_kel +'">' + result[i].nama + '</option>'); 
			} 
		});  
	});
	$("#inputKelurahan").change(function(){
		$("#hiddenKelurahan").val($( "#inputKelurahan option:selected" ).text());
	}); 
</script>

</body>

<!-- Mirrored from html.tonatheme.com/2018/bari-landing/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Jul 2018 07:36:30 GMT -->
</html>