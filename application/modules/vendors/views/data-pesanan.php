<?php $this->load->view('header'); ?>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.css">
<?php $this->load->view('sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header sty-one">
    <h1>Data Pesanan</h1>
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li><i class="fa fa-angle-right"></i> Data Pesanan</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline">
          <!-- <div class="card-header bg-blue">
            <h5 class="text-white m-b-0">Data Pesanan</h5>
          </div> -->
          <div class="card-body">
            <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr style="text-align:center;">
                    <th style="width : 5%">No</th>
                    <th style="width : 60%">Detail Pesanan</th>
                    <th style="width : 15%">Total Harga</th>
                    <th style="width : 20%; ">Aksi</th>
                  </tr>
                </thead>
                <tbody id="data-pesanan">
                </tbody>
                <tbody id="menu-data-pesanan">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </section>
  <!-- /.content --> 
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('footer'); ?>
<div class="modal" id="detail-belanja" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Detail Data Belanja</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script type="text/javascript">
  $('#data-pesanan').on('click','.detail-belanja', function(e){
    e.preventDefault();
    var uuid = $(this).data('uuid');
    $.ajax({
      url : "<?php echo base_url('vendors/Data_pesanan/get_detail_belanja/');?>"+uuid,
      success : function(e){
       $('#detail-belanja .modal-body').html(e);
       $('#detail-belanja').modal({show:true});
     }
   });
  })
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() { 

    $.ajax({
      url: '<?= $this->config->item('api_http'); ?>/vendors/produk_order_all',
      type: 'POST',
      data: {
        uuid : '<?phP echo $this->session->userdata('uuid'); ?>'
      },
      dataType: 'json',
      success: function(result){
       response = result.response;
       var no = 1;
       for(var i = 0; i < response.total; i++){
        var template = '<tr><td style="text-align:center;">'+no+'</td><td><span style="font-size:14px; font-weight: 600; color: #1374d2;"><a href="" data-toggle="modal" data-target="#detail-belanja" data-uuid="'+response.data[i].uuid+'" class="detail-belanja">'+response.data[i].kode_invoice+'</a><br></span><span style="font-weight: 600; color: #00000;">'+response.data[i].nama_jasa+'</span></br> Qty : '+response.data[i].qty+'</br> Tanggal Event: '+indonesia_date(response.data[i].date_order) + ' - ' + response.data[i].time_order +' WIB </br><hr> <span style="font-weight: 600; color: #38c338;">Sisa Waktu Konfirmasi : '+response.data[i].qty+' Menit </span></td><td>Rp. '+ convertRupiah(response.data[i].total_biaya) +'</td><td style="text-align: center;">'+(response.data[i].status == 'ACCEPT' || response.data[i].status == 'accept' || response.data[i].status == 'reject' || response.data[i].status == 'REJECT' ? response.data[i].status : '<button data-uuid="'+response.data[i].uuid+'" class="btn btn-success btn-sm accept">Terima</button>&nbsp;<button data-uuid="'+response.data[i].uuid+'" class="btn btn-danger btn-sm reject">Tolak</button>')+'</td></tr>';
        no++;
        $("#data-pesanan").append(template);
      }
      $('#example1').DataTable();
      $("#example1").on("click", ".accept", function(e) {
        uuid = $(this).data('uuid');
        //alert(uuid);
        swal({
          title: "Terima pesanan",
          text: "Anda yakin ingin menerima pesanan ini?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Ya, saya yakin!',
          cancelButtonText: "Tidak, batalkan!",
        }).then((result) => {
          if(result.value){
            $.ajax({
              type : "POST",
              data : {uuid :  uuid, proses : "true"},
              url : "<?= $this->config->item('api_http'); ?>/vendors/proses_pesanan",
              success : function(hasil){
                hasil = JSON.parse(hasil);
                swal({
                  title: hasil.status,
                  text: hasil.message,
                  type: hasil.status
                }).then((result) => {
                  location.reload();
                });
              }
            });
          }
        });
      });

      $("#example1").on("click", ".reject", function(e) {
       uuid = $(this).data('uuid');
       swal({
        title: "Tolak pesanan",
        text: "Anda yakin ingin menolak pesanan ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Ya, saya yakin!',
        cancelButtonText: "Tidak, batalkan!",
      }).then((result) => {
        if(result.value){
          $.ajax({
            type : "POST",
            data : {uuid :  uuid, proses : "false"},
            url : "<?= $this->config->item('api_http'); ?>/vendors/proses_pesanan_tolak",
            success : function(hasil){
              hasil = JSON.parse(hasil);
              swal({
                title: hasil.status,
                text: hasil.message,
                type: hasil.status
              }).then((result) => {
                location.reload();
              });
            }
          });
        }
      });
    });     
    }
  });
  });

  function convertRupiah(bilangan){
    var reverse = bilangan.toString().split('').reverse().join(''),
    ribuan  = reverse.match(/\d{1,3}/g);
    ribuan  = ribuan.join('.').split('').reverse().join('');

    return ribuan;
  }

  function createButton(text, cb) {
    return $('<button>' + text + '</button>').on('click', cb);
  }
</script>