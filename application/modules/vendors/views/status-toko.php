<?php $this->load->view('header'); ?>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.css">
<?php $this->load->view('sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header sty-one">
    <h1>Daftar Produk</h1>
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li><i class="fa fa-angle-right"></i> Daftar Produk</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-lg-12">
        <div class="info-box">
          <div class="card tab-style1"> 
            <!-- Nav tabs -->

            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane active" id="home" role="tabpanel" aria-expanded="true">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-12 col-xs-6">
                     <div class="card m-t-3">
                      <div class="card-body">
                        <h4 class="text-black">Status Toko <span style="font-size: 14px; font-weight: bold; margin-left: 20px" id="status_toko">
                        <img id="gambar_status" alt="" style="height: 30px; padding-right: 10px"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<input type="submit" id="aktif_toko" class="btn btn-sm btn-success" value="Aktifkan Toko"></h4> 
                        
                        <p>Atur Jadwal Tutup Toko</p>
                        <hr>
                        <form id="form_tutup_toko">
                          <div class="row m-t-2">
                            <div class="col-md-6">
                            <input type="hidden" name="uuid" value="<?php echo $this->session->userdata('uuid'); ?>">
                              <div class="form-group">
                                <label>Mulai Tanggal</label>
                                <input class="form-control" name="mulai" id="awal_tanggal" type="date">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Sampai Tanggal</label>
                                <input class="form-control" name="selesai" id="akhir_tanggal" type="date">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label>Catatan</label>
                                <textarea class="form-control" name="keterangan" rows="3"></textarea>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-12">
                              <input type="submit" class="btn btn-sm btn-success" value="Simpan">
                            </div>
                          </div>
                        </form>

                      </div>
                      </div>
                      <div class="card-body">
                      <h4 class="text-black">Riwayat Tutup Toko<span style="font-size: 14px; font-weight: bold; margin-left: 20px"></h4>
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped">
                            <thead>
                              <tr style="text-align:center;">
                                <th style="width : 5%">No</th>
                                <th style="width : 20%">Mulai</th>
                                <th style="width : 20%">Sampai</th>
                                <th style="width : 55%; ">Keterangan</th>
                              </tr>
                            </thead>
                            <tbody id="data-tutup-toko">
                            </tbody>
                          </table>
                        </div>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Main row --> 
    </section>
    <!-- /.content --> 
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.js"></script>
  <script type="text/javascript">
   $(document).ready(function(){
          $.post("<?= $this->config->item('api_http'); ?>/vendors/get_tutup_toko/<?php echo $this->session->userdata('uuid');?>",  function(hasil){
              var response = JSON.parse(hasil);
              if(response[0].aktifVendor=="1"){
                document.getElementById('gambar_status').src='<?php echo base_url('dist/img/buka.png');?>';
                $("#status_toko").append("Toko Aktif");
                }
              else{ 
                document.getElementById('gambar_status').src='<?php echo base_url('dist/img/close.png');?>';
                $("#status_toko").append("Toko Tutup");
                }
              
                var no = 1;
                for(var i = 0; i < response.length; i++){
                  var template = '<tr><td>'+no+'</td><td>'+response[i].date_start+'</td><td>'+response[i].date_end+'</td><td>'+response[i].keterangan+'</td></tr>';
                  no++;
                  $("#data-tutup-toko").append(template);
                }   
            }); 
        }); 


  $("#form_tutup_toko").submit(function(e){
    e.preventDefault();
    $.post('<?= $this->config->item('api_http'); ?>/vendors/tutup_toko', $(this).serialize(), function(response){
      var response = JSON.parse(response);
      if(response.status == "success"){
        swal({
          title: "Berhasil!",
          text: response.message,
          type: "success"
        }).then((result) => {
          window.location.href = '<?php echo base_url('vendors/home');?> ';
        });
      }
      else{
        swal({
          title: "Gagal!",
          text: response.message,
          type: "error"
        }).then((result) => {
          window.location.href = '<?php echo base_url('vendors/home');?> ';
        });
      }

    });
  });

  $("#aktif_toko").click(function(e){
    e.preventDefault();
    $.post('<?= $this->config->item('api_http'); ?>/vendors/aktif_toko/<?php echo $this->session->userdata('uuid');?>', function(response){
      var response = JSON.parse(response);
      if(response.status == "success"){
        swal({
          title: "Berhasil!",
          text: response.message,
          type: "success"
        }).then((result) => {
          window.location.href = '<?php echo base_url('vendors/home');?> ';
        });
      }
      else{
        swal({
          title: "Gagal!",
          text: response.message,
          type: "error"
        }).then((result) => {
          window.location.href = '<?php echo base_url('vendors/home');?> ';
        });
      }

    });
  });
  
  </script>
  