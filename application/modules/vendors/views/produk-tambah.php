<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>
<!-- Load Leaflet from CDN-->
<link rel="stylesheet" href="<?php echo base_url('assets/leaflet/leaflet.css'); ?>" />
<!-- <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script> -->
<script type="text/javascript" src="<?php echo base_url('assets/leaflet/leaflet.js'); ?>"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.min.css">

<!-- Load Esri Leaflet from CDN -->
<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet/1.0.0-rc.2/esri-leaflet.js"></script>

<style>
body {margin:0;padding:0;}
#map {position: relative;top:0;bottom:0;right:0;left:0;
  height: 250px;}
</style>
<link rel="stylesheet" type="text/css" href="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 

  <!-- Modal -->
  <div class="modal fade" id="videoTutorial" tabindex="100" role="dialog" aria-labelledby="videoTutorialTitle" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 700px; min-height:380px">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="videoTutorialTitle">Video Tutorial</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="iframe-wrap">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/EylGGU1aqus" frameborder="0" allowfullscreen></iframe>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end:Modal -->

  <!-- Content Header (Page header) -->
  <section class="content-header sty-one">
    <h1>Tambah Service</h1>
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li><i class="fa fa-angle-right"></i> Tambah Service</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-lg-12">
        <div class="info-box">
          <div class="card tab-style1">
            <div class="tab-content">
              <div class="tab-pane active" id="home" role="tabpanel" aria-expanded="true">
                <div class="card-body">
                  <div class="row">

                    <!-- Nav tabs -->
                    <div class="col-md-10">
                      <h3 style="padding-top: 10px; font-weight: bold;">Video Tutorial Upload Service</h3>
                    </div>
                    <div class="col-md-2 hidden-xs">
                      <a class="btn btn-danger text-light" data-toggle="modal" data-target="#videoTutorial">
                        <i class="fa fa-play"></i>&nbsp;&nbsp;Lihat Video
                      </a>
                    </div>
                    <div class="col-md-2 mobile">
                      <a class="btn btn-danger text-light" href="https://www.youtube.com/watch?v=EylGGU1aqus" target="_blank">
                        <i class="fa fa-play"></i>&nbsp;&nbsp;Lihat Video
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> 
  </section>
  <!-- /.content --> 
  <!-- Main content -->
  <section class="content" style="margin-top: -100px">
    <div class="row">
      <div class="col-lg-12">
        <div class="info-box">
          <div class="card tab-style1"> 
            <!-- Nav tabs -->

            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane active" id="home" role="tabpanel" aria-expanded="true">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-12 col-xs-6">
                      <!-- <hr style="background-color: #00a65a; height: 2px; "> -->

                      <div class="col-md-12" style="margin-top: 40px">
                        <div class="hr-title hr-long center"><abbr>Data Service</abbr> </div>
                      </div>
                      <form class="form-horizontal container" id="form_tambah_produk">
                        <input type="hidden" name="uuid_vendor" id="uuid_vendor">
                        <div class="form-group row">
                          <label class="col-sm-3">Pilih Kategori</label>
                          <div class="col-sm-9">
                            <select class="form-control form-control-line" id="kategori_vendor" name="kategori_vendor">
                              <option disabled selected>-- Silahkan Pilih Kategori --</option>
                            </select>
                          </div>
                        </div>
                        
                        <div class="form-group row">
                            
                          <div id="example">

                        </div>
                        <div class="form-group" style="margin-top: 20px;">
                          <div class="col-sm-12">
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="submit" class="btn btn-success btn-sm" value="Simpan"> <input type="reset" class="btn btn-danger btn-sm" value="Batal">
                          </div>
                        </div>
                      </form>
                    </div>
                    <!-- TUTUP FORM -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Main row --> 
  </section>
  <!-- /.content --> 
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('footer'); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/classic/ckeditor.js"></script>
<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.js"></script>
<script type="text/javascript">
  $.getJSON('<?= $this->config->item('api_http'); ?>/vendors/kategori', function(data) {
    <?php
    if(@$this->session->userdata('cro') == 'true'){

      ?>
      localStorage.setItem('kategori', '<?php echo @$this->session->userdata('kategori'); ?>');
      <?php
    }
    ?>
    var kategori = localStorage.getItem("kategori").split(", ");
    for(var i = 0; i < kategori.length; i++){
      data.forEach(function (arrayItem) {
        if(kategori[i] == arrayItem.nama)
         $("#kategori_vendor").append("<option value='"+arrayItem.id+"'>"+arrayItem.nama+"</option>");
     });
    }
  });
  $("#uuid_vendor").val('<?php echo $this->session->userdata('uuid'); ?>');
    
  var duplikat_produk = false;
  $("#kategori_vendor").change(function(){
    $("#example").html("");
    var id = $("#kategori_vendor").val();
    //$.getJSON('https://api.yepsindonesia.com/vendors/parameter/' + id, function(data) {
    $.getJSON('<?= $this->config->item('api_http'); ?>/vendors/parameter/' + id, function(data) {
       data.forEach(function (arrayItem) {
        if(arrayItem.parameter_id == 4){
          var template = '<div class="form-group row"><label class="col-md-3">'+ arrayItem.nama +'<span style="color: red">*</span></label><div class="col-md-9 row">'+ arrayItem.jenis +'</div></div>';
          $("#example").append(template); 
        }
        else if(arrayItem.parameter_id != 4 || arrayItem.parameter_id != 13){
          var template = '<div class="form-group row"><label class="col-md-3">'+ arrayItem.nama +'<span style="color: red">*</span></label><div class="col-md-9">'+ arrayItem.jenis +'<span class="help-inline form-control-feedback" aria-hidden="true">'+ arrayItem.help_text+'</span></div></div>';
          $("#example").append(template);  
        }
        else{
          var template = '<input type="hidden" value="" name="value[]"/>';
          $("#example").append(template);
        }

      });
      
      //$.getJSON('https://api.yepsindonesia.com/vendors/kabupaten', function(data) {
      $.getJSON('<?= $this->config->item('api_http'); ?>/vendors/kabupaten', function(data) {
        data.forEach(function (arrayItem) {
          var template = '<option value="' + arrayItem.nama + '">' + arrayItem.provinsi + ' - ' + arrayItem.nama + '</option>';  
          $("#p12").append(template);
        });
        $("#p12").select2();
      });


      var myEditor;

      ClassicEditor
      .create( document.querySelector( '#p5' ), {
        toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote', 'undo', 'redo' ],
        heading: {
          options: [
          { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
          { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
          { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
          ]
        }
      } )
      .then( editor => {
        console.log( 'Editor was initialized', editor );
        myEditor = editor;
      } )
      .catch( error => {
        console.log( error );
      } );

      $("#form_tambah_produk").submit(function(e){
        e.preventDefault();
        $("#p5").html(myEditor.getData());
        if(!duplikat_produk){
          if($("#p58_hidden").val() == '' || $("#p58").val() == ''){
            swal("Warning", "Anda belum memilih map!", "warning");
          }
          else{
           
            var data_produk = $("#form_tambah_produk").serializeArray();
            
            var foto = [data_produk[5].value, data_produk[6].value, data_produk[7].value, data_produk[8].value];
            var foto_join = foto.join(';');
            data_produk.splice(6,3);
            data_produk[5].value = foto_join;
            var serialize = '';
            for (var i = 0, l = data_produk.length; i < l; i++) {
              serialize += data_produk[i].name + '=' + data_produk[i].value;
              if(i < l - 1){
                serialize += '&';
              }
            }
            //alert(serialize);
            //$.post("https://api.yepsindonesia.com/vendors/vendor_add_produk", serialize, function(data, status) {
            $.post("<?= $this->config->item('api_http'); ?>/vendors/vendor_add_produk", serialize, function(data, status) {
              console.log(data);
              data = JSON.parse(data);
              $("#field_name").val();
              $("#field_value").val();
              $("#field_price").val();
              if(data.status == "success"){
                swal({
                  title: "Berhasil!",
                  text: data.message,
                  type: "success"
                }).then((result) => {
                  location.reload();
                });
              }
              else{
                swal("Gagal!", data.message, "error");
              }
            });
          }
        }
        else{
          swal("Duplikat Produk", "Nama Produk sudah ada!", "warning").then((result) => {
            $("#p1").focus().select();
          });
        }
      });

      $("#example").append('<div id="map"></div>');
      var map = L.map('map').setView([-6.1761317,106.8206753], 13);
      var marker;


      ACCESS_TOKEN = 'pk.eyJ1IjoiaGFuc2VuZHVzZW5vdiIsImEiOiJjajNwd2gzODcwMGs4MzNuNDFpczAwY3dmIn0.pq3-o52y_eUHZKNB00ZGCg';

      L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + ACCESS_TOKEN, {
        attribution: 'Imagery &copy <a href="http://mapbox.com">Mapbox</a>  | Custom by <a href="https://hansendusenov.web.id" target="blank">Hansen Dusenov</a>',
        id: 'mapbox.streets'
      }).addTo(map); 
      /* untuk additional pertama kali*/ 
      //$("#example").append('<div class="form-group row"><label class="col-sm-3">Nama Additional Produk</label><div class="col-sm-9"><div class="field_wrapper"><input type="text" class="form-control form-control-line" name="field_name[]" id="field_name[]" placeholder="nama" value=""/><input type="text" class="form-control form-control-line" name="field_value[]" id="field_value[]" placeholder="jumlah barang" value=""/><input type="text" class="form-control form-control-line" name="field_price[]" id="field_price[]" placeholder="harga barang" value=""/><a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa fa-plus"></i></a></div></div></div>');
      $("#example").append('<div class="col-md-12" style="margin-top: 40px"><div class="hr-title hr-long center"><abbr>Additional Service</abbr><p style="font-size: 12px; color; #666f73">Kosongkan jika tidak ada service / produk tambahan</p></div></div><div class="field_wrapper"><div class="form-group row"><label class="col-sm-3">Nama Item</label><div class="col-sm-9"><input type="text" class="form-control" name="field_name[]" id="field_name[]" placeholder="Masukkan Nama Item" value=""/></div></div><div class="form-group row"><label class="col-sm-3">Harga</label><div class="col-sm-9"><input type="number" class="form-control" name="field_price[]" id="field_price[]" placeholder="Masukkan Harga Service" value=""/><span class="help-inline form-control-feedback" aria-hidden="true">Rupiah</span></div></div><div class="form-group row"><label class="col-sm-3">Minimal Pesanan</label><div class="col-sm-9"><input type="text" class="form-control" name="field_value[]" id="field_value[]" placeholder="Masukkan Minimal Pesanan" value=""/></div></div><div class="form-group row"><label class="col-sm-3"></label><div class="col-sm-9"><a href="javascript:void(0);" class="btn btn-primary btn-sm add_button">Tambah Item</a></div></div></div>');
      /* untuk menambahkan additional dinamis */
      //awal
      var maxField = 100; //Input fields increment limitation
      var addButton = $('.add_button'); //Add button selector
      var wrapper = $('.field_wrapper'); //Input field wrapper
      var x = 1; //Initial field counter is 1
      //vdHTML = 
      //Once add button is clicked
      $(addButton).click(function(){
          //Check maximum number of input fields
          if(x < maxField){ 
              x++; //Increment field counter
              //$(wrapper).append('<div><input type="text" name="field_name[]" id="field_name[]" placeholder="nama" value=""/><input type="text" name="field_value[]" id="field_value[]" placeholder="jumlah barang" value=""/><input type="text" name="field_price[]" id="field_price[]" placeholder="harga barang" value=""/><a href="javascript:void(0);" class="remove_button"><i class="fa fa-minus"></i></a></div>'); //Add field html
              $(wrapper).append('<div class="depan"><div class="form-group row"><label class="col-sm-3">Nama Item</label><div class="col-sm-9"><input type="text" class="form-control" name="field_name[]" id="field_name[]" placeholder="Masukkan Nama Item" value=""/></div></div><div class="form-group row"><label class="col-sm-3">Harga</label><div class="col-sm-9"><input type="number" class="form-control" name="field_price[]" id="field_price[]" placeholder="Masukkan Harga Service" value=""/><span class="help-inline form-control-feedback" aria-hidden="true">Rupiah</span></div></div><div class="form-group row"><label class="col-sm-3">Minimal Pesanan</label><div class="col-sm-9"><input type="text" class="form-control" name="field_value[]" id="field_value[]" placeholder="Masukkan Minimal Pesanan" value=""/></div></div><div class="form-group row"><label class="col-sm-3"></label><div class="col-sm-9"><a href="javascript:void(0);" class="btn btn-primary btn-sm add_button2">Tambah Item</a><a href="javascript:void(0);" class="btn btn-danger btn-sm remove_button">Hapus Item</a></div></div>'); //Add field html
          }
      });
      
      //Once remove button is clicked
      $(wrapper).on('click', '.remove_button', function(e){
          e.preventDefault();
          $(this).parents('.depan').remove(); //Remove field html
          x--; //Decrement field counter
      });
      $(wrapper).on('click', '.add_button2', function(e){
          e.preventDefault();
          if(x < maxField){ 
              x++; //Increment field counter
              //$(wrapper).append('<div><input type="text" name="field_name[]" id="field_name[]" placeholder="nama" value=""/><input type="text" name="field_value[]" id="field_value[]" placeholder="jumlah barang" value=""/><input type="text" name="field_price[]" id="field_price[]" placeholder="harga barang" value=""/><a href="javascript:void(0);" class="remove_button"><i class="fa fa-minus"></i></a></div>'); //Add field html
              $(wrapper).append('<div class="depan"><div class="form-group row"><label class="col-sm-3">Nama Item</label><div class="col-sm-9"><input type="text" class="form-control" name="field_name[]" id="field_name[]" placeholder="Masukkan Nama Item" value=""/></div></div><div class="form-group row"><label class="col-sm-3">Harga</label><div class="col-sm-9"><input type="number" class="form-control" name="field_price[]" id="field_price[]" placeholder="Masukkan Harga Service" value=""/><span class="help-inline form-control-feedback" aria-hidden="true">Rupiah</span></div></div><div class="form-group row"><label class="col-sm-3">Minimal Pesanan</label><div class="col-sm-9"><input type="text" class="form-control" name="field_value[]" id="field_value[]" placeholder="Masukkan Minimal Pesanan" value=""/></div></div><div class="form-group row"><label class="col-sm-3"></label><div class="col-sm-9"><a href="javascript:void(0);" class="btn btn-primary btn-sm add_button2">Tambah Item</a><a href="javascript:void(0);" class="btn btn-danger btn-sm remove_button">Hapus Item</a></div></div>'); //Add field html
          }
          //$(this).parents('.depan').remove(); //Remove field html
          //x--; //Decrement field counter
      });
      //akhirar fiel
      var searchControl = new L.esri.Controls.Geosearch().addTo(map);

      var results = new L.LayerGroup().addTo(map);

      searchControl.addEventListener('keypress', function (e) {
        var key = e.which || e.keyCode;
        if (key === 13) {

          alert('gk boleh pencet enter');
        }
      });

      searchControl.on('results', function(data){
        if (marker) { 
          map.removeLayer(marker); 
        }
        results.clearLayers();

        results.addLayer(L.marker(data.results[data.results.length - 1].latlng));
        $("#p58").val(data.results[data.results.length - 1].latlng.lat+','+data.results[data.results.length - 1].latlng.lng);
        $("#p58_hidden").val(data.results[data.results.length - 1].latlng.lat+','+data.results[data.results.length - 1].latlng.lng);
        console.log(data.results[data.results.length - 1]);

      });

      map.on('click', function(e){
        if (marker) { 
          map.removeLayer(marker);
        }
        if (results){
          results.clearLayers();
        }
        marker = new L.marker(e.latlng).addTo(map);
        $("#p58").val(e.latlng.lat+','+e.latlng.lng);
        $("#p58_hidden").val(e.latlng.lat+','+e.latlng.lng);
        console.log(e);
      });

      var drEvent1 = $("#p4_1.dropify-event").dropify();

      drEvent1.on('dropify.beforeClear', function(event, element){
          //return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
          return true;
        });

      drEvent1.on('dropify.afterClear', function(event, element){
        $("#p4_1_hidden").val('');
      });

      drEvent1.on('dropify.errors', function(event, element){
        console.log('Has Errors');
      });

      var drEvent2 = $("#p4_2.dropify-event").dropify();

      drEvent2.on('dropify.beforeClear', function(event, element){
        return true;
      });

      drEvent2.on('dropify.afterClear', function(event, element){
        $("#p4_2_hidden").val('');
      });

      drEvent2.on('dropify.errors', function(event, element){
        console.log('Has Errors');
      });

      var drEvent3 = $("#p4_3.dropify-event").dropify();

      drEvent3.on('dropify.beforeClear', function(event, element){
        return true;
      });

      drEvent3.on('dropify.afterClear', function(event, element){
        $("#p4_3_hidden").val('');
      });

      drEvent3.on('dropify.errors', function(event, element){
        console.log('Has Errors');
      });

      var drEvent4 = $("#p4_4.dropify-event").dropify();

      drEvent4.on('dropify.beforeClear', function(event, element){
        return true;
      });

      drEvent4.on('dropify.afterClear', function(event, element){
        $("#p4_4_hidden").val('');
      });

      drEvent4.on('dropify.errors', function(event, element){
        console.log('Has Errors');
      });



         /*  $("#p1").attr('pattern', '[A-Za-z0-9\s]{1,}');
         $("#p1").attr('title', 'Tidak boleh menggunakan spesial character \*:;!?+/@#_|~`={}\\$^%\'"');*/
         $("#p1").on('keyup', function(){
          var pattern = /[^\s\w\d]/g;
          if(pattern.test($("#p1").val())){
            console.log("Mengandung illegal karakter");
          }
          else{
            console.log("Tidak mengandung illegal karakter");
          }
        });

         $("#p1").on("change", function(e){

          //$.post("https://api.yepsindonesia.com/vendors/cek_nama_produk", "uuid=" + '<?phP echo $this->session->userdata('uuid'); ?>' + "&nama_produk=" + $("#p1").val(), function(data){
          $.post("<?= $this->config->item('api_http'); ?>/vendors/cek_nama_produk", "uuid=" + '<?phP echo $this->session->userdata('uuid'); ?>' + "&nama_produk=" + $("#p1").val(), function(data){
            data = JSON.parse(data);
            if(data.length != 0){
              duplikat_produk = true;
              swal("Duplikat Produk", "Nama Produk sudah ada!", "warning").then((result) => {
                $("#p1").focus().select();
              });

            }else{
              duplikat_produk = false;
            }
          });
        });

         $("#p4_1").on("change", function(e){ 
          var file = this.files[0];
          var form = new FormData();
          form.append('image', file);
          $.ajax({
            //url : "https://api.yepsindonesia.com/vendors/cloudinary",
            url : "<?= $this->config->item('api_http'); ?>/vendors/cloudinary",
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            data : form,
            success: function(response){

              data = JSON.parse(response);
              $("#p4_1_hidden").val(data.public_id);
              console.log("Foto 1");
            }
          });
        });

         $("#p4_2").on("change", function(e){ 
          var file = this.files[0];
          var form = new FormData();
          form.append('image', file);
          $.ajax({
            //url : "https://api.yepsindonesia.com/vendors/cloudinary",
            url : "<?= $this->config->item('api_http'); ?>/vendors/cloudinary",
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            data : form,
            success: function(response){

              data = JSON.parse(response);
              $("#p4_2_hidden").val(data.public_id);
              console.log("Foto 2");
            }
          });
        });

         $("#p4_3").on("change", function(e){ 
          var file = this.files[0];
          var form = new FormData();
          form.append('image', file);
          $.ajax({
            //url : "https://api.yepsindonesia.com/vendors/cloudinary",
            url : "<?= $this->config->item('api_http'); ?>/vendors/cloudinary",
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            data : form,
            success: function(response){

              data = JSON.parse(response);
              $("#p4_3_hidden").val(data.public_id);
              console.log("Foto 3");
            }
          });
        });

         $("#p4_4").on("change", function(e){ 
          var file = this.files[0];
          var form = new FormData();
          form.append('image', file);
          $.ajax({
            //url : "https://api.yepsindonesia.com/vendors/cloudinary",
            url : "<?= $this->config->item('api_http'); ?>/vendors/cloudinary",
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            data : form,
            success: function(response){

              data = JSON.parse(response);
              $("#p4_4_hidden").val(data.public_id);
              console.log("Foto 4");
            }
          });
        });


       });
});
</script>
