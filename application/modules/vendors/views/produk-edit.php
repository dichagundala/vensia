<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>
<!-- Load Leaflet from CDN-->
<link rel="stylesheet" href="<?php echo base_url('assets/leaflet/leaflet.css'); ?>" />
<!-- <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script> -->
<script type="text/javascript" src="<?php echo base_url('assets/leaflet/leaflet.js'); ?>"></script>

<!-- Load Esri Leaflet from CDN -->
<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet/1.0.0-rc.2/esri-leaflet.js"></script>

<style>
body {margin:0;padding:0;}
#map {position: relative;top:0;bottom:0;right:0;left:0;
  height: 250px;}
</style>
<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header sty-one">
    <h1>Tambah Jasa</h1>
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li><i class="fa fa-angle-right"></i> Tambah Jasa</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- <div class="row">
      <div class="col-lg-12">
        <div class="info-box">
          <div class="card tab-style1">
            <div class="tab-content">
              <div class="tab-pane active" id="home" role="tabpanel" aria-expanded="true">
                <div class="card-body">
                  <div class="row"> -->

                    <!-- Nav tabs -->
                    <!-- <div class="col-md-10">
                      <h3 style="padding-top: 10px; font-weight: bold;">Anda Memiliki Paket?</h3></div>
                      <div class="col-md-2"><button class="btn btn-success">Buat Packages</button></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> -->
      <!-- Main row --> 
    </section>
    <!-- /.content --> 
    <!-- Main content -->
    <section class="content" style="margin-top: -250px">
      <div class="row">
        <div class="col-lg-12">
          <div class="info-box">
            <div class="card tab-style1"> 
              <!-- Nav tabs -->

              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane active" id="home" role="tabpanel" aria-expanded="true">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-lg-12 col-xs-6">
                        <!-- <hr style="background-color: #00a65a; height: 2px; "> -->

                        <div class="col-md-12" style="margin-top: 40px">
                          <div class="hr-title hr-long center"><abbr>Data Jasa</abbr> </div>
                        </div>
                        <form class="form-horizontal container" id="form_edit_produk">
                          <input type="hidden" name="uuid_vendor" id="uuid_vendor">
                          <input type="hidden" name="uuid_produk" id="uuid_produk" value="<?php echo $uuid; ?>">
                          <div class="form-group row">
                            <label class="col-sm-3">Pilih Kategori</label>
                            <div class="col-sm-9">
                              <select class="form-control form-control-line" id="kategori_vendor" name="kategori_vendor">
                                <option disabled selected>-- Silahkan Pilih Kategori --</option>
                              </select>
                            </div>
                          </div>
                          <div id="example">

                          </div>
                          <div class="form-group" style="margin-top: 20px;">
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <div class="col-sm-12">
                              <input type="submit" class="btn btn-success" value="Simpan"> <button type="reset" class="btn btn-danger" onclick="window.history.back();">Batal</button>
                            </div>
                          </div>
                        </form>
                      </div>
                      <!-- TUTUP FORM -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Main row --> 
    </section>
    <!-- /.content --> 
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.min.css">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="http://cdn.ckeditor.com/ckeditor5/11.0.1/classic/ckeditor.js"></script>
  <script type="text/javascript">
    $("#uuid_vendor").val('<?php echo $this->session->userdata('uuid'); ?>');
    $.getJSON('<?= $this->config->item('api_http'); ?>/vendors/kategori', function(data) {
      <?php
          if(@$this->session->userdata('cro') == 'true'){

      ?>
          localStorage.setItem('kategori', '<?php echo @$this->session->userdata('kategori'); ?>');
      <?php
          }
      ?>
      var kategori = localStorage.getItem("kategori").split(", ");
      for(var i = 0; i < kategori.length; i++){
        data.forEach(function (arrayItem) {
          if(kategori[i] == arrayItem.nama)
            if(arrayItem.id == <?php echo $id; ?>)
          $("#kategori_vendor").append("<option value='"+arrayItem.id+"' selected>"+arrayItem.nama+"</option>");
          else
           $("#kategori_vendor").append("<option value='"+arrayItem.id+"'>"+arrayItem.nama+"</option>");
       });
      }
    });

    var myEditor;

    $(document).ready(function() {
      $("#example").html("");
      var id = $("#kategori_vendor").val();
      $.getJSON('<?= $this->config->item('api_http'); ?>/vendors/parameter/' + <?php echo $id; ?>, function(data) {
        data.forEach(function(arrayItem) {
          if(arrayItem.parameter_id == 4){
            var template = '<div class="form-group row"><label class="col-md-3">'+ arrayItem.nama +'<span style="color: red">*</span></label><div class="col-md-9 row">'+ arrayItem.jenis +'</div></div>';
            $("#example").append(template); 
          }
          else if(arrayItem.parameter_id != 4 || arrayItem.parameter_id != 13){
            var template = '<div class="form-group row"><label class="col-md-3">'+ arrayItem.nama +'<span style="color: red">*</span></label><div class="col-md-9">'+ arrayItem.jenis +'<span class="help-inline form-control-feedback" aria-hidden="true">'+ arrayItem.help_text+'</span></div></div>';
            $("#example").append(template);  
          }
          else{
            var template = '<input type="hidden" value="" name="value[]"/>';
            $("#example").append(template);
          }

        });

       /* $("#p1").attr('pattern', '[A-Za-z0-9\s]{1,}');
       $("#p1").attr('title', 'Tidak boleh menggunakan spesial character \*:;!?+/@#_|~`={}\\$^%\'"');*/

       $.getJSON('<?= $this->config->item('api_http'); ?>/vendors/kabupaten', function(data) {
        data.forEach(function(arrayItem) {
          var template = '<option value="' + arrayItem.nama + '">' + arrayItem.provinsi + ' - ' + arrayItem.nama + '</option>';
          $("#p12").append(template);
        });
        $("#p12").select2();
      });

       ClassicEditor
       .create(document.querySelector('#p5'), {
        toolbar: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote', 'undo', 'redo'],
        heading: {
          options: [{
            model: 'paragraph',
            title: 'Paragraph',
            class: 'ck-heading_paragraph'
          },
          {
            model: 'heading1',
            view: 'h1',
            title: 'Heading 1',
            class: 'ck-heading_heading1'
          },
          {
            model: 'heading2',
            view: 'h2',
            title: 'Heading 2',
            class: 'ck-heading_heading2'
          }
          ]
        }
      })
       .then(editor => {
        console.log('Editor was initialized', editor);
        myEditor = editor;
      })
       .catch(error => {
        console.log(error);
      });

       $.ajax({
        url: "<?= $this->config->item('api_http'); ?>/vendors/data_produk",
        type: "POST",
        data: {uuid : '<?php echo $uuid; ?>'},
        success: function(res) {
          var res = JSON.parse(res);

          $("#example").append('<div id="map"></div>');
          var map = L.map('map').setView([-6.1761317, 106.8206753], 13);
          var marker;


          ACCESS_TOKEN = 'pk.eyJ1IjoiaGFuc2VuZHVzZW5vdiIsImEiOiJjajNwd2gzODcwMGs4MzNuNDFpczAwY3dmIn0.pq3-o52y_eUHZKNB00ZGCg';

          L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + ACCESS_TOKEN, {
            attribution: 'Imagery &copy <a href="http://mapbox.com">Mapbox</a>  | Custom by <a href="https://hansendusenov.web.id" target="blank">Hansen Dusenov</a>',
            id: 'mapbox.streets'
          }).addTo(map);
      
      //$("#example").append('<div class="form-group row"><label class="col-sm-3">Nama Additional Produk</label><div class="col-sm-9"><div class="field_wrapper"><input type="text" class="form-control form-control-line" name="field_name[]" id="field_name[]" placeholder="nama" value=""/><input type="text" class="form-control form-control-line" name="field_value[]" id="field_value[]" placeholder="jumlah barang" value=""/><input type="text" class="form-control form-control-line" name="field_price[]" id="field_price[]" placeholder="harga barang" value=""/><a href="javascript:void(0);" class="add_button" title="Add field"><i class="fa fa-plus"></i></a></div></div></div>');
      //untuk cek data additional
     $("#example").append('<div class="col-md-12" style="margin-top: 40px"><div class="hr-title hr-long center"><abbr>Additional Service</abbr></div></div><div class="field_wrapper"><div class="col-sm-9"><a href="javascript:void(0);" class="btn btn-primary btn-sm chk_button"style="display: none;">Cek Item</a></div> </br></br>');
      

      var maxField = 100; //Input fields increment limitation
      //var chkButton = $('.chk_button'); //chk button selector
      var wrapper = $('.field_wrapper'); //Input field wrapper
      var x = 1; //Initial field counter is 1
      //vdHTML = 
      //Once add button is clicked
      //$(chkButton).click(function(){
        $.ajax({
                url : "<?php echo base_url();?>index.php/Shop/get_sub_add_qty_byuuid",
                method : "POST",
                data: {uuid : '<?php echo $uuid; ?>'},
                async : false,
                dataType : 'json',
                success: function(data){
                    var i;
                  for(i=0; i<data.length; i++){
			              $(wrapper).append('<div class="depan"><div class="form-group row"><label class="col-sm-3">Nama Item</label><input type="hidden" name="field_id[]" id="field_id[]" value="'+data[i].id+'"/><div class="col-sm-9"><input type="text" class="form-control" name="field_name_add[]" id="field_name_add[]" value="'+data[i].nama+'"/></div></div><div class="form-group row"><label class="col-sm-3">Harga</label><div class="col-sm-9"><input type="number" class="form-control" name="field_price_add[]" id="field_price_add[]" value="'+data[i].harga+'"/><span class="help-inline form-control-feedback" aria-hidden="true">Rupiah</span></div></div><div class="form-group row"><label class="col-sm-3">Minimal Pesanan</label><div class="col-sm-9"><input type="text" class="form-control" name="field_value_add[]" id="field_value_add[]" value="'+data[i].qty+'"/></div></div><div class="form-group row"><label class="col-sm-3"></label><div class="col-sm-9"><a href="javascript:void(0);" class="btn btn-danger btn-sm remove_button_add_produk" id="'+data[i].id+'">Hapus Item</a></div></div></br></br>'); 
                }
              }
            }); 
     // });

        // Delete 
        //Once remove button is clicked
      $(wrapper).on('click', '.remove_button_add_produk', function(e){
          e.preventDefault();
          var job=confirm("Are you sure to delete permanently?");
          if(job==true)
          {
            $(this).parents('.depan').remove(); //Remove field html
              x--; //Decrement field counter
            // Delete id
            var deleteid = this.id;
            $.ajax({
              url: "<?= $this->config->item('api_http'); ?>/vendors/delete_add_produk",
              type: "POST",
              data: { id:deleteid },
              success: function(response){

                if(response == 1){
                  
            
                }else{
          // alert('Invalid ID.');
                }

              }
            });
          }
          
      });

      /* untuk additional pertama kali*/ 
      $("#example").append('<div class="form-group row"><label class="col-sm-3">Nama Item</label><div class="col-sm-9"><input type="text" class="form-control" name="field_name[]" id="field_name[]" placeholder="Masukkan Nama Item" value=""/></div></div><div class="form-group row"><label class="col-sm-3">Harga</label><div class="col-sm-9"><input type="number" class="form-control" name="field_price[]" id="field_price[]" placeholder="Masukkan Harga Service" value=""/><span class="help-inline form-control-feedback" aria-hidden="true">Rupiah</span></div></div><div class="form-group row"><label class="col-sm-3">Minimal Pesanan</label><div class="col-sm-9"><input type="text" class="form-control" name="field_value[]" id="field_value[]" placeholder="Masukkan Minimal Pesanan" value=""/></div></div><div class="form-group row"><label class="col-sm-3"></label><div class="col-sm-9"><a href="javascript:void(0);" class="btn btn-primary btn-sm add_button">Tambah Item</a></div></div></div>');
      /* untuk menambahkan additional dinamis */
      //awal
      var maxField = 100; //Input fields increment limitation
      var addButton = $('.add_button'); //Add button selector
      var wrapper = $('.field_wrapper'); //Input field wrapper
      var x = 1; //Initial field counter is 1
      //vdHTML = 
      //Once add button is clicked
      $(addButton).click(function(){
          //Check maximum number of input fields
          if(x < maxField){ 
              x++; //Increment field counter
              //$(wrapper).append('<div><input type="text" name="field_name[]" id="field_name[]" placeholder="nama" value=""/><input type="text" name="field_value[]" id="field_value[]" placeholder="jumlah barang" value=""/><input type="text" name="field_price[]" id="field_price[]" placeholder="harga barang" value=""/><a href="javascript:void(0);" class="remove_button"><i class="fa fa-minus"></i></a></div>'); //Add field html
              $(wrapper).append('<div class="depan"><div class="form-group row"><label class="col-sm-3">Nama Item</label><div class="col-sm-9"><input type="text" class="form-control" name="field_name[]" id="field_name[]" placeholder="Masukkan Nama Item" value=""/></div></div><div class="form-group row"><label class="col-sm-3">Harga</label><div class="col-sm-9"><input type="number" class="form-control" name="field_price[]" id="field_price[]" placeholder="Masukkan Harga Service" value=""/><span class="help-inline form-control-feedback" aria-hidden="true">Rupiah</span></div></div><div class="form-group row"><label class="col-sm-3">Minimal Pesanan</label><div class="col-sm-9"><input type="text" class="form-control" name="field_value[]" id="field_value[]" placeholder="Masukkan Minimal Pesanan" value=""/></div></div><div class="form-group row"><label class="col-sm-3"></label><div class="col-sm-9"><a href="javascript:void(0);" class="btn btn-primary btn-sm add_button2">Tambah Item</a><a href="javascript:void(0);" class="btn btn-danger btn-sm remove_button">Hapus Item</a></div></div>'); //Add field html
          }
      });
      
      //Once remove button is clicked
      $(wrapper).on('click', '.remove_button', function(e){
          e.preventDefault();
          $(this).parents('.depan').remove(); //Remove field html
          x--; //Decrement field counter
      });
      $(wrapper).on('click', '.add_button2', function(e){
          e.preventDefault();
          if(x < maxField){ 
              x++; //Increment field counter
              //$(wrapper).append('<div><input type="text" name="field_name[]" id="field_name[]" placeholder="nama" value=""/><input type="text" name="field_value[]" id="field_value[]" placeholder="jumlah barang" value=""/><input type="text" name="field_price[]" id="field_price[]" placeholder="harga barang" value=""/><a href="javascript:void(0);" class="remove_button"><i class="fa fa-minus"></i></a></div>'); //Add field html
              $(wrapper).append('<div class="depan"><div class="form-group row"><label class="col-sm-3">Nama Item</label><div class="col-sm-9"><input type="text" class="form-control" name="field_name[]" id="field_name[]" placeholder="Masukkan Nama Item" value=""/></div></div><div class="form-group row"><label class="col-sm-3">Harga</label><div class="col-sm-9"><input type="number" class="form-control" name="field_price[]" id="field_price[]" placeholder="Masukkan Harga Service" value=""/><span class="help-inline form-control-feedback" aria-hidden="true">Rupiah</span></div></div><div class="form-group row"><label class="col-sm-3">Minimal Pesanan</label><div class="col-sm-9"><input type="text" class="form-control" name="field_value[]" id="field_value[]" placeholder="Masukkan Minimal Pesanan" value=""/></div></div><div class="form-group row"><label class="col-sm-3"></label><div class="col-sm-9"><a href="javascript:void(0);" class="btn btn-primary btn-sm add_button2">Tambah Item</a><a href="javascript:void(0);" class="btn btn-danger btn-sm remove_button">Hapus Item</a></div></div>'); //Add field html
          }
          //$(this).parents('.depan').remove(); //Remove field html
          //x--; //Decrement field counter
      });
      //akhirar fiel
          var searchControl = new L.esri.Controls.Geosearch().addTo(map);

          var results = new L.LayerGroup().addTo(map);

          searchControl.addEventListener('keypress', function(e) {
            var key = e.which || e.keyCode;
            if (key === 13) {

              alert('gk boleh pencet enter');
            }
          });

          searchControl.on('results', function(data) {
            if (marker) {
              map.removeLayer(marker);
            }
            results.clearLayers();

            results.addLayer(L.marker(data.results[data.results.length - 1].latlng));
            $("#p58").val(data.results[data.results.length - 1].latlng.lat + ',' + data.results[data.results.length - 1].latlng.lng);
            $("#p58_hidden").val(data.results[data.results.length - 1].latlng.lat + ',' + data.results[data.results.length - 1].latlng.lng);
            console.log(data.results[data.results.length - 1]);

          });

          map.on('click', function(e) {
            if (marker) {
              map.removeLayer(marker);
            }
            if (results) {
              results.clearLayers();
            }
            marker = new L.marker(e.latlng).addTo(map);
            $("#p58").val(e.latlng.lat + ',' + e.latlng.lng);
            $("#p58_hidden").val(e.latlng.lat + ',' + e.latlng.lng);
            console.log(e);
          });

          var foto1 = '';
          var foto2 = '';
          var foto3 = '';
          var foto4 = '';
          var coordinate = '';
          for (var i = 0; i < res.data.produk.length; i++) {
            console.log(res.data.produk[i].id + ' ' + res.data.produk[i].value);
            if (res.data.produk[i].id == "4") {
              //console.log('https://res.cloudinary.com/yepsindo/image/upload/v1536728466/' + res.data.produk[i].value + '.jpg');
              foto_array = res.data.produk[i].value.split(";");
              foto1 = foto_array[0] == '' ? '' : 'https://res.cloudinary.com/yepsindo/image/upload/v1536728466/' + foto_array[0] + '.jpg';
              foto2 = foto_array[1] == '' ? '' : 'https://res.cloudinary.com/yepsindo/image/upload/v1536728466/' + foto_array[1] + '.jpg';
              foto3 = foto_array[2] == '' ? '' : 'https://res.cloudinary.com/yepsindo/image/upload/v1536728466/' + foto_array[2] + '.jpg';
              foto4 = foto_array[3] == '' ? '' : 'https://res.cloudinary.com/yepsindo/image/upload/v1536728466/' + foto_array[3] + '.jpg';
              console.log(foto1);
              console.log(foto2);
              console.log(foto3);
              console.log(foto4);
              $("#p4_1").removeAttr("required");
              $("#p4_1_hidden").val(foto_array[0]);
              $("#p4_2").removeAttr("required");
              $("#p4_2_hidden").val(foto_array[1]);
              $("#p4_3").removeAttr("required");
              $("#p4_3_hidden").val(foto_array[2]);
              $("#p4_4").removeAttr("required");
              $("#p4_4_hidden").val(foto_array[3]);
            } else if (res.data.produk[i].id == "5") {
              myEditor.setData(res.data.produk[i].value);
            } else if (res.data.produk[i].id == "12") {
              $("#p" + res.data.produk[i].id).val(res.data.produk[i].value).trigger('change');
            } else if (res.data.produk[i].id == "58") {
              coordinate = res.data.produk[i].value.split(",");
              $("#p58").val(res.data.produk[i].value);
              $("#p58_hidden").val(res.data.produk[i].value);
              var latlng = {
                lat: coordinate[0],
                lng: coordinate[1]
              };
              marker = new L.marker(latlng).addTo(map);
            } else {
              $("#p" + res.data.produk[i].id).val(res.data.produk[i].value);
            }

          }
          $("#field_name").val('a');
          var drEvent1 = $('#p4_1.dropify-event').dropify({
            defaultFile: foto1,
            messages: {
              'default': 'Drag and drop a file here or click',
              'replace': 'Drag and drop or click to replace',
              'remove': 'Remove',
              'error': 'Ooops, something wrong happended.'
            },
            error: {
              'fileSize': 'The file size is too big ({{ value }} max).',
              'minWidth': 'The image width is too small ({{ value }}}px min).',
              'maxWidth': 'The image width is too big ({{ value }}}px max).',
              'minHeight': 'The image height is too small ({{ value }}}px min).',
              'maxHeight': 'The image height is too big ({{ value }}px max).',
              'imageFormat': 'The image format is not allowed ({{ value }} only).'
            }
          });

          var drEvent2 = $('#p4_2.dropify-event').dropify({
            defaultFile: foto2,
            messages: {
              'default': 'Drag and drop a file here or click',
              'replace': 'Drag and drop or click to replace',
              'remove': 'Remove',
              'error': 'Ooops, something wrong happended.'
            },
            error: {
              'fileSize': 'The file size is too big ({{ value }} max).',
              'minWidth': 'The image width is too small ({{ value }}}px min).',
              'maxWidth': 'The image width is too big ({{ value }}}px max).',
              'minHeight': 'The image height is too small ({{ value }}}px min).',
              'maxHeight': 'The image height is too big ({{ value }}px max).',
              'imageFormat': 'The image format is not allowed ({{ value }} only).'
            }
          });

          var drEvent3 = $('#p4_3.dropify-event').dropify({
            defaultFile: foto3,
            messages: {
              'default': 'Drag and drop a file here or click',
              'replace': 'Drag and drop or click to replace',
              'remove': 'Remove',
              'error': 'Ooops, something wrong happended.'
            },
            error: {
              'fileSize': 'The file size is too big ({{ value }} max).',
              'minWidth': 'The image width is too small ({{ value }}}px min).',
              'maxWidth': 'The image width is too big ({{ value }}}px max).',
              'minHeight': 'The image height is too small ({{ value }}}px min).',
              'maxHeight': 'The image height is too big ({{ value }}px max).',
              'imageFormat': 'The image format is not allowed ({{ value }} only).'
            }
          });

          var drEvent4 = $('#p4_4.dropify-event').dropify({
            defaultFile: foto4,
            messages: {
              'default': 'Drag and drop a file here or click',
              'replace': 'Drag and drop or click to replace',
              'remove': 'Remove',
              'error': 'Ooops, something wrong happended.'
            },
            error: {
              'fileSize': 'The file size is too big ({{ value }} max).',
              'minWidth': 'The image width is too small ({{ value }}}px min).',
              'maxWidth': 'The image width is too big ({{ value }}}px max).',
              'minHeight': 'The image height is too small ({{ value }}}px min).',
              'maxHeight': 'The image height is too big ({{ value }}px max).',
              'imageFormat': 'The image format is not allowed ({{ value }} only).'
            }
          });
          
          drEvent1.on('dropify.beforeClear', function(event, element){
            return true;
          });

          drEvent1.on('dropify.afterClear', function(event, element){
            $("#p4_1_hidden").val('');
          });

          drEvent1.on('dropify.errors', function(event, element){
            console.log('Has Errors');
          });


          drEvent2.on('dropify.beforeClear', function(event, element){
            return true;
          });

          drEvent2.on('dropify.afterClear', function(event, element){
            $("#p4_2_hidden").val('');
          });

          drEvent2.on('dropify.errors', function(event, element){
            console.log('Has Errors');
          });

          drEvent3.on('dropify.beforeClear', function(event, element){
            return true;
          });

          drEvent3.on('dropify.afterClear', function(event, element){
            $("#p4_3_hidden").val('');
          });

          drEvent3.on('dropify.errors', function(event, element){
            console.log('Has Errors');
          });

          drEvent4.on('dropify.beforeClear', function(event, element){
            return true;
          });

          drEvent4.on('dropify.afterClear', function(event, element){
            $("#p4_4_hidden").val('');
          });

          drEvent4.on('dropify.errors', function(event, element){
            console.log('Has Errors');
          });

          $("#p4_1").on("change", function(e){ 
            var file = this.files[0];
            var form = new FormData();
            form.append('image', file);
            $.ajax({
              url : "<?= $this->config->item('api_http'); ?>/vendors/cloudinary",
              type: "POST",
              cache: false,
              contentType: false,
              processData: false,
              data : form,
              success: function(response){

                data = JSON.parse(response);
                $("#p4_1_hidden").val(data.public_id);
                console.log("Foto 1");
              }
            });
          });

          $("#p4_2").on("change", function(e){ 
            var file = this.files[0];
            var form = new FormData();
            form.append('image', file);
            $.ajax({
              url : "<?= $this->config->item('api_http'); ?>/vendors/cloudinary",
              type: "POST",
              cache: false,
              contentType: false,
              processData: false,
              data : form,
              success: function(response){

                data = JSON.parse(response);
                $("#p4_2_hidden").val(data.public_id);
                console.log("Foto 2");
              }
            });
          });

          $("#p4_3").on("change", function(e){ 
            var file = this.files[0];
            var form = new FormData();
            form.append('image', file);
            $.ajax({
              url : "<?= $this->config->item('api_http'); ?>/vendors/cloudinary",
              type: "POST",
              cache: false,
              contentType: false,
              processData: false,
              data : form,
              success: function(response){

                data = JSON.parse(response);
                $("#p4_3_hidden").val(data.public_id);
                console.log("Foto 3");
              }
            });
          });

          $("#p4_4").on("change", function(e){ 
            var file = this.files[0];
            var form = new FormData();
            form.append('image', file);
            $.ajax({
              url : "<?= $this->config->item('api_http'); ?>/vendors/cloudinary",
              type: "POST",
              cache: false,
              contentType: false,
              processData: false,
              data : form,
              success: function(response){

                data = JSON.parse(response);
                $("#p4_4_hidden").val(data.public_id);
                console.log("Foto 4");
              }
            });
          });

        }
      });
});
});

$("#kategori_vendor").change(function() {
  $("#example").html("");
  var id = $("#kategori_vendor").val();
  $.getJSON('<?= $this->config->item('api_http'); ?>/vendors/parameter/' + id, function(data) {
    data.forEach(function(arrayItem) {
      if(arrayItem.parameter_id == 4){
        var template = '<div class="form-group row"><label class="col-md-3">'+ arrayItem.nama +'<span style="color: red">*</span></label><div class="col-md-9 row">'+ arrayItem.jenis +'</div></div>';
        $("#example").append(template); 
      }
      else if(arrayItem.parameter_id != 4 || arrayItem.parameter_id != 13){
        var template = '<div class="form-group row"><label class="col-md-3">'+ arrayItem.nama +'<span style="color: red">*</span></label><div class="col-md-9">'+ arrayItem.jenis +'<span class="help-inline form-control-feedback" aria-hidden="true">'+ arrayItem.help_text+'</span></div></div>';
        $("#example").append(template);  
      }
      else{
        var template = '<input type="hidden" value="" name="value[]"/>';
        $("#example").append(template);
      }

    });

    $.getJSON('<?= $this->config->item('api_http'); ?>/vendors/kabupaten', function(data) {
      data.forEach(function(arrayItem) {
        var template = '<option value="' + arrayItem.nama + '">' + arrayItem.provinsi + ' - ' + arrayItem.nama + '</option>';
        $("#p12").append(template);
      });
      $("#p12").select2();
    });




    ClassicEditor
    .create(document.querySelector('#p5'), {
      toolbar: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote', 'undo', 'redo'],
      heading: {
        options: [{
          model: 'paragraph',
          title: 'Paragraph',
          class: 'ck-heading_paragraph'
        },
        {
          model: 'heading1',
          view: 'h1',
          title: 'Heading 1',
          class: 'ck-heading_heading1'
        },
        {
          model: 'heading2',
          view: 'h2',
          title: 'Heading 2',
          class: 'ck-heading_heading2'
        }
        ]
      }
    })
    .then(editor => {
      console.log('Editor was initialized', editor);
      myEditor = editor;
    })
    .catch(error => {
      console.log(error);
    });

    $.ajax({
      url: "<?= $this->config->item('api_http'); ?>/vendors/data_produk",
      type: "POST",
      data: {uuid : '<?php echo $uuid; ?>'},
      success: function(res) {
        var res = JSON.parse(res);

        $("#example").append('<div id="map"></div>');
        var map = L.map('map').setView([-6.1761317, 106.8206753], 13);
        var marker;


        ACCESS_TOKEN = 'pk.eyJ1IjoiaGFuc2VuZHVzZW5vdiIsImEiOiJjajNwd2gzODcwMGs4MzNuNDFpczAwY3dmIn0.pq3-o52y_eUHZKNB00ZGCg';

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + ACCESS_TOKEN, {
          attribution: 'Imagery &copy <a href="http://mapbox.com">Mapbox</a>  | Custom by <a href="https://hansendusenov.web.id" target="blank">Hansen Dusenov</a>',
          id: 'mapbox.streets'
        }).addTo(map);
        
        var searchControl = new L.esri.Controls.Geosearch().addTo(map);

        var results = new L.LayerGroup().addTo(map);

        searchControl.addEventListener('keypress', function(e) {
          var key = e.which || e.keyCode;
          if (key === 13) {

            alert('gk boleh pencet enter');
          }
        });

        searchControl.on('results', function(data) {
          if (marker) {
            map.removeLayer(marker);
          }
          results.clearLayers();

          results.addLayer(L.marker(data.results[data.results.length - 1].latlng));
          $("#p58").val(data.results[data.results.length - 1].latlng.lat + ',' + data.results[data.results.length - 1].latlng.lng);
          $("#p58_hidden").val(data.results[data.results.length - 1].latlng.lat + ',' + data.results[data.results.length - 1].latlng.lng);
          console.log(data.results[data.results.length - 1]);

        });

        map.on('click', function(e) {
          if (marker) {
            map.removeLayer(marker);
          }
          if (results) {
            results.clearLayers();
          }
          marker = new L.marker(e.latlng).addTo(map);
          $("#p58").val(e.latlng.lat + ',' + e.latlng.lng);
          $("#p58_hidden").val(e.latlng.lat + ',' + e.latlng.lng);
          console.log(e);
        });

        var foto1 = '';
        var foto2 = '';
        var foto3 = '';
        var foto4 = '';
        var coordinate = '';
        for (var i = 0; i < res.data.produk.length; i++) {
          console.log(res.data.produk[i].id + ' ' + res.data.produk[i].value);
          if (res.data.produk[i].id == "4") {
            console.log('https://res.cloudinary.com/yepsindo/image/upload/v1536728466/' + res.data.produk[i].value + '.jpg');
            foto_array = res.data.produk[i].value.split(";");
            foto1 = foto_array[0] == '' ? '' : 'https://res.cloudinary.com/yepsindo/image/upload/v1536728466/' + foto_array[0] + '.jpg';
            foto2 = foto_array[1] == '' ? '' : 'https://res.cloudinary.com/yepsindo/image/upload/v1536728466/' + foto_array[1] + '.jpg';
            foto3 = foto_array[2] == '' ? '' : 'https://res.cloudinary.com/yepsindo/image/upload/v1536728466/' + foto_array[2] + '.jpg';
            foto4 = foto_array[3] == '' ? '' : 'https://res.cloudinary.com/yepsindo/image/upload/v1536728466/' + foto_array[3] + '.jpg';
            $("#p4_1").removeAttr("required");
            $("#p4_1_hidden").val(foto_array[0]);
            $("#p4_2").removeAttr("required");
            $("#p4_2_hidden").val(foto_array[1]);
            $("#p4_3").removeAttr("required");
            $("#p4_3_hidden").val(foto_array[2]);
            $("#p4_4").removeAttr("required");
            $("#p4_4_hidden").val(foto_array[3]);
          } else if (res.data.produk[i].id == "5") {
            myEditor.setData(res.data.produk[i].value);
          } else if (res.data.produk[i].id == "12") {
            $("#p" + res.data.produk[i].id).val(res.data.produk[i].value).trigger('change');
          } else if (res.data.produk[i].id == "58") {
            coordinate = res.data.produk[i].value.split(",");
            $("#p58").val(res.data.produk[i].value);
            $("#p58_hidden").val(res.data.produk[i].value);
            var latlng = {
              lat: coordinate[0],
              lng: coordinate[1]
            };
            marker = new L.marker(latlng).addTo(map);
          } else {
            $("#p" + res.data.produk[i].id).val(res.data.produk[i].value);
          }

        }
        var drEvent1 = $('#p4_1.dropify-event').dropify({
          defaultFile: foto1,
          messages: {
            'default': 'Drag and drop a file here or click',
            'replace': 'Drag and drop or click to replace',
            'remove': 'Remove',
            'error': 'Ooops, something wrong happended.'
          },
          error: {
            'fileSize': 'The file size is too big ({{ value }} max).',
            'minWidth': 'The image width is too small ({{ value }}}px min).',
            'maxWidth': 'The image width is too big ({{ value }}}px max).',
            'minHeight': 'The image height is too small ({{ value }}}px min).',
            'maxHeight': 'The image height is too big ({{ value }}px max).',
            'imageFormat': 'The image format is not allowed ({{ value }} only).'
          }
        });

        var drEvent2 = $('#p4_2.dropify-event').dropify({
          defaultFile: foto2,
          messages: {
            'default': 'Drag and drop a file here or click',
            'replace': 'Drag and drop or click to replace',
            'remove': 'Remove',
            'error': 'Ooops, something wrong happended.'
          },
          error: {
            'fileSize': 'The file size is too big ({{ value }} max).',
            'minWidth': 'The image width is too small ({{ value }}}px min).',
            'maxWidth': 'The image width is too big ({{ value }}}px max).',
            'minHeight': 'The image height is too small ({{ value }}}px min).',
            'maxHeight': 'The image height is too big ({{ value }}px max).',
            'imageFormat': 'The image format is not allowed ({{ value }} only).'
          }
        });

        var drEvent3 = $('#p4_3.dropify-event').dropify({
          defaultFile: foto3,
          messages: {
            'default': 'Drag and drop a file here or click',
            'replace': 'Drag and drop or click to replace',
            'remove': 'Remove',
            'error': 'Ooops, something wrong happended.'
          },
          error: {
            'fileSize': 'The file size is too big ({{ value }} max).',
            'minWidth': 'The image width is too small ({{ value }}}px min).',
            'maxWidth': 'The image width is too big ({{ value }}}px max).',
            'minHeight': 'The image height is too small ({{ value }}}px min).',
            'maxHeight': 'The image height is too big ({{ value }}px max).',
            'imageFormat': 'The image format is not allowed ({{ value }} only).'
          }
        });

        var drEvent4 = $('#p4_4.dropify-event').dropify({
          defaultFile: foto4,
          messages: {
            'default': 'Drag and drop a file here or click',
            'replace': 'Drag and drop or click to replace',
            'remove': 'Remove',
            'error': 'Ooops, something wrong happended.'
          },
          error: {
            'fileSize': 'The file size is too big ({{ value }} max).',
            'minWidth': 'The image width is too small ({{ value }}}px min).',
            'maxWidth': 'The image width is too big ({{ value }}}px max).',
            'minHeight': 'The image height is too small ({{ value }}}px min).',
            'maxHeight': 'The image height is too big ({{ value }}px max).',
            'imageFormat': 'The image format is not allowed ({{ value }} only).'
          }
        });

        drEvent1.on('dropify.beforeClear', function(event, element){
          return true;
        });

        drEvent1.on('dropify.afterClear', function(event, element){
          $("#p4_1_hidden").val('');
        });

        drEvent1.on('dropify.errors', function(event, element){
          console.log('Has Errors');
        });


        drEvent2.on('dropify.beforeClear', function(event, element){
          return true;
        });

        drEvent2.on('dropify.afterClear', function(event, element){
          $("#p4_2_hidden").val('');
        });

        drEvent2.on('dropify.errors', function(event, element){
          console.log('Has Errors');
        });

        drEvent3.on('dropify.beforeClear', function(event, element){
          return true;
        });

        drEvent3.on('dropify.afterClear', function(event, element){
          $("#p4_3_hidden").val('');
        });

        drEvent3.on('dropify.errors', function(event, element){
          console.log('Has Errors');
        });

        drEvent4.on('dropify.beforeClear', function(event, element){
          return true;
        });

        drEvent4.on('dropify.afterClear', function(event, element){
          $("#p4_4_hidden").val('');
        });

        drEvent4.on('dropify.errors', function(event, element){
          console.log('Has Errors');
        });

        $("#p4_1").on("change", function(e){ 
          var file = this.files[0];
          var form = new FormData();
          form.append('image', file);
          $.ajax({
            url : "<?= $this->config->item('api_http'); ?>/vendors/cloudinary",
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            data : form,
            success: function(response){

              data = JSON.parse(response);
              $("#p4_1_hidden").val(data.public_id);
              console.log("Foto 1");
            }
          });
        });

        $("#p4_2").on("change", function(e){ 
          var file = this.files[0];
          var form = new FormData();
          form.append('image', file);
          $.ajax({
            url : "<?= $this->config->item('api_http'); ?>/vendors/cloudinary",
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            data : form,
            success: function(response){

              data = JSON.parse(response);
              $("#p4_2_hidden").val(data.public_id);
              console.log("Foto 2");
            }
          });
        });

        $("#p4_3").on("change", function(e){ 
          var file = this.files[0];
          var form = new FormData();
          form.append('image', file);
          $.ajax({
            url : "<?= $this->config->item('api_http'); ?>/vendors/cloudinary",
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            data : form,
            success: function(response){

              data = JSON.parse(response);
              $("#p4_3_hidden").val(data.public_id);
              console.log("Foto 3");
            }
          });
        });

        $("#p4_4").on("change", function(e){ 
          var file = this.files[0];
          var form = new FormData();
          form.append('image', file);
          $.ajax({
            url : "<?= $this->config->item('api_http'); ?>/vendors/cloudinary",
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            data : form,
            success: function(response){

              data = JSON.parse(response);
              $("#p4_4_hidden").val(data.public_id);
              console.log("Foto 4");
            }
          });
        });

      }
    });
});
});

$("#form_edit_produk").submit(function(e){
  e.preventDefault();
  $("#p5").html(myEditor.getData());
  var data_produk = $("#form_edit_produk").serializeArray();
  var foto = [data_produk[6].value, data_produk[7].value, data_produk[8].value, data_produk[9].value];
  var foto_join = foto.join(';');
  data_produk.splice(7,3);
  data_produk[6].value = foto_join;
  var serialize = '';
  for (var i = 0, l = data_produk.length; i < l; i++) {
    serialize += data_produk[i].name + '=' + data_produk[i].value;
    if(i < l - 1){
      serialize += '&';
    }
  }
  $.post("<?= $this->config->item('api_http'); ?>/vendors/vendor_edit_produk", serialize, function(data, status) {
    console.log(data);
    data = JSON.parse(data);
    $("#field_id").val();
    $("#field_name_add").val();
    $("#field_value_add").val();
    $("#field_price_add").val();
    $("#field_name").val();
    $("#field_value").val();
    $("#field_price").val();
    if(data.status == "success"){
            //swal("Berhasil!", data.message, "success");
            
            swal({
              title: "Berhasil!",
              text: data.message,
              type: "success"
            }).then((result) => {
              window.location = '<?php echo base_url('vendors/produk');?>';
            });
          }
          else{
            swal("Gagal!", data.message, "error");
          }
        });
});
</script>
