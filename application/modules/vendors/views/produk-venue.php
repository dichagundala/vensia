<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>
<!-- Load Leaflet from CDN-->
<link rel="stylesheet" href="<?php echo base_url('assets/leaflet/leaflet.css'); ?>" />
<!-- <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script> -->
<script type="text/javascript" src="<?php echo base_url('assets/leaflet/leaflet.js'); ?>"></script>

<!-- Load Esri Leaflet from CDN -->
<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet/1.0.0-rc.2/esri-leaflet.js"></script>

<style>
body {margin:0;padding:0;}
#map {position: relative;top:0;bottom:0;right:0;left:0;
  height: 250px;}
</style>
<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header sty-one">
    <h1>Tambah Jasa</h1>
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li><i class="fa fa-angle-right"></i> Tambah Jasa</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-lg-12">
        <div class="info-box">
          <div class="card tab-style1">
            <div class="tab-content">
              <div class="tab-pane active" id="home" role="tabpanel" aria-expanded="true">
                <div class="card-body">
                  <div class="row">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Main row --> 
    </section>
    <!-- /.content --> 
    <!-- Main content -->
    <section class="content" style="margin-top: -100px">
      <div class="row">
        <div class="col-lg-12">
          <div class="info-box">
            <div class="card tab-style1"> 
              <!-- Nav tabs -->

              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane active" id="home" role="tabpanel" aria-expanded="true">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-lg-12 col-xs-6">
                        <!-- <hr style="background-color: #00a65a; height: 2px; "> -->

                        <div class="col-md-12" style="margin-top: 40px">
                          <div class="hr-title hr-long center"><abbr>Data Jasa</abbr> </div>
                        </div>
                        <form class="form-horizontal container" id="form_tambah_produk">
                          <input type="hidden" name="uuid_vendor" id="uuid_vendor">
                          <div class="form-group row">
                            <label class="col-sm-3">Kategori</label>
                            <div class="col-sm-9">
                             <h4>Venue</h4>
                           </div>
                         </div>
                         <div id="example">
                          <div class="form-group row">
                           <label class="col-md-3">Nama<span style="color: red">*</span></label>
                           <div class="col-md-9"><input placeholder="Masukkan nama produk" type="text" class="form-control" name="value[]" required />
                            <span class="help-inline form-control-feedback" aria-hidden="true"></span>
                          </div>
                        </div>
                        <div class="form-group row">
                         <label class="col-md-3">Harga Normal<span style="color: red">*</span></label>
                         <div class="col-md-9"><input placeholder="Masukkan harga produk" type="number" class="form-control" name="value[]" required /><span class="help-inline form-control-feedback" aria-hidden="true">Rupiah</span></div>
                       </div>
                       <div class="form-group row">
                         <label class="col-md-3">Harga Promosi<span style="color: red">*</span></label>
                         <div class="col-md-9"><input placeholder="Masukkan diskon produk" type="number" value="0" class="form-control" name="value[]" required /><span class="help-inline form-control-feedback" aria-hidden="true">Rupiah</span></div>
                       </div>
                       <div class="form-group row">
                         <label class="col-md-3">Foto<span style="color: red">*</span></label>
                         <div class="col-md-9"><input type="file"  id="input-file-max-fs" data-max-file-size="2M" class="dropify" required />
                          <input type="hidden" id="foto" name="value[]"><span class="help-inline form-control-feedback" aria-hidden="true"></span>
                        </div>
                      </div>
                      <div class="form-group row">
                       <label class="col-md-3">Fasilitas<span style="color: red">*</span></label>
                       <div class="col-md-9"><textarea id="fasilitas" placeholder="Masukkan fasilitas produk" class="form-control textjs" name="value[]" rows="50" required> </textarea><span class="help-inline form-control-feedback" aria-hidden="true"></span></div>
                     </div>
                     <div class="form-group row">
                       <label class="col-md-3">Kapasitas Orang<span style="color: red">*</span></label>
                       <div class="col-md-9"><input type="number" class="form-control" name="value[]" required /><span class="help-inline form-control-feedback" aria-hidden="true">Orang</span></div>
                     </div>
                     <div class="form-group row">
                       <label class="col-md-3">Toilet<span style="color: red">*</span></label>
                       <div class="col-md-9">
                        <select class="form-control" required name="value[]">
                         <option>--Silahkan Pilih--</option>
                         <option value="ada">Ada</option>
                         <option value="tidak">Tidak</option>
                       </select>
                       <span class="help-inline form-control-feedback" aria-hidden="true"></span>
                     </div>
                   </div>
                   <div class="form-group row">
                     <label class="col-md-3">Lahan Parkir<span style="color: red">*</span></label>
                     <div class="col-md-9">
                      <select class="form-control" required name="value[]">
                       <option>--Silahkan Pilih--</option>
                       <option value="ada">Ada</option>
                       <option value="tidak">Tidak</option>
                     </select>
                     <span class="help-inline form-control-feedback" aria-hidden="true"></span>
                   </div>
                 </div>
                 <div class="form-group row">
                   <label class="col-md-3">Luas Area<span style="color: red">*</span></label>
                   <div class="col-md-9"><input type="number" class="form-control" min="0" name="value[]" required /><span class="help-inline form-control-feedback" aria-hidden="true">m2</span></div>
                 </div>
                 <div class="form-group row">
                   <label class="col-md-3">Deskripsi<span style="color: red">*</span></label>
                   <div class="col-md-9"><textarea placeholder="Masukkan deskripsi produk" class="form-control textjs" row="7" name="value[]" required> </textarea><span class="help-inline form-control-feedback" aria-hidden="true"></span></div>
                 </div>
                 <input type="hidden" value="" name="value[]"/>
                 <div class="form-group row">
                   <label class="col-md-3">Lokasi<span style="color: red">*</span></label>
                   <div class="col-md-9">
                    <select class="form-control" name="value[]" id="lokasi_wilayah" required>
                     <option value="" disabled selected>Pilih lokasi</option>
                   </select>
                   <span class="help-inline form-control-feedback" aria-hidden="true"></span>
                 </div>
               </div>
               <div class="form-group row">
                 <label class="col-md-3">Maps<span style="color: red">*</span></label>
                 <div class="col-md-9"><input id="coordinate" placeholder="Pilih lokasi dari map" type="text" class="form-control disabled" disabled required />
                  <input type="hidden" id="coordinate_hidden" name="value[]"/><span class="help-inline form-control-feedback" aria-hidden="true"></span>
                </div>
              </div>
            </div>
            <div class="form-group" style="margin-top: 20px;">
              <div class="col-sm-12">
                <input type="submit" class="btn btn-success" value="Simpan"> <input type="reset" class="btn btn-danger" value="Batal">
              </div>
            </div>
          </form>
        </div>
        <!-- TUTUP FORM -->
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
<!-- Main row --> 
</section>
<!-- /.content --> 
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('footer'); ?>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/classic/ckeditor.js"></script>
<script type="text/javascript">
  $("#uuid_vendor").val(localStorage.getItem("uuid"));
  /*$.getJSON('<?= $this->config->item('api_http'); ?>/vendors/kategori', function(data) {
    var kategori = localStorage.getItem("kategori").split(", ");
    for(var i = 0; i < kategori.length; i++){
      data.forEach(function (arrayItem) {
        if(kategori[i] == arrayItem.nama)
         $("#kategori_vendor").append("<option value='"+arrayItem.id+"'>"+arrayItem.nama+"</option>");
     });
    }
  });*/

  /* $("#kategori_vendor").change(function(){*/
    //$("#example").html("");
    var id = 1;
    $.getJSON('<?= $this->config->item('api_http'); ?>/vendors/parameter/' + id, function(data) {
      var dataTemplate = '';
      data.forEach(function (arrayItem) {
        if(arrayItem.parameter_id != 13){
          var template = '<div class="form-group row"><label class="col-md-3">'+ arrayItem.nama +'<span style="color: red">*</span></label><div class="col-md-9">'+ arrayItem.jenis +'<span class="help-inline form-control-feedback" aria-hidden="true">'+ arrayItem.help_text+'</span></div></div>';
         // $("#example").append(template);  
       }
       else{
        var template = '<input type="hidden" value="" name="value[]"/>';
         // $("#example").append(template);
       }
       dataTemplate += template; 
     });
      //console.log(dataTemplate);

      $.getJSON('<?= $this->config->item('api_http'); ?>/vendors/kabupaten', function(data) {
        data.forEach(function (arrayItem) {
          var template = '<option value="' + arrayItem.nama + '">' + arrayItem.provinsi + ' - ' + arrayItem.nama + '</option>';  
          $("#lokasi_wilayah").append(template);
        });
        $("#lokasi_wilayah").select2();
      });


      var myEditor;

      ClassicEditor
      .create( document.querySelector( '#fasilitas' ), {
        toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote', 'undo', 'redo' ],
        heading: {
          options: [
          { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
          { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
          { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
          ]
        }
      } )
      .then( editor => {
        console.log( 'Editor was initialized', editor );
        myEditor = editor;
      } )
      .catch( error => {
        console.log( error );
      } );

      $("#form_tambah_produk").submit(function(e){
        $("#fasilitas").html(myEditor.getData());
        $.post("<?= $this->config->item('api_http'); ?>/vendors/vendor_add_produk", $("#form_tambah_produk").serialize(), function(data, status) {
          console.log(data);
          data = JSON.parse(data);

          if(data.status == "success"){
            swal("Berhasil!", data.message, "success");
          }
          else{
            swal("Gagal!", data.message, "error");
          }
        });
      });

      $("#example").append('<div id="map"></div>');
      var map = L.map('map').setView([-6.1761317,106.8206753], 13);
      var marker;


      ACCESS_TOKEN = 'pk.eyJ1IjoiaGFuc2VuZHVzZW5vdiIsImEiOiJjajNwd2gzODcwMGs4MzNuNDFpczAwY3dmIn0.pq3-o52y_eUHZKNB00ZGCg';

      L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + ACCESS_TOKEN, {
        attribution: 'Imagery &copy <a href="http://mapbox.com">Mapbox</a>  | Custom by <a href="https://hansendusenov.web.id" target="blank">Hansen Dusenov</a>',
        id: 'mapbox.streets'
      }).addTo(map); 

      var searchControl = new L.esri.Controls.Geosearch().addTo(map);

      var results = new L.LayerGroup().addTo(map);

      searchControl.addEventListener('keypress', function (e) {
        var key = e.which || e.keyCode;
        if (key === 13) {

          alert('gk boleh pencet enter');
        }
      });

      searchControl.on('results', function(data){
        if (marker) { 
          map.removeLayer(marker); 
        }
        results.clearLayers();

        results.addLayer(L.marker(data.results[data.results.length - 1].latlng));
        $("#coordinate").val(data.results[data.results.length - 1].latlng.lat+','+data.results[data.results.length - 1].latlng.lng);
        $("#coordinate_hidden").val(data.results[data.results.length - 1].latlng.lat+','+data.results[data.results.length - 1].latlng.lng);
        console.log(data.results[data.results.length - 1]);

      });

      map.on('click', function(e){
        if (marker) { 
          map.removeLayer(marker);
        }
        if (results){
          results.clearLayers();
        }
        marker = new L.marker(e.latlng).addTo(map);
        $("#coordinate").val(e.latlng.lat+','+e.latlng.lng);
        $("#coordinate_hidden").val(e.latlng.lat+','+e.latlng.lng);
        console.log(e);
      });

      $('.dropify').dropify();

      $('.dropify-fr').dropify({
        messages: {
          default: 'Glissez-déposez un fichier ici ou cliquez',
          replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
          remove:  'Supprimer',
          error:   'Désolé, le fichier trop volumineux'
        }
      });


      var drEvent = $('#input-file-events').dropify();

      drEvent.on('dropify.beforeClear', function(event, element){
        return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
      });

      drEvent.on('dropify.afterClear', function(event, element){
        alert('File deleted');
      });

      drEvent.on('dropify.errors', function(event, element){
        console.log('Has Errors');
      });

      var drDestroy = $('#input-file-to-destroy').dropify();
      drDestroy = drDestroy.data('dropify')
      $('#toggleDropify').on('click', function(e){
        e.preventDefault();
        if (drDestroy.isDropified()) {
          drDestroy.destroy();
        } else {
          drDestroy.init();
        }
      });

      $("#input-file-max-fs").on("change", function(e){ 
        var file = this.files[0];
        var form = new FormData();
        form.append('image', file);
        $.ajax({
          url : "<?= $this->config->item('api_http'); ?>/vendors/cloudinary",
          type: "POST",
          cache: false,
          contentType: false,
          processData: false,
          data : form,
          success: function(response){

            data = JSON.parse(response);
            $("#foto").val(data.public_id);
          }
        });
      });
    });
/*});*/
</script>
