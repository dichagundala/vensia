<?php $this->load->view('header'); ?>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.min.css">
<?php $this->load->view('sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header sty-one">
    <h1>Profile page</h1>
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li><i class="fa fa-angle-right"></i> Profil</li>&nbsp;
      <li><i class="fa fa-angle-right"></i> Pengaturan Profil</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">

      <div class="col-lg-4" data-step="1" data-intro="Ini adalah ringkasan profil anda" data-position="right">
        <div class="user-profile-box m-b-3">
          <div class="box-profile text-white"> <img id="profil_image" style="height: 120px; width: 120px;" class="profile-user-img img-responsive m-b-2" alt="User profile picture">
            <h3 class="profile-username text-center" id="profil_name"></h3>
            <p class="text-center" id="profil_deskripsi"></p>
          </div>
        </div>
        <div class="card m-b-3">
          <div class="card-body">
            <div class="box-body"> 
              <strong><i class="fas fa-map-marker-alt margin-r-5"></i> Location</strong>
              <a href="" data-toggle="modal" data-target="#ubahlokasi">Ubah</a>
              <p class="text-muted" id="profil_lokasi"></p>
              <hr>
              <strong><i class="fa fa-envelope margin-r-5"></i> Email address </strong>
              <p class="text-muted" id="profil_email"></p>
              <hr>
              <strong><i class="fa fa-phone margin-r-5"></i> Phone</strong>
              <p class="text-muted" id="profil_telp"></p>
              <hr>
              <strong><i class="fa fa-list margin-r-5"></i> Kategori</strong>
              <a href="" data-toggle="modal" data-target="#ubahkategori">Ubah</a>
              <p class="text-muted" id="spesial_kategori_vendor"></p>
              <hr>
              <strong><i class="fa fa-phone margin-r-5"></i> Social Profile</strong>
              <div class="text-left" id="profil_sosial"> 
                <!-- <a class="btn btn-social-icon btn-facebook btn-sm"><i class="fa fa-facebook"></i></a> 
                <a class="btn btn-social-icon btn-instagram btn-sm"><i class="fa fa-instagram"></i></a>
                <a class="btn btn-social-icon btn-twitter btn-sm"><i class="fa fa-twitter"></i></a> --></div> 
              </div>
              <!-- /.box-body --> 
            </div>
          </div>
        </div>

        <div class="col-lg-8">
          <div class="info-box">
            <div class="card tab-style1"> 
              <!-- Nav tabs -->
              <ul class="nav nav-tabs profile-tab" role="tablist">
                <li class="nav-item" id="step-2" data-step="2" data-intro="Tambah rekening bank kamu disini" data-position="right"> <a class="nav-link active" data-toggle="tab" href="#rekening_bank" role="tab" aria-expanded="false">Rekening Bank</a> </li>
                <li class="nav-item" id="step-3" data-step="3" data-intro="Ubah profil? disini tempatnya." data-position="right"> <a class="nav-link" data-toggle="tab" href="#ubah_profil" role="tab">Ubah Profil</a> </li>
                <li class="nav-item" id="step-4" data-step="4" data-intro="Bosen sama password sekarang?, tinggal ubah aja disini" data-position="right"> <a class="nav-link" data-toggle="tab" href="#ubah_password" role="tab">Ubah Password</a> </li>
              </ul>
              <!-- Tab panes -->
              <?php if(@$this->session->userdata('cro') == 'true'){

              }
              else
              {
                ?>
                <div class="tab-content">

                  <div class="tab-pane active" id="rekening_bank" role="tabpanel" aria-expanded="false">
                    <div class="card-body">
                      <div class="container">
                        <div class="row" id="btn_tambah_rekening">
                          <button class="btn btn-success" data-toggle="modal" data-target="#modalContactForm">Tambah Rekening Bank</button>
                        </div>
                      </div>
                      <hr>
                      <div class="table-responsive" id="table_rekening">
                        <table class="table">
                          <thead class="text-white" style="background-color: #1375d1">
                            <tr>
                             <!--  <th scope="col"></th> -->
                             <th scope="col" style="width:20%;">Nama Akun</th>
                             <th scope="col" style="width:20%;">Nomor Rekening</th>
                             <th scope="col" style="width:20%;">Nama Bank</th>
                             <th scope="col" style="width:20%;">Nama Cabang</th>
                             <!-- <th scope="col" style="width:20%;">Aksi</th> -->
                           </tr>
                         </thead>
                         <tbody>
                          <tr>
                            <!-- <th scope="row"><img src="dist/img/bank/bcabank.png"></th> -->
                            <td id="nama_pemilik_akun"></td>
                            <td id="no_rek_akun"></td>
                            <td id="nama_bank_akun"></td>
                            <td id="nama_cabang_akun"></td>
                          <!-- <td>
                            <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>
                            <button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>
                          </td> -->
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="ubah_profil" role="tabpanel" aria-expanded="true">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-12 col-xs-12">
                      <form class="form-horizontal form-material" id="data_profil_vendor">
                        <div class="row">
                          <div class="col-lg-3"></div>
                          <div class="col-lg-6">
                            <div class="box-profile m-b-3" style="background-color: #fafafa; border: 1px solid #c8c8c8">
                              <div class="box-profile"> 
                                <input type="hidden" name="uuid" id="uuid_vendor_profil">
                                <input type="hidden" name="public_id" id="public_id">
                                <input type="hidden" name="url" id="url">
                                <input type="hidden" name="secure_url" id="secure_url">
                                <input type="file" id="input-file-now-custom-1" class="dropify" data-max-file-size="2M"/>
                                <p class="justify" style="font-size: 12px"></p>
                                <p class="justify" style="font-size: 12px">Besar file: maksimum 2 Megabytes, Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG 
                                </p>
                              </div>
                              <div id="uploadFotoProgress">*foto sedang di upload</div>
                              <div id="uploadFotoDone">*foto selesai di upload</div>
                            </div>
                          </div>
                          <div class="col-lg-3"></div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-12">Nama</label>
                          <div class="col-md-12">
                            <input class="form-control form-control-line" type="text" id="nama" name="nama">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-12">Nama Pemilik / PT</label>
                          <div class="col-md-12">
                            <input class="form-control form-control-line" type="text" id="nama_pemilik" name="nama_pemilik">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-12">Slug</label>
                          <div class="col-md-12">
                            <input class="form-control form-control-line" type="text" id="slug" name="slug">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-12">Deskripsi</label>
                          <div class="col-md-12">
                            <textarea class="form-control form-control-line" rows="7" id="deskripsi" name="deskripsi" minlenght="50" maxlength="255"></textarea>
                            <span class="hint" id="textarea_message" style="font-size:0.8em;color:#800;"></span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-12">Phone No</label>
                          <div class="col-md-12">
                            <input class="form-control form-control-line" type="text" id="telp" name="telp">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-12">Website</label>
                          <div class="col-md-12">
                            <input class="form-control form-control-line" type="text" id="website" name="website">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-12">Whatsapp</label>
                          <div class="col-md-12">
                            <input class="form-control form-control-line" type="text" id="whatsapp" name="whatsapp">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-12">Facebook</label>
                          <div class="col-md-12">
                            <input class="form-control form-control-line" type="text" id="facebook" name="facebook">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-12">Instagram</label>
                          <div class="col-md-12">
                            <input class="form-control form-control-line" type="text" id="instagram" name="instagram">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-12">Line</label>
                          <div class="col-md-12">
                            <input class="form-control form-control-line" type="text" id="line" name="line">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-12">
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <button class="btn btn-success">Update Profile</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="ubah_password" role="tabpanel">
                <div class="card-body">
                  <form class="form-horizontal form-material" id="form_ubah_password">
                    <div class="form-group">
                      <label for='password' class="col-md-12">Password baru</label>
                      <div class="col-md-12">
                        <input class="form-control form-control-line"  minlength="8" title="minimal 8 karakter" type="password" id="password1">
                        <span toggle="#password1" class="field-icon-2 fa fa-fw fa-eye toggle-password"></span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="konfirmasi" class="col-md-12">Konfirmasi Password baru</label>
                      <div class="col-md-12">
                        <input class="form-control form-control-line" minlength="8" title="minimal 8 karakter" type="password" id="password2">
                        <span toggle="#password2" class="field-icon-2 fa fa-fw fa-eye toggle-password"></span>
                        <div id="password_help" style="color:red;">Password tidak sama</div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-12">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <button class="btn btn-success">Update Password</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <!-- Close Tab Panes -->
            <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
  <!-- Main row --> 
</section>
<!-- /.content --> 
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="modalContactForm" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Tambah akun bank</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        <form id="form_tambah_bank">
          <fieldset>
            <input type="hidden" name="uuid_vendor" id="uuid_vendor_bank">
            <div class="form-group">
              <label class="col-md-12">Nama Bank <span style="color:red;">*</span></label>
              <div class="col-md-12">
                <select class="form-control" id="list_bank" name="kode_bank" style="width: 100%;" required="required">
                  <option value="" disabled selected>Pilih nama bank</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-12">Nama Pemilik Rekening <span style="color:red;">*</span></label>
              <div class="col-md-12">
                <input class="form-control" type="text" id="nama_pemilik_rekening" name="nama_rekening" placeholder="Masukkan nama pemilik rekening" required="required">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-12">Nomor Rekening <span style="color:red;">*</span></label>
              <div class="col-md-12">
                <input class="form-control" type="text" id="no_rek" name="no_rek" placeholder="Masukkan nomor rekening" required="required">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-12">Cabang Bank <span style="color:red;">*</span></label>
              <div class="col-md-12">
                <input class="form-control" type="text" id="cabang_bank" name="cabang_bank" placeholder="Masukkan nama cabang bank" required="required">
              </div>
            </div>
          </fieldset>
        </div>
        <div class="modal-footer d-flex justify-content-center">
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
          <input type="submit" value="Simpan" class="btn btn-info"/><input type="reset" value="Batal" class="btn btn-danger" data-dismiss="modal"/>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="ubahlokasi" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Ubah Lokasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        <form id="form_edit_lokasi">
          <fieldset>
            <input type="hidden" name="uuid_vendor" id="uuid_vendor_lokasi">
            <div class="form-group col-md-12">
								<label for="inputAddress">Alamat <strong style="color: red;">*</strong></label>
								<textarea class="form-control" id="address" name="address" placeholder="Alamat" rows="3" required="required"></textarea>
							</div>
							<div class="form-row">
								<div class="form-group col-md-3">
									<label for="inputProvinsi">Provinsi <strong style="color: red;">*</strong></label>
									<select id="inputProvinsi" class="form-control" required="required">
									</select>
									<input type="hidden" id="hiddenProvinsi" name="provinsi">
								</div>
								<div class="form-group col-md-3">
									<label for="inputKabupaten">Kabupaten <strong style="color: red;">*</strong></label>
									<select id="inputKabupaten" class="form-control" required="required">
									</select>
									<input type="hidden" id="hiddenKabupaten" name="kabupaten">
								</div>
								<div class="form-group col-md-3">
									<label for="inputKecamatan">Kecamatan <strong style="color: red;">*</strong></label>
									<select id="inputKecamatan" class="form-control" required="required">
									</select>
									<input type="hidden" id="hiddenKecamatan" name="kecamatan">
								</div>
								<div class="form-group col-md-3">
									<label for="inputKelurahan">Kelurahan <strong style="color: red;">*</strong></label>
									<select id="inputKelurahan" class="form-control" required="required">
									</select>
									<input type="hidden" id="hiddenKelurahan" name="kelurahan">
								</div>
							</div>
          </fieldset>
        </div>
        <div class="modal-footer d-flex justify-content-center">
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
          <input type="submit" value="Simpan" class="btn btn-info"/><input type="reset" value="Batal" class="btn btn-danger" data-dismiss="modal"/>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="ubahkategori" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Ubah Kategori</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        <form id="form_edit_kategori">
          <fieldset>
          <input type="hidden" name="uuid_vendor" id="uuid_vendor_kategori">
            <div class="form-group col-md-12">
								<div class="form-row">
									<div class="col-md-6">
										<div class="checkbox">                   
											<label><input type="checkbox" value="Venue" name="kategori[]">&nbsp;Venue</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Photography" name="kategori[]" >&nbsp;Photography</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Wardrobe" name="kategori[]" >&nbsp;Wardrobe</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Decoration" name="kategori[]" >&nbsp;Decoration</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Souvenir" name="kategori[]" >&nbsp;Souvenir</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Catering" name="kategori[]" >&nbsp;Catering</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Make Up" name="kategori[]" >&nbsp;Make Up</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="checkbox">
											<label><input type="checkbox" value="Invitation" name="kategori[]" >&nbsp;Invitation</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Entertainment" name="kategori[]">&nbsp;Entertainment</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Transportation" name="kategori[]" >&nbsp;Transportation</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Event Cake" name="kategori[]" >&nbsp;Event Cake</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Videography" name="kategori[]" >&nbsp;Videography</label>
										</div>
									</div>
								</div>
                <p>Kategori anda sebelumnya adalah</p>
                <p id="all_kategori"></p>
							</div>
              
          </fieldset>
        </div>
        <div class="modal-footer d-flex justify-content-center">
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
          <input type="submit" value="Simpan" class="btn btn-info"/><input type="reset" value="Batal" class="btn btn-danger" data-dismiss="modal"/>
        </div>
      </form>
    </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<script type="text/javascript">
  $(".toggle-password").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
  });
</script>
<script type="text/javascript">
  var readySubmit = true;
  $(document).ready(function(){
    $("#uploadFotoProgress").hide();
    $("#uploadFotoDone").hide();
    $("#btn_tambah_rekening").hide();
    $("#uuid_vendor_bank").val('<?php echo $this->session->userdata('uuid'); ?>'); 
    $("#uuid_vendor_lokasi").val('<?php echo $this->session->userdata('uuid'); ?>');
    $("#uuid_vendor_kategori").val('<?php echo $this->session->userdata('uuid'); ?>'); 

    $("#inputProvinsi").append('<option value="">Pilih</option>'); 
		$("#inputKabupaten").html(''); 
		$("#inputKecamatan").html(''); 
		$("#inputKelurahan").html(''); 
		$("#inputKabupaten").append('<option value="">Pilih</option>'); 
		$("#inputKecamatan").append('<option value="">Pilih</option>'); 
		$("#inputKelurahan").append('<option value="">Pilih</option>'); 
		url = '<?= $this->config->item('api_http'); ?>/vendors/provinsi'; 
		$.ajax({ url: url, 
			type: 'GET', 
			dataType: 'json', 
			success: function(result) { 
				for (var i = 0; i < result.length; i++) 
					$("#inputProvinsi").append('<option value="' + result[i].id_prov + '">' + result[i].nama + '</option>'); 
			} 
		}); 

    $.ajax({
      url: '<?= $this->config->item('api_http'); ?>/vendors/list_bank',
      type: 'GET',
      dataType: 'json',
      success: function(result){
       for (var i = 0; i < result.length; i++) 
        $("#list_bank").append('<option value="' + result[i].code + '">' + result[i].name + ' ( '+ result[i].code + ' ) '+ '</option>');
      $("#list_bank").select2();
    }
  });
  });
  
	$("#inputProvinsi").change(function(){ 
		var id_prov = $("#inputProvinsi").val(); 
		$("#hiddenProvinsi").val($( "#inputProvinsi option:selected" ).text());
		var url = '<?= $this->config->item('api_http'); ?>/vendors/kabupatens?id_prov=' + id_prov; 
		$("#inputKabupaten").html(''); $("#inputKecamatan").html(''); 
		$("#inputKelurahan").html(''); $("#inputKabupaten").append('<option value="">Pilih</option>'); 
		$("#inputKecamatan").append('<option value="">Pilih</option>'); 
		$("#inputKelurahan").append('<option value="">Pilih</option>'); 
		$.ajax({ url : url, 
			type: 'GET', 
			dataType : 'json', 
			success : function(result){ 
				for(var i = 0; i < result.length; i++) 
					$("#inputKabupaten").append('<option value="'+ result[i].id_kab +'">' + result[i].nama + '</option>'); 
			} 
		});  
	}); 

	$("#inputKabupaten").change(function(){ 
		var id_kab = $("#inputKabupaten").val(); 
		$("#hiddenKabupaten").val($( "#inputKabupaten option:selected" ).text());
		var url = '<?= $this->config->item('api_http'); ?>/vendors/kecamatan?id_kab=' + id_kab; 
		$("#inputKecamatan").html(''); $("#inputKelurahan").html(''); 
		$("#inputKecamatan").append('<option value="">Pilih</option>'); 
		$("#inputKelurahan").append('<option value="">Pilih</option>'); 
		$.ajax({ url : url, 
			type: 'GET', 
			dataType : 'json', 
			success : function(result){ 
				for(var i = 0; i < result.length; i++) 
					$("#inputKecamatan").append('<option value="'+ result[i].id_kec +'">' + result[i].nama + '</option>'); 
			} 
		});  
	}); 
	$("#inputKecamatan").change(function(){ 
		var id_kec = $("#inputKecamatan").val(); 
		$("#hiddenKecamatan").val($( "#inputKecamatan option:selected" ).text());
		var url = '<?= $this->config->item('api_http'); ?>/vendors/kelurahan?id_kec=' + id_kec; $("#inputKelurahan").html(''); 
		$("#inputKelurahan").append('<option value="">Pilih</option>'); 
		$.ajax({ url : url, 
			type: 'GET', 
			dataType : 'json', 
			success : function(result){ 
				for(var i = 0; i < result.length; i++) 
					$("#inputKelurahan").append('<option value="'+ result[i].id_kel +'">' + result[i].nama + '</option>'); 
			} 
		});  
	});
	$("#inputKelurahan").change(function(){
		$("#hiddenKelurahan").val($( "#inputKelurahan option:selected" ).text());
	}); 

  $("#form_tambah_bank").submit(function(e){
    e.preventDefault();
    $.post('<?= $this->config->item('api_http'); ?>/vendors/akun_bank', $(this).serialize(), function(response){
      var response = JSON.parse(response);
      if(response.status == "success"){
        swal({
          title: "Berhasil!",
          text: response.message,
          type: "success"
        }).then((result) => {
          location.reload();
        });
      }
      else{
        swal({
          title: "Gagal!",
          text: response.message,
          type: "error"
        }).then((result) => {
          location.reload();
        });
      }

    });
  });

  $("#form_edit_lokasi").submit(function(e){
    e.preventDefault();
    $.post('<?= $this->config->item('api_http'); ?>/vendors/update_lokasi_vendor', $(this).serialize(), function(response){
      var response = JSON.parse(response);
      if(response.status == "success"){
        swal({
          title: "Berhasil!",
          text: response.message,
          type: "success"
        }).then((result) => {
          location.reload();
        });
      }
      else{
        swal({
          title: "Gagal!",
          text: response.message,
          type: "error"
        }).then((result) => {
          location.reload();
        });
      }

    });
  });

  $("#form_edit_kategori").submit(function(e){
    e.preventDefault();
    $.post('<?= $this->config->item('api_http'); ?>/vendors/update_kategori_vendor', $(this).serialize(), function(response){
      var response = JSON.parse(response);
      if(response.status == "success"){
        swal({
          title: "Berhasil!",
          text: response.message,
          type: "success"
        }).then((result) => {
          location.reload();
        });
      }
      else{
        swal({
          title: "Gagal!",
          text: response.message,
          type: "error"
        }).then((result) => {
          location.reload();
        });
      }

    });
  });

  $.post('<?= $this->config->item('api_http'); ?>/vendors/data_akun_bank', "uuid_vendor=" + '<?php echo $this->session->userdata('uuid'); ?>', function(response){
    var response = JSON.parse(response);
    if(response.length == 0){
      $("#btn_tambah_rekening").show();
      $("#table_rekening").hide();
    }
    else{
      $("#table_rekening").show();
      $("#nama_pemilik_akun").html(response[0].nama);
      $("#no_rek_akun").html(response[0].norek);
      $("#nama_bank_akun").html(response[0].name);
      $("#nama_cabang_akun").html(response[0].cabang_bank); 
    }

  });
  $.ajax({
    url: '<?= $this->config->item('api_http'); ?>/vendors/user/' + '<?php echo $this->session->userdata('uuid'); ?>',
    type: 'GET',
    dataType: 'json',
    success: function(result){
        //console.log(result[0].secure_url == "" ? 'https://res.cloudinary.com/yepsindo/image/upload/v1536898735/profile.jpg' : result[0].secure_url);

        $("#profil_name").append(result[0].namaVendor);
        $("#profil_image").attr('src', result[0].secureUrl == "" ? 'https://res.cloudinary.com/yepsindo/image/upload/v1536898735/profile.jpg' : result[0].secureUrl);
        $("#profil_deskripsi").append(result[0].bioVendor);
        $("#profil_lokasi").append(result[0].alamatVendor + ', ' + result[0].vKelurahan + ', ' + result[0].vKecamatan + ', ' + result[0].vKabupaten + ', ' + result[0].vProvinsi);
        $("#profil_email").append(result[0].email);
        $("#profil_telp").append(result[0].telp);
        $("#spesial_kategori_vendor").append(result[0].kategori);
        $("#all_kategori").append(result[0].kategori);
        var sosial_template = result[0].website == "" ? "" : '<a class="btn btn-social-icon btn-facebook btn-sm" href="' + result[0].website +'"><i class="fa fa-globe"></i></a> &nbsp;'; 

        sosial_template += result[0].whatsapp == "" ? "" : '<a class="btn btn-social-icon btn-success btn-sm" href="https://api.whatsapp.com/send?phone=' + result[0].whatsapp +'"><i class="fab fa-whatsapp"></i></a> &nbsp;';

        sosial_template += result[0].facebook == "" ? "" : '<a class="btn btn-social-icon btn-facebook btn-sm" href="' + result[0].facebook +'"><i class="fab fa-facebook"></i></a> &nbsp;';

        sosial_template += result[0].instagram == "" ? "" : '<a class="btn btn-social-icon btn-instagram btn-sm" href="' + result[0].instagram +'"><i class="fab fa-instagram"></i></a> &nbsp;';

        sosial_template += result[0].line == "" ? "" : '<a class="btn btn-social-icon btn-success btn-sm" href="' + result[0].line +'"><i class="fab fa-line"></i></a>';
        $("#profil_sosial").append(sosial_template);
        $("#uuid_vendor_profil").val(result[0].uuid);
        $("#public_id").val(result[0].publicId);
        $("#url").val(result[0].url);
        $("#secure_url").val(result[0].secureUrl);
        $("#nama").val(result[0].namaVendor);
        $("#nama_pemilik").val(result[0].namaPemilik);
        $("#slug").val(result[0].slug);
        $("#deskripsi").val(result[0].bioVendor);
        $("#email").val(result[0].email);
        $("#telp").val(result[0].telpVendor);
        $("#website").val(result[0].website);
        $("#whatsapp").val(result[0].whatsapp);
        $("#facebook").val(result[0].facebook);
        $("#instagram").val(result[0].instagram);
        $("#line").val(result[0].line);
        $('.dropify').dropify({
          defaultFile: result[0].secureUrl == "" ? 'https://res.cloudinary.com/yepsindo/image/upload/v1536898735/profile.jpg' : result[0].secureUrl,
          messages: {
            'default': 'Drag and drop a file here or click',
            'replace': 'Drag and drop or click to replace',
            'remove':  'Remove',
            'error':   'Ooops, something wrong happended.'
          },
          error: {
            'fileSize': 'The file size is too big ({{ value }} max).',
            'minWidth': 'The image width is too small ({{ value }}}px min).',
            'maxWidth': 'The image width is too big ({{ value }}}px max).',
            'minHeight': 'The image height is too small ({{ value }}}px min).',
            'maxHeight': 'The image height is too big ({{ value }}px max).',
            'imageFormat': 'The image format is not allowed ({{ value }} only).'
          }
        });

        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element){
          return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element){
          alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element){
          console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e){
          e.preventDefault();
          if (drDestroy.isDropified()) {
            drDestroy.destroy();
          } else {
            drDestroy.init();
          }
        });
      }
    });


$("#password_help").hide();
$("#password2").on('keypress', function(){
  if($("#password1").val() != $("#password2").val()){
    $("#password_help").show();
  }
  else{
    $("#password_help").hide();
  }
});

$("#password2").on('change', function(){
  if($("#password1").val() != $("#password2").val()){
    $("#password_help").show();
  }
  else{
    $("#password_help").hide();
  }
});

$("#form_ubah_password").submit(function (e){
  e.preventDefault();

  if($("#password1").val() == $("#password2").val()){
    if($("#password1").val().length >= 8){
      $.post('<?= $this->config->item('api_http'); ?>/vendors/change_password', "uuid="+'<?php echo $this->session->userdata('uuid'); ?>'+"&password=" +$("#password1").val(), function(response) {
        response = JSON.parse(response);
        swal(response.status, response.message, response.status);
      });
    }
    else{
      swal("error", "Password minimal 8 karakter", "error");
    }
  }
  else{
    $("#password_help").show();
  }
  
});

$('#deskripsi').on('keyup',function() 
{
  var maxlen = $(this).attr('maxlength');
  
  var length = $(this).val().length;
  if(length > (maxlen-10) ){
    $('#textarea_message').text('max length '+maxlen+' characters only!')
  }
  else if(length < 50){
    $('#textarea_message').text('min length 50 characters only!')
  }
  else
  {
    $('#textarea_message').text('');
  }
});

$("#data_profil_vendor").submit(function (e){
  e.preventDefault();
  /*$.ajax({
    url: '<?= $this->config->item('api_http'); ?>/vendors/user_update',
    data: $("#data_profil_vendor").serialize(),
    cache: false,
    contentType: false,
    processData: false,
    method: 'POST',
    success: function(data){
      alert(JSON.stringify(data));
    }
  });*/
  if(readySubmit){
    $.post('<?= $this->config->item('api_http'); ?>/vendors/user_update', $("#data_profil_vendor").serialize(), function(response) {
    //response = JSON.parse(response);
    if(response.status == "success"){
      swal({
        title: response.status,
        text: response.message,
        type: response.status
      }).then((result) => {
        location.reload();
      });
    }
    else{
      swal(response.status, response.message, response.status);
    }
    
  });
  }
  else{
    swal("Info", "Foto masih diupload, sabar ya.", "info");
  }
});

$(".dropify").on("change", function(e){ 
  var file = this.files[0];
  var form = new FormData();
  readySubmit = false;
  $("#uploadFotoProgress").show();
  $("#uploadFotoDone").hide();
  form.append('image', file);
  $.ajax({
    url : "<?= $this->config->item('api_http'); ?>/vendors/cloudinary",
    type: "POST",
    cache: false,
    contentType: false,
    processData: false,
    data : form,
    success: function(response){
      data = JSON.parse(response);
      $("#public_id").val(data.publicId);
      $("#url").val(data.url);
      $("#secure_url").val(data.secureUrl);
      readySubmit = true;
      $("#uploadFotoProgress").hide();
      $("#uploadFotoDone").show();
    }
  });
});

if(RegExp('profil', 'gi').test(window.location.search)){
  $(".sidebar-menu>li").removeAttr("data-step");
  $(".sidebar-menu>li").removeAttr("data-intro");
  $(".sidebar-menu>li").removeAttr("data-position");
  $(".treeview-menu>li").removeAttr("data-step");
  $(".treeview-menu>li").removeAttr("data-intro");
  $(".treeview-menu>li").removeAttr("data-position");
  var intro = introJs();
  intro.setOption("exitOnEsc", false);
  intro.setOption("exitOnOverlayClick", false);
  intro.setOption("showStepNumbers", false);
  intro.setOption("doneLabel", "Next Page");
  intro.onchange(function(targetElement){
    console.log(targetElement.id);
    switch (targetElement.id) 
    { 
      case "step-2": 
      $("#rekening_bank").addClass("active");
      $("#ubah_profil").removeClass("active");
      $("#ubah_password").removeClass("active");
      break; 
      case "step-3": 
      $("#rekening_bank").removeClass("active");
      $("#ubah_profil").addClass("active");
      $("#ubah_password").removeClass("active");
      break;
      case "step-4":
      $("#rekening_bank").removeClass("active");
      $("#ubah_profil").removeClass("active");
      $("#ubah_password").addClass("active");
      break;
    }
  });
  intro.start().oncomplete(function() {
    window.location.href = '<?php echo base_url('profil');?>';
  });
}

</script>