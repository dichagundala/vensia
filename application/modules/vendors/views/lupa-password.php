<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from uxliner.com/adminkit/demo/material/ltr/pages-recover-password.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 23 Apr 2018 08:20:52 GMT -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YEPS - Lupa Password</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('dist/img/favicon-16x16.png'); ?>">
  <!-- v4.0.0-alpha.6 -->
  <link rel="stylesheet" href="<?php echo base_url('dist/bootstrap/css/bootstrap.min.css'); ?>">

  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('dist/css/style.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('dist/css/font-awesome/css/font-awesome.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('dist/css/et-line-font/et-line-font.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('dist/css/themify-icons/themify-icons.css'); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body class="hold-transition login-page">
  <div class="login-logo" style="margin-top:5%;">
    <a href="<?php echo base_url(''); ?>"><img src="<?php echo base_url('dist/img/logo-blue.png'); ?>" alt="" style="width: 20%;height: 20%"></a>
  </div>
  <div class="login-box" style="border-radius: 7px;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
  <div class="login-box-body">
    <h3 class="login-box-msg m-b-1" style="font-weight: bold">Lupa Password</h3>
    <p>Masukkan email Anda dan ikuti perintah yang Kami kirimkan. </p>
    <form id="form_reset_password">
      <div class="form-group has-feedback">
        <input type="email" class="form-control sty1" placeholder="Masukkan Email Anda" id="email">
      </div>
      <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
      <div>
        <div class="col-xs-4 m-t-1">
          <input type="submit" class="btn btn-primary btn-block btn-flat" value="Kirim">
        </div>
        <!-- /.col --> 
      </div>
    </form>
  </div>
  <!-- /.login-box-body --> 
</div>
<!-- /.login-box --> 

<!-- jQuery 3 --> 
<script src="<?php echo base_url('dist/js/jquery.min.js'); ?>"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>

<!-- v4.0.0-alpha.6 --> 
<script src="<?php echo base_url('dist/bootstrap/js/bootstrap.min.js'); ?>"></script> 

<script type="text/javascript">
  $("#form_reset_password").submit(function(e){
    e.preventDefault();
    $.post('<?= $this->config->item('api_http'); ?>/auth/reset_password', "email=" +$("#email").val(), function(response) {
      response = JSON.parse(response);
      swal(response.status, response.message, response.status);
    });
  });
</script>
</body>

<!-- Mirrored from uxliner.com/adminkit/demo/material/ltr/pages-recover-password.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 23 Apr 2018 08:20:52 GMT -->
</html>