<?php $this->load->view('header'); ?>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.css">
<?php $this->load->view('sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header sty-one">
    <h1>Porfolio Vendor</h1>
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li><i class="fa fa-angle-right"></i> Pengaturan Toko</li>&nbsp;
      <li><i class="fa fa-angle-right"></i> Portfolio</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-lg-12">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" id="buatAlbum">
          <i class="fa fa-images"></i>&nbsp; Buat Album
        </button>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="border-bottom: 5px solid #0073b7; border-top: 3px solid #0073b7">
          <div class="modal-header">
            <h5 class="modal-title" style="font-weight: bold" id="myModalLongTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form id="modalAlbum" method="POST" action="#">
            <div class="modal-body">
              <div class="form-group">
                <label>Nama Album</label>
                <input type="text" class="form-control" placeholder="Masukkan Nama Album" name="nama_album" id="nama_album" required="required">
              </div>
              <div class="form-group">
                <label>Deskripsi</label>
                <input type="text" class="form-control" placeholder="Masukkan Deskripsi Album" name="deskripsi_album" id="deskripsi_album" required="required">
              </div>
              <div class="form-group">
                <label>Tanggal Event / Kegiatan</label>
                <input type="date" class="form-control" name="tgl_kegiatan" id="tgl_kegiatan" required="required">
              </div>
              <input type="hidden" name="uuid_vendor" value="<?php echo $this->session->userdata('uuid'); ?>">
            </div>
            <div class="modal-footer">
              <button type="reset" class="btn btn-secondary" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="row m-t-3">
      <div class="col-lg-12">
        <div class="card card-outline">
          <div class="card-header bg-blue">
            <h5 class="text-white m-b-0">Data Portfolio</h5>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Judul Album</th>
                    <th>Deskripsi</th>
                    <th>Tanggal</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </section>
  <!-- /.content --> 
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('footer'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() { 

    function formatDate(date){
      var today = new Date(date);
      var dd = today.getDate();
      var mm = today.getMonth()+1;

      var yyyy = today.getFullYear();
      if(dd<10){
        dd='0'+dd;
      } 
      if(mm<10){
        mm='0'+mm;
      } 
      var today = dd+'/'+mm+'/'+yyyy;
      return today;
    }

    $.ajax({
      url: '<?= $this->config->item('api_http'); ?>/vendors/get_album_all',
      type: 'POST',
      data: {
        uuid_vendor : '<?php echo $this->session->userdata('uuid'); ?>'
      },
      dataType: 'json',
      success: function(result){
       var no = 1;
       for(var i = 0; i < result.length; i++){
        var template = '<tr><td>'+no+'</td><td>'+result[i].judul+' (' + result[i].total +')</td><td>'+result[i].deskripsi+'</td><td>'+ formatDate(result[i].tgl_kegiatan) +'</td><td><a href="#" class="btn btn-success btn-sm" id="editAlbum" data-uuid="'+ result[i].uuid +'" title="Ubah Album"><i class="fa fa-edit"></i></a><a href="<?php echo base_url('vendors/portfolio/album')?>/'+result[i].uuid+'" class="btn btn-primary btn-sm" title="Tambah Foto Album"><i class="fa fa-plus"></i></a><button class="btn btn-danger btn-sm" id="hapusAlbum" data-uuid="'+result[i].uuid+'" title="Hapus Album"><i class="fa fa-trash"></i></button></td></tr>';
        no++;
        $("#example1 tbody").append(template);
      }
      $('#example1').DataTable();
    }
  });

    $('body').delegate('#buatAlbum', 'click', function(event) {
      $('form#modalAlbum').trigger('reset');
      $("#uuid_album").remove();
      $('form#modalAlbum').attr('action', '<?= $this->config->item('api_http'); ?>/vendors/add_album');
      $("#myModalLongTitle").html("Tambah Album");
      $('#myModal').modal('show');
      return false;
    });

    $('body').delegate('#editAlbum', 'click', function(event) {
      console.log($(this).data('uuid'));
      $('form#modalAlbum').trigger('reset');
      $('form#modalAlbum').attr('action', '<?= $this->config->item('api_http'); ?>/vendors/edit_album');
      $("#myModalLongTitle").html("Ubah Album");
      $('<input />').attr('type', 'hidden').attr('id', 'uuid_album').attr('name', "uuid").attr('value', $(this).data('uuid')).appendTo('form#modalAlbum');

      $.ajax({
        url: '<?= $this->config->item('api_http'); ?>/vendors/get_album',
        type: 'POST',
        data: {
          uuid_vendor : '<?php echo $this->session->userdata('uuid'); ?>',
          uuid : $(this).data('uuid')
        },
        dataType: 'json',
        success: function(result){
          $("#nama_album").val(result[0].judul);
          $("#deskripsi_album").val(result[0].deskripsi);
          var today = new Date(result[0].tgl_kegiatan);
          var dd = today.getDate();
          var mm = today.getMonth()+1;

          var yyyy = today.getFullYear();
          if(dd<10){
            dd='0'+dd;
          } 
          if(mm<10){
            mm='0'+mm;
          } 
          var today = yyyy+'-'+mm+'-'+dd;
          $("#tgl_kegiatan").val(today);
        }
      });
      $('#myModal').modal('show');
      return false;
    });

    $("form#modalAlbum").submit(function(e){
      e.preventDefault();
      $.post($(this).attr('action'), $(this).serialize(), function(result){
        result = JSON.parse(result);
        swal({
          title: result.status,
          text: result.message,
          type: result.status
        }).then((result) => {
          location.reload();
        });
      });
    });

    $('body').delegate('#hapusAlbum', 'click', function(event) {
      swal({
        title: "Hapus Album",
        text: "Anda yakin ingin menghapus album ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Ya, saya yakin!',
        cancelButtonText: "Tidak, batalkan!",
      }).then((result) => {
        if(result.value){
          $.ajax({
            url: '<?= $this->config->item('api_http'); ?>/vendors/delete_album',
            type: 'POST',
            data: {
              uuid_vendor : '<?php echo $this->session->userdata('uuid'); ?>',
              uuid : $(this).data('uuid')
            },
            dataType: 'json',
            success: function(result){
              swal({
                title: result.status,
                text: result.message,
                type: result.status
              }).then((result) => {
                location.reload();
              });
            }
          });
        }
      });
    });
  });

</script>