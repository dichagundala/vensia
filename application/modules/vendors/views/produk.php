<?php $this->load->view('header'); ?>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.css">
<?php $this->load->view('sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header sty-one">
    <h1>Daftar Produk</h1>
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li><i class="fa fa-angle-right"></i> Daftar Service</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-lg-12">
        <div class="info-box">
          <div class="card tab-style1"> 
            <!-- Nav tabs -->

            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane active" id="home" role="tabpanel" aria-expanded="true">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-12 col-xs-6">
                     <div class="card m-t-3">
                      <div class="card-body">
                        <h4 class="text-black">Daftar Service</h4>
                        <p>Daftar Service</p>
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Service</th>
                                <th>Harga</th>
                                <th>Status</th>
                                <th>Aksi</th>
                              </tr>
                            </thead>
                            <tbody id="tbody">
                            </tbody>
                          </table>
                        </div>
                      </div></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Main row --> 
  </section>
  <!-- /.content --> 
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('footer'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.js"></script>
<script type="text/javascript">
  $.ajax({

    url: '<?= $this->config->item('api_http'); ?>/vendors/produk',
    type: 'POST',
    data: {
      uuid : '<?php echo $this->session->userdata('uuid'); ?>'
    },
    dataType: 'json',
    success: function(result){
     console.log(result);
     console.log(result.length);
     var no = 1;
     for(var i = 0; i < result.length; i++){
      var template = '<tr><td>'+no+'</td><td><span style="font-weight:bold">'+result[i].nama_produk+'</span><br> Kategori : '+result[i].kategori+'</td><td>'+result[i].harga_produk+'<br> Promosi : '+(result[i].harga_promosi == '0' ? result[i].harga_produk : result[i].harga_promosi) +'</td><td>'+ 'Ditampilkan / Tidak / Tutup' +'</td><td><a class="btn btn-primary btn-sm edit" title="Edit Service" href="<?php echo base_url('vendors/produk/edit/'); ?>'+result[i].id +'/' + result[i].uuid +'"><i class="fa fa-edit"></i></a>&nbsp;<button class="btn btn-success btn-sm tol-del" title="Atur Service" data-uuid="'+result[i].uuid+'"><i class="fa fa-gears"></i></button>&nbsp;<button class="btn btn-danger btn-sm delete" title="Hapus" data-uuid="'+result[i].uuid+'"><i class="fa fa-trash"></i></button></td></tr>';
      no++;
      $("#tbody").append(template);
    }
    /* $('.edit').on('click', function(e){
      e.preventDefault();
      var a = $(this);
      $.ajax({
        type : "POST",
        data : {uuid : a.data('uuid')},
        url : "<?= $this->config->item('api_http'); ?>/vendors/mail_aktivasi",
        success : function(hasil){
          location.reload();
        }
      })
    }); */

    $('.delete').on('click', function(e){
      e.preventDefault();     
      swal({
        title: "Hapus produk",
        text: "Anda yakin ingin menghapus produk ini?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Ya, saya yakin!',
        cancelButtonText: "Tidak, batalkan!",
      }).then((result) => {
        if(result.value){
          $.ajax({
            type : "POST",
            data : {uuid :  $(this).attr("uuid")},
            //url : "https://api.yepsindonesia.com/vendors/delete_produk",
            url : "<?= $this->config->item('api_http'); ?>/vendors/delete_produk",
            success : function(hasil){
              swal({
                title: "Berhasil!",
                text: "Produk berhasil di hapus!",
                type: "success"
              }).then((result) => {
                location.reload();
              });
            }
          });
        }
      });
    });
    $('#example1').DataTable();
  }
});
</script>