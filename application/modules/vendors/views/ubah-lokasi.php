<div class="row">
					<div class="container">
						<form method="POST" id="register-form" action="#" enctype="multipart/form-data">
							<strong style="color: red;">*</strong> Wajib di isi
							<div class="form-group col-md-12">
								<label for="inputNameVendor">Nama Vendor <strong style="color: red;">*</strong></label>
								<input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama Vendor" required="required">
							</div>
							<div class="form-group col-md-12">
								<label for="inputNameVendor">Nama Pemilik / PT <strong style="color: red;">*</strong></label>
								<input type="text" class="form-control" id="nama_pemilik" name="nama_pemilik" placeholder="Masukkan Nama Pemilik Vendor" required="required">
							</div>
							<div class="form-group col-md-12">
								<label for="inputNameVendor">Deskripsi Vendor <strong style="color: red;">*</strong></label>
								<textarea class="form-control" id="deskripsi" name="deskripsi" rows="3" placeholder="Deskripsi Vendor" required="required"></textarea>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="inputEmail">Email <strong style="color: red;">*</strong></label>
									<input type="email" class="form-control" id="email" name="email" placeholder="Email" required="required">
								</div>
								<div class="form-group col-md-6">
									<label for="inputTelepon">Telepon <strong style="color: red;">*</strong></label>
									<input type="text" class="form-control" id="tel" name="telp" placeholder="Telepon" required="required">
								</div>
								<div class="form-group col-md-6">
									<label for="inputTelepon">Telepon <strong style="color: red;">*</strong></label>
									<input type="text" class="form-control" id="tel" name="telp" placeholder="Telepon" required="required">
								</div>
							</div>
							<div class="form-group col-md-12">
								<label for="inputAddress">Alamat <strong style="color: red;">*</strong></label>
								<textarea class="form-control" id="address" name="address" placeholder="Alamat" rows="3" required="required"></textarea>
							</div>
							<div class="form-row">
								<div class="form-group col-md-3">
									<label for="inputProvinsi">Provinsi <strong style="color: red;">*</strong></label>
									<select id="inputProvinsi" class="form-control" required="required">
									</select>
									<input type="hidden" id="hiddenProvinsi" name="provinsi">
								</div>
								<div class="form-group col-md-3">
									<label for="inputKabupaten">Kabupaten <strong style="color: red;">*</strong></label>
									<select id="inputKabupaten" class="form-control" required="required">
									</select>
									<input type="hidden" id="hiddenKabupaten" name="kabupaten">
								</div>
								<div class="form-group col-md-3">
									<label for="inputKecamatan">Kecamatan <strong style="color: red;">*</strong></label>
									<select id="inputKecamatan" class="form-control" required="required">
									</select>
									<input type="hidden" id="hiddenKecamatan" name="kecamatan">
								</div>
								<div class="form-group col-md-3">
									<label for="inputKelurahan">Kelurahan <strong style="color: red;">*</strong></label>
									<select id="inputKelurahan" class="form-control" required="required">
									</select>
									<input type="hidden" id="hiddenKelurahan" name="kelurahan">
								</div>
							</div>
							<div class="form-group col-md-12">
								<label for="inputLine">Photo</label>
								<input type="file" class="form-control" id="inputFoto" name="image">
								<small id="photoHelp" class="form-text text-muted">Upload Photo. Maksimal size 5mb</small>
							</div>
							<div class="form-group col-md-12">
								<label for="inputLine">Kategori Vendor</label><br>
								<small id="kategoriHelp" class="form-text text-muted">Kategori bisa di pilih lebih dari 1</small>
								<div class="form-row">
									<div class="col-md-6">
										<div class="checkbox">
											<label><input type="checkbox" value="Venue" name="kategori[]">&nbsp;Venue</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Photography" name="kategori[]">&nbsp;Photography</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Wardrobe" name="kategori[]">&nbsp;Wardrobe</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Decoration" name="kategori[]">&nbsp;Decoration</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Souvenir" name="kategori[]">&nbsp;Souvenir</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Catering" name="kategori[]">&nbsp;Catering</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Make Up" name="kategori[]">&nbsp;Make Up</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="checkbox">
											<label><input type="checkbox" value="Invitation" name="kategori[]">&nbsp;Invitation</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Entertainment" name="kategori[]">&nbsp;Entertainment</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Transportation" name="kategori[]">&nbsp;Transportation</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Event Cake" name="kategori[]">&nbsp;Event Cake</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="Videography" name="kategori[]">&nbsp;Videography</label>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group col-md-12">
								<label for="inputWebsite">Website</label>
								<input type="text" class="form-control" id="inputWebsite" name="website" placeholder="Masukkan Url Website">
							</div>
							<div class="form-row">
								<div class="form-group col-md-3">
									<label for="inputWebsite">Whatsapp</label>
									<input type="text" class="form-control" id="inputWhatsapp" name="whatsapp" placeholder="Masukkan Nomor Whatsapp">
								</div>
								<div class="form-group col-md-3">
									<label for="inputFacebook">Facebook</label>
									<input type="text" class="form-control" id="inputFacebook" name="facebook" placeholder="Masukkan username facebook">
								</div>
								<div class="form-group col-md-3">
									<label for="inputInstagram">Instagram</label>
									<input type="text" class="form-control" id="inputInstagram" name="instagram" placeholder="Masukkan username Instagram">
								</div>
								<div class="form-group col-md-3">
									<label for="inputLine">Line</label>
									<input type="text" class="form-control" id="inputLine" name="line" placeholder="Masukkan username Line">
								</div>
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
							</div>
							<!-- <div class="form-group">
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
									Launch demo modal
								</button>
							</div> -->
							<div class="form-group col-md-12">
								<center>
									<div style="background-color: #fafafa; padding: 20px; border: 1px solid #eeeeee;
									border-radius: 4px;
									box-shadow: 0 1px 3px 0 rgba(0,0,0,.12);">
									<p>Dengan mengklik Daftar, Anda telah menyetujui segala <a data-toggle="modal" data-target="#exampleModalLong">Syarat dan Ketentuan</a> yang telah dibuat oleh Kami. <br>Anda akan menerima notifikasi Email dari YEPS Indonesia.</p>
								</div>
							</center>
						</div>
						<div class="form-group col-md-12">
							<input type="submit" class="btn btn-primary" name="daftar" value="Daftar">&nbsp;<input type="reset" class="btn btn-danger" name="batal" value="Batal">
						</div>
					</form>
				</div>
			</div>