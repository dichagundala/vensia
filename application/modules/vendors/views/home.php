<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header sty-one">
    <h1>Vendor Dashboard</h1>
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li><i class="fa fa-angle-right"></i> Vendor Dashboard</li>
    </ol>
  </section>

  <style type="text/css" media="screen">
   .tour-a:hover{
      color: black;
   } 
  </style>
<?php //echo "<script type='text/javascript'>alert(".$this->session->userdata('uuid').")</script>";?>
  <!-- Main content -->
  <section class="content" data-step="6" data-intro="Semua rincian mengenai pesanan masuk , total produk, dan pendapatan ada disini" data-position="right">

    <!-- NOTIFIKASI UNTUK TOUR -->
    <!-- <div class="alert alert-secondary alert-dismissible fade show" role="alert"><a class="tour-a" href="#" style="text-decoration: none;"><strong>Ikuti Tour</strong></a> - Jika Anda bingung dengan Fitur yang ada di Dashboard. 
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    </div> -->

    <div class="row">
      <div class="col-lg-3">
        <div class="tile-progress tile-pink">  
          <div class="tile-header">
            <h5>Pesanan Bulan ini</h5> 
            <h3><span id="bulan"></span></h3>
          </div>
          <div class="tile-progressbar"> <span data-fill="80%" style="width: 80%;"></span> </div>
          <div class="tile-footer">
            <!-- <h4> <span class="pct-counter">8</span> di Minggu ini </h4> -->
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="tile-progress tile-red">
          <div class="tile-header">
            <h5>Pesanan di Terima</h5>
            <h3><h3><span id="accepted"></span></h3></h3>
          </div>
          <div class="tile-progressbar"> <span data-fill="80%" style="width: 80%;"></span> </div>
          <div class="tile-footer">
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="tile-progress tile-cyan">
          <div class="tile-header">
            <h5>Pesanan di Tolak</h5>
            <h3><h3><span id="rejected"></span></h3></h3>
          </div>
          <div class="tile-progressbar"> <span data-fill="80%" style="width: 80%;"></span> </div>
          <div class="tile-footer">
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="tile-progress tile-aqua">
          <div class="tile-header">
            <h5>Total Pesanan</h5>
            <h3><h3><span id="alls"></span></h3></h3>
          </div>
          <div class="tile-progressbar"> <span data-fill="80%" style="width: 80%;"></span> </div>
          <div class="tile-footer">
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->


    <div class="row">
      <div class="col-xl-8 m-b-3">
        <div class="box box-widget widget-user-2">
          <div class="widget-user-header bg-yellow">
            <h5 class="widget-user-desc" style="margin-left: 0px;">Jumlah Produk</h5>
          </div>
          <div class="box-footer no-padding">
            <ul class="nav nav-stacked">
              <li id="1"><a href="#">Venue <span class="pull-right badge bg-blue">0</span></a></li>
              <li id="2"><a href="#">Photography <span class="pull-right badge bg-aqua">0</span></a></li>
              <li id="3"><a href="#">Wardrobe <span class="pull-right badge bg-green">0</span></a></li>
              <li id="4"><a href="#">Decoration <span class="pull-right badge bg-red">0</span></a></li>
              <li id="5"><a href="#">Souvenir <span class="pull-right badge bg-gray">0</span></a></li>
              <li id="6"><a href="#">Catering <span class="pull-right badge bg-navy">0</span></a></li>
              <li id="7"><a href="#">Make Up <span class="pull-right badge bg-teal">0</span></a></li>
              <li id="8"><a href="#">Invitation <span class="pull-right badge bg-olive">0</span></a></li>
              <li id="9"><a href="#">Entertainment <span class="pull-right badge bg-fuchsia">0</span></a></li>
              <li id="10"><a href="#">Transportation <span class="pull-right badge bg-orange">0</span></a></li>
              <li id="11"><a href="#">Event Cake <span class="pull-right badge bg-purple">0</span></a></li>
              <li id="12"><a href="#">Videography <span class="pull-right badge bg-maroon">0</span></a></li>
              <li id="produk_kosong"><a href="#">Produk masih kosong</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-xl-4 m-b-3">
        <div class="box-gradient">
          <div class="card-body text-white">
            <h5>Total Pendapatan</h5>
            <h2>Rp. 0</h2>
            <h5>November 2018</h5>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->

    
  </section>
  <!-- /.content --> 
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('footer'); ?>

<script type="text/javascript">
  for (var i = 1; i <= 12; i++){
    $("#" + i).hide();
  }

  $(document).ready(function(){
    $.post('<?= $this->config->item('api_http'); ?>/vendors/statistik_jumlah_produk', "uuid=<?php echo $this->session->userdata('uuid'); ?>", function(response){
      var response = JSON.parse(response);
      response.forEach(function (arrayItem) {
        $("#" + arrayItem.id + '>a>span').html(arrayItem.total);
        $("#" + arrayItem.id).show();
        $("#produk_kosong").hide();
      });
    });

      $.post("<?= $this->config->item('api_http'); ?>/vendors/pesanan_bln_ini/<?php echo $this->session->userdata('uuid'); ?>/<?php echo getdate()['mon'];?>/<?php echo getdate()['year'];?>",  function(hasil){
        var response = JSON.parse(hasil);
         $("#bulan").append(response[0].total);     
      });

      $.post("<?= $this->config->item('api_http'); ?>/vendors/pesanan_diterima/<?php echo $this->session->userdata('uuid');?>",  function(hasil){
        var response = JSON.parse(hasil);
         $("#accepted").append(response[0].total);     
      });

      $.post("<?= $this->config->item('api_http'); ?>/vendors/pesanan_ditolak/<?php echo $this->session->userdata('uuid');?>",  function(hasil){
        var response = JSON.parse(hasil);
         $("#rejected").append(response[0].total);    
      });
      
      $.post("<?= $this->config->item('api_http'); ?>/vendors/pesanan_all/<?php echo $this->session->userdata('uuid'); ?>",  function(hasil){
        var response = JSON.parse(hasil);
         $("#alls").append(response[0].total);    
      });
      /*
      $.post("<?= $this->config->item('api_http'); ?>/vendors/total_pendapatan/<?php echo $this->session->userdata('uuid'); ?>/<?php echo getdate()['mon'];?>/<?php echo getdate()['year'];?>",  function(hasil){
        var response = JSON.parse(hasil);
        $("#penjualan").append(response[0].total);    
      }); */

  });
</script>

<script type="text/javascript">
  if(RegExp('true', 'gi').test(window.location.search)){
    $(".sidebar-menu>li").addClass("active");
    $(".treeview-menu>li").addClass("active");
    var intro = introJs();
    intro.setOption("exitOnEsc", false);
    intro.setOption("exitOnOverlayClick", false);
    intro.setOption("showStepNumbers", false);
    intro.setOption("doneLabel", "Next Page");
    intro.start().oncomplete(function() {
      intro.exit();
      window.location.href = '<?php echo base_url('vendor1/profil');?>?tour=profil';
    });
  }
</script>


