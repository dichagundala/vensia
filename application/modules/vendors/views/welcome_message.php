<?php
if( !isset($_SERVER['HTTPS']) && $_SERVER['HTTP_HOST'] != 'localhost') {
	$url = $_SERVER['HTTP_HOST'];
	header("Location: ".'https://'.$url);	
}
?>
<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from uxliner.com/adminkit/demo/material/ltr/pages-login-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 23 Apr 2018 08:20:52 GMT -->
<style type="text/css">
body {
	overflow: visible;
}
</style>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>YEPS - Login Vendor</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<!-- v4.0.0-alpha.6 -->
	<link rel="stylesheet" href="<?php echo base_url('dist/bootstrap/css/bootstrap.min.css'); ?>">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('dist/img/favicon-16x16.png'); ?>">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url('dist/css/style.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('dist/css/custom.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('dist/css/font-awesome/css/font-awesome.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('dist/css/et-line-font/et-line-font.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('dist/css/themify-icons/themify-icons.css'); ?>">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body class="hold-transition login-page sty1">
	<div class="login-box sty1" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); margin-top: 15px; ">
		<div class="login-box-body sty1">
			<div class="login-logo">
				<a href="<?php echo base_url(''); ?>"><img src="<?php echo base_url('dist/img/logo-blue.png'); ?>" alt="" style="width: 75%"></a>
			</div>
			<p class="login-box-msg" style="margin-top: 30px">Masuk ke Dashboard Vendor</p>
			<form method="post" action="#" id="login_form">
				<div class="form-group has-feedback">
					<input type="email" class="form-control sty1" placeholder="Email" name="email" id="email" autocomplete="email">
					<div style="color:red;" id="email_message"></div>
				</div>
				<div class="form-group has-feedback">
					<input type="password" class="form-control sty1" placeholder="Password" name="password" id="password" autocomplete="password">
					<span toggle="#password" class="field-icon fa fa-fw fa-eye toggle-password"></span>
					<div style="color:red;" id="password_message"></div>
				</div>
				
				
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
				<div>
					<div class="col-xs-8">
						<div class="checkbox icheck">
							<label>
								<!-- <input type="checkbox">Remember Me --> 
							</label>
							<a href="<?php echo base_url('vendors/forgot_password');?>" class="pull-right"><i class="fa fa-lock"></i> Lupa Password</a>
						</div>
						<!-- /.col -->
						<div class="col-xs-4 m-t-1">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Masuk</button>
						</div>
						<!-- /.col --> 
						<center style="margin-top: 20px">
							<a href="<?php echo base_url('vendors/daftar'); ?>" style="text-align: center;"> Klik disini untuk daftar sebagai Vendor</a>
						</center>
					</div>
				</div>
			</form>
		</div>
		<!-- /.login-box-body --> 
	</div>
	<!-- /.login-box --> 

	<!-- jQuery 3 --> 
	<script src="<?php echo base_url('dist/js/jquery.min.js'); ?>"></script> 

	<!-- v4.0.0-alpha.6 --> 
	<script src="<?php echo base_url('dist/bootstrap/js/bootstrap.min.js'); ?>"></script> 

</body>

<!-- Mirrored from uxliner.com/adminkit/demo/material/ltr/pages-login-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 23 Apr 2018 08:20:52 GMT -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.js"></script>
<script type="text/javascript">
	$("#login_form").submit(function(e){
		e.preventDefault();
		if($("#email").val().length === 0 ){
			$("#email_message").html("Email tidak boleh kosong!");
		}


			$.post("<?php echo base_url('vendors/auth/login'); ?>", $("#login_form").serialize(), function(data, status) {
				data = JSON.parse(data);
				
				if(data.status == "error"){
					swal({
						title: "Login gagal!",
						text: data.message,
						type: "error"
					}).then((result) => {
						location.reload();
					});
				}
				else{
					
					localStorage.setItem("namaVendor", data[0].namaVendor);
					localStorage.setItem("url", data[0].secureUrl);
					localStorage.setItem("kategori", data[0].kategori);
					swal({
						title: "Login berhasil!",
						text: data.message,
						type: "success"
					}).then((result) => {
						window.location.href = '<?php echo base_url('vendors/home');?> ';
					});
				}
			});
		
	});

</script>

<script type="text/javascript">
	$(".toggle-password").click(function() {

		$(this).toggleClass("fa-eye fa-eye-slash");
		var input = $($(this).attr("toggle"));
		if (input.attr("type") == "password") {
			input.attr("type", "text");
		} else {
			input.attr("type", "password");
		}
	});
</script>
</html>