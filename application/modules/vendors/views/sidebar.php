<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar"> 
  <!-- sidebar -->
  <section class="sidebar"> 
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <?php if(@$this->session->userdata('cro') == 'true'){

      }
      else
      {
        ?>
        <div class="image text-center" ><img id="image_vendor_side" style="height: 50px; border-radius: 3px" alt="User Image"> </div>
        <div class="info">
          <p><div id="nama_vendor"></div></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a> </div>
        </div>
        <?php 
      } 
      ?>


      <!-- sidebar menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">PERSONAL</li>
        <li data-step="1" data-intro="Semua ringkasan laporan ada di dashboard anda" data-position="right" class="<?php echo $id == 1 ? 'active' : ''; ?>"> <a href="<?php echo base_url('vendors/home'); ?>"> <i class="icon-home"></i> <span>Dashboard</span></a> 
        </li>
        <li data-step="2" data-intro="Memperbaharui profil, password dan nomor rekening ada disini" data-position="right" class="treeview <?php echo $id == 2 ? 'active' : ''; ?>"> <a href="#"> <i class="icon-user"></i> <span>Pengaturan Profil </span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
          <ul class="treeview-menu">
            <li data-step="2" data-intro="Tambah produk anda disini" data-position="right" class="<?php echo $id == 2 ? 'active' : ''; ?>"><a href="<?php echo base_url('vendors/profil'); ?>"><i class="fa fa-angle-right"></i> Profil</a></li>
          </ul>
        </li>
        <li data-step="3" data-intro="Pengaturan toko anda" data-position="right" class="treeview <?php echo $id >= 3 && $id <= 7 ? 'active' : ''; ?>"> <a href="#"> <i class="icon-handbag"></i> <span>Pengaturan Toko </span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
          <ul class="treeview-menu">
            <li data-step="4" data-intro="Tambah produk anda disini" data-position="right" class="<?php echo $id == 4 ? 'active' : ''; ?>">
              <a href="<?php echo base_url('vendors/produk_tambah'); ?>"><i class="fa fa-angle-right"></i> Tambah Service</a>
            </li>
            <li data-step="5" data-intro="Lihat produk yang sudah tersimpan disini" data-position="right" class="<?php echo $id == 3 ? 'active' : ''; ?>">
              <a href="<?php echo base_url('vendors/produk'); ?>"><i class="fa fa-angle-right"></i> Daftar Service</a>
            </li>
            <li>
              <a href="<?php echo base_url('vendors/portfolio'); ?>"><i class="fa fa-angle-right"></i> Portfolio</a>
            </li>
            <li class="<?php echo $id == 7 ? 'active' : ''; ?>">
              <a href="<?php echo base_url('vendors/status_toko'); ?>"><i class="fa fa-angle-right"></i> Status Toko</a>
            </li>
          </ul>
        </li>
        <li data-step="6" data-intro="Disini tempat melihat produk yang dipesan oleh klien" data-position="right"class="<?php echo $id == 8 ? 'active' : ''; ?>"> <a href="<?php echo base_url('vendors/data_pesanan'); ?>"> <i class=" icon-basket-loaded"></i> <span>Data Pesanan<span class="badge badge-danger badge-pill" style="margin-left: 30%" id="total_notif_2"></span></span></a> 
        </li>
        <!-- <li class=""> <a href="<?php echo base_url('vendors/profil'); ?>"> <i class="icon-bubble"></i> <span>Live Chat</span></a> 
        </li> -->
      </ul>
    </section>
    <!-- /.sidebar --> 
  </aside>