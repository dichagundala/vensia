<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Produka extends CI_Model{
	

	public function get_product_by_kategori($uuid){
		$result = $this->db->query("select produk.uuid as 'uuid', vendor.nama, vendor_kategori.nama, parameter.nama as 'nama', produk_detail.value as 'value' from produk, produk_detail, parameter, vendor_kategori, vendor, trx_kategori_parameter where 
			produk.vendor_uuid = vendor.uuid and 
			produk.kategori_id = vendor_kategori.id and 
			produk_detail.produk_uuid = produk.uuid and
			produk_detail.kategori_parameter_id = trx_kategori_parameter.id and
			vendor_kategori.id = trx_kategori_parameter.kategori_id and 
			parameter.id = trx_kategori_parameter.parameter_id and vendor_kategori.nama like '$kategori'")->result_array();

		return $result;
	}
	public function get_produk_by_uuid($uuid){
		$result = $this->db->query("select produk.uuid as 'uuid', vendor.nama as 'nama_vendor', vendor_kategori.nama as 'nama_kategori', parameter.nama as 'nama', produk_detail.value as 'value' from produk, produk_detail, parameter, vendor_kategori, vendor, trx_kategori_parameter where 
			produk.vendor_uuid = vendor.uuid and 
			produk.kategori_id = vendor_kategori.id and 
			produk_detail.produk_uuid = produk.uuid and
			produk_detail.kategori_parameter_id = trx_kategori_parameter.id and
			vendor_kategori.id = trx_kategori_parameter.kategori_id and 
			parameter.id = trx_kategori_parameter.parameter_id and produk.uuid like '$uuid'")->result_array();

		return $result;
	}
	
	public function get_id_product($uuid){
		$result = $this->db->query("select DISTINCT(produk.uuid) as 'uuid' from produk, produk_detail, parameter, vendor_kategori, vendor, trx_kategori_parameter where 
			produk.vendor_uuid = vendor.uuid and 
			produk.kategori_id = vendor_kategori.id and 
			produk_detail.produk_uuid = produk.uuid and
			produk_detail.kategori_parameter_id = trx_kategori_parameter.id and
			vendor_kategori.id = trx_kategori_parameter.kategori_id and 
			parameter.id = trx_kategori_parameter.parameter_id and vendor.uuid like '$uuid'")->result_array();

		return $result;
	}
	
	public function get_transaksi($uuid){
		$hasil = $this->db->query("SELECT note, address_order FROM transaksi where uuid = '$uuid'")->result_array()[0];
		return $hasil;
	}

	public function get_transaksi_add($uuid){
		$hasil = $this->db->query("SELECT nama_add, qty_add FROM transaksi_add where uuid_transaksi = '$uuid'")->result_array()[0];
		return $hasil;
	}

}

?>