<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
    if($this->session->userdata('status') != "login"){
      redirect(site_url("dashboard/welcome"));
    }
		$this->load->library('session');
		$this->load->model('Portfolio_model');

	}
	public function index()
	{
		$data['data'] = $this->Portfolio_model->read('portfolio');
		$this->load->view('career',$data);
	}

	public function add()
	{
		$this->load->view('career-tambah');
	}

	public function simpan(){

            $config['upload_path']          = 'asset/images/';
            $config['allowed_types']        = 'jpg|png';
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('foto'))
            {
               $data = array(
                       'foto' 	    => $this->upload->data('file_name'),
                       'type' 		=> $this->input->post('type'),
                       'brief' 	=> $this->input->post('brief'),
                       'konten' 	=> $this->input->post('konten'),
                       'date_event'=> $this->input->post('date_event'),
                       'link' 		=> $this->input->post('link'),
                       'status' 	=> $this->input->post('status'),
                       'judul' 	=> $this->input->post('judul')
               );
               $response = $this->Portfolio_model->create('portfolio',$data);
               if ($response == 1) {
                       $this->session->set_flashdata('swal', 1);
               }else{
                       $this->session->set_flashdata('swal', 2);
               } 
               redirect('/dashboard/Portfolio');
                        // echo json_encode($response);
       }
       else
       {
                	// $error = array('error' => $this->upload->display_errors());
                	// print_r($error);
              $this->session->set_flashdata('swal', 3);
              redirect('/dashboard/Portfolio');
      }
}
}
