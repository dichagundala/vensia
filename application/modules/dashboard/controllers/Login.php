<?php 

class Login extends CI_Controller{

    function __construct(){
        parent::__construct();      
        $this->load->model('cro');

    }

    function index(){
        $this->load->view('welcome_message');
    }

    function aksi_login(){
        $data2 = $this->cro->cek_login('cro',array('username' => $this->input->post('username'), 
                                                  'password' => strtoupper(md5($this->input->post('password')))));
        if($data2 != NULL){
        $data = array('status'   => 'login',
                      'id'       => $data2['id'],
                      'username' => $data2['username'],
                      'password' => $data2['password'],
                      'akses'    => $data2['akses']);
        $this->session->set_userdata($data);
        // $this->load->view('dashboard/home', $data);
         redirect(site_url('dashboard/home'));
        }else{
            $this->session->set_flashdata('swal', 1);
            redirect(site_url("dashboard/welcome"));
        }
    }

    function logout(){
        $this->session->sess_destroy();
        redirect(site_url('dashboard/welcome'));
    }
}