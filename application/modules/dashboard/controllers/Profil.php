<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != "login"){
	      redirect(site_url("dashboard/welcome"));
	    }

		if($this->session->userdata('type') != "client"){
			redirect(base_url("index.php/Authss/login"));
		}else{
			$this->load->model("users");
			$this->load->model("transaksi");
			$this->load->model("invoice");
		}
	}
	public function index()
	{
		$data = array();
		if ($this->ion_auth->logged_in()){
			// set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$data['users'] = $this->ion_auth->users()->result();
			foreach ($data['users'] as $k => $user)
			{
				$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			$data['login'] = $this->ion_auth->is_admin();

		}

		$data = $this->users->get_client($this->session->userdata("uuid"));

		$data['invoice'] = $this->invoice->get_invoice($this->session->userdata("uuid"));
		
		$this->load->view('profil', $data);
	}

	public function update_profil()
	{
		$nama = $this->input->post('nama');
		$kelamin = $this->input->post('kelamin');
		$telp = $this->input->post('telp');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$alamat = $this->input->post('alamat');
		$email = $this->input->post('email');
		$img = $this->input->post('img');
		$uuid = $this->session->userdata('uuid');

		$data = array(
			'nama' => $nama,
			'kelamin' => $kelamin,
			'telp' => $telp,
			'tgl_lahir' => $tgl_lahir,
			'alamat' => $alamat,
			'email' => $email
		);

		if($this->users->update_client($data,$uuid)){
			$icon = "success";
			$text = "Berhasil update profile";
			$direct = base_url("index.php/profil");
		}else{
			$icon = "error";
//			$text = $this->db->last_query();
			$text = "Oops, sepertinya ada kesalahan, silahkan coba lagi nanti";
			$direct = base_url();
		}

		$result = array(
			"icon" => $icon,
			"text" => $text,
			"direct" => $direct
		);
		echo json_encode($result);
	}

	public function update_img()
	{
		$uuid = $this->session->userdata('uuid');

		$config = array(
			'upload_path' => "./uploads/",
			'allowed_types' => "jpg|png|jpeg",
			'overwrite' => TRUE,
			'max_size' => "2048000"
		);

		$this->load->library('upload', $config);
		if($this->upload->do_upload('file'))
		{
			$data = array('upload_data' => $this->upload->data()); //ambil file name yang diupload
 
            $image= $data['upload_data']['file_name'];

			$data = array(
				'img' => $image,
				'uuid' => $uuid
			);

			if($this->users->update_client_img($data)){
				$icon = "success";
				$text = "Berhasil update profile image";
				$direct = base_url("index.php/profil");
			}else{
				$icon = "error";
//			$text = $this->db->last_query();
				$text = "Oops, sepertinya ada kesalahan, silahkan coba lagi nanti";
				$direct = base_url();
			}

		}
		else
		{
			$icon = "error";
//			$text = $this->db->last_query();
			$text = $this->upload->display_errors();
			$direct = base_url();
		}

		$result = array(
			"icon" => $icon,
			"text" => $text,
			"direct" => $direct
		);
		echo json_encode($result);
	}
}
