<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
    if($this->session->userdata('status') != "login"){
      redirect(site_url("dashboard/welcome"));
    }
		//$this->load->library('session');
		//$this->load->model('Portfolio_model');
        $this->load->model("model_chat");
	}
	public function index()
	{
		//$data['data'] = $this->Portfolio_model->read('portfolio');
        //$this->load->view('career',$data);
        $this->load->model("model_chat");
		$data['user']	= $this->model_chat->getAll2(array("ip_user1 !=" => $this->session->userdata('id')));
		//$this->session->userdata('id')));
		$this->load->view('dashboard/chat',$data);
	}

	function getUserIpAddr(){
		if(!empty($_SERVER['HTTP_CLIENT_IP'])){
			//ip from share internet
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
			//ip pass from proxy
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
		
	}
	function login(){
		if($this->session->userdata('loggin')){
			redirect('index.php/welcome');
		}
		$this->load->model('model_user');
        $data['user']	= $this->model_chat->getAll(array('ip_user1 !=' => 
        1));
		//$this->session->userdata('id')));
		$this->load->view('dashboard/chat',$data);
	}
	
	function getChat(){
		//$this->load->model("model_user");
		$this->load->model("model_chat");
		
		$id_user	= $this->input->post("id_user",true); //tujuan
		//$id			= $this->session->userdata('id'); //dari
		$id			= $this->session->userdata('id'); //dari/user_1
		$id_max		= $this->input->post('id_max'); //dari
		$where	= "(((ip_user1 = '$id_user' AND admin = '$id') OR (admin = '$id_user' AND ip_user1 = '$id')) AND id > '$id_max')";
		$chat	= $this->model_chat->getAll($where);
		$data['id_max']		= $id_max;
		$data['ip_user1']	= $id_user;
		$data['chat'] 		= $chat;
		$this->load->view("vwChatBox",$data);
	}
	
	function getChatAll(){
	
		$id_user	= $this->input->post("id_user",true); //tujuan
		//$id			= $this->session->userdata('id'); //dari
		$id				= $this->session->userdata('id'); //dari/user_1
		$id_max		= $this->input->post('id_max'); //dari

		//$where	= "(((user_1 = '$id_user' AND user_2 = '$id') OR (user_2 = '$id_user' AND user_1 = '$id')))";
		$where	= "(((ip_user1 = '$id' AND admin = '$id_user') OR (admin = '$id' AND ip_user1 = '$id_user')))";
		$chat	= $this->model_chat->getAll($where);
		
		//$where2	= "(((user_1 = '$id_user' AND user_2 = '$id') OR (user_2 = '$id_user' AND user_1 = '$id')) AND id_chat > '$id_max')";
		$where2	= "(((ip_user1 = '$id' AND admin = '$id_user') OR (admin = '$id' AND ip_user1 = '$id_user')) AND id > '$id_max')";
		$get_id = $this->model_chat->getLastId($where2);
		echo "<script>alert('a')</script>";
		$data['id_max']		= $get_id['id'];
		$data['ip_user1']	= $id_user;
		$data['chat'] 		= $chat;
		$this->load->view("chat",$data);
	}
	
	function getLastId(){
		//$this->load->model("model_user");
		$this->load->model("model_chat");
		
		$id_user	= $this->input->post("id_user",true); //tujuan
		//$id			= $this->session->userdata('id'); //dari
		$id			= 1; //dari/user_1
		$id_max		= $this->input->post('id_max'); //dari
		
		$where	= "(((ip_user1 = '$id' AND admin = '$id_user') OR (admin = '$id_user' AND ip_user1 = '$id')) AND id > '$id_max')";
		$get_id = $this->model_chat->getLastId($where);
		
		echo json_encode(array("id" => $get_id['id'] != '' ?  $get_id['id'] : $id_max ));
	}
	
	function sendMessage(){
		$this->load->model("model_chat");
		$id_user	= $this->input->post("id_user",true); //tujuan/user_2
		//$id			= $this->session->userdata('id'); //dari/user_1
		$id			= $this->session->userdata('id');; //dari/user_1
		$pesan		= ($this->input->post("pesan",true));
		
		$data	= array(
			'ip_user1' => $id,
			'admin' => $id_user,
			'chat' => $pesan,
		);
		
		$query	=	$this->model_chat->getInsert($data);
		
		if($query){
			$rs = 1;
		}else{
			$rs	= 2;
		}
		
		echo json_encode(array("result"=>$rs));
		
	}
	
	function masuk(){
		$this->load->model('model_chat');
		
		$id		= $this->input->post('id_user',true);
		$data	= $this->model_chat->getAll(array('ip_user1' => $id))->row_array();
		
		$session = array('id' => $id, 'ip_user1' => $data['ip_user1'], 'loggin' => true);
		$this->session->set_userdata($session);
		redirect('index.php/welcome');
	}
	
	function keluar(){
		session_destroy();
		redirect("index.php/welcome/login");
	}
}
