<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masalah extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        if($this->session->userdata('status') != "login"){
        redirect(site_url("dashboard/welcome"));
        }
		$this->load->library('session');
		$this->load->model('Masalah_model');
               // $this->load->helper("file");
	}
	public function index()
	{
		$data['data'] = $this->Masalah_model->read('masalah');
		$this->load->view('masalah',$data);
	}

	public function add()
	{
		$this->load->view('masalah-tambah');
	}

        public function formedit()
        {                      
                $data = $this->Masalah_model->readbyid('masalah',$this->input->post('id'));
                $data['edit'] = true;
                $this->load->view('masalah-tambah',$data);
        }

        public function editsimpan()
        {
           
                $data = array(
                     'question'       => $this->input->post('pertanyaan'),
                     'answer'         => $this->input->post('jawaban')
                );
                $response = $this->Masalah_model->update($data,$this->input->post('id'),'masalah');
                if ($response == 1) {
                     $this->session->set_flashdata('swal', 1); //berhasil disimpan
                }else{
                     $this->session->set_flashdata('swal', 2); //gagal disimpan
                } 
             redirect('/dashboard/Masalah');
            
        }

        public function simpan(){
            $this->load->library('upload');
            //Configure upload.
            $this->upload->initialize(array(
                "allowed_types" => "gif|jpg|png|jpeg",
                "upload_path"   => "asset/images/portfolio/"
            ));
            //Proses Multiupload.
            if($this->upload->do_upload("foto")) {
                $uploaded = $this->upload->data();
                if ($uploaded['file_name'] == null) {
                  foreach ($uploaded as $key) {
                    $array_foto[] = $key['file_name'];
                  }
                  $name_foto = implode(",",$array_foto);
                }else{
                  $name_foto = $uploaded['file_name'];
                }
                
                // echo '<pre>';
                // var_export($uploaded);
                // echo '</pre>';
                 $data = array(
                               'foto'          => $name_foto,
                               'type'          => $this->input->post('type'),
                               'brief'         => $this->input->post('brief'),
                               'konten'        => $this->input->post('konten'),
                               'date_event'    => $this->input->post('date_event'),
                               'link'          => $this->input->post('link'),
                               'status'        => $this->input->post('status'),
                               'judul'         => $this->input->post('judul')
                          );
                          $response = $this->Masalah_model->create('masalah',$data);
                          if ($response == 1) {
                               $this->session->set_flashdata('swal', 1); //berhasil disimpan
                          }else{
                               $this->session->set_flashdata('swal', 2); //gagal disimpan
                          }
                       unset($array_foto); 
                       redirect('/dashboard/Masalah');
            }
            else{
                redirect('/dashboard/Masalah');
                $this->session->set_flashdata('swal', 3); //message bermasalah dengan foto
                // die('UPLOAD FAILED');
                // $data['error'] = implode('<br />',$error);
            } 
        }

        public function hapus(){
                $data['data'] = $this->Masalah_model->readbyid('masalah',$this->input->post('id'));
                $response = $this->Masalah_model->delete('masalah', $this->input->post('id'));
                        
                /*foreach ($data as $data => $value) {
                        $ex = explode(",",$value['foto']);
                       for ($i=0; $i <= sizeof($ex); $i++) {
                        if ($ex[$i] != null || $ex[$i] != " ") {
                                $file = './asset/images/portfolio/'.$ex[$i];
                                unlink($file);
                                // if (!unlink($file))
                                //   {
                                //         echo ("Error deleting $file \n");
                                //   }
                                // else
                                //   {     
                                //         echo ("Deleted $file \n");
                                //   }
                        }
                       }
                }*/
                echo json_encode($response);       
        }
}
