<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_customer extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != "login"){
      redirect(site_url("dashboard/welcome"));
    }
		$this->load->model('users');

	}
	public function index()
	{
		$data['users'] = $this->users->get_User();
		$data['all_user'] = $this->users->get1();
		$data['member'] = $this->users->get2();
		$data['client'] = $this->users->get3();
		$data['user_bulanan'] = $this->users->get4(date("Y/m/d"));
		$data['menu'] = "klien";
		$this->load->view('data-customer', $data);
	}

	public function edit(){
		$uuid = $this->input->post('uuid');
	}
	
	public function getbyuuid($uuid){
		
		$data['detail'] = $this->users->getall($uuid);

		$this->load->view('data-lengkap-users', $data);
	}
	
	public function banned(){
		$uuid = $this->input->post('uuid');
		if($this->users->banned_user($uuid)){
			echo "berhasil";
		}else{
			echo "gagal";
		}
		echo $this->db->last_query();
	}

}
