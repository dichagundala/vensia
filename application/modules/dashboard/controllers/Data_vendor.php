<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_vendor extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != "login"){
      redirect(site_url("dashboard/welcome"));
    }
		$this->load->model('vendor');
		$this->load->model('produk');
		

	}
	public function index()
	{
		$data['menu'] = "vendor";
		$vendor = $this->vendor->get();
		$data['count_vendor'] = $this->vendor->get1();
		$data['count_vendor1'] = $this->vendor->get2(date("Y/m/d"));
		$data['status_aktif'] = $this->vendor->get3();
		$data['servis'] = $this->produk->get_total_service1();
		$result = array();
		$data_json['result'] = array();
		foreach($vendor as $key1){			
			$result = $this->produk->get_total_service($key1['uuid']);
			array_push($data_json['result'], array("id" => $key1['id'],
				"uuid" => $key1['uuid'],
				"namaVendor" => $key1['namaVendor'],
				"namaPemilik" => $key1['namaPemilik'],
				"bioVendor" => $key1['bioVendor'],
				"alamatVendor" => $key1['alamatVendor'],
				"telpVendor" => $key1['telpVendor'],
				"kategori" => $key1['kategori'],
				"website" => $key1['website'],
				"whatsapp" => $key1['whatsapp'],
				"facebook" => $key1['facebook'],
				"line" => $key1['line'],
				"instagram" => $key1['instagram'],
				"telp" => $key1['telp'],
				"email" => $key1['email'],
				"vCreated" => $key1['vCreated'],
				"vEdited" => $key1['vEdited'],
				"aktifVendor" => $key1['aktifVendor'], 
				"hasil" => $result[0]['hasil']));
		}
		$data['vendor'] = $data_json['result'];
		$this->load->view('data-vendor', $data);
	}

	public function edit(){
		$uuid = $this->input->post('uuid');
	}

	public function proses(){
		$uuid = $this->input->post('uuid');
		if($this->vendor->prosesaktivasi($uuid)){
			echo "berhasil";
		}else{
			echo "gagal";
		}
		echo $this->db->last_query();
	}
	//untuk konfirasi terima vendor dari CRO
	public function setuju_vendor(){
		$uuid = $this->input->post('uuid');
		if($this->vendor->aktivasisetuju($uuid)){
			echo "berhasil";
		}else{
			echo "gagal";
		}
		echo $this->db->last_query();
	}
	//untuk konfirasi tolak vendor dari CRO
	public function tolak_vendor(){
		$uuid = $this->input->post('uuid');
		if($this->vendor->aktivasitolak($uuid)){
			echo "berhasil";
		}else{
			echo "gagal";
		}
		echo $this->db->last_query();
	}
	//untuk mail Konfirmasi Vendor
	public function mail_aktivasi_post(){
        $uuid = $this->input->post('uuid');

        if($this->Vendor_model->aktivasi_user($uuid)){
            $vendor = $this->Vendor_model->get_uuid($uuid);
            $email = $vendor[0]->email;
            $nama = $vendor[0]->nama;

        # Instantiate the client.
            $mgClient = new Mailgun('54c915413905fdcbe811213e8f59be3f-7efe8d73-1855dae2');
            $domain = "mg.yepsindonesia.com";
            $data['email'] = $email;
        # Make the call to the client.
			$result = $mgClient->sendMessage($domain, array(
				'from'    => 'YEPS Indonesia no-reply@yepsindonesia.com',
				'to'      => $nama.' '.$email,
				'subject' => 'Selamat akun anda telah di verifikasi menjadi Vendor YEPS Indonesia',
				//'text'    => 'Do Not Reply Email from This!',
				'html'    => $this->load->view('validasi-sukses-vendor',$data,TRUE)
			));

			echo json_encode(array("status" => "true"));
		}
		else{
			echo json_encode(array("status" => "false"));
		}
    }
	
		public function banned(){
			$uuid = $this->input->post('uuid');
			if($this->vendor->banned_vendor($uuid)){
				echo "berhasil";
			}else{
				echo "gagal";
			}
			echo $this->db->last_query();
		}

	public function produk($uuid){
		$data['menu'] = "vendor";
		$vendor = $this->produk->get_produk_by_vendor($uuid);
		$result = array();
		$data_json['result'] = array();
		foreach($vendor as $key1){
			$a = array();
			$b = array();
			$result = $this->produk->get_produk_by_uuid($key1['uuid']);
			foreach($result as $key2){
				if($key2['nama'] == "Foto" || $key2['nama'] == "Harga Promosi" || $key2['nama'] == "Harga Normal" || $key2['nama'] == "Nama"){
					array_push($a, $key2['nama']);
					array_push($b, $key2['value']);
				}
			}
			$array_dummy = array_combine($a, $b);
			array_push($data_json['result'], array("uuid" => $key1['uuid'], "kategori" => $key2['kategori'], "tanggal" => $key2['tanggal'], "status" => $key1['status_produk'], "vendor" => $result[0]['nama_vendor'], "parameter" => $array_dummy));
		}
		$data['produk'] = $data_json['result'];
		
		$this->load->view('produk-vendor', $data);
	}

	public function detail($uuid){
		$vendor = $this->produk->get_produk_by_vendor($uuid);
		$result = array();
		$data_json['result'] = array();
		foreach($vendor as $key1){
			$a = array();
			$b = array();
			$result = $this->produk->get_produk_by_uuid($key1['uuid']);
			foreach($result as $key2){
				if($key2['nama'] == "Foto" || $key2['nama'] == "Harga Promosi" || $key2['nama'] == "Harga Normal" || $key2['nama'] == "Foto" || $key2['nama'] == "Nama"){
					array_push($a, $key2['nama']);
					array_push($b, $key2['value']);
				}
			}
			$array_dummy = array_combine($a, $b);
			array_push($data_json['result'], array("uuid" => $key1['uuid'], "status" => $key1['status_produk'], "slug" => $key1['slug'], "vendor" => $result[0]['nama_vendor'], "parameter" => $array_dummy));
		}
		$data['produk'] = $data_json['result'];
		$this->load->view('detail-vendor', $data);
	}

	public function getbyuuid($uuid){
		
		$data['detail'] = $this->vendor->getall($uuid);

		$this->load->view('data-lengkap-vendor', $data);
	}

	public function status_produk(){
		$uuid = $this->input->post('uuid');
		$status = $this->input->post('status');
		if($this->produk->status_produk_update($uuid, $status)){
			$type = "success";
			$text = "Berhasil mengubah status produk";
		}else{
			$type = "warning";
			$text = "Gagal mengubah status produk";
		}

		$rs = array(
			"icon" => $type,
			"text" => $text
		);
		echo json_encode($rs);
	}

	public function request(){
		$data['vendor'] = $this->vendor->getubah();
		$this->load->view('req-edit-data',$data);
	}
	public function accepted_data_vendor($id){
		$this->vendor->accepted_cro($id);
		$data1= array();
		$data2= array();
		$data1 = $this->vendor->getubah_byid($id);
		if($data1['kategori'] == ""){
			$data2['alamatVendor']=$data1['alamatVendor'];
			$data2['vProvinsi']=$data1['vProvinsi'];
			$data2['vKabupaten']=$data1['vKabupaten'];
			$data2['vKecamatan']=$data1['vKecamatan'];
			$data2['vKelurahan']=$data1['vKelurahan'];
			$uuid=$data1['uuid'];

			$this->vendor->ubah_vendor($uuid,$data2);
		}
		else{
			$data2['kategori']=$data1['kategori'];
			$uuid=$data1['uuid'];
			$this->vendor->ubah_vendor($uuid,$data2);
		}
		redirect(base_url("dashboard/data_vendor/request"));

	}
	public function rejected_data_vendor($id){
		$this->vendor->rejected_cro($id);
		redirect(base_url("dashboard/data_vendor/request"));
	}

	public function riwayat(){
		$data['vendor'] = $this->vendor->lihat_ubah();
		$this->load->view('req-edit-data_riwayat',$data);
	}

}


