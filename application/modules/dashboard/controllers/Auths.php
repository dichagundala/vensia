<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auths extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//$this->load->library("bcrypt");
		$this->load->model("users");
		$this->load->library('email');

		$this->load->library('uuid');
	}

	public function index()
	{
		redirect('/auth/login/', 'refresh');
	}

	public function login($action = "", $uuid = ""){
		$this->load->view('login');
		
	}

	public function user(){
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}

		$data['menu'] = "cro";

		$this->load->view('data-cro', $data);
	}
	public function cek_login(){
		$email = $this->input->post("email");
		$password = $this->input->post("password");
		$hasil = $this->users->cek_user($email);
		
		$rs = $hasil->result_array();
		if($hasil->num_rows() >= 1 && $this->bcrypt->verify($password, $rs[0]['password']) ){

			$datauser = array(
				'nama' => $rs[0]['nama'],
				'uuid' => $rs[0]['uuid'],
				'type' => "client"
			);

			$this->session->set_userdata($datauser);
			$icon = "success";
			$text = "Selamat datang ".$datauser['nama'];
			$direct = base_url('index.php/profil');
		}else{
			$icon = "error";
			$text = "Password atau email Anda salah";
			$direct = base_url();

		}

		$result = array(
			'icon' => $icon,
			'text' => $text,
			'direct' => $direct
		);

		echo json_encode($result);
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url(''));
	}

	public function register(){
		$this->load->view('register');
	}

	public function forgotpassword(){
		$this->load->view('forgot-password');
	}

	public function verification($verifikasi){
		$auth_email = $verifikasi;
		if($this->users->verifikasi_email($auth_email) == 1){
			redirect(base_url('index.php/Authss/login'));
		}
	}

	public function verification_forgetPass($verifikasi){
		$auth_email = $verifikasi;
		if($this->users->verifikasi_forgetPass($auth_email) == 0){
			redirect(base_url('index.php/Authss/login'));
		}else{
			$data['uuid'] = $this->users->get_client_forget($auth_email);
			$this->load->view('new_pass', $data);
		}
	}

	public function change_pass(){
		$uuid = $this->input->post('uuid');
		$password = $this->input->post('pass');
		$password = $this->bcrypt->hash($password);
		$this->db->query("UPDATE t_user set password = '$password' where uuid = '$uuid'");
		if(($this->db->affected_rows() >= 1)){
			$text = "Yeps, password berhasil diubah, silahkan login menggunakan password baru Anda";
			$icon = "success";
			$direct = base_url("Authss/login");
		}else{
			$text = "Oops, sepertinya ada kesalahan, coba lagi nanti";
			$icon = "info";
			$direct = "";
		}

		$data = array(
			"text" => $text,
			"icon" => $icon,
			"direct" => $direct
		);

		echo json_encode($data);
	}

	public function generateForgot(){
		$email = $this->input->post('email');
		$direct = "";
		if( $this->users->cek_account($email)){

			$auth_forget = md5(date("Y-m-d H:i:s").$this->input->post('email'));
			if($this->users->update_auth_email($email, $auth_forget)){

				$data = array(
					"name" => "FORGOT",
					"email" => $email,
					"auth_email" => $auth_forget
				);


				$this->email->from('test@yepsindonesia.com', 'YEPS');
				$this->email->to($this->input->post('email'));
				$this->email->subject('Forget Password');
				$this->email->message($this->load->view('email/forgot',$data,true));
				if ( ! $this->email->send())
				{
					//$report = $this->email->print_debugger();
					$icon = "error";
					$text = "Oops, sepertinya ada kesalahan, silahkan coba lagi nanti";
					$direct = "";
				}else{
					$icon = "success";
					$text = "Silahkan cek email Anda.";
					$direct = "";
				}
			}else{
				$icon = "error";
				$text = "Oops, sepertinya ada kesalahan, silahkan coba lagi nanti";
				$direct = "";
			}
		}else{
			$icon = "error";
			$text = "Oops, sepertinya ada kesalahan, silahkan coba lagi nanti";
			$direct = "";
		}

		$data_register = array(
			"icon" => $icon,
			"text" => $text,
			"direct" => $direct
		);

		echo json_encode($data_register);
		return true;
	}

	public function clientRegister(){

		$auth_email = md5(date("Y-m-d H:i:s").$this->input->post('email'));
		$data = array(
			"name" => $this->input->post('fullName'),
			"email" => $this->input->post('email'),
			"auth_email" => $auth_email
		);

		if($this->users->cek_account($this->input->post('email'))){
			$text = "Jika anda sudah melakukan pendaftaran, silahkan konfirmasi akun anda terlebih dahulu.<br>
			Jika anda lupa lupa password, silahkan klik <a href='".base_url('authss/forgotpassword')."'>disini</a>.";
			$icon = "info";
			$direct = "";
		}else{
			$this->load->library('email');

			$this->email->from('test@yepsindonesia.com', 'YEPS Indonesia');
			$this->email->to($this->input->post('email'));
			$this->email->subject('Registrasi YEPS Indonesia');
			$this->email->message($this->load->view('email/registration',$data,true));

			if ( ! $this->email->send())
			{
				$report = $this->email->print_debugger();
				$icon = "error";
				$text = "Oops, sepertinya ada kesalahan, silahkan coba lagi nanti";
				$direct = "";
			}else{

				$data_register = array(
					"nama" => $this->input->post('fullName'),
					"email" => $this->input->post('email'),
					"telp" => $this->input->post('phone'),
					"password" => $this->bcrypt->hash($this->input->post('password')),
					"auth_email" => $auth_email,
					"uuid" => md5($this->input->post('email').rand())
				);

				$this->users->input_data($data_register);
				$icon = "success";
				$text = "Silahkan cek email Anda untuk melakukan aktifasi akun";
				$direct = base_url();

			}
		}

		$rs = array(
			"icon" => $icon,
			"text" => $text,
			"direct" => $direct
		);

		echo json_encode($rs);

	}
}
