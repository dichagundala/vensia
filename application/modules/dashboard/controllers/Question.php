<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        if($this->session->userdata('status') != "login"){
        redirect(site_url("dashboard/welcome"));
        }
		$this->load->library('session');
		$this->load->model('Question_model');
               // $this->load->helper("file");
	}
	public function index()
	{
		$data['data'] = $this->Question_model->read('q_and_a');
		$this->load->view('question',$data);
	}

	public function add()
	{
		$this->load->view('question-tambah');
	}

        public function formedit()
        {                      
                $data = $this->Question_model->readbyid('q_and_a',$this->input->post('id'));
                $data['edit'] = true;
                $this->load->view('question-tambah',$data);
        }

        public function editsimpan()
        {
           
                $data = array(
                     'question'       => $this->input->post('pertanyaan'),
                     'answer'         => $this->input->post('jawaban')
                );
                $response = $this->Question_model->update($data,$this->input->post('id'),'q_and_a');
                if ($response == 1) {
                     $this->session->set_flashdata('swal', 1); //berhasil disimpan
                }else{
                     $this->session->set_flashdata('swal', 2); //gagal disimpan
                } 
             redirect('/dashboard/Question');
            
        }

	public function simpan(){
     
        // echo '<pre>';
        // var_export($uploaded);
        // echo '</pre>';
         $data = array(
                       'question'      => $this->input->post('pertanyaan'),   
                       'answer'        => $this->input->post('jawaban')
                  );
                  $response = $this->Question_model->create('q_and_a',$data);
                  if ($response == 1) {
                       $this->session->set_flashdata('swal', 1); //berhasil disimpan
                  }else{
                       $this->session->set_flashdata('swal', 2); //gagal disimpan
                  }
               unset($array_foto); 
               redirect('/dashboard/Question');
    
	}

        public function hapus(){
                $data['data'] = $this->Question_model->readbyid('q_and_a',$this->input->post('id'));
                $response = $this->Question_model->delete('q_and_a', $this->input->post('id'));
                        
                /*foreach ($data as $data => $value) {
                        $ex = explode(",",$value['foto']);
                       for ($i=0; $i <= sizeof($ex); $i++) {
                        if ($ex[$i] != null || $ex[$i] != " ") {
                                $file = './asset/images/portfolio/'.$ex[$i];
                                unlink($file);
                                // if (!unlink($file))
                                //   {
                                //         echo ("Error deleting $file \n");
                                //   }
                                // else
                                //   {     
                                //         echo ("Deleted $file \n");
                                //   }
                        }
                       }
                }*/
                echo json_encode($response);       
        }
}
