<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_produk extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != "login"){
      redirect(site_url("dashboard/welcome"));
    }
		$this->load->model('produk');

	}
	public function index()
	{
		$data['menu'] = "produk";
		
		$vendor = $this->produk->get_produk_all();
		$result = array();
		$data_json['result'] = array();
		foreach($vendor as $key1){
			$a = array();
			$b = array();
			$result = $this->produk->get_produk_by_uuid($key1['uuid']);
			foreach($result as $key2){
				if($key2['nama'] == "Foto" || $key2['nama'] == "Harga Promosi" || $key2['nama'] == "Harga Normal" || $key2['nama'] == "Foto" || $key2['nama'] == "Nama"){
					array_push($a, $key2['nama']);
					array_push($b, $key2['value']);
				}
			}

			$array_dummy = array_combine($a, $b);
			array_push($data_json['result'], array("uuid" => $key1['uuid'], "status" => $key1['status_produk'], "tanggal" => $result[0]['tanggal'], "vendor" => $result[0]['nama_vendor'], "kategori" => $result[0]['kategori'], "slug" => $result[0]['slug'], "parameter" => $array_dummy));
		}

		$data['produk'] = $data_json['result'];
		
		$this->load->view('produk-vendor', $data);
	}

	public function status_produk(){
		$uuid = $this->input->post('uuid');
		$status = $this->input->post('status');
		if($this->produk->status_produk_update($uuid, $status)){
			$type = "success";
			$text = "Berhasil mengubah status produk";
		}else{
			$type = "warning";
			$text = "Berhasil mengubah status produk";
		}

		$rs = array(
			"icon" => $type,
			"text" => $text
		);
		echo json_encode($rs);
	}

}
