<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){	
	parent::__construct();
	$this->load->model("vendor");
		if($this->session->userdata('status') != "login"){
			redirect(site_url("dashboard/welcome"));
		}
	}
	public function index()
	{
		$this->load->view('home');
	}
	
	public function notif(){    
		header('Content-Type: application/json');
		
        echo json_encode($this->vendor->notif_ubah());
    }
}
