<?php
use Mailgun\Mailgun;
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_pesanan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != "login"){
      redirect(site_url("dashboard/welcome"));
    }

		$this->load->model("invoice");
		$this->load->model("transaksi");
		$this->load->model("users");
		$this->load->model("cicilan");
		$this->load->library('uuid');
		$this->load->helper('tgl_indo');

		$params = array('server_key' => 'SB-Mid-server-IalaLlr_GXaUaVpYoziUfJLr', 'production' => false);
		$this->load->library('midtrans');
		$this->midtrans->config($params);
		$this->load->helper('url');	

	}
	public function index()
	{
		$result = $this->invoice->get();
		$invoice = array();
		foreach ($result as $key) {
			$date = $this->transaksi->get_min_date($key['Invoice Uuid']);
			$client = $this->transaksi->get_client($key['Invoice Uuid']);
			array_push($invoice, array(
				"Invoice Uuid" => $key['Invoice Uuid'], 
				"Kode Invoice" => $key['Kode Invoice'], 
				"Client" => $client[0]['nama'], 
				"Status Invoice" => $key['Status Invoice'], 
				"Tanggal Pesanan" => $key['Tanggal Pesanan'], 
				"Tanggal Event" => $date[0]['Tanggal Event']
			));
		}

		$data['invoice'] = $invoice;
		$data['menu'] = "pesanan";
		$this->load->view('pesanan', $data);
	}

	public function detail($uuid)
	{
		$transaksi = $this->transaksi->get($uuid);
		$transaksi_add = $this->transaksi->get1($uuid);
		$client = $this->users->get($transaksi[0]['uuid_user']);
		$invoice = $this->invoice->get_by_uuid($uuid);
		$cicilan = $this->cicilan->get($uuid);
		$cicilan1 = $this->cicilan->get1($uuid);
		$data['invoice'] = $invoice;
		$data['transaksi'] = $transaksi;
		$data['transaksi_add'] = $transaksi_add;
		$data['client'] = $client[0];
		$data['menu'] = "pesanan";
		$data['cicilan'] = $cicilan;
		$data['cicilan1'] = $cicilan1;
		$temp = $this->db->query("select * from bukti_transfer where uuid like '$uuid'")->result_array();
		if(isset($temp[0])){
			$data['bukti'] = $temp[0];
		}
		$this->load->view('detail-pesanan',$data);
	}

	public function konfirmasi(){

		$uuid = $this->input->post("uuid");
		if($this->invoice->konfirmasi($uuid)){
			$icon = "success";
			$text = "Pesanan berhasil di konfirmasi";
		}else{
			$icon = "info";
			$text = "Oops, coba lagi nanti";
		}

		$data = array(
			'icon' => $icon,
			'text' => $text
		);

		echo json_encode($data);
		
	}

	public function failed(){

		$uuid = $this->input->post("uuid");
		if($this->invoice->failed($uuid)){
			$icon = "success";
			$text = "Pesanan berhasil di batalkan";
		}else{
			$icon = "info";
			$text = "Oops, coba lagi nanti";
		}

		$data = array(
			'icon' => $icon,
			'text' => $text
		);

		echo json_encode($data);
		
	}

	public function konfirmTransfer($uuid){

		if($this->invoice->transfer($uuid, 'DIBAYAR (DP)')){
			$icon = "success";
			$text = "Pembayaran berhasil di konfirmasi";
		}else{
			$icon = "info";
			$text = "Oops, coba lagi nanti";
		}

		$data = array(
			'icon' => $icon,
			'text' => $this->db->last_query()
		);

		echo json_encode($data);
		
	}

	public function editCicilan($uuid_invoice){
		$uuid = $this->input->post('uuid');
		$biaya = $this->input->post('biaya');
		$tanggal = $this->input->post('tanggal');
		for ($i=0; $i < count($uuid) ; $i++) {
			$insert = array(
				"uuid_invoice" => $uuid_invoice,
				"uuid" => $this->uuid->v4(),
				"biaya_cicilan" => $biaya[$i],
				"tanggal" => $tanggal[$i]
			);

			$update = array(
				"biaya_cicilan" => $biaya[$i],
				"tanggal" => $tanggal[$i]
			);
			$this->cicilan->edit($uuid[$i], $insert, $update);
		}
	}

	public function lunasCicilan($uuid){
		$this->cicilan->lunas($uuid);
	}

	public function va(){      
		$url = 'https://my.ipaymu.com/api/getva';  

		$params = array(   
			"key" => "RwcnGa7MC9A15jvJ2xufU50uQrtuU1",
			"uniqid" => $this->input->post('invoice'),
			"price" => $this->input->post('price'),
			"unotify" => "http://localhost/api15/notify.php"
		);

		$params_string = http_build_query($params);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, count($params));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$request = curl_exec($ch);

		if ( $request === false ) {
			echo 'Curl Error: ' . curl_error($ch);
		} else {
			$result = json_decode($request, true);
			
			$biaya_dp = $this->input->post('price');
			$biaya_total = $this->input->post('total_price');
			$uuid = $this->input->post('uuid');
			$this->db->query("update invoice set total_biaya = '$biaya_total', biaya_dp = '$biaya_dp' where uuid like '$uuid'");
			echo $this->db->last_query();
			$this->invoice->ipaymu($this->input->post('uuid'), $result['va'], $result['id']);
		}
		$data = array(
			"dp" => $this->input->post('price'),
			"total" => $this->input->post('total_price')
		);

		$this->email_client($this->input->post('uuid'), "va");

		curl_close($ch); 
	}



	public function email_client($uuid, $tipe){

		$transaksi = $this->transaksi->get($uuid);
		$client = $this->users->get($transaksi[0]['uuid_user']);
		$invoice = $this->invoice->get_by_uuid($uuid);
		$cicilan = $this->cicilan->get("e8af604a-6e57-42ab-aa7e-d25f9d0b5fef");
		$invoice[0]['Tanggal Konfirm'];
		$tanggal = new Datetime($invoice[0]['Tanggal Konfirm']);
		$tanggal->modify('+1 day');
		$temp = explode("-", $invoice[0]['Metode Pembayaran']);
		if($tipe == "transfer"){
			$rek = $this->invoice->get_rek($temp[1]);
		}else{
			$rek = $this->invoice->get_va($uuid);
		}
		$data = array(
			"email" => $client[0]['email'],
			"nama" => $client[0]['nama'],
			"batas_tanggal" => longdate_indo($tanggal->format('Y-m-d')),
			"batas_jam" => $tanggal->format('H.i'),
			"dp" => $invoice[0]['Biaya DP'],
			"metode" => $temp[0],
			"bank" => $rek[0]['bank_number'],
			"bank_account" => $rek[0]['account_name'],
			"invoice" => $invoice[0]['Kode Invoice'],
			"angsuran" => $invoice[0]['Cicilan'],
			"transaksi" => $transaksi
		);

		if($this->generateEmail("testing", 'email/menunggupembayaran_client', $data)){
			$icon = "success";
			$text = "Berhasil";
		}else{
			$icon = "info";
			$text = "gagal";
		}


		$hasil = array(
			"icon" => $icon,
			"text" => $text
		);
		return json_encode($hasil);
	}

	public function generateEmail($subject, $tpl, $data){
		$mgClient = new Mailgun('54c915413905fdcbe811213e8f59be3f-7efe8d73-1855dae2');
		$domain = "mg.yepsindonesia.com";
        $result = $mgClient->sendMessage($domain, array(
			'from'    => 'YEPS Indonesia no-reply@yepsindonesia.com',
			'to'      => $data['email'],
			'subject' => $subject,
			'html'    => $this->load->view($tpl, $data, TRUE)
		));
		if($result->http_response_code == 200){
			return true;
		}else{
			return false;
		}

	}

	public function midtrans_data(){
		$response = $this->input->post("data");

		$midtrans = json_decode($response);

		$transactions_id =  $midtrans->transaction_id;
		$invoice =  $midtrans->order_id;
		$payment_type = $midtrans->payment_type;
		$bank =  $midtrans->va_numbers[0]->bank;
		$va = $midtrans->va_numbers[0]->va_number;
		$pdf = $midtrans->pdf_url;

		$data = array(
			"id_ipaymu" => $va,
			"no_ipaymu" => $transactions_id,
			"pdf_link" => $pdf,
			"response" => $response
		);

		if($this->invoice->midtrans($data, $invoice)){
			$icon = "success";
			$text = "Berhasil";
		}else{
			$icon = "info";
			$text = "Gagal";
		}

		$hasil = array(
			"icon" => $icon,
			"text" => $text
		);

		echo json_encode($hasil);

	}
}