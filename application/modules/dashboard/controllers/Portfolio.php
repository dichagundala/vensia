<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
    if($this->session->userdata('status') != "login"){
      redirect(site_url("dashboard/welcome"));
    }
		$this->load->library('session');
		$this->load->model('Portfolio_model');
                $this->load->helper("file");
	}
	public function index()
	{
		$data['data'] = $this->Portfolio_model->read('portfolio');
		$this->load->view('portfolio',$data);
	}

	public function add()
	{
		$this->load->view('portfolio-tambah');
	}

        public function formedit()
        {                      
                $data = $this->Portfolio_model->readbyid('portfolio',$this->input->post('id'));
                $data['edit'] = true;
                $this->load->view('portfolio-tambah',$data);
        }

        public function editsimpan()
        {
            $this->load->library('upload');
            //Configure upload.
            $this->upload->initialize(array(
                "allowed_types" => "gif|jpg|png|jpeg",
                "upload_path"   => "asset/images/portfolio/"
            ));
            //Proses Multiupload.
            if($this->upload->do_upload("foto")) {
                $uploaded = $this->upload->data();
                if ($uploaded['file_name'] == null) {
                  foreach ($uploaded as $key) {
                    $array_foto[] = $key['file_name'];
                  }
                  $name_foto = implode(",",$array_foto);
                }else{
                  $name_foto = $uploaded['file_name'];
                }

                // echo '<pre>';
                // var_export($uploaded);
                // echo '</pre>';
                 $data = array(
                               'foto'          => $name_foto,
                               'type'          => $this->input->post('type'),
                               'brief'         => $this->input->post('brief'),
                               'konten'        => $this->input->post('konten'),
                               'date_event'    => $this->input->post('date_event'),
                               'link'          => $this->input->post('link'),
                               'status'        => $this->input->post('status'),
                               'judul'         => $this->input->post('judul')
                          );
                 $foto['data'] = $this->Portfolio_model->readbyid('portfolio',$this->input->post('id'));
                 foreach ($foto as $foto => $value) {
                        $ex = explode(",",$value['foto']);
                       for ($i=0; $i <= sizeof($ex); $i++) {
                        if ($ex[$i] != null || $ex[$i] != " ") {
                                $file = './asset/images/portfolio/'.$ex[$i];
                                unlink($file);
                                // if (!unlink($file))
                                //   {
                                //         echo ("Error deleting $file \n");
                                //   }
                                // else
                                //   {     
                                //         echo ("Deleted $file \n");
                                //   }
                        }
                       }
                }
            $response = $this->Portfolio_model->update($data,$this->input->post('id'),'portfolio');
                          if ($response == 1) {
                               $this->session->set_flashdata('swal', 1); //berhasil disimpan
                          }else{
                               $this->session->set_flashdata('swal', 2); //gagal disimpan
                          } 
                       unset($array_foto);
                       redirect('/dashboard/Portfolio');
            }
            else{
                $data = array(
                     'type'          => $this->input->post('type'),
                     'brief'         => $this->input->post('brief'),
                     'konten'        => $this->input->post('konten'),
                     'date_event'    => $this->input->post('date_event'),
                     'link'          => $this->input->post('link'),
                     'status'        => $this->input->post('status'),
                     'judul'         => $this->input->post('judul')
                );
                $response = $this->Portfolio_model->update($data,$this->input->post('id'),'portfolio');
                if ($response == 1) {
                     $this->session->set_flashdata('swal', 1); //berhasil disimpan
                }else{
                     $this->session->set_flashdata('swal', 2); //gagal disimpan
                } 
             redirect('/dashboard/Portfolio');
            }
        }

	public function simpan(){
    $this->load->library('upload');
    //Configure upload.
    $this->upload->initialize(array(
        "allowed_types" => "gif|jpg|png|jpeg",
        "upload_path"   => "asset/images/portfolio/"
    ));
    //Proses Multiupload.
    if($this->upload->do_upload("foto")) {
        $uploaded = $this->upload->data();
        if ($uploaded['file_name'] == null) {
          foreach ($uploaded as $key) {
            $array_foto[] = $key['file_name'];
          }
          $name_foto = implode(",",$array_foto);
        }else{
          $name_foto = $uploaded['file_name'];
        }
        
        // echo '<pre>';
        // var_export($uploaded);
        // echo '</pre>';
         $data = array(
                       'foto'          => $name_foto,
                       'type'          => $this->input->post('type'),
                       'brief'         => $this->input->post('brief'),
                       'konten'        => $this->input->post('konten'),
                       'date_event'    => $this->input->post('date_event'),
                       'link'          => $this->input->post('link'),
                       'status'        => $this->input->post('status'),
                       'judul'         => $this->input->post('judul')
                  );
                  $response = $this->Portfolio_model->create('portfolio',$data);
                  if ($response == 1) {
                       $this->session->set_flashdata('swal', 1); //berhasil disimpan
                  }else{
                       $this->session->set_flashdata('swal', 2); //gagal disimpan
                  }
               unset($array_foto); 
               redirect('/dashboard/Portfolio');
    }
    else{
        redirect('/dashboard/Portfolio');
        $this->session->set_flashdata('swal', 3); //message bermasalah dengan foto
        // die('UPLOAD FAILED');
        // $data['error'] = implode('<br />',$error);
    } 
	}

        public function hapus(){
                $data['data'] = $this->Portfolio_model->readbyid('portfolio',$this->input->post('id'));
                $response = $this->Portfolio_model->delete('portfolio', $this->input->post('id'));
                        
                foreach ($data as $data => $value) {
                        $ex = explode(",",$value['foto']);
                       for ($i=0; $i <= sizeof($ex); $i++) {
                        if ($ex[$i] != null || $ex[$i] != " ") {
                                $file = './asset/images/portfolio/'.$ex[$i];
                                unlink($file);
                                // if (!unlink($file))
                                //   {
                                //         echo ("Error deleting $file \n");
                                //   }
                                // else
                                //   {     
                                //         echo ("Deleted $file \n");
                                //   }
                        }
                       }
                }
                echo json_encode($response);       
        }
}
