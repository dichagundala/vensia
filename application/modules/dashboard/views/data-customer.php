<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>

<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Data Client <a href="#" class="btn btn-default btn-sm">Add Client</a></h1>
    </div>

    <div class="col-lg-3 col-md-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-group fa-4x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge"><?php echo $all_user; ?></div>
              <div>Users YEPS</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-user fa-4x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge"><?php echo $member; ?></div>
              <div>Members YEPS</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-user fa-4x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge"><?php echo $client; ?></div>
              <div>Clients YEPS</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-user fa-4x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge"><?php echo $user_bulanan; ?></div>
              <div>User Bulan <?php 
              $bulan = array (1 =>   'Januari',
                        'Februari',
                        'Maret',
                        'April',
                        'Mei',
                        'Juni',
                        'Juli',
                        'Agustus',
                        'September',
                        'Oktober',
                        'November',
                        'Desember'
                      );
              echo $bulan[date('n')];
              ?></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->  
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
         Data Client                     
       </div>
       <!-- /.panel-heading -->
       <div class="panel-body">

        <form class="form-inline">
          <div class="form-group mb-3">
            <label>Filter : </label>
          </div>
          <select class="custom-select custom-select-sm form-control form-control-sm mx-sm-3 mb-3" id="table-filter">
            <option value="" selected="">All</option>
            <option value="Member">Member</option>
            <option value="Client">Client</option>
          </select>
          <select class="custom-select custom-select-sm form-control form-control-sm mx-sm-3 mb-3" id="table-filter1">
            <option selected="">Aktif</option>
            <option>Banned</option>
          </select>
          <button type="submit" class="btn btn-peimary btn-sm mb-3">Filter</button>
        </form>
        <hr>

        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Alamat</th>
              <th>Kontak</th>
              <th>Kelamin</th>
              <th>Type</th>
              <th>Status</th>
              <th>Tanggal Daftar</th>
            </tr>
          </thead>
          <tbody>                                 
            <?php
            $i = 0;
            foreach ($users as $data) {
              $i++;
              ?>
              <tr>
                <td><a href="" data-toggle="modal" data-target="#detail-users" data-uuid="<?php echo $data['uuid']; ?>" class="detail-users"><?php echo $data['namaDepan']?>&nbsp;<?php echo $data['namaBelakang']?></a></td>
                <td><?php echo $data['address']?></td>
                <td>
                  <i class="fa fa-phone"></i> : <?php echo $data['telp']?> <br> 
                  <i class="fa fa-envelope "></i> : <?php echo $data['email']?>
                </td>
                <?php 
                  $data['gender'];
                  if ($data['gender'] == "male"){
                    $kelamin = "Laki - Laki";
                  } else {
                    $kelamin = "Perempuan";
                  }
                ?>
                <td><?php echo $kelamin ?></td>
                <?php
                if ($data['status'] == 1){
                  $status = "Member";
                }else if ($data['status'] == 2){
                  $status = "Client";
                }else if ($data['status'] == 3){
                  $status = "Member - Vendor";
                }else if ($data['status'] == 4){
                  $status = "Client - Vendor";
                }
                ?>
                <td><?php echo $status ?></td>
                <?php
                if ($data['aktifUser'] == 0){
                  $aktif_user = "Belum Validasi Email";
                } else if ($data['aktifUser'] == 1){
                  $aktif_user = "Aktif";
                } else if ($data['aktifUser'] == 3){
                  $aktif_user = "Banned";
                }
                ?>
                <td><?php echo $aktif_user ?></td>
                <td><?php echo date_format(date_create($data['created']), 'd F Y, H:i');?></td>
              <?php } ?>
            </tbody>
          </table>
          <!-- /.table-responsive -->

        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
</div>


<?php $this->load->view('footer'); ?>
<div class="modal" id="detail-users" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Detail Data Users</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- Table filter -->
<script type="text/javascript">
  $(document).ready(function (){
    var table = $('#dataTables-example').DataTable({
       //dom: 'lrtip', //untuk menghilangkan search
       // stateSave: true, //untuk menyimpan yang sudah pernah di search
       "bDestroy": true
    });
    
    $('#table-filter').on('change', function(){
       // table.search(this.value).draw();
       var tb1 = $("#table-filter1").val();
       if(tb1 == null){ table.search(this.value).draw(); }
       else{ table.search(this.value+" "+tb1).draw(); }
    });
    $('#table-filter1').on('change', function(){
       var tb = $("#table-filter").val();
       if(tb == null){ table.search(this.value).draw(); }
       else { table.search(tb+" "+this.value).draw(); }
    });
});
</script>
<script type="text/javascript">
  $('.detail-users').on('click', function(e){
    e.preventDefault();
    var uuid = $(this).data('uuid');
    $.ajax({
      url : "<?php echo base_url('dashboard/Data_customer/getbyuuid/');?>"+uuid,
      success : function(e){
       $('#detail-users .modal-body').html(e);
       $('#detail-users').modal({show:true});
     }
   });
  })
</script>
<script type="text/javascript">
  $("#detail-users").on("click", ".reset-password", function(e) {
    e.preventDefault();
    uuid = $(this).data('id');
    console.log(uuid);
    swal({
      title: "Reset Password User",
      text: "Ingin reset password?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Ya, saya yakin!',
      cancelButtonText: "Tidak, batalkan!",
    }).then((result) => {
      if(result.value){
        $.ajax({
          type : "POST",
          data : {uuid :  uuid},
          url : "http://localhost/api15/vendors/reset-password",
          success : function(hasil){
            hasil = JSON.parse(hasil);
            swal({
              title: hasil.status,
              text: hasil.message,
              type: hasil.status
            }).then((result) => {
              location.reload();
            });
          }
        });
      }
    });
  });
</script>


<!-- <script type="text/javascript">
  $('.detail').on('click', function(e){
    e.preventDefault();
    var uuid = $(this).data('uuid');
    $.ajax({
      url : "<?php echo base_url('Data_customer/detail/');?>"+uuid,
      success : function(e){
       $('#detail .modal-body').html(e);
       $('#detail').modal({show:true});
     }
   });
  })
</script>
 --><!-- <script type="text/javascript">
  $('.aktivasi').on('click', function(e){
    e.preventDefault();
    var a = $(this);
    $.ajax({
      type : "POST",
      data : {uuid : a.data('uuid')},
      url : "http://localhost/api15/vendors/mail_aktivasi",
      success : function(hasil){
        location.reload();
      }
    })
  })
</script>
-->
<!-- <script type="text/javascript">
  $('.view').on('click', function(e){
    e.preventDefault();
    var a = $(this);
    $.ajax({
      type : "POST",
      data : {uuid : a.data('uuid')},
      url : "<?php echo base_url('Data_customer/index')?>",
      success : function(hasil){
        location.reload();
      }
    })
  })
</script> -->