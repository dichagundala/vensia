<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>


<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Dashboard <?php echo $this->session->userdata('username'); ?></h1>
      </div>
      <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</div>

<?php $this->load->view('footer'); ?>


