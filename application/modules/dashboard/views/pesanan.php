<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>


<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Data Pesanan</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->  
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <label>Data Pesanan</label> <a href="#" class="btn btn-default btn-sm">Add Pesanan</a>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <form class="form-inline">
            <div class="form-group mb-2">
              <label>Filter Status</label>
            </div>
            <select class="custom-select custom-select-sm form-control form-control-sm mx-sm-3 mb-2" id="table-filter">
              <option value="" selected="">All</option>
              <option>Menunggu Konfirmasi</option>
              <option>Menunggu Pembayaran</option>
              <option>Dibayar</option>
              <option>Proses</option>
              <option>Selesai</option>
              <option>Refund</option>
            </select>
          </form>
          <hr>
          <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
              <tr>
                <th>No</th>
                <th>Kode Invoice </th>
                <th>Nama Client </th>
                <th>Tanggal Pesan</th>
                <th>Tanggal Event</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>                                 
              <?php
              $i = 0;
              foreach ($invoice as $key) {
                $i++;
                ?>
                <tr>
                  <td><?php echo $i?></td>
                  <td><a href="<?php echo base_url('dashboard/Data_pesanan/detail/'.$key['Invoice Uuid'])?>"><?php echo $key['Kode Invoice']?></a></td>
                  <td><?php echo $key['Client']?></td>
                  <td><?php echo date_format(date_create($key['Tanggal Pesanan']), 'd-M-Y | H:i');?></td>
                  <td><?php echo date_format(date_create($key['Tanggal Event']), 'd-M-Y');?></td>
                  <td><?php echo $key['Status Invoice']?></td>
                </tr>
                <?php
              }
              ?>
            </tbody>
          </table>
          <!-- /.table-responsive -->

        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
</div>



<?php $this->load->view('footer'); ?>
<!--- MODAL KONFIRMASI -->
<div class="modal fade" id="status" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 id="modal-label-3" class="modal-title">Status Pesanan Anda </h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('invoice-detail');?>
      </div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-b" type="button">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Table filter -->
<script type="text/javascript">
  $(document).ready(function (){
    var table = $('#dataTables-example').DataTable({
       //dom: 'lrtip', //untuk menghilangkan search
       // stateSave: true, //untuk menyimpan yang sudah pernah di search
       "bDestroy": true
    });
    $('#table-filter').on('change', function(){
        table.search(this.value).draw();
    });
});
</script>