</div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo base_url('vendor/jquery/jquery.min.js');?>"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.min.js');?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url('vendor/metisMenu/metisMenu.min.js');?>"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?php echo base_url('vendor/raphael/raphael.min.js');?>"></script>
    <script src="<?php echo base_url('vendor/morrisjs/morris.min.js');?>"></script>
    <script src="<?php echo base_url('data/morris-data.js');?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url('dist/js/sb-admin-2.js');?>"></script>

    <script src="<?php echo base_url('vendor/datatables/js/jquery.dataTables.min.js');?>"></script>
    <script src="<?php echo base_url('vendor/datatables-plugins/dataTables.bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('vendor/datatables-responsive/dataTables.responsive.js');?>"></script>

    <script src="<?php echo base_url('dist/plugins/gmaps/gmaps.int.js'); ?>"></script>
    <script src="<?php echo base_url('dist/plugins/gmaps/gmaps.js'); ?>"></script> 

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.11/dist/sweetalert2.all.min.js"></script>

    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $.get("home/notif",function(result){
                    $("#data_req").append(result[0].total);                         
                });  
        });           
    </script>
    <script>
    $(document).ready(function() {
        /*$('.sidebar-toggle').trigger('click');*/

        $(function () {
            $('#example1').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false

            });     
            $('#vendor').DataTable({
                /*'scrollY': '500px',
                'scrollX': '100%'*/
            });
            $('#produk').DataTable({
                /*'scrollY': '500px',
                'scrollX': '100%'*/
            });

            $('#customer').DataTable({
                /*'scrollY': '250px',
                'scrollX': '100%'*/
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            });

            $('#pesanan').DataTable({
                /*'scrollY': '250px'*/
            });

            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            });
        });
    });
    </script>

<script type="text/javascript">
    $('.pengembangan').on('click', function(e){
        e.preventDefault();
        swal({
            type : "info",
            text : "Dalam pengembangan"
        })
    })
</script>


</body>

</html>
