<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>

<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Data Vendor <a href="#" class="btn btn-default btn-sm">Add Vendor</a></h1>
    </div>

    <div class="col-lg-3 col-md-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-group fa-4x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge"><?php echo $count_vendor; ?></div>
              <div>Vendor YEPS</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-group fa-4x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge"><?php echo $count_vendor1; ?></div>
              <div>Bulan <?php 
              $bulan = array (1 =>   'Januari',
                        'Februari',
                        'Maret',
                        'April',
                        'Mei',
                        'Juni',
                        'Juli',
                        'Agustus',
                        'September',
                        'Oktober',
                        'November',
                        'Desember'
                      );
              echo $bulan[date('n')];
              ?></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-green">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-tasks fa-4x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge"><?php echo $servis; ?></div>
              <div>Service Vendor</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-green">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-tasks fa-4x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge"><?php echo $status_aktif; ?></div>
              <div>Service di Tampilkan</div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->  
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Data Vendor 
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">

          <form class="form-inline">
            <div class="form-group mb-3">
              <label>Filter : </label>
            </div>
            <select class="custom-select custom-select-sm form-control form-control-sm mx-sm-3 mb-3" id="table-filter">
              <option value="" selected>All</option>
              <option value="Venue">Venue</option>
              <option value="Photography">Photography</option>
              <option value="Wardrobe">Wardrobe</option>
              <option value="Decoration">Decoration</option>
              <option value="Souvenir">Souvenir</option>
              <option value="Catering">Catering</option>
              <option value="Make Up">Make Up</option>
              <option value="Invitation">Invitation</option>
              <option value="Entertainment">Entertainment</option>
              <option value="Transportation">Transportation</option>
              <option value="Event Cake">Event Cake</option>
              <option value="Videography">Videography</option>
            </select>
            <select class="custom-select custom-select-sm form-control form-control-sm mx-sm-3 mb-3" id="table-filter1">
              <option selected disabled>status</option>
              <option>Aktif</option>
              <option>Proses</option>
              <option>Belum Aktif</option>
              <option>Banned</option>
            </select>
            
          </form>
          <hr>

          <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
              <tr>
                <th width="30%">Nama</th>
                <th width="30%">Kontak</th>
                <th width="6%">Service</th>
                <th width="8%">Tanggal Daftar</th>
                <th width="6%">Status</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach ($vendor as $key) {
                $today = date("F j, Y, g:i a");  
                $tanggal = new dateTime($key['vCreated']);
                ?>
                <tr>
                  <td>
                    <p><a href="" data-toggle="modal" data-target="#detail-vendor" data-uuid="<?php echo $key['uuid']; ?>" class="detail-vendor"><?php echo $key['namaVendor']?></a></p>
                    <p><?php echo $key['namaPemilik']?></p>
                    <p style="color: grey; font-size: 12px">Kategori :<br><?php echo $key['kategori'] ?></p>                   
                  </td>

                  <td style="text-transform: capitalize;">
                    <?php echo $key['alamatVendor']
                    .'<br><i class="fa fa-phone"></i> : '.$key['telpVendor']
                    .'<br><i class="fa fa-envelope "></i> : '.$key['email'];?>
                  </td>

                  <td>
                    <a href="" data-toggle="modal" data-target="#detail" data-uuid="<?php echo $key['uuid']; ?>" class="detail"><?php echo $key['hasil'] ?></a>
                  </td>

                  <td style="font-size: 12px"><?php echo $tanggal->format("j F Y, g:i a")?></td>

                  <td>
                    <?php 
                    if($key['aktifVendor'] == "1"){ echo "Aktif"; }
                    elseif ($key['aktifVendor'] == "2"){ echo "Belum Aktif"; }
                    elseif ($key['aktifVendor'] == "3"){ echo "Diproses"; }
                    elseif ($key['aktifVendor'] == "4"){ echo " Ditolak"; }
                    elseif ($key['aktifVendor'] == "5"){ echo " Tutup"; }
                    else { echo  "Banned";}?>
                  </td>

                </tr>
                <?php
              }
              ?>
            </tbody>
          </table>
          <!-- /.table-responsive -->

        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
</div>

<?php $this->load->view('footer'); ?>


<div class="modal" id="detail" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Detail Produk Vendor</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal" id="detail-vendor" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Detail Data Vendor</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.js"></script>

<script type="text/javascript">
  $('.detail-vendor').on('click', function(e){
    e.preventDefault();
    var uuid = $(this).data('uuid');
    $.ajax({
      url : "<?php echo base_url('dashboard/Data_vendor/getbyuuid/');?>"+uuid,
      success : function(e){
       $('#detail-vendor .modal-body').html(e);
       $('#detail-vendor').modal({show:true});
     }
   });
  })
</script>
<script type="text/javascript">
  $('.detail').on('click', function(e){
    e.preventDefault();
    var uuid = $(this).data('uuid');
    $.ajax({
      url : "<?php echo base_url('dashboard/Data_vendor/detail/');?>"+uuid,
      success : function(e){
       $('#detail .modal-body').html(e);
       $('#detail').modal({show:true});
     }
   });
  })
</script>

<script type="text/javascript">
  $("#detail-vendor").on("click", ".reset-password", function(e) {
    e.preventDefault();
    uuid = $(this).data('id');
    console.log(uuid);
    swal({
      title: "Reset Password Vendor",
      text: "Ingin reset password vendor?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Ya, saya yakin!',
      cancelButtonText: "Tidak, batalkan!",
    }).then((result) => {
      if(result.value){
        $.ajax({
          type : "POST",
          data : {uuid :  uuid},
          url : "http://localhost/api15/vendors/reset-password",
          success : function(hasil){
            hasil = JSON.parse(hasil);
            swal({
              title: hasil.status,
              text: hasil.message,
              type: hasil.status
            }).then((result) => {
              location.reload();
            });
          }
        });
      }
    });
  });
</script>
<!-- Table filter -->
<script type="text/javascript">
  $(document).ready(function (){
    var table = $('#dataTables-example').DataTable({
       //dom: 'lrtip', //untuk menghilangkan search
       // stateSave: true, //untuk menyimpan yang sudah pernah di search
       "bDestroy": true
    });
    
    $('#table-filter').on('change', function(){
       // table.search(this.value).draw();
       var tb1 = $("#table-filter1").val();
       if(tb1 == null){ table.search(this.value).draw(); }
       else{ table.search(this.value+" "+tb1).draw(); }
    });
    $('#table-filter1').on('change', function(){
       var tb = $("#table-filter").val();
       if(tb == null){ table.search(this.value).draw(); }
       else { table.search(tb+" "+this.value).draw(); }
    });
});
</script>
