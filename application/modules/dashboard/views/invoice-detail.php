<div class="row mb20">
	<div class="col-sm-12">
		<!-- TABLE PESANAN -->
		<div class="table table-condensed table-striped table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th class="cart-product-thumbnail" style="text-align: center">Nama Vendor</th>
						<th class="cart-product-name" style="text-align: center">Kategori</th>
						<th class="cart-product-price" style="text-align: center">Biaya</th>
						<th class="cart-product-price" style="text-align: center">Kuantitas</th>
						<th class="cart-product-price" style="text-align: center">Catatan</th>
						<th class="cart-product-price" style="text-align: center">Sub Total</th>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach ($result as $key) {
						?>
						<tr>
							<td class="cart-product-thumbnail"  style="text-align: center">
								<div class="cart-product-thumbnail-name"><?php echo $key['vendor'];?></div>
							</td>
							<td class="cart-product-description" style="text-align: center">
								<p><span><?php echo $key['kategori'];?></span></p>
							</td>

							<td class="cart-product-price"  style="text-align: center">
								<span class="amount">Rp. 
									<?php 
									if($key['parameter']['Harga Promosi'] == "0"){
										echo number_format($key['parameter']['Harga Normal'],0,",",".");
									}else{
										echo number_format($key['parameter']['Harga Promosi'],0,",",".");
									}
									?> 
								</span>
							</td>
							<td class="cart-product-price" style="text-align: center">
								<span class="amount">
									<?php 
									echo $key['qty'];
									?> 
								</span>
							</td>
							<td class="cart-product-price" style="text-align: center">
								<span class="amount">
									<?php 
									echo $key['Note'];
									?> 
								</span>
							</td>
							<td class="cart-product-price"  style="text-align: center">
								<span class="amount">Rp. 
									<?php 
									if($key['parameter']['Harga Promosi'] == "0"){
										echo number_format($key['parameter']['Harga Normal']*$key['qty'],0,",",".");
									}else{
										echo number_format($key['parameter']['Harga Promosi']*$key['qty'],0,",",".");
									}
									?> 
								</span>
							</td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
		<!-- / TABLE PESANAN -->
	</div>
</div>