<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>
<style type="text/css">
/*.carousel-inner .active.left { left: -33%; }*/
.carousel-inner .next        { left:  33%; }
.carousel-inner .prev        { left: -33%; }
.carousel-control.left,.carousel-control.right {background-image:none;}
.item:not(.prev) {visibility: visible;}
.item.right:not(.prev) {visibility: hidden;}
.rightest{ visibility: visible;}

</style>

<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Pertanyaan Masalah</h1>
    </div>
  </div>

  <!-- /.row -->  
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Data FAQs 
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
              <tr>
                <th width="20%">Jenis Pertanyaan</th>
                <th width="28%">Nama Lengkap</th>
                <th width="28%">Email</th>
                <th width="38%">Detail Masalah</th>
                <th width="6%">Tanggal</th>
                <!--<th width="6%">Status</th>-->
              </tr>
            </thead>
            <tbody>
              <?php foreach ($data as $d) { $tanggal = new dateTime($d['create_at']); ?>
              <tr>
                <td>
                  <a href="#detailQuestion" class="detailtable" data-toggle="modal" 
                  data-id                 = "<?php echo $d['id']; ?>"
                  data-jenispertanyaan    = "<?php echo $d['jenis_pertanyaan']; ?>"
                  data-namalengkap        = "<?php echo $d['nama_lengkap']; ?>"
                  data-email              = "<?php echo $d['email']; ?>"
                  data-detailmasalah      = "<?php echo $d['detail_masalah']; ?>"
                  data-createat           = "<?php echo $d['create_at']; ?>"
                  
                  ><?php echo $d['jenis_pertanyaan']; ?></a>
                </td>
                <td><?php echo $d['nama_lengkap']; ?></td>
                <td><?php echo $d['email']; ?></td>
                <td><?php echo $d['detail_masalah']; ?></td>
                <td><?php echo $tanggal->format("j F Y"); ?></td>
                <!--<td>
                <?php //if($d['status'] == 1){ ?>
                  Tampil
                <?php//} else { ?>
                  Tidak Tampil
                <?php //} ?>
                </td>-->
              </tr>
            <?php } ?>
            </tbody>
          </table>
          <!-- /.table-responsive -->

        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
</div>

<!-- Modal -->
<div class="modal fade" id="detailQuestion" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Question Detail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <form method="post" action="<?php echo site_url('dashboard/Question/formedit') ?>" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="table-responsive-sm">
          <table class="table borderless detail">
           <!-- <tr>
              <div class="carousel slide" id="myCarousel">
              <div class="carousel-inner" id="cr">
                <div class="item active">
                  <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                  <a href="#"><img src="" class="img-responsive" id="foto1"></a></div>
                </div>
              </div>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
              </div>
            </tr>-->
            <tr>
              <td>Jenis Pertanyaan</td>
              <td><p name="jenispertanyaan" id="jenispertanyaan"></p></td>
            </tr>
            <tr>
              <td>Nama Lengkap</td>
              <td><p name="namalengkap" id="namalengkap"></td>
            </tr>
            <!--<tr>
              <td>Konten</td>
              <td><p name="konten" id="konten"></td>
            </tr>
            <tr>
              <td>Tanggal Event</td>
              <td><p name="tanggal" id="tanggal"></td>
            </tr>-->
            <tr>
              <td>Email</td>
              <td><p name="email" id="email"></td>
            </tr>
            <tr>
              <td>Detail Masalah</td>
              <td><p name="detailmasalah" id="detailmasalah"></td>
            </tr>
            <tr>
              <td>Created at</td>
              <td><p name="createat" id="createat"></td>
            </tr>
           <!-- <tr>
              <td>Edited at</td>
              <td><p name="updateat" id="updateat"></td>
            </tr>-->
        
            <td><input type="hidden" name="id" id="id"  value="" ></td>
            <!--<td><p name="id" id="id"></td>-->
            <!--<td style="display: none;"><p name="foto" id="foto"></td>
            <td style="display: none;"><p name="link" id="link"></td>
            <td style="display: none;"><p name="type" id="type"></td>
            <td style="display: none;"><p name="status" id="status"></td>-->
            
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="hapus btn btn-danger">Hapus</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="edit btn btn-primary">Edit</button>
      </div>
      </form>
    </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.js"></script>

<!-- Swall2 -->
<script type="text/javascript">
  <?php if($this->session->userdata('swal') == 1) {?>
  Swal.fire({
    position: 'top-center',
    type: 'success',
    title: 'Data berhasil disimpan !',
    showConfirmButton: false,
    timer: 2500
  })
  <?php }elseif ($this->session->userdata('swal') == 3) {?>
  Swal.fire({
      position: 'top-center',
      type: 'error',
      title: 'Bermasalah dengan file foto !',
      showConfirmButton: false,
      timer: 2500
    })
  <?php }elseif ($this->session->userdata('swal') == 2) {?>
    Swal.fire({
    position: 'top-center',
    type: 'error',
    title: 'Data gagal disimpan !',
    showConfirmButton: false,
    timer: 2500
  })
  <?php } ?>
</script>

<script type="text/javascript">
$(document).on('click', '.hapus', function(e){ e.preventDefault();
        var id = $('.detail #id').val();
        Swal.fire({
          title: 'Anda yakin ?',
          text: "Ingin menghapus ini ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya !'
        }).then((result) => {
          if (result.value) {
            $('#detailQuestion').modal('hide');
            $.ajax({
            url: "<?php echo base_url('dashboard/Question/hapus'); ?>",
            type: "post",
            data: {id:id},
            success: function(response) {
                if (response == true || response == 1) {
                    Swal.fire({
                      position: 'center',
                      type: 'success',
                      title: 'Berhasil dihapus !',
                      showConfirmButton: false,
                      timer: 1500
                    })
                    var delayInMilliseconds = 1500; //1 second
                    setTimeout(function() {
                      location.reload();
                    }, delayInMilliseconds);
                }else {
                  Swal.fire({
                      position: 'center',
                      type: 'warning',
                      title: 'Gagal dihapus !',
                      showConfirmButton: false,
                      timer: 1500
                    })
                  var delayInMilliseconds = 1500; //1 second
                    setTimeout(function() {
                      location.reload();
                    }, delayInMilliseconds);
                }
            }
        });
      }
    })
});
</script>
<script type="text/javascript">
  $(".detailtable").on("click", function(e) {
    e.preventDefault();

    $(".detail #jenispertanyaan").html($(this).data('jenispertanyaan'));
    $('.detail #namalengkap').html($(this).data('namalengkap'));
    $('.detail #email').html($(this).data('email'));
    $('.detail #detailmasalah').html($(this).data('detailmasalah'));
    $('.detail #createat').html($(this).data('createat'));
    $('.detail #id').val($(this).data('id'));
    
    
    $('#myCarousel').carousel({
        interval: 40000
      });

      $('.carousel .item').each(function(){
        var next = $(this).next();
        if (!next.length) {
          next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));
        if (next.next().length>0) {
            next.next().children(':first-child').clone().appendTo($(this)).addClass('rightest');
        }
        else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
    });
  })
 /* $('#detailQuestion').on('hidden.bs.modal', function () {
      $(".foto2").remove();
      $(".itmfoto2").remove();
  })*/
</script>

<script type="text/javascript">

</script>
