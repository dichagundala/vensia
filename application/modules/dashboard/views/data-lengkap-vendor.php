<center>
  <img style="height: 150px; margin-bottom: 20px" src="<?php echo $detail['url']; ?>">
</center>
<table class="table" border="0">
  <tr>
    <td>Nama</td> 
    <td><?php echo $detail['namaVendor']; ?></td>
  </tr>
  <tr>
    <td>Nama Pemilik</td> 
    <td><?php echo $detail['namaPemilik']; ?></td>
  </tr>
  <tr>
    <td>Deskrisi</td> 
    <td><?php echo $detail['bioVendor']; ?></td>
  </tr>                
  <tr>
    <td>Telp</td> 
    <td><?php echo $detail['telpVendor']; ?></td>
  </tr>                
  <tr>
    <td>Alamat</td> 
    <td><?php echo $detail['alamatVendor']; ?></td>
  </tr>                
  <tr>
    <td>Kelurahan</td> 
    <td><?php echo $detail['vKelurahan']; ?></td>
  </tr>                
  <tr>
    <td>Kecamatan</td> 
    <td><?php echo $detail['vKecamatan']; ?></td>
  </tr>                
  <tr>
    <td>Kabupaten</td> 
    <td><?php echo $detail['vKabupaten']; ?></td>
  </tr>                
  <tr>
    <td>Provinsi</td> 
    <td><?php echo $detail['vProvinsi']; ?></td>
  </tr>                
  <tr>
    <td>Kategori</td> 
    <td><?php echo $detail['kategori']; ?></td>
  </tr>                
  <tr>
    <td>Sosmed</td> 
    <td></td>
  </tr>                
  <tr>
    <td>Website</td> 
    <td><?php echo $detail['website']; ?></td>
  </tr>                
  <tr>
    <td>Facebook</td> 
    <td><?php echo $detail['facebook']; ?></td>
  </tr>                
  <tr>
    <td>Instagram</td> 
    <td><?php echo $detail['instagram']; ?></td>
  </tr>                
  <tr>
    <td>Line</td> 
    <td><?php echo $detail['line']; ?></td>
  </tr>                
  <tr>
    <td>slug</td> 
    <td><?php echo $detail['slug']; ?></td>
  </tr>                
  <tr>
    <td>NPWP</td> 
    <td><?php echo $detail['npwp']; ?></td>
  </tr> 
  <tr>
    <?php if($detail['aktifVendor'] == 2){ ?>
      <td>Validasi Vendor</td>
      <td> 
        <a href="" class="btn btn-primary btn-sm proses" title="Proses" data-uuid="<?php echo $detail['uuid'];?>">
          <i class="fa fa-sign-in"></i> Proses
        </a>
      </td>
    <?php }
    else if($detail['aktifVendor'] == 3){ ?>
      <td>Validasi Vendor</td>
      <td> 
        <a href="" class="btn btn-success btn-sm setuju" title="Setuju" data-uuid="<?php echo $detail['uuid'];?>">
          <i class="fa fa-check "></i> Setuju
        </a>
        <a href="" class="btn btn-danger btn-sm tolak" title="Tolak" data-uuid="<?php echo $detail['uuid'];?>">
          <i class="fa fa-times "></i> Tolak
        </a>
      </td>
    <?php }?>   
  </tr>              
  <tr>
    <td>Aksi</td>
    <td>
      <button class="btn btn-warning btn-sm reset-password" data-id="<?php echo $detail["uuid"]; ?>" title="Reset Password Vendor"><i class="fa fa-refresh" aria-hidden="true"></i> Reset Password
      </button>

      <a href="" class="btn btn-danger btn-sm banned" data-uuid="<?php echo $detail['uuid'];?>" title="Non Aktifkan Akun"><i class="fa fa-ban"></i> Banned
      </a>
    </td>
  </tr>
  <tr>
    <td>
      <?php $data_encode = $detail["email"].";".$detail["nama"].";".$detail["secure_url"].";".$detail["uuid"].";".$detail["kategori"]; ?>

      <form method="POST" action="https://vendor.yepsindonesia.com/cro/cro_login">
        <input type="hidden" value="<?php echo base64_encode($data_encode);?>" name="uuid">
        <button type="submit" class="btn btn-primary btn-sm" target="_blank" title="login as vendor"><i class="fa fa-sign-in" aria-hidden="true"></i> Login as Vendor</button>
      </form>
    </td>
  </tr>

</table>
<script type="text/javascript">
  $('.setuju').on('click', function(e){
    e.preventDefault();
    var a = $(this);
    $.ajax({
      type : "POST",
      data : {uuid : a.data('uuid')},
      url : "<?php echo base_url('/dashboard/Data_vendor/setuju_vendor/')?>",
      success : function(hasil){
        location.reload();
      }
    })
  })
</script>
<script type="text/javascript">
  $('.tolak').on('click', function(e){
    e.preventDefault();
    var a = $(this);
    $.ajax({
      type : "POST",
      data : {uuid : a.data('uuid')},
      url : "<?php echo base_url('/dashboard/Data_vendor/tolak_vendor/')?>",
      success : function(hasil){
        location.reload();
      }
    })
  })
</script>
<script type="text/javascript">
  $('.proses').on('click', function(e){
    e.preventDefault();
    var a = $(this);
    $.ajax({
      type : "POST",
      data : {uuid : a.data('uuid')},
      url : "<?php echo base_url('dashboard/Data_vendor/proses/')?>",
      success : function(hasil){
        location.reload();
      }
    })
  })
</script>
<script type="text/javascript">
  $('.banned').on('click', function(e){
    e.preventDefault();
    var a = $(this);
    $.ajax({
      type : "POST",
      data : {uuid : a.data('uuid')},
      url : "<?php echo base_url('/dashboard/Data_vendor/banned/')?>",
      success : function(hasil){
        location.reload();
      }
    })
  })
</script>