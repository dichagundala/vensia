<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>

<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Data Riwayat Request Edit Data Vendor</h1>
    </div>

    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->  
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Data Riwayat Request Edit Data Vendor 
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">

          <form class="form-inline">
            <div class="form-group mb-3">
              <label>Filter : </label>
            </div>
            <select class="custom-select custom-select-sm form-control form-control-sm mx-sm-3 mb-3">
              <option>Setujui</option>
              <option>Tidak</option>
            </select>
            <button type="submit" class="btn btn-peimary btn-sm mb-3">Filter</button>
          </form>
          <hr>

          <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
              <tr>
                <th>Nama Vendor</th>
                <th>Kategori</th>
                <th>Alamat</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach ($vendor as $key) {
                ?>
                <tr>
                  <form  enctype="multipart/form-data">
                    <td><?php echo $key['nama']; ?></td>
                    <td><?php echo $key['kategori']; ?></td>
                    <td><?php echo $key['alamatVendor']; ?><br><?php echo $key['vKelurahan']; ?>,<?php echo $key['vKecamatan']; ?>,<?php echo $key['vKabupaten']; ?>,<?php echo $key['vProvinsi']; ?></span></td>
                    <td><?php echo $key['keterangan']; ?></td>
                </tr>
                    </form>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
              <!-- /.table-responsive -->

            </div>
            <!-- /.panel-body -->
          </div>
          <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
    </div>

    <?php $this->load->view('footer'); ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.js"></script>
    
    