<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>

<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Add Portfolio</h1>
    </div>
  </div>

  <!-- /.row -->  
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Data Portfolio
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <form method="post" action="<?php echo base_url('#'); ?>" enctype="multipart/form-data">
            
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Lowongan Pekerjaan</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="" name="judul" placeholder="Judul Event" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Divisi</label>
              <div class="col-sm-10">
                <textarea class="form-control" id="" name="brief" rows="3" required></textarea>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Konten</label>
              <div class="col-sm-10">
                <textarea class="form-control" id="" name="konten" rows="5" required></textarea>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Tanggal Event</label>
              <div class="col-sm-10">
                <input type="date" class="form-control" id="" name="date_event" placeholder="Tanggal Event" required>
              </div>
            </div>
            <div class="form-group row" id="video">
              <label class="col-sm-2 col-form-label">Link Video Youtube</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="" name="link" placeholder="Masukkan Link">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Foto</label>
              <div class="col-sm-10">
                <input type="file" class="form-control-file" id="foto" name="foto">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Status</label>
              <div class="col-sm-10">
                <select class="form-control" id="" name="status" required>
                  <option value="1">Tampilkan</option>
                  <option value="2">Tidak di Tampilkan</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-2"></div>
              <div class="col-sm-8">
                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
              </div>
              <div class="col-sm-2"></div>
            </div>
          </form>

        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
</div>

<?php $this->load->view('footer'); ?>


<div class="modal" id="detail" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Detail Produk Vendor</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal" id="detail-vendor" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Detail Data Vendor</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.js"></script>

<script>
  $(document).ready(function(){
    $("#video").hide();
    $("#video-type").click(function(){
      $("#video").show();
    });

    $("#text").click(function(){
      $("#video").hide();
    });
  });
</script>

  <script type="text/javascript">
  $('.detail-vendor').on('click', function(e){
    e.preventDefault();
    var uuid = $(this).data('uuid');
    $.ajax({
      url : "<?php echo base_url('dashboard/Data_vendor/getbyuuid/');?>"+uuid,
      success : function(e){
       $('#detail-vendor .modal-body').html(e);
       $('#detail-vendor').modal({show:true});
     }
   });
  })
</script>
<script type="text/javascript">
  $('.detail').on('click', function(e){
    e.preventDefault();
    var uuid = $(this).data('uuid');
    $.ajax({
      url : "<?php echo base_url('dashboard/Data_vendor/detail/');?>"+uuid,
      success : function(e){
       $('#detail .modal-body').html(e);
       $('#detail').modal({show:true});
     }
   });
  })
</script>

<script type="text/javascript">




  // $("#form-tambah").submit(function(e) {
  //       e.preventDefault();

  //       // var formData = $("#form-tambah").serialize();
  //       var formData = new FormData(this);
  //       // formData.append('file', document.getElementById("foto").files[0]);
        
  //       $.ajax({
  //           url: "<?php echo base_url('dashboard/Portfolio/simpan'); ?>",
  //           type: "post",
  //           data: formData, //penggunaan FormData
  //            processData:false,
  //            contentType:false,
  //            cache:false,
  //            async:false,
  //           // data: new FormData(this),
  //           success: function(response) {
  //               if (response == 1) {
  //                   Swal.fire(
  //                   'Data Berhasil Ditambahkan !',
  //                   '',
  //                   'success'
  //                 )
  //               } if(response == 2) {
  //                   Swal.fire({
  //                     type: 'error',
  //                     title: 'Oops...',
  //                     text: 'Terjadi Kesalahan Pada File Foto',
  //                     // footer: '<a href>Why do I have this issue?</a>'
  //                   })
  //               }else {
  //                 Swal.fire({
  //                     type: 'error',
  //                     title: 'Oops...',
  //                     text: 'Data Gagal Ditambahkan !',
  //                     // footer: '<a href>Why do I have this issue?</a>'
  //                   })
  //               }
  //           }
  //       });
  //   });

  $("#detail-vendor").on("click", ".reset-password", function(e) {
    e.preventDefault();
    uuid = $(this).data('id');
    console.log(uuid);
    swal({
      title: "Reset Password Vendor",
      text: "Ingin reset password vendor?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Ya, saya yakin!',
      cancelButtonText: "Tidak, batalkan!",
    }).then((result) => {
      if(result.value){
        $.ajax({
          type : "POST",
          data : {uuid :  uuid},
          url : "http://localhost/api15/vendors/reset-password",
          success : function(hasil){
            hasil = JSON.parse(hasil);
            swal({
              title: hasil.status,
              text: hasil.message,
              type: hasil.status
            }).then((result) => {
              location.reload();
            });
          }
        });
      }
    });
  });
</script>
