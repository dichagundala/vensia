<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>


<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Data Service Vendor</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->  
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Data Service Vendor <a href="#" class="btn btn-default btn-sm">Add Service</a>
				</div>
				<!-- /.panel-heading -->
				<div class="panel-body">

					<form class="form-inline" href="">
						<div class="form-group mb-3">
							<label>Filter: </label>
						</div>
						<select class="custom-select custom-select-sm form-control form-control-sm mx-sm-3 mb-3" name="kategori" id="table-filter">
							<option value="" selected="">All Category</option>
							<option value="1" data-str="Venue">Venue</option>
							<option value="2" data-str="Photography">Photography</option>
							<option value="3" data-str="Wardrobe">Wardrobe</option>
							<option value="4" data-str="Decoration">Decoration</option>
							<option value="5" data-str="Souvenir">Souvenir</option>
							<option value="6" data-str="Catering">Catering</option>
							<option value="7" data-str="Make Up">Make Up</option>
							<option value="8" data-str="Invitation">Invitation</option>
							<option value="9" data-str="Entertainment">Entertainment</option>
							<option value="10" data-str="Transportation">Transportation</option>
							<option value="11" data-str="Event Cake">Event Cake</option>
							<option value="12" data-str="Videography">Videography</option>
						</select>
						<select class="custom-select custom-select-sm form-control form-control-sm mx-sm-3 mb-3" name="status" id="table-filter1">
							<option value="#" selected="" disabled>Status</option>
							<option value="Tidak Tampil">Tidak Tampil</option>
							<option value="Tahap Review">Tahap Review</option>
							<option value="Tampil">Tampil</option>
							<option value="Banned">Banned</option>
						</select>
						
					</form>
					<hr>

					<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
								<th>No.</th>	
								<th>Vendor</th>	
								<th>Nama Produk</th>	
								<th>Kategori</th>	
								<th>Harga</th>	
								<th>Status</th>	
								<th>Tanggal dibuat</th>	
								<th>Aksi</th>	
							</tr>
						</thead>
						<tbody>

							<?php
							$i = 0;
							foreach ($produk as $key) {
								$i++;
								$vendor = str_replace(" ","_",$key['vendor']);
								$jasa = $key['slug'];

								if($key['status'] == "0"){
									$status = "Tidak Tampil";
								}elseif ($key['status'] == "1") {
									$status = "Tahap Review";
								}elseif ($key['status'] == "2") {
									$status = "Boleh Tampil";
								}elseif ($key['status'] == "3") {
									$status = "Banned";
								}
								?>
								<tr>
									<td><?php echo $i;?></td>
									<td><?php echo $key['vendor'];?></td>
									<td><a href="<?php echo base_url('');?>jasa/<?php echo $vendor;?>/<?php echo $jasa;?>" target="_blank"><?php echo $key['parameter']['Nama'];?></a></td>
									<td><?php echo $key['kategori'];?></td>
									<td>
										Normal: Rp <?php echo number_format($key['parameter']['Harga Normal'], 0, ',','.');?> 
										<br>
										Promo: Rp <?php echo number_format($key['parameter']['Harga Promosi'], 0, ',','.');?> 

									</td>
									<td class="status-value status-isi"><?php echo $status;?></td>
									<td class="status-value"><?php echo date_format(date_create($key['tanggal']), 'd M Y  H:i');?></td>
									<td>
										<div class="dropdown">
											<button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Ubah Status
												<span class="caret"></span></button>
												<ul class="dropdown-menu">
													<li><a class="dropdown-item status" data-tipe="0" data-uuid="<?php echo $key['uuid'];?>">Tidak Tampil</a></li>
													<li><a class="dropdown-item status" data-tipe="1" data-uuid="<?php echo $key['uuid'];?>">Tahap Review</a></li>
													<li><a class="dropdown-item status" data-tipe="2" data-uuid="<?php echo $key['uuid'];?>">Boleh Tampil</a></li>
													<li><a class="dropdown-item status" data-tipe="3" data-uuid="<?php echo $key['uuid'];?>">Banned</a></li>
												</ul>
											</div>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						<!-- /.table-responsive -->

					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div>


	<?php $this->load->view('footer'); ?>


	<div class="modal" id="detail" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Daftar Produk Vendor</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$('.detail').on('click', function(e){
			e.preventDefault();
			var uuid = $(this).data('uuid');
			$.ajax({
				url : "<?php echo base_url('Data_vendor/detail/');?>"+uuid,
				success : function(e){
					$('#detail .modal-body').html(e);
					$('#detail').modal({show:true});
				}
			});
		})
	</script>
	<script type="text/javascript">
		$('.aktivasi').on('click', function(e){
			e.preventDefault();
			var a = $(this);
			$.ajax({
				type : "POST",
				data : {uuid : a.data('uuid')},
				url : "https://api.yepsindonesia.com/vendors/mail_aktivasi",
				success : function(hasil){
					location.reload();
				}
			})
		})
		$('.status').on('click', function(e){
			e.preventDefault();
			var a = $(this);
			console.log("a");
			swal({
				title: 'Ubah Status Produk',
				text: "Apakah Anda yakin akan mengubah status produk ini?",
				type: 'info',
				showCancelButton: true,
				confirmButtonColor: '#107ADE',
				cancelButtonColor: '#C30000',
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then((result) => {
				if (result.value) {

					$.ajax({
						type : "POST",
						data : {uuid : a.data('uuid'), status : a.data('tipe')},
						url : "<?php echo base_url('dashboard/Data_vendor/status_produk')?>",
						success : function(hasil){
							console.log(hasil);
							var rs = $.parseJSON(hasil);
							swal({
								type : rs['icon'],
								text : rs['text']
							}).then( function(e) {
								if(rs['icon'] == "success"){
								var isi = "";
								if(a.data('tipe') == "0"){
									isi = "Tidak Tampil";
								}else if(a.data('tipe') == "1"){
									isi = "Tahap Review";
								}else if(a.data('tipe') == "2"){
									isi = "Boleh Tampil";
								}else if(a.data('tipe') == "3"){
									isi = "Banned";
								}
								a.parent().parent().parent().parent().parent().find(".status-isi").html(isi);
								}
							});
						}
					})
				}
			})
		})
	</script>
	<script type="text/javascript">
		$('.view').on('click', function(e){
			e.preventDefault();
			var a = $(this);
			$.ajax({
				type : "POST",
				data : {uuid : a.data('uuid')},
				url : "<?php echo base_url('Data_vendor/view')?>",
				success : function(hasil){
					location.reload();
				}
			})
		})
	</script>

	<script type="text/javascript">
		$("#vendor").on("click", ".reset-password", function(e) {
			e.preventDefault();
			uuid = $(this).data('id');
			console.log(uuid);
			swal({
				title: "Reset Password Vendor",
				text: "Ingin reset password vendor?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Ya, saya yakin!',
				cancelButtonText: "Tidak, batalkan!",
			}).then((result) => {
				if(result.value){
					$.ajax({
						type : "POST",
						data : {uuid :  uuid},
						url : "https://api.yepsindonesia.com/vendors/reset-password",
						success : function(hasil){
							hasil = JSON.parse(hasil);
							swal({
								title: hasil.status,
								text: hasil.message,
								type: hasil.status
							}).then((result) => {
								location.reload();
							});
						}
					});
				}
			});
		});
	</script>

	<!-- Table filter -->
<script type="text/javascript">
  $(document).ready(function (){
    var table = $('#dataTables-example').DataTable({
       //dom: 'lrtip', //untuk menghilangkan search
       // stateSave: true, //untuk menyimpan yang sudah pernah di search
       "bDestroy": true
    });
    
    $('#table-filter').on('change', function(){
       var data = $(this).find(':selected').attr('data-str');
       var tb1 = $("#table-filter1").val();
       if(tb1 == null){ table.search(data).draw(); }
       else{ table.search(data+" "+tb1).draw(); }
    });
    $('#table-filter1').on('change', function(){
       var tb = $("#table-filter").find(':selected').attr('data-str');
       if(tb == null){ table.search(this.value).draw(); }
       else { table.search(tb+" "+this.value).draw(); }
    });
});
</script>
