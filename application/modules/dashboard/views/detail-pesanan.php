<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>
<script type="text/javascript"
src="https://app.sandbox.midtrans.com/snap/snap.js"
data-client-key="<SB-Mid-client-gampCCrldTZVOghQ>"></script>

  <form id="payment-form" method="post" action="">
    <input type="hidden" name="result_type" id="result-type" value="">
    <input type="hidden" name="result_data" id="result-data" value="">
  </form>
  
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <br/>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->  
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-primary">
            <div class="panel-heading">
              Data User
            </div>
            <div class="panel-body">
              <div class="col-md-3">
                <?php
                if($client['img'] == ""){
                  $img = 'https://www.yepsindonesia.com/assets/images/profil.jpg';
                }else{
                  $img = 'https://private.yepsindonesia.com/./uploads/'.$client['img'];

                }
                ?>
                <img src="<?php echo $img;?>" width="200" height="200">
              </div>
              <div class="col-md-9">
                <table class="table">
                  <tr>
                    <td style="width: 10%">Nama</td>
                    <td>: <?php echo $client['namaDepan']." ".$client['namaBelakang'];?></td>
                  </tr>
                  <tr>
                    <td style="width: 20%">Jenis Kelamin</td>
                    <td>: <?php 
                    if($client['gender']=="male"){
                      echo "Pria";
                    }
                    if($client['gender']=="female"){
                      echo "Wanita";
                    }?></td>
                  </tr>
                  <tr>
                    <td style="width: 10%">Tanggal Lahir</td>
                    <td>: <?php 
                    if($client['tgl_lahir'] != "0000-00-00"){
                      echo date_format(date_create($client['tgl_lahir']), "d M Y");
                    } else{
                      echo "-";
                    }
                    ?></td>
                  </tr>
                  <tr>
                    <td style="width: 10%">Telp</td>
                    <td>: <?php echo $client['telp'];?> <a href="https://api.whatsapp.com/send?phone=<?php echo $client['telp'];?>" class="btn btn-success" target="_blank">Whatsapp</a></td>
                  </tr>
                  <tr>
                    <td style="width: 10%">Email</td>
                    <td>: <?php echo $client['email'];?></td>
                  </tr>
                </table>
              </div>
            </div>

          </div>
        </div>

        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              Data Pesanan : <?php echo $invoice[0]['Kode Invoice'].' - '.$invoice[0]['Status Invoice'];?>                         
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
              <?php
              $i = 0;
              ?>
              <table class="table">
                <tr>
                  <th>No.</th>
                  <th>Vendor</th>
                  <th>Pesanan</th>
                  <th>Harga</th>
                  <th>Status</th>
                </tr>
                <?php
                $cek = 0;
                $biaya = 0;
                foreach ($transaksi as $key) {
                  $i++;
                  ?>
                  <tr>
                    <td><?php echo $i?></td>
                    <td>
                      <span style="font-size: 15px; font-weight: 500;"><?php echo $key['nama']?></span>
                      <br>Hp : <a href="https://api.whatsapp.com/send?phone=<?php echo $key['telp'];?>" target="_blank"><?php echo $key['telp']?></a>
                      <br>Email : <?php echo $key['email']?>
                      <br>Tgl Event : <?php echo date_format(date_create($key['date_order']), 'd M Y');?>
                    </td>
                    <td>
                      <span style="font-size: 15px; font-weight: 500;"><?php echo $key['nama_jasa'];?></span>
                      <br>Qty : <?php echo $key['qty']?>
                      <!-- Kalau ada additional baru muncul tombol tambahan -->
                      <?php if ($transaksi_add != null) { ?>
                        <br><a href="#" data-toggle="modal" data-target="#tambahan">Lihat Tambahan</a>
                      <?php } ?>
                    </td>
                    <td>
                      <?php echo number_format($key['harga_normal'],0,",",".")?>
                      <br><?php echo number_format($key['harga_promo'],0,",",".")?>
                    </td>
                    <td><?php if($key['Status'] == "INVOICE"){ echo "-";}else{ echo $key['Status']; $cek++;}?></td>
                  </tr>
                  <?php 
                  if($key['Status'] == "INVOICE" || $key['Status'] == "ACCEPT"){
                    if($key['harga_promo'] == "0"){
                      $biaya += (int)$key['harga_normal']*(int)$key['qty'];
                    }else{
                      $biaya += (int)$key['harga_promo']*(int)$key['qty'];
                    }
                  }
                  $sta = $key['Status'];
                }
                $eventC = (int)$biaya*0.05;
                  //            $pajak = (int)$biaya*0.1;
                $total = (int)$biaya+$eventC;
                $dp = (int)$total*0.3;
                ?>
              </table>
              <!-- /.table-responsive -->
              <hr>
              <form id="payment">
                <input type="hidden" name="uuid" value="<?php echo $invoice[0]['Invoice Uuid']?>">
                <input type="hidden" name="invoice" value="<?php echo $invoice[0]['Kode Invoice']?>">
                <input type="hidden" name="price" value="<?php echo (int)$dp;?>">
                <input type="hidden" name="total_price" value="<?php echo (int)$total;?>">
                <input type="hidden" name="email_client" value="<?php echo $client['email'];?>">
                              <!-- <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Pajak</label>
                                <div class="col-sm-4">
                                  <input type="text" readonly class="form-control-plaintext" id="staticEmail" value=": Rp. <?php echo number_format(((int)$pajak),0,',','.')?>"">
                                </div>
                              </div> -->
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Event Charge</label>
                                <div class="col-sm-4">
                                  <input type="text" readonly class="form-control-plaintext" id="staticEmail" value=": Rp. <?php echo number_format(((int)$eventC),0,',','.')?>"">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Harga Total</label>
                                <div class="col-sm-4">
                                  <input type="text" readonly class="form-control-plaintext" id="staticEmail" value=": Rp. <?php echo number_format($total,0,',','.')?>">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">DP</label>
                                <div class="col-sm-4">
                                  <input type="text" readonly class="form-control-plaintext" value=": Rp. <?php echo  number_format(((int)$dp),0,',','.')?>"">
                                  <input type="hidden" readonly class="form-control-plaintext" id="dp" value="<?php echo (int)$dp;?>">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Metode Pembayaran</label>
                                <div class="col-sm-4">
                                  <input type="text" readonly class="form-control-plaintext" id="staticEmail" value=": <?php echo $invoice[0]['Metode Pembayaran']?>"">
                                </div>
                              </div>
                            </form>
                            
                          </div>
                          <div class="panel-footer" align="center">
                            
                            <?php if($invoice[0]['Status Invoice'] == "KONFIRMASI"){ ?>
                              <button class="btn btn-success" data-toggle="modal" data-target="#cicilan">Cicilan</button>
                              <!--             <button class="btn btn-primary konfirmasi" <?php //if($cek != $i){ echo "disabled";}?>>Konfirmasi ke Client</button> -->
                              <!-- <button class="btn btn-primary konfirmasi">Konfirmasi ke Client</button> -->
                            <?php }else if($invoice[0]['Status Invoice'] == "KONFIRMASI TRANSFER"){ ?>
                              <button class="btn btn-success" data-toggle="modal" data-target="#cekTransfer">Cek Transfer Client</button>
                            <?php }else if($invoice[0]['Status Invoice'] == "REJECT"){ ?>
                              <button class="btn btn-success pengembangan">Tambah Jasa</button>
                            <?php } ?>
                          </div>
                          <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                      </div>
                      <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                  </div>
                  <!-- /.container-fluid -->
                </div>


                <?php $this->load->view('footer'); ?>



                <!--- MODAL KONFIRMASI -->
                <div class="modal fade" id="status" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                        <h4 id="modal-label-3" class="modal-title">Status Pesanan Anda </h4>
                      </div>
                      <div class="modal-body">
                        <?php $this->load->view('invoice-detail');?>
                      </div>
                      <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-b" type="button">Close</button>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal fade" id="cicilan" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 id="modal-label-3" class="modal-title">Status Pesanan Anda </h4>
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                      </div>
                      <div class="modal-body">
                        <h7 id="modal-label-3" class="modal-title"><b>Sisa cicilan anda :Rp. <?php 
                        //echo (number_format($total,0,',','.') - number_format(((int)$dp),0,',','.'));
                          echo number_format($total-$cicilan1[0]['biaya_cicilan'],0,',','.');
                         // number_format($total-$dp,0,',','.');
                        ?></b></h7>
                        <table class="table">
                          <tr>
                            <th>Cicilan Ke</th>
                            <th>Tanggal</th>
                            <th>Besaran Biaya Cicilan</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                          <form id="form-cicilan">
                            <?php
                            for ($i=1; $i <= (int)$invoice[0]['Cicilan']; $i++) {
                             if(isset($cicilan[$i-1]['uuid'])){
                              $temp_uuid = $cicilan[$i-1]['uuid'];
                              $tanggal = $cicilan[$i-1]['tanggal'];
                              $biaya = $cicilan[$i-1]['biaya_cicilan'];
                              $status = $cicilan[$i-1]['status'];
                            }else{
                              $temp_uuid = "none";
                              $tanggal = "-";
                              $biaya = "-";
                              $status = "-";
                            }
                            ?>
                            <input type="hidden" name="uuid[]" value="<?php echo $temp_uuid; ?>">
                            <tr class="view">
                              <td><?php echo $i;?></td>

                              <td><?php echo $tanggal;?></td>
                              <td><?php echo $biaya;?></td>
                              <td><?php echo $status;?></td>

                              <td>
                                <?php
                                if($status != "LUNAS"){

                                  ?>
                                  <button class="btn btn-success Lunas" data-uuid="<?php echo $temp_uuid?>">Lunas</button>
                                  <?php
                                }
                                ?>
                              </td>
                            </tr>
                            <tr class="data" style="display: none;">
                              <td><?php echo $i;?></td>
                              <td><div class="form-group"><input type="date" class="form-control" name="tanggal[]" value="<?php echo $tanggal?>"></div></td>
                              <td><div class="form-group"><input type="number" class="form-control" name="biaya[]" value="<?php echo $biaya?>"></div></td>
                              <td><?php echo $status;?></td>
                              <td>
                               <?php
                               if($status != "LUNAS"){

                                ?>
                                <button class="btn btn-success Lunas" data-uuid="<?php echo $temp_uuid?>">Lunas</button>
                                <?php
                              }
                              ?>
                            </td>
                          </tr>
                          <?php
                        }
                        ?>
                      </form>
                    </table>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-primary cicilan-edit">Edit</button>
                    <button class="btn btn-primary cicilan-simpan" style="display: none">Simpan</button>
                    &nbsp;<button data-dismiss="modal" class="btn btn-b" type="button">Close</button>
                  </div>
                </div>
              </div>
            </div>

            <div class="modal fade" id="tambahan" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 id="modal-label-3" class="modal-title">List Tambahan</h4>
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  </div>
                  <div class="modal-body">
                    <table class="table">
                      <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Jumlah</th>
                        <th>Harga</th>
                      </tr>
                      <?php $i = 1; foreach ($transaksi_add as $key) { ?>
                        <tr>
                          <td><?php echo $i++ ?></td>
                          <td><?php echo $key['nama_add']; ?></td>
                          <td><?php echo $key['qty_add']; ?></td>
                          <td><?php echo $key['harga_add']; ?></td>
                        </tr>
                      <?php } ?>
                    </table>
                  </div>
                  <!-- <div class="modal-footer">
                    
                  </div> -->
                </div>
              </div>
            </div>

            <div class="modal fade" id="cekTransfer" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 id="modal-label-3" class="modal-title">Detail Transfer Klien </h4>
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  </div>
                  <div class="modal-body">

                    <table class="table">
                      <form id="form-bukti">
                        <input type="hidden" name="id_bukti" value="<?php echo $bukti['id']?>">
                        <tr>
                          <td>Nama Bank Klien</td>
                          <td><?php echo $bukti['bank_name_fr']?></td>
                        </tr>
                        <tr>
                          <td>No Rekening Bank Klien</td>
                          <td><?php echo $bukti['bank_no_fr']?></td>
                          <tr>
                            <td>Atas Nama</td>
                            <td><?php echo $bukti['bank_account_fr']?></td>
                          </tr>
                          <td>Tanggal</td>
                          <td><?php echo date_format(date_create($bukti['transfer_date']), 'd-M-Y')?></td>
                          <tr>
                            <td>Berita</td>
                            <td><?php echo $bukti['transfer_note']?></td>
                          </tr>
                          <tr>
                            <td>Gambar</td>
                            <td><img src="<?php echo 'https://www.yepsindonesia.com/uploads/tagihan/'.$bukti['img'].'.jpg';?>"></td>
                          </tr>

                        </form>
                      </table>
                    </div>
                    <div class="modal-footer">
                      <button class="btn btn-primary bukti-konfirmasi">Konfirmasi Transfer</button><button class="btn btn-primary cicilan-simpan" style="display: none">Simpan</button>&nbsp;<button data-dismiss="modal" class="btn btn-b" type="button">Close</button>
                    </div>
                  </div>
                </div>
              </div>

              <script type="text/javascript">
                $('.Lunas').on('click', function(e){
                  e.preventDefault();
                  var a = $(this);
                  $.ajax({
                    url : "<?php echo base_url('Data_pesanan/lunasCicilan/')?>" + a.data("uuid"),
                    success : function(e){
                      location.reload();
                    } 
                  });
                });
              </script>
              <script type="text/javascript">
                $('.cicilan-edit').on('click', function(e){
                  $('.view').hide();
                  $('.data').show();
                  $('.cicilan-simpan').show();
                  $('.cicilan-edit').hide();
                })
                $('.cicilan-simpan').on('click', function(e){
                  $('.data').hide();
                  $('.view').show();
                  $('.cicilan-simpan').hide();
                  $('.cicilan-edit').show();
                  var mydata = $('form#form-cicilan').serialize();
                  alert(mydata);
                  $.ajax({
                    url : "<?php echo base_url('Data_pesanan/editCicilan/'.$invoice[0]['Invoice Uuid'])?>",
                    data : mydata,
                    type : "POST",
                    success : function(e){
                      location.reload();
                    },
                  })
                })

                $('.bukti-konfirmasi').on('click', function(e){
                  var mydata = $('form#form-bukti').serialize();
                  swal({
                    title: 'Konfirmasi Pesanan',
                    text: "Konfirmasi pembayaran ini?",
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#107ADE',
                    cancelButtonColor: '#C30000',
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak'
                  }).then((result) => {
                    if (result.value) {

                      $.ajax({
                        url : "<?php echo base_url('Data_pesanan/konfirmTransfer/'.$invoice[0]['Invoice Uuid'])?>",
                        data : mydata,
                        type : "POST",
                        success : function(e){
                          alert(e);
                        },
                      })

                    }
                  })
                })

              </script>
              <!-- test -->
              <script type="text/javascript">

                $('.konfirmasiv2').click(function (event) {
                  event.preventDefault();
                  alert("a")
      // $(this).attr("disabled", "disabled");

      var dp = $('#dp').val();
      console.log(dp);
      $.ajax({
        url: '<?=site_url()?>/snap/token_invoice/'+'<?=$invoice[0]['Invoice Uuid']?>/'+dp,
        cache: false,

        success: function(data) {
        //location = data;
        console.log('token = '+data);

        snap.pay(data, {
          onSuccess: function(result){
            console.log(result);
            $.ajax({
              type : "POST",
              url: "<?php echo base_url('Data_pesanan/midtrans_data')?>",
              cache: false,
              data: { data : JSON.stringify(result) },
              success : function(e){
                console.log(e);
                var hasil = JSON.parse(e);
                swal({
                  type : hasil.icon,
                  text : hasil.text
                })
              }
            })
          },
          onPending: function(result){
            console.log(result);
            $.ajax({
              type : "POST",
              url: "<?php echo base_url('Data_pesanan/midtrans_data')?>",
              cache: false,
              data: { data : JSON.stringify(result) },
              success : function(e){
                console.log(e);
                var hasil = JSON.parse(e);
                swal({
                  type : hasil.icon,
                  text : hasil.text
                })
              }
            })
          },
          onError: function(result){
            console.log(result);
            $.ajax({
              type : "POST",
              url: "<?php echo base_url('Data_pesanan/midtrans_data')?>",
              cache: false,
              data: { data : JSON.stringify(result) },
              success : function(e){
                var hasil = JSON.parse(e);
                console.log(e);
                swal({
                  type : hasil.icon,
                  text : hasil.text
                })
              }
            })
          }
        });
      }
    });
    });

  </script>

  <script type="text/javascript">
    $('.konfirmasi').on('click', function(e){
      var uuid_invoice = "<?php echo $invoice[0]['Invoice Uuid']?>";
      swal({
        title: 'Konfirmasi Pesanan',
        text: "Konfirmasi Pesanan ke Client?",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#107ADE',
        cancelButtonColor: '#C30000',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
      }).then((result) => {
        if (result.value) {

          $.ajax({
            type : "POST",
            data : { uuid : uuid_invoice},
            url : "<?php echo base_url('dashboard/Data_pesanan/konfirmasi')?>",
            success : function(e){
              var rs = $.parseJSON(e);
              swal({
                type : rs['icon'],
                text : rs['text']
              }).then( function(e) {

                if("<?php echo $invoice[0]['Metode Pembayaran']?>" == "VA"){
                  var mydata = $('form#payment').serialize();
                  console.log(mydata);
                  $.ajax({
                    url : "<?php echo base_url('Data_pesanan/va');?>",
                    data : mydata,
                    type : 'POST',
                    success : function(hasil){
                      console.log(hasil);
                      var rs = JSON.parse(hasil);
                      swal({
                        type : rs.icon,
                        text : rs.text
                      }).then( function(e) {

                      })                     
                    }
                  })

                }else{
                  $.ajax({
                    url : "<?php echo base_url('Data_pesanan/email_client/');?>"+uuid_invoice+"VA",
                    success : function(hasil){
                      location.reload();
                    }
                  })

                }
              })
            }
          });

        }
      })
    });
  </script>
  <script type="text/javascript">

 /*$.ajax({
  url: '<?php echo base_url('Snap/token')?>',
  cache: false,
  success: function(data) {
        //location = data;

        console.log('token = '+data);
        
        var resultType = document.getElementById('result-type');
        var resultData = document.getElementById('result-data');

        function changeResult(type,data){
          $("#result-type").val(type);
          $("#result-data").val(JSON.stringify(data));
          //resultType.innerHTML = type;
          resultData.innerHTML = JSON.stringify(data);
        }

        snap.pay(data, {

          onSuccess: function(result){
            changeResult('success', result);
            console.log(result.status_message);
            console.log(result);
            $("#payment-form").submit();
          },
          onPending: function(result){
            changeResult('pending', result);
            console.log(result.status_message);
            console.log(result);
            $("#payment-form").submit();
          },
          onError: function(result){
            changeResult('error', result);
            console.log(result.status_message);
            $("#payment-form").submit();
          }
        });
      }
    })*/
  </script>