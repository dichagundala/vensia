	<div class="table-responsive">

		<table class="table">
			<tr>
				<th>No.</th>	
				<th>Nama Produk</th>	
				<th>Harga</th>	
				<th>Status</th>	
			</tr>
			<?php
			$i = 0;
			foreach ($produk as $key) {
				$i++;
				$vendor = str_replace(" ","_",$key['vendor']);
				$jasa = $key['slug'];

				if($key['status'] == "0"){
					$status = "Tidak Tampil";
				}elseif ($key['status'] == "1") {
					$status = "Tahap Review";
				}elseif ($key['status'] == "2") {
					$status = "Boleh Tampil";
				}elseif ($key['status'] == "3") {
					$status = "Banned";
				}
				?>
				<tr>
					<td><?php echo $i;?></td>
					<td><a href="<?php echo base_url('jasa/');?><?php echo $vendor;?>/<?php echo $jasa;?>" target="_blank"><?php echo $key['parameter']['Nama'];?></a></td>
					<td>
						Normal: Rp <?php echo number_format($key['parameter']['Harga Normal'], 0, ',','.');?> 
						<br>
						Promo: Rp <?php echo number_format($key['parameter']['Harga Promosi'], 0, ',','.');?> 

					</td>
					<td><?php echo $status;?></td>

				</tr>
				<?php } ?>
			</table>
		</div>