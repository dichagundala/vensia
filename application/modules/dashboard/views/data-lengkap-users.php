<center>
  <img style="height: 150px; margin-bottom: 20px" src="<?php echo $detail['url']; ?>">
</center>
<table class="table" border="0">
  <tr>
    <td>Nama</td> 
    <td><?php echo $detail['namaDepan']." ".$detail['namaBelakang']; ?></td>
  </tr>
  <tr>
    <td>Tanggal Lahir</td> 
    <td><?php echo $detail['tglLahir']; ?></td>
  </tr>
  <tr>
    <td>Email</td> 
    <td><?php echo $detail['email']; ?></td>
  </tr>                
  <tr>
    <td>Telp</td> 
    <td><?php echo $detail['telp']; ?></td>
  </tr>                
  <tr>
    <td>Alamat</td> 
    <td><?php echo $detail['address']; ?></td>
  </tr> 
  <tr>
    <td>Tanggal Daftar</td> 
    <td><?php echo $detail['created']; ?></td>
  </tr>                
  <tr>
    <td>Kelurahan</td> 
    <td><?php echo $detail['kelurahan']; ?></td>
  </tr>                
  <tr>
    <td>Kecamatan</td> 
    <td><?php echo $detail['kecamatan']; ?></td>
  </tr>                
  <tr>
    <td>Kabupaten</td> 
    <td><?php echo $detail['kabupaten']; ?></td>
  </tr>                
  <tr>
    <td>Provinsi</td> 
    <td><?php echo $detail['provinsi']; ?></td>
  </tr>                             
  <tr>
    <td>Aksi</td>
    <td>
      <button class="btn btn-warning btn-sm reset-password" data-id="<?php echo $key["uuid"]; ?>" title="Reset Password Users"><i class="fa fa-refresh" aria-hidden="true"></i> Reset Password
      </button>

      <a class="btn btn-danger btn-sm banned" data-uuid="<?php echo $detail['uuid'];?>" title="Non Aktifkan Akun"><i class="fa fa-ban"></i> Banned
      </a>
    </td>
  </tr>
  <tr>
    <td>
      <?php $data_encode = $detail["email"].";".$detail["nama"].";".$detail["secure_url"].";".$detail["uuid"].";".$detail["kategori"]; ?>

      <form method="POST" action="https://vendor.yepsindonesia.com/cro/cro_login">
        <input type="hidden" value="<?php echo base64_encode($data_encode);?>" name="uuid">
        <button type="submit" class="btn btn-primary btn-sm" target="_blank" title="login as vendor"><i class="fa fa-sign-in" aria-hidden="true"></i> Login as Vendor</button>
      </form>
    </td>
  </tr>

</table>
<script type="text/javascript">
  $('.banned').on('click', function(e){
    e.preventDefault();
    var a = $(this);
    $.ajax({
      type : "POST",
      data : {uuid : a.data('uuid')},
      url : "<?php echo base_url('/dashboard/Data_customer/banned/')?>",
      success : function(hasil){
        location.reload();
      }
    })
  })
</script>