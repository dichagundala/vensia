<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header sty-one">
    <h1>Daftar Produk</h1>
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li><i class="fa fa-angle-right"></i> Daftar Produk</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-lg-12">
        <div class="card m-t-3">
          <div class="card-body">
            <h4 class="text-black">Data Pemesanan</h4>
            <div class="table-responsive">
              <table id="pesanan" class="table table-bordered table-striped">
                <thead>
                  <tr>
                  </tr>
                </thead>
                <tbody>
                 
                </tbody>
              </table>
            </div>
          </div></div>
        </div>
      </div>
      <!-- Main row --> 
    </section>
    <!-- /.content --> 
  </div>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer'); ?>
  <!--- MODAL KONFIRMASI -->
  <div class="modal fade" id="status" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
          <h4 id="modal-label-3" class="modal-title">Status Pesanan Anda </h4>
        </div>
        <div class="modal-body">
          <?php $this->load->view('invoice-detail');?>
        </div>
        <div class="modal-footer">
          <button data-dismiss="modal" class="btn btn-b" type="button">Close</button>
        </div>
      </div>
    </div>
  </div>