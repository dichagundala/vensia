<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>

<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">


<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Add FAQs</h1>
    </div>
  </div>

  <!-- /.row -->  
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Data FAQs
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <form method="post" id="inptform" action="<?php echo base_url('dashboard/Question/simpan'); ?>" enctype="multipart/form-data">

            <!--<div class="form-group row">
              <label class="col-sm-2 col-form-label">Type Posting</label>
              <div class="col-sm-10">
                <select class="form-control" id="type" name="type" required>
                  <option value="" disabled selected>-- Pilih Type --</option>
                  <option value="1" id="text">Text</option>
                  <option value="2" id="video-type">Video</option>
                </select>
              </div>
            </div>-->
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Pertanyaan</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="pertanyaan" name="pertanyaan" placeholder="Question FAQs"  required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Jawaban</label>
              <div class="col-sm-10">
                <textarea class="form-control" id="jawaban" name="jawaban" rows="3" placeholder="Answer FAQs"  required></textarea>
              </div>
            </div>
            <!--<div class="form-group row">
              <label class="col-sm-2 col-form-label">Konten</label>
              <div class="col-sm-10">
                <textarea class="form-control" id="konten" name="konten" rows="5" required></textarea>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Tanggal Event</label>
              <div class="col-sm-10">
                <input type="date" class="form-control" id="" name="date_event" placeholder="Tanggal Event" value="<?php //echo $date_event ?>" required>
              </div>
            </div>
            <div class="form-group row" id="video">
              <label class="col-sm-2 col-form-label">Link Video Youtube</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="video" name="link" placeholder="Masukkan Link" value="<?php //echo $link ?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Foto</label>
              <div class="col-sm-10">
                <input type="file" class="form-control-file" id="foto" name="foto[]" size="10" multiple>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Status</label>
              <div class="col-sm-10">
                <select class="form-control" id="" name="status" value="<?php//echo $status ?>" required>
                  <option value="1">Tampilkan</option>
                  <option value="2">Tidak di Tampilkan</option>
                </select>
              </div>
            </div>-->
            <div class="form-group row">
              <div class="col-sm-2"></div>
              <div class="col-sm-8">
                <input type="hidden" class="form-control" id="" name="id" value="<?php echo $id ?>">
                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
              </div>
              <div class="col-sm-2"></div>
            </div>
          </form>

        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
</div>

<?php $this->load->view('footer'); ?>


<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>

<script>
  $(document).ready(function() {
    $('#konten').summernote();
  });
</script>


<div class="modal" id="detail" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Detail Produk Vendor</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal" id="detail-vendor" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Detail Data Vendor</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.js"></script>
<script type="text/javascript">
    if (<?php echo $edit ?> == true) {
      $('#inptform').attr('action', '<?php echo site_url('dashboard/Question/editsimpan') ?>');  
    }
</script>

<script>
    document.getElementById("pertanyaan").value = "<?php echo $question ?>";
    document.getElementById("jawaban").value = "<?php echo $answer ?>";
</script>
<script>
    $("#video").hide();
    $('#type').on('change',function(){ 
      var value = $(this).val();
      if (value == 2) {
        $("#video").show();
      }else{
        $("#video").hide();
      }
    });
</script>

<script type="text/javascript">
  $('.detail-vendor').on('click', function(e){
    e.preventDefault();
    var uuid = $(this).data('uuid');
    $.ajax({
      url : "<?php echo base_url('dashboard/Data_vendor/getbyuuid/');?>"+uuid,
      success : function(e){
       $('#detail-vendor .modal-body').html(e);
       $('#detail-vendor').modal({show:true});
     }
   });
  })
</script>
<script type="text/javascript">
  $('.detail').on('click', function(e){
    e.preventDefault();
    var uuid = $(this).data('uuid');
    $.ajax({
      url : "<?php echo base_url('dashboard/Data_vendor/detail/');?>"+uuid,
      success : function(e){
       $('#detail .modal-body').html(e);
       $('#detail').modal({show:true});
     }
   });
  })
</script>