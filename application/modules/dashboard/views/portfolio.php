<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>
<style type="text/css">
/*.carousel-inner .active.left { left: -33%; }*/
.carousel-inner .next        { left:  33%; }
.carousel-inner .prev        { left: -33%; }
.carousel-control.left,.carousel-control.right {background-image:none;}
.item:not(.prev) {visibility: visible;}
.item.right:not(.prev) {visibility: hidden;}
.rightest{ visibility: visible;}

</style>

<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Our Portfolio <a href="<?php echo base_url('dashboard/portfolio/add')?>" class="btn btn-default btn-sm">Add Portfolio</a></h1>
    </div>
  </div>

  <!-- /.row -->  
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Data Portfolio 
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
              <tr>
                <th width="30%">Judul</th>
                <th width="38%">Brief</th>
                <th width="6%">Tanggal</th>
                <th width="6%">Status</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($data as $d) { $tanggal = new dateTime($d['date_event']); ?>
              <tr>
                <td>
                  <a href="#detailPortfolio" class="detailtable" data-toggle="modal" 
                  data-judul    ="<?php echo $d['judul']; ?>"
                  data-brief    ="<?php echo $d['brief']; ?>"
                  data-konten   ="<?php echo $d['konten']; ?>"
                  data-tanggal  ="<?php echo $d['date_event']; ?>"
                  data-createby ="<?php echo $d['create_by']; ?>"
                  data-editby   ="<?php echo $d['edit_by']; ?>"
                  data-createat ="<?php echo $d['created_at']; ?>"
                  data-updateat ="<?php echo $d['update_at']; ?>"

                  data-status   ="<?php echo $d['status']; ?>"
                  data-link     ="<?php echo $d['link']; ?>"
                  data-id       ="<?php echo $d['id']; ?>"
                  data-foto     ="<?php echo $d['foto']; ?>"
                  data-type     ="<?php echo $d['type']; ?>"
                  ><?php echo $d['judul']; ?></a>
                </td>
                <td><?php echo $d['brief']; ?></td>
                <td><?php echo $tanggal->format("j F Y"); ?></td>
                <td>
                <?php if($d['status'] == 1){ ?>
                  Tampil
                <?php } else { ?>
                  Tidak Tampil
                <?php } ?>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
          <!-- /.table-responsive -->

        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
</div>

<!-- Modal -->
<div class="modal fade" id="detailPortfolio" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Portfolio Detail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <form method="post" action="<?php echo site_url('dashboard/Portfolio/formedit') ?>" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="table-responsive-sm">
          <table class="table borderless detail">
            <tr>
              <div class="carousel slide" id="myCarousel">
              <div class="carousel-inner" id="cr">
                <div class="item active">
                  <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                  <a href="#"><img src="" class="img-responsive" id="foto1"></a></div>
                </div>
              </div>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
              </div>
            </tr>
            <tr>
              <td>Judul</td>
              <td><p name="judul" id="judul"></p></td>
            </tr>
            <tr>
              <td>Brief</td>
              <td><p name="brief" id="brief"></td>
            </tr>
            <tr>
              <td>Konten</td>
              <td><p name="konten" id="konten"></td>
            </tr>
            <tr>
              <td>Tanggal Event</td>
              <td><p name="tanggal" id="tanggal"></td>
            </tr>
            <tr>
              <td>Created by</td>
              <td><p name="createby" id="createby"></td>
            </tr>
            <tr>
              <td>Edited by</td>
              <td><p name="editby" id="editby"></td>
            </tr>
            <tr>
              <td>Created at</td>
              <td><p name="createat" id="createat"></td>
            </tr>
            <tr>
              <td>Edited at</td>
              <td><p name="updateat" id="updateat"></td>
            </tr>
        
            <td><input type="hidden" name="id" id="id" value=""></td>
            <!-- <td><p name="id" id="id"></td> -->
            <td style="display: none;"><p name="foto" id="foto"></td>
            <td style="display: none;"><p name="link" id="link"></td>
            <td style="display: none;"><p name="type" id="type"></td>
            <td style="display: none;"><p name="status" id="status"></td>
            
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="hapus btn btn-danger">Hapus</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="edit btn btn-primary">Edit</button>
      </div>
      </form>
    </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.js"></script>

<!-- Swall2 -->
<script type="text/javascript">
  <?php if($this->session->userdata('swal') == 1) {?>
  Swal.fire({
    position: 'top-center',
    type: 'success',
    title: 'Data berhasil disimpan !',
    showConfirmButton: false,
    timer: 2500
  })
  <?php }elseif ($this->session->userdata('swal') == 3) {?>
  Swal.fire({
      position: 'top-center',
      type: 'error',
      title: 'Bermasalah dengan file foto !',
      showConfirmButton: false,
      timer: 2500
    })
  <?php }elseif ($this->session->userdata('swal') == 2) {?>
    Swal.fire({
    position: 'top-center',
    type: 'error',
    title: 'Data gagal disimpan !',
    showConfirmButton: false,
    timer: 2500
  })
  <?php } ?>
</script>

<script type="text/javascript">
$(document).on('click', '.hapus', function(e){ e.preventDefault();
        var id = $('.detail #id').val();
        Swal.fire({
          title: 'Anda yakin ?',
          text: "Ingin menghapus ini ?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya !'
        }).then((result) => {
          if (result.value) {
            $('#detailPortfolio').modal('hide');
            $.ajax({
            url: "<?php echo base_url('dashboard/Portfolio/hapus'); ?>",
            type: "post",
            data: {id:id},
            success: function(response) {
                if (response == true || response == 1) {
                    Swal.fire({
                      position: 'center',
                      type: 'success',
                      title: 'Berhasil dihapus !',
                      showConfirmButton: false,
                      timer: 1500
                    })
                    var delayInMilliseconds = 1500; //1 second
                    setTimeout(function() {
                      location.reload();
                    }, delayInMilliseconds);
                }else {
                  Swal.fire({
                      position: 'center',
                      type: 'warning',
                      title: 'Gagal dihapus !',
                      showConfirmButton: false,
                      timer: 1500
                    })
                  var delayInMilliseconds = 1500; //1 second
                    setTimeout(function() {
                      location.reload();
                    }, delayInMilliseconds);
                }
            }
        });
      }
    })
});
</script>
<script type="text/javascript">
  $(".detailtable").on("click", function(e) {
    e.preventDefault();

    $(".detail #judul").html($(this).data("judul"));
    $('.detail #brief').html($(this).data('brief'));
    $('.detail #konten').html($(this).data('konten'));
    $('.detail #tanggal').html($(this).data('tanggal'));
    $('.detail #createby').html($(this).data('createby'));
    $('.detail #editby').html($(this).data('editby'));
    $('.detail #createat').html($(this).data('createat'));
    $('.detail #updateat').html($(this).data('updateat'));
    $('.detail #status').html($(this).data('status'));
    $('.detail #link').html($(this).data('link'));
    $('.detail #id').val($(this).data('id'));
    $('.detail #type').html($(this).data('type'));
    var s = $(this).data('foto').split(",");
    $("#foto1").attr("src", "<?php echo base_url('asset/images/portfolio/'); ?>"+s[0]);
    for (var i = 1; i < s.length; i++) {
      $("#cr").append('<div class="item foto2">'+
                '<div class="col-lg-4 col-xs-4 col-md-4 col-sm-4 itmfoto2">'+
                '<a href="#"><img src="<?php echo base_url('asset/images/portfolio/'); ?>'+s[i]+'" class="img-responsive"></a>'+
                '</div>'+
                '</div>');
    }
    
    $('#myCarousel').carousel({
        interval: 40000
      });

      $('.carousel .item').each(function(){
        var next = $(this).next();
        if (!next.length) {
          next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));
        if (next.next().length>0) {
            next.next().children(':first-child').clone().appendTo($(this)).addClass('rightest');
        }
        else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
    });
  })
  $('#detailPortfolio').on('hidden.bs.modal', function () {
      $(".foto2").remove();
      $(".itmfoto2").remove();
  })
</script>

<script type="text/javascript">

</script>
