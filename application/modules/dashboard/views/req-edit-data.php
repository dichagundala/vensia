<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>

<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Request Edit Data Vendor</h1>
    </div>

    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->  
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Request Edit Data Vendor <a href="<?php echo base_url('dashboard/data_vendor/riwayat');?>" class="btn btn-default btn-sm">Lihat Riwayat</a>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">

          <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
              <tr>
                <th>Nama Vendor</th>
                <th>Kategori</th>
                <th>Alamat</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
            <?php
              foreach ($vendor as $key) {
                ?>
                <tr>
                <form  method="POST" id="persetujuan"  enctype="multipart/form-data">
                  <td><?php echo $key['nama']; ?></td>
                  <?php if ($key['kategori']!=""){ ?>
                  <td><?php echo $key['kategori_lama']; ?><br> <span style="font-weight: bold; margin-top: 10px">Kategori Baru : <br><?php echo $key['kategori']; ?></span></td>
                  <td></td>

                  <!-- data yang akan di panggil  -->
                  <input type="hidden" name="kategori" value="<?php echo $key['kategori']; ?>">
                  <?php } 
                  else { ?>
                  <td></td>
                  <td><?php echo $key['alamat_lama']; ?><br><?php echo $key['kelurahanlama']; ?>,<?php echo $key['kecamatanlama']; ?>,<?php echo $key['kabupatenlama']; ?>,<?php echo $key['provinsilama']; ?><br>
                  <span style="font-weight: bold; margin-top: 10px">Alamat Baru : <br><?php echo $key['alamatVendor']; ?><br><?php echo $key['vKelurahan']; ?>,<?php echo $key['vKecamatan']; ?>,<?php echo $key['vKabupaten']; ?>,<?php echo $key['vProvinsi']; ?></span></td>
                  
                  <!-- data yang akan di panggil  -->
                  <?php } ?>
                  <td>
                    <div class="dropdown" id="aksi">
                      <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">Pilih Aksi
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                          <li><a href="<?php echo base_url('dashboard/Data_vendor/accepted_data_vendor/'.$key["id"]); ?>">Setujui</a></li>
                          <li><a href="<?php echo base_url('dashboard/Data_vendor/rejected_data_vendor/'.$key["id"]); ?>"  class="tidaksetuju">Tidak</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                  </form>
                  <?php
              }
              ?>
              </tbody>
            </table>
            <!-- /.table-responsive -->

          </div>
          <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
      </div>
      <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
  </div>

  <?php $this->load->view('footer'); ?>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.js"></script>
  
 