<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>

<style>
  .blue {
    background-color: #2196F3 !important;
  }
  
  .blue.darken-1 {
    background-color: #1E88E5 !important;
  }

  .blue-text.text-darken-1 {
    color: #1E88E5 !important;
  }

  .blue.darken-2 {
    background-color: #1976D2 !important;
  }

  .blue-text.text-darken-2 {
    color: #1976D2 !important;
  }

  .blue.darken-3 {
    background-color: #1565C0 !important;
  }

  .blue-text.text-darken-3 {
    color: #1565C0 !important;
  }

  .blue.darken-4 {
    background-color: #0D47A1 !important;
  }

  .blue-text.text-darken-4 {
    color: #0D47A1 !important;
  }
  
  .blue.accent-1 {
    background-color: #82B1FF !important;
  }

  .blue-text.text-accent-1 {
    color: #82B1FF !important;
  }

  .blue.accent-2 {
    background-color: #448AFF !important;
  }

  .blue-text.text-accent-2 {
    color: #448AFF !important;
  }

  .blue.accent-3 {
    background-color: #2979FF !important;
  }

  .blue-text.text-accent-3 {
    color: #2979FF !important;
  }

  .blue.accent-4 {
    background-color: #2962FF !important;
    color:white;
  }

  .blue-text.text-accent-4 {
    color: #2962FF !important;
  }
  
  //comment panel
  .thumbnail {
    padding:0px;
  }
  .panel-comment {
    position:relative;width:45%;
  }
  
  .panel-comment > .panel-heading{
    background-color:#E6E2E2 !important;
  }
  .panel-comment>.panel-heading:after,.panel>.panel-heading:before{
    position:absolute;
    top:11px;left:-16px;
    right:100%;
    width:0;
    height:0;
    display:block;
    content:" ";
    border-color:transparent;
    border-style:solid solid outset;
    pointer-events:none;
  }
  
  .panel-comment>.panel-heading:after{
    border-width:7px;
    border-right-color:#E6E2E2;
    margin-top:1px;
    margin-left:2px;
  }
  .panel-comment>.panel-heading:before{
    border-right-color:#ddd;
    border-width:8px;
  }

</style>

<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1></h1>
    </div>
  </div>

  <!-- /.row -->  
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Live CHat
        </div>		
        <!-- /.panel-heading -->
        <div class="panel-body">

          <div class="col-md-12">
            <ul class="list-group">
              <li class="list-group-item blue darken-4 " style="color:white;font-weight:bold"><i class="fa fa-users"></i> Chat List</li>
              <?php foreach($user->result() as $row){   ?>

                <a href="javascript:void(0)" style="text-decoration:none" onClick="getChat2('<?php echo $row->ip_user1; ?>','0')"><li class="list-group-item " onclick="aktifkan('<?php echo $row->id;  ?>')" id="aktif-<?php echo $row->id; ?>" id="<?php echo $row->id; ?>" id="user" ><i class="fa fa-angle-right"></i> <?php echo $row->ip_user1; ?></li></a>
              <?php } ?>
            </ul>
          </div>
          <div class="col-md-12">
            <div class="panel panel-info">
              <div class="panel-heading  blue darken-4" style="color:white;font-weight:bold" ><i class="fa fa-comments"></i> Chat Box</div>
              <div class="panel-body" style="height:400px;overflow-y:auto" id="box">
                <div id="chat-box">
                  <div class='panel-body'><h2 style='text-align:center;color:grey'>Click User on Chat List to Start Chat</h2></div>
                </div>
              </div>
              <div class="panel-footer" >
                <div class="input-group">
                <input id="btn-input" type="text" class="form-control input-sm chat_set_height" placeholder="Type your message here..." tabindex="0" dir="ltr" spellcheck="false" autocomplete="off" autocorrect="off" autocapitalize="off" contenteditable="true" />

                <span class="input-group-btn">
                  <button class="btn bt_bg btn-sm btn-primary" id="btn-chat" onClick="sendMessage()">
                  Send</button>
                </span>
              </div>
              </div>
            </div>
          </div>
          

        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
</div>


<?php $this->load->view('footer'); ?>
<!--chat js awal-->
<!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>  -->   

<!--chat js akhir-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.js"></script>

<script>
//untuk online chat
$(document).ready(function(){
	//getChat(0);
	$("#user").click(function(){
		$("#id_max").val('0');
	});
	
	setInterval(function(){ 
		if($("#id_user").val() > 0){
			getLastId($("#id_user").val(),$("#id_max").val()); 
			getChat($("#id_user").val(),$("#id_max").val()); 
			autoScroll();
		}else{
			
		}
	},3000);
});

function getChatAll(id_user,id_max){
	$.ajax({
		url		: "<?php echo site_url('index.php/dashboard/chat/getChatAll') ?>",
		type	: 'POST',
		dataType: 'html',
		data 	: {id_user:id_user,id_max:id_max},
		
		success	: function(result){
		//	$("#loading").hide();
   $("#chat-box").html(result);
			//$(".panel-footer").show();
			
			autoScroll();
			//document.getElementById('btn-input').focus();
		}
	});
}
function getChat2(id_user,id_max){
	//var id_user = $("#id_user").val();
    //var id_ip = $("#id_ip").val();
    //var id_max = $("#id_max").val();
    $.ajax({
      url		: "<?php echo site_url('index.php/dashboard/chat/getChat') ?>",
      type	: 'POST',
      dataType: 'html',
      data 	: {id_user:id_user,id_max:id_max},
      beforeSend	: function(){
			//$("#loading").show();
		},
		success	: function(result){
			//$("#loading").hide();
			if(id_user != $("#id_user").val() ){
				$("#chat-box").html(result);
			}else{
				$("#chat-box").append(result);
			}
			//$(".panel-footer").show();
			//document.getElementById('btn-input').focus();
		}
	});
  }
  function getChat(id_user,id_max){
   var id_user = $("#id_user").val();
    //var id_ip = $("#id_ip").val();
    var id_max = $("#id_max").val();
    $.ajax({
      url		: "<?php echo site_url('index.php/dashboard/chat/getChat') ?>",
      type	: 'POST',
      dataType: 'html',
      data 	: {id_user:id_user,id_max:id_max},
      beforeSend	: function(){
		//	$("#loading").show();
  },
  success	: function(result){
   $("#loading").hide();
   if(id_user != $("#id_user").val() ){
    $("#chat-box").html(result);
  }else{
    $("#chat-box").append(result);
  }
			//$(".panel-footer").show();
			//document.getElementById('btn-input').focus();
		}
	});
  }

  function getLastId(id_user,id_max){
   $.ajax({
    url		: "<?php echo site_url('index.php/dashboard/chat/getLastId') ?>",
    type	: 'POST',
    dataType: 'json',
    data 	: {id_user:id_user,id_max:id_max},
    beforeSend	: function(){

    },
    success	: function(result){
     $("#id_max").val(result.id);
   }
 });
 }

 function sendMessage(){
   var pesan 	= $("#btn-input").val();
   var id_user = $("#id_user").val();
    //var id_ip = $("#id_ip").val();

    if(pesan == ''){
      document.getElementById('btn-input').focus();
    }else{
      $.ajax({
       url		: "<?php echo site_url('index.php/dashboard/chat/sendMessage') ?>",
       type	: 'POST',
       dataType: 'json',
       data 	: {id_user:id_user,  pesan:pesan},
       beforeSend	: function(){
       },
       success	: function(result){
        getChat($("#id_user").val() ,$("#id_max").val());
        getLastId($("#id_user").val() ,$("#id_max").val()); 
        $("#btn-input").val('');
        autoScroll();
      }
    });
    }
  }
/*
  function autoScroll(){
   var elem = document.getElementById('box');
   elem.scrollTop = elem.scrollHeight;
 }*/

 function aktifkan(i){
   $("li").removeClass("active");
   $("#aktif-"+i).addClass("active");
 }
</script>