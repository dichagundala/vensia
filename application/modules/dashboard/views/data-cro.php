<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>
<!-- Content Wrapper. Contains page content -->

<div id="page-wrapper">
            <div class="container-fluid">
            <div class="row">
                    <div class="col-lg-12">
                        <br/>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->  
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
						<h4 class="text-black">Data Back Office</h4>
						<button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#tambahCRO">Tambah Akun</button><br><br></b>                         
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						
						<table id="cro_akun" class="table table-bordered table-striped table-responsive" style="width: 960px;">
							<thead>
								<tr>
									<td style="width: 5%">No</td>
									<td style="width: 40%">Nama</td>
									<td style="width: 15%">Username</td>
									<td style="width: 25%">akses</td>
									<td style="width: 15%">aksi</td>
								</tr>
							</thead>
							<tbody>
							 <?php
                                  $i = 0;
                                  foreach ($cro as $data) {
                                    $i++;
                                    ?>
								<tr>
									<td><?php echo $i?></td>
									<td><?php echo $data['nama']?></td>
									<td><?php echo $data['username']?></td>
									<td><?php echo $data['akses']?></td>
									<td><button class="btn btn-danger">Hapus</button></td>
									<?php } ?>
								</tbody>
							</table>
                            </table>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
		</div>



	<div class="modal fade" id="tambahCRO" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<b>Tambah Akun CRO</b>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form method="POST" action="Data_user/addCro">
					<div class="modal-body">
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Nama User</label>
							<input type="text" class="form-control" name="nama" id="nama">
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Username</label>
							<input type="text" class="form-control" name="username" id="username">
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Password</label>
							<input type="password" class="form-control" name="password">
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Menu Akses</label><br>
							<div class="checkbox">
								<label><input type="checkbox" name="menu[]" value="1">&nbsp;Dashboard</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" name="menu[]" value="2">&nbsp;Data Vendor</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" name="menu[]" value="3">&nbsp;Data Produk</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" name="menu[]" value="4">&nbsp;Data Customer</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" name="menu[]" value="5">&nbsp;Data Pesananan</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" name="menu[]" value="6">&nbsp;Akun CRO</label>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php $this->load->view('footer'); ?>
	<script type="text/javascript">
		$("#cro_akun").dataTable();
	</script>