<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header sty-one">
    <h1>Profile page</h1>
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li><i class="fa fa-angle-right"></i> Profile page</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-lg-12">
        <div class="info-box">
          <div class="card tab-style1"> 
            <!-- Nav tabs -->
            <ul class="nav nav-tabs profile-tab" role="tablist">
              <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab" aria-expanded="true">Biodata Diri</a> </li>
              <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-expanded="false">Rekening Bank</a> </li>
              <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Tentang Vendor</a> </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane active" id="home" role="tabpanel" aria-expanded="true">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-4 col-xs-6">
                      <div class="box-profile m-b-3" style="background-color: #fafafa; border: 1px solid #c8c8c8">
                        <div class="box-profile"> 
                          <input type="file" id="input-file-now-custom-1" class="dropify" data-default-file="dist/img/img13.jpg" />
                          <p class="justify" style="font-size: 12px"></p>
                          <p class="justify" style="font-size: 12px">Besar file: maksimum 10.000.000 bytes (10 Megabytes), Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG 
                          </p>

                        </div>
                      </div>
                    </div>
                    <div class="col-lg-8 col-xs-6">
                     <form class="form-horizontal form-material">
                      <div class="form-group">
                        <label class="col-md-12">Full Name</label>
                        <div class="col-md-12">
                          <input placeholder="Fernando Astrada" class="form-control form-control-line" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="example-email" class="col-md-12">Email</label>
                        <div class="col-md-12">
                          <input placeholder="florencedouglas@admin.com" class="form-control form-control-line" name="example-email" id="example-email" type="email">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-12">Password</label>
                        <div class="col-md-12">
                          <input value="password" class="form-control form-control-line" type="password">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-12">Phone No</label>
                        <div class="col-md-12">
                          <input placeholder="123 456 7890" class="form-control form-control-line" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-12">Message</label>
                        <div class="col-md-12">
                          <textarea rows="5" class="form-control form-control-line"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-12">Select Country</label>
                        <div class="col-sm-12">
                          <select class="form-control form-control-line">
                            <option>London</option>
                            <option>India</option>
                            <option>Usa</option>
                            <option>Canada</option>
                            <option>Thailand</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-12">
                          <button class="btn btn-success">Update Profile</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <!--second tab-->
            <div class="tab-pane" id="profile" role="tabpanel" aria-expanded="false">
              <div class="card-body">
                <div class="container">
                  <div class="row">
                    <button class="btn btn-success">Rambah Rekening Bank</button>
                  </div>
                </div>
                <hr>
                <div class="table-responsive">
                  <table class="table">
                    <thead class="text-white" style="background-color: #1375d1">
                      <tr>
                        <th scope="col"></th>
                        <th scope="col">Nama Akun</th>
                        <th scope="col">Nomor Rekening</th>
                        <th scope="col">Nama Bank</th>
                        <th scope="col">Nama Cabang</th>
                        <th scope="col">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <th scope="row"><img src="dist/img/bank/bcabank.png"></th>
                        <td>Adhika Trisna Dwi Putra</td>
                        <td>7420247081</td>
                        <td>PT. BANK CENTRAL ASIA TBK (BCA)</td>
                        <td>Percetakan Negara</td>
                        <td>
                          <button type="button" class="btn btn-primary bulat"><i class="fa fa-edit"></i></button>
                          <button type="button" class="btn btn-danger bulat"><i class="fa fa-trash-o"></i></button>
                        </td>
                      </tr>
                      <tr>
                        <th scope="row"><img src="dist/img/bank/mandiri.png"></th>
                        <td>Adhika Trisna Dwi Putra</td>
                        <td>7420247081</td>
                        <td>PT. BANK MANDIRI</td>
                        <td>Jakarta Stasiun Senen</td>
                        <td>
                          <button type="button" class="btn btn-primary bulat"><i class="fa fa-edit"></i></button>
                          <button type="button" class="btn btn-danger bulat"><i class="fa fa-trash-o"></i></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="settings" role="tabpanel">
              <div class="card-body">
                <form class="form-horizontal form-material">
                  <div class="form-group">
                    <label class="col-md-12">Full Name</label>
                    <div class="col-md-12">
                      <input placeholder="Florence Douglas" class="form-control form-control-line" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="example-email" class="col-md-12">Email</label>
                    <div class="col-md-12">
                      <input placeholder="florencedouglas@admin.com" class="form-control form-control-line" name="example-email" id="example-email" type="email">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12">Password</label>
                    <div class="col-md-12">
                      <input value="password" class="form-control form-control-line" type="password">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12">Phone No</label>
                    <div class="col-md-12">
                      <input placeholder="123 456 7890" class="form-control form-control-line" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-12">Message</label>
                    <div class="col-md-12">
                      <textarea rows="5" class="form-control form-control-line"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-12">Select Country</label>
                    <div class="col-sm-12">
                      <select class="form-control form-control-line">
                        <option>London</option>
                        <option>India</option>
                        <option>Usa</option>
                        <option>Canada</option>
                        <option>Thailand</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <button class="btn btn-success">Update Profile</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Main row --> 
</section>
<!-- /.content --> 
</div>
<!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>