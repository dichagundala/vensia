<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>

<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">YEPS Career <a href="<?php echo base_url('dashboard/career/add')?>" class="btn btn-default btn-sm">Add Career</a></h1>
    </div>
  </div>

  <!-- /.row -->  
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Data Career
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
              <tr>
                <th width="30%">Lowongan Kerja</th>
                <th width="38%">Gaji</th>
                <th width="6%">Expired</th>
                <th width="6%">Status</th>
              </tr>
            </thead>
            <tbody>
              
              <tr>
                <td>
                  <a href="#" data-toggle="modal" data-target="#detailPortfolio">Judul</a>
                  <p style="font-size: 12px">Divisi : Marketing</p>
                </td>
                <td>
                  <p>IDR 4.000.000 - 5.000.0000</p>
                  <p style="font-size: 12px">Type: Full Time - Experience: Less than 1 year</p>
                </td>
                <td>20 Januari 2019</td>
                <td>
                <!-- <?php if($d['status'] == 1){ ?>
                  Tampil
                <?php } else { ?>
                  Tidak Tampil
                <?php } ?> -->
                </td>
              </tr>
            </tbody>
          </table>
          <!-- /.table-responsive -->

        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
</div>

<!-- Modal -->
<div class="modal fade" id="detailPortfolio" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Portfolio Detail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="table-responsive-sm">
          <table class="table borderless">
            <tr>
              <td>Judul</td>
              <td>Isi</td>
            </tr>
            <tr>
              <td>Brief</td>
              <td>Isi</td>
            </tr>
            <tr>
              <td>Konten</td>
              <td>Isi</td>
            </tr>
            <tr>
              <td>Tanggal Event</td>
              <td>Isi</td>
            </tr>
            <tr>
              <td>Tag</td>
              <td>Isi</td>
            </tr>
            <tr>
              <td>Created by</td>
              <td>Isi</td>
            </tr>
            <tr>
              <td>Edited by</td>
              <td>Isi</td>
            </tr>
            <tr>
              <td>Created at</td>
              <td>Isi</td>
            </tr>
            <tr>
              <td>Edited at</td>
              <td>Isi</td>
            </tr>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Edit</button>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.29/sweetalert2.min.js"></script>

<!-- Swall2 -->
<script type="text/javascript">
  <?php if($this->session->userdata('swal') == 1) {?>
  Swal.fire({
    position: 'top-center',
    type: 'success',
    title: 'Data berhasil disimpan !',
    showConfirmButton: false,
    timer: 2500
  })
  <?php }elseif ($this->session->userdata('swal') == 3) {?>
  Swal.fire({
      position: 'top-center',
      type: 'error',
      title: 'Bermasalah dengan file foto !',
      showConfirmButton: false,
      timer: 2500
    })
  <?php }elseif ($this->session->userdata('swal') == 2) {?>
    Swal.fire({
    position: 'top-center',
    type: 'error',
    title: 'Data gagal disimpan !',
    showConfirmButton: false,
    timer: 2500
  })
  <?php } ?>
</script>
<!-- ====== -->

<script type="text/javascript">
  $('.detail-vendor').on('click', function(e){
    e.preventDefault();
    var uuid = $(this).data('uuid');
    $.ajax({
      url : "<?php echo base_url('dashboard/Data_vendor/getbyuuid/');?>"+uuid,
      success : function(e){
       $('#detail-vendor .modal-body').html(e);
       $('#detail-vendor').modal({show:true});
     }
   });
  })
</script>
<script type="text/javascript">
  $('.detail').on('click', function(e){
    e.preventDefault();
    var uuid = $(this).data('uuid');
    $.ajax({
      url : "<?php echo base_url('dashboard/Data_vendor/detail/');?>"+uuid,
      success : function(e){
       $('#detail .modal-body').html(e);
       $('#detail').modal({show:true});
     }
   });
  })
</script>

<script type="text/javascript">
  $("#detail-vendor").on("click", ".reset-password", function(e) {
    e.preventDefault();
    uuid = $(this).data('id');
    console.log(uuid);
    swal({
      title: "Reset Password Vendor",
      text: "Ingin reset password vendor?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Ya, saya yakin!',
      cancelButtonText: "Tidak, batalkan!",
    }).then((result) => {
      if(result.value){
        $.ajax({
          type : "POST",
          data : {uuid :  uuid},
          url : "http://localhost/api15/vendors/reset-password",
          success : function(hasil){
            hasil = JSON.parse(hasil);
            swal({
              title: hasil.status,
              text: hasil.message,
              type: hasil.status
            }).then((result) => {
              location.reload();
            });
          }
        });
      }
    });
  });
</script>
