<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a href="<?php echo base_url('dashboard/home'); ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-group fa-fw"></i> Vendor<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">Vendors <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="<?php echo base_url('dashboard/data_vendor'); ?>">All Vendors</a>
                            </li>
                            
                            <li>                            
                                <a href="<?php echo base_url('dashboard/data_vendor/request'); ?>">Request Edit Data 
                                <span style="background-color: red; border-radius: 50%; padding: 50%; padding: 
                                5px; color: white; font-weight: bold" id="data_req"></span></a>
                            </li>
                           
                        </ul>
                    </li>
                    <li>
                        <a href="#">Services <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="<?php echo base_url('dashboard/data_produk'); ?>">All Services</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Orders <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="<?php echo base_url('dashboard/data_pesanan'); ?>">All Orders</a>
                            </li>
                            <li>
                                <a href="#">Add Orders</a>
                            </li>
                        </ul>
                        <!-- /.nav-third-level -->
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-user fa-fw"></i> Client<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">Users <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="<?php echo base_url('dashboard/data_customer'); ?>">All Users</a>
                            </li>
                            <li>
                                <a href="#">Add Users</a>
                            </li>
                        </ul>
                        <!-- /.nav-third-level -->
                    </li>
                    <li>
                        <a href="#">Cart List</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-th-large fa-fw"></i> Others<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo base_url('dashboard/data_user'); ?>">Akun Back Office</a>
                    </li>
                    <li>
                        <a href="#">Interest <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="#">Categories</a>
                            </li>
                            <li>
                                <a href="#">Tags</a>
                            </li>
                        </ul>
                        <!-- /.nav-third-level -->
                    </li>
                    <li>
                        <!--<a href="#" class="pengembangan">Pertanyaan</a>-->
                        <a href="#">Pertanyaan <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="<?php echo base_url('dashboard/masalah'); ?>">All Question</a>
                            </li>
                            <!--<li>
                                <a href="<?php // echo base_url('dashboard/question/add'); ?>">Add Question & Answer</a>
                            </li>-->
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="pengembangan">Data User</a>
                    </li>
                    <li>
                        <a href="#">Live Chat <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="<?php echo base_url('dashboard/chat'); ?>">Chat Users</a>
                            </li>
                            <li>
                                <a href="#">Chat Vendors</a>
                            </li>
                        </ul>
                        <!-- /.nav-third-level -->
                    </li>                          
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-th-large fa-fw"></i> YEPS Admin<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">Portfolio <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="<?php echo base_url('dashboard/portfolio'); ?>">All Portfolio</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('dashboard/portfolio/add'); ?>">Add Portfolio</a>
                            </li>
                        </ul>
                        <!-- /.nav-third-level -->
                    </li>
                    <li>
                        <a href="#">FAQs<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="<?php echo base_url('dashboard/question'); ?>">All FAQs</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('dashboard/question/add'); ?>">Add FAQs</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Karir <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="<?php echo base_url('dashboard/career'); ?>">All Karir</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('dashboard/career/add'); ?>">Add Karir</a>
                            </li>
                        </ul>
                        <!-- /.nav-third-level -->
                    </li>                     
                </ul>
                <!-- /.nav-second-level -->
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>
