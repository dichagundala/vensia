<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Model{
	
	public function get(){
		return $this->db->query("Select invoice.uuid as 'Invoice Uuid', invoice.kode_invoice as 'Kode Invoice',  invoice.status as 'Status Invoice', invoice.created_at as 'Tanggal Pesanan' from invoice order by invoice.created_at desc")->result_array();
	}

	public function get_by_uuid($uuid){
		return $this->db->query("Select invoice.uuid as 'Invoice Uuid', invoice.kode_invoice as 'Kode Invoice',  invoice.status as 'Status Invoice', invoice.confirm_at as 'Tanggal Konfirm', invoice.created_at as 'Tanggal Pesanan', invoice.cicilan as 'Cicilan', invoice.total_biaya as 'Biaya Total', invoice.biaya_dp as 'Biaya DP', invoice.metode_pembayaran as 'Metode Pembayaran' from invoice where uuid like '$uuid'")->result_array();
	}

	public function get_rek($name){
		return $this->db->query("Select bank_number, account_name from manual_bank_account where bank_name like '$name'")->result_array();
	}

	public function get_va($uuid){
		return $this->db->query("Select id_ipaymu as 'bank_number', no_ipaymu as 'account_name' from invoice where uuid like '$uuid'")->result_array();
	}

	public function konfirmasi($uuid){
		$this->db->query("update invoice set `status` = 'MENUNGGU PEMBAYARAN', `confirm_at` = CURRENT_TIME where uuid like '$uuid'");
		return ($this->db->affected_rows() != 1) ? false : true;
	}
	public function failed($uuid){
		$this->db->query("update invoice set `status` = 'FAILED', `confirm_at` = CURRENT_TIME where uuid like '$uuid'");
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function transfer($uuid, $status){
		$this->db->query("update invoice set `status` = '$status' where uuid like '$uuid'");
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function ipaymu($uuid, $ipaymu, $id){

		$this->db->where('uuid' , $uuid);
		$this->db->update('invoice', array('id_ipaymu' => $ipaymu, 'no_ipaymu' => $id));
		return ($this->db->affected_rows() >= 1) ? true : false;
	}

	public function midtrans($data, $invoice){
		$this->db->where("kode_invoice", $invoice);
		$this->db->update("invoice", $data);
		return ($this->db->affected_rows() >= 1) ? true : false;
	}
}

?>