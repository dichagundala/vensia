<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Question_model extends CI_Model{
	
	public function create($table,$data){
      $query = $this->db->insert($table, $data);
      return $query;
   }

	public function read($table){
     $this->db->select('*');
	 $this->db->from($table); 
	 $this->db->order_by('id','DESC');
	 $query = $this->db->get();
	 return $query->result_array();
   }

   public function readbyid($table,$id){
     $this->db->select('*');
	 $this->db->from($table); 
	 $this->db->where('id', $id);
	 $query = $this->db->get();
	 // return $query->result_array();
	 return $query->row_array();
   }

	public function update($data,$id,$table) {
		$this->db->set($data);
		$this->db->where('id', $id);
		$this->db->update($table);
		return $this->db->affected_rows();
	}

	public function delete($table, $id){
	   	$this->db->where('id', $id);
		$query = $this->db->delete($table);
		return $this->db->affected_rows();
		
   }
}

?>