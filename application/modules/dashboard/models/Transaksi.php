<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Model{
	
	public function get_min_date($uuid){
		return $this->db->query("Select distinct(date_order) as 'Tanggal Event' from transaksi where uuid_invoice like '$uuid' order by transaksi.date_order asc")->result_array();
	}

	public function get_client($uuid){
		return $this->db->query("Select distinct(date_order) as 'Tanggal Event', users.namaDepan as 'namadepan',users.namaBelakang as 'namaBelakang' from transaksi, users where transaksi.uuid_user = users.uuid and transaksi.uuid_invoice like '$uuid' order by transaksi.date_order asc")->result_array();
	}

	public function get($uuid){
		return $this->db->query("select users.namaVendor as 'nama', email, telp, nama_jasa, harga_normal, date_order, harga_promo, qty, uuid_user, transaksi.status as 'Status', vendor_kategori.nama as 'kategori', transaksi.note as 'note' from transaksi, produk, users, vendor_kategori where produk.kategori_id = vendor_kategori.id and transaksi.uuid_produk = produk.uuid and produk.uuid_vendor = users.uuid and uuid_invoice like '$uuid'")->result_array();
	}

	public function get1($uuid){
		return $this->db->query("select * from transaksi_add where uuid_transaksi = '$uuid'")->result_array();
	}

}

?>