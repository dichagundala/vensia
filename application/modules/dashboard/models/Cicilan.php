<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Cicilan extends CI_Model{
	
	public function edit($uuid, $insert, $update){
		$this->db->where('uuid', $uuid);
		if($this->db->get('cicilan')->num_rows() >= 1){
			$this->db->where('uuid', $uuid);
			$this->db->update('cicilan', $update);
		}else{
			$this->db->insert('cicilan', $insert);
		}
	}

	public function lunas($uuid){
		$this->db->where("uuid", $uuid);
		$this->db->update('cicilan', array("status" => "LUNAS"));
	}

	public function get($uuid){
		$this->db->where("uuid_invoice", $uuid);
		return $this->db->get("cicilan")->result_array();
	}

	public function get1($uuid){
		return $this->db->query("select SUM(biaya_cicilan) from cicilan where uuid_invoice = '$uuid'")->result_array();
	}
}

?>