<?php 

class Cro extends CI_Model{ 
    function cek_login($table,$where){      
        $this->db->select('*');
		    $this->db->from($table);
		    $this->db->where($where);
    	  $query = $this->db->get();
    	  return $query->result_array()[0];
    }

    public function get(){  
		return $this->db->query("select * from cro order by created_date desc")->result_array();
    }

    public function add($data_register){  
		return $this->db->insert("cro", $data_register);
    }   
}