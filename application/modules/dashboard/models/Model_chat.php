<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_chat extends CI_Model {

	function getAll($where="")
	{
		if($where) $this->db->where($where);
		$this->db->order_by("created","ASC");
		return $this->db->get("chatclient");
	}
	function getAll2($where)
	{
		if($where) $this->db->where($where);
		return $this->db->get('user_pengunjung');
	}
	function getInsert($data){
		return $this->db->set($data)->insert("chatclient");
	}
	
	function getLastId($where){
		return $this->db->where($where)->order_by("id","DESC")->limit(1)->get("chatclient")->row_array();
	}
}
