<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Model{
	
	public function get($uuid){
		$this->db->where('uuid', $uuid);
		return $this->db->get('users')->result_array();
	}

	public function get_User(){
		return $this->db->query("select * from users ORDER BY id DESC")->result_array();
	}

	public function get1(){
		return $this->db->query("select * from users")->num_rows();	
	}

	public function get2(){
		return $this->db->query("select * from users where status = 1 or status = 3")->num_rows();	
	}

	public function get3(){
		return $this->db->query("select * from users where status=2 or status=4")->num_rows();	
	}

	public function get4($bulan){
		return $this->db->query("SELECT * FROM users WHERE vCreated LIKE '%".$bulan."%'")->num_rows();
	}
	public function getall($uuid){
		$hasil = $this->db->query("SELECT * FROM users where uuid = '$uuid'")->result_array()[0];
		return $hasil;
	}
	public function banned_user($uuid){
		$this->db->where('uuid', $uuid);
		$this->db->update('users', array('aktifUser' => '3'));
		return ($this->db->affected_rows() != 1) ? false : true;
	}
}
 
?>