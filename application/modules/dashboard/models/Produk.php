<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Model{
	
	public $table = "product";

	public function get_product_by_kategori($kategori){
		$result = $this->db->query("select produk.uuid as 'uuid', vendor.nama, vendor_kategori.nama, parameter.nama as 'nama', produk_detail.value as 'value' from produk, produk_detail, parameter, vendor_kategori, vendor, trx_kategori_parameter where 
			produk.uuid_vendor = vendor.uuid and 
			produk.kategori_id = vendor_kategori.id and 
			produk_detail.uuid_produk = produk.uuid and
			produk_detail.kategori_parameter_id = trx_kategori_parameter.id and
			vendor_kategori.id = trx_kategori_parameter.kategori_id and 
			parameter.id = trx_kategori_parameter.parameter_id and vendor_kategori.nama like '$kategori'")->result_array();

		return $result;
	}
	public function get_produk_by_uuid($uuid){
		$result = $this->db->query("select produk.uuid as 'uuid', vendor.nama as 'nama_vendor', vendor_kategori.nama as 'nama_kategori', parameter.nama as 'nama', produk_detail.value as 'value', vendor_kategori.nama as 'kategori', produk.created_at as 'tanggal', produk.slug as 'slug' from produk, produk_detail, parameter, vendor_kategori, vendor, trx_kategori_parameter where 
			produk.uuid_vendor = vendor.uuid and 
			produk.kategori_id = vendor_kategori.id and 
			produk_detail.uuid_produk = produk.uuid and
			produk_detail.kategori_parameter_id = trx_kategori_parameter.id and
			vendor_kategori.id = trx_kategori_parameter.kategori_id and 
			parameter.id = trx_kategori_parameter.parameter_id and produk.uuid like '$uuid' and parameter.nama not like 'Fasilitas'")->result_array();

		return $result;
	}
	
	public function get_id_product($kategori){
		$result = $this->db->query("select DISTINCT(produk.uuid) as 'uuid' from produk, produk_detail, parameter, vendor_kategori, vendor, trx_kategori_parameter where 
			produk.uuid_vendor = vendor.uuid and 
			produk.kategori_id = vendor_kategori.id and 
			produk_detail.uuid_produk = produk.uuid and
			produk_detail.kategori_parameter_id = trx_kategori_parameter.id and
			vendor_kategori.id = trx_kategori_parameter.kategori_id and 
			parameter.id = trx_kategori_parameter.parameter_id and vendor_kategori.nama like '$kategori'")->result_array();

		return $result;
	}
	public function countKategori($kategori){
		$result = $this->db->query("select * from produk, vendor_kategori where produk.kategori_id = vendor_kategori.id and vendor_kategori.nama like '$kategori'");
		return $result->num_rows();
	}

	public function cekMinimal($uuid){

		$result = $this->db->query("SELECT produk_detail.value as 'value' from produk_detail, trx_kategori_parameter, parameter where trx_kategori_parameter.parameter_id = parameter.id and parameter.nama = 'Minimal Order' and produk_detail.kategori_parameter_id = trx_kategori_parameter.id and uuid_produk = '$uuid'");

		$a = $result->result_array();
		if($result->num_rows() >= 1){
			$rs = (int)$a[0]["value"];
		}else{
			$rs = 1;
		}
		return $rs;
	}

	public function get_produk_by_vendor($vendor){
		$result = $this->db->query("select uuid, status_produk, kategori_id, slug from produk where uuid_vendor like '$vendor'")->result_array();

		return $result;
	}

	public function get_produk_all(){
		$result = $this->db->query("select uuid, status_produk from produk order by created_at desc")->result_array();

		return $result;
	}

	public function status_produk_update($uuid, $status){

		$this->db->query("UPDATE produk set status_produk = '$status', updated_at = CURRENT_TIME where uuid = '$uuid'");
		return ($this->db->affected_rows() >= 1) ? true : false;
	}
	
	public function get_total_service($uuid){
		$result = $this->db->query("SELECT count(uuid_vendor) as hasil from (SELECT * FROM `produk` WHERE uuid_vendor like '$uuid') hasil")->result_array();

		return $result;
	}

	public function get_total_service1(){
		return $this->db->query("select * from produk")->num_rows();
	}
}

?>