<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends CI_Model{
	
	public function get(){
		return $this->db->query("select * from users where status=3 or status=4")->result_array();	
	}

	public function get1(){
		return $this->db->query("select * from users where status=3 or status=4")->num_rows();	
	}

	public function get3(){
		return $this->db->query("select * from users where aktifVendor=1")->num_rows();	
	}

	public function get2($bulan){
		return $this->db->query("SELECT * FROM users WHERE vCreated LIKE '%".$bulan."%'")->num_rows();	
	}
	public function getall($uuid){
		$hasil = $this->db->query("SELECT * FROM users where uuid = '$uuid'")->result_array()[0];
		return $hasil;
	}

	public function getubah(){
		$hasil = $this->db->query("SELECT vendor_address.*,users.namaVendor AS nama,users.kategori as kategori_lama,users.alamatVendor as alamat_lama,
			users.vProvinsi as provinsilama,users.vKabupaten as kabupatenlama,users.kecamatan AS kecamatanlama,users.kelurahan as kelurahanlama 
			FROM vendor_address,users WHERE vendor_address.status=0 and vendor_address.uuid=users.uuid")->result_array();
		return $hasil;
	}

	public function prosesaktivasi($uuid){
		$this->db->where('uuid', $uuid);
		$this->db->update('users', array('aktifVendor' => '3'));
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function aktivasisetuju($uuid){
		$this->db->where('uuid', $uuid);
		$this->db->update('users', array('aktifVendor' => '1'));
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function banned_vendor($uuid){
		$this->db->where('uuid', $uuid);
		$this->db->update('users', array('aktifVendor' => '6'));
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function aktivasiditolak($uuid){
		$this->db->where('uuid', $uuid);
		$this->db->update('users', array('aktifVendor' => '4'));
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function getNotif($uuid){
		return $this->db->query("select * from vendor where view like 'false' limit 4");
	}

	public function notif_ubah(){
		return $this->db->query("SELECT COUNT(id) AS total FROM vendor_address WHERE status=0")->result_array();	
	}
	
	public function  accepted_cro($id){
		$this->db->set('status', "1");
		$data = array(
			'status' => "1",
			'keterangan' => "Diterima"
		);
		$this->db->where('id', $id);
		return $this->db->update('vendor_address',$data);	
	}
	public function  rejected_cro($id){
		$this->db->set('status', "1");
		$data = array(
			'status' => "1",
			'keterangan' => "Ditolak"
		);
		$this->db->where('id', $id);
		return $this->db->update('vendor_address',$data);	
	}
	public function ubah_vendor($uuid,$data){
		$this->db->where('uuid', $uuid);
		return $this->db->update('users', $data);	
	}
	public function getubah_byid($id){
		return $this->db->query("select * from vendor_address where id='$id'")->result_array()[0];	
	}
	public function lihat_ubah(){
		$hasil = $this->db->query("SELECT vendor_address.*,users.namaVendor AS nama 
			FROM vendor_address,users WHERE vendor_address.status=1 and vendor_address.uuid=users.uuid")->result_array();
		return $hasil;
	}

}

?>