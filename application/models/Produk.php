<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Model{
	
	public $table = "product";

	public function get_product_by_kategori($kategori){
		$result = $this->db->query("select produk.uuid as 'uuid', users.namaVendor, vendor_kategori.nama, parameter.nama as 'nama', produk_detail.value as 'value' from produk, produk_detail, parameter, vendor_kategori, users, trx_kategori_parameter where 
			produk.uuid_vendor = users.uuid and 
			produk.kategori_id = vendor_kategori.id and 
			produk_detail.uuid_produk = produk.uuid and
			produk_detail.kategori_parameter_id = trx_kategori_parameter.id and
			vendor_kategori.id = trx_kategori_parameter.kategori_id and 
			parameter.id = trx_kategori_parameter.parameter_id and vendor_kategori.nama like '$kategori' and (users.aktifVendor like '1' OR users.aktifVendor like '5') and produk.status_produk='2'")->result_array();

		return $result;
	}
	//untuk harga & qty additional Produk
	function get_subkategori($id){
        $hasil=$this->db->query("SELECT * FROM add_produk WHERE id='$id'");
        return $hasil->result();
	}

	//untuk mengambil data add produk dengan uuid
	function get_subkategori_byuuid($uuid){
        $hasil=$this->db->query("SELECT * FROM add_produk WHERE uuidProduk='$uuid'");
        return $hasil->result();
	}
	
	public function get_produk_by_vendor($vendor){
		$result = $this->db->query("select produk.uuid as 'uuid', parameter.nama as 'nama', produk_detail.value as 'value' from produk, produk_detail, parameter, vendor_kategori, users, trx_kategori_parameter where 
			produk.uuid_vendor = users.uuid and 
			produk.kategori_id = vendor_kategori.id and 
			produk_detail.uuid_produk = produk.uuid and
			produk_detail.kategori_parameter_id = trx_kategori_parameter.id and
			vendor_kategori.id = trx_kategori_parameter.kategori_id and 
			parameter.id = trx_kategori_parameter.parameter_id and users.uuid like '$vendor'")->result_array();

		return $result;
	}

	public function get_produk_by_uuid($uuid){
		$result = $this->db->query("select produk.uuid as 'uuid', users.uuid as 'uuid_vendor', users.namaVendor as 'nama_vendor',users.aktifVendor AS 'aktif_vendor', vendor_kategori.nama as 'nama_kategori', parameter.nama as 'nama', produk_detail.value as 'value', produk.slug as 'slug' 
			from produk, produk_detail, parameter, vendor_kategori, users, trx_kategori_parameter where 
			produk.uuid_vendor = users.uuid and 
			produk.kategori_id = vendor_kategori.id and 
			produk_detail.uuid_produk = produk.uuid and
			produk_detail.kategori_parameter_id = trx_kategori_parameter.id and
			vendor_kategori.id = trx_kategori_parameter.kategori_id and 
			parameter.id = trx_kategori_parameter.parameter_id and produk.uuid like '$uuid'  and (users.aktifVendor like '1' OR users.aktifVendor like '5') and produk.status_produk='2'")->result_array();

		return $result;
	}
	//untuk memanggil additional yang sudah masuk ke cart
	public function get_cart_add($uuid){
		$result= $this->db->query("select transaksi_add.id as 'id', transaksi_add.nama_add as 'nama_add', 
		transaksi_add.harga_add as 'harga_add', transaksi_add.qty_add as 'qty_add' , add_produk.qty as 'minimal' , 
		(transaksi_add.harga_add*transaksi_add.qty_add) as total_add
		from transaksi, transaksi_add, add_produk where transaksi_add.uuid_transaksi = transaksi.uuid AND 
			transaksi_add.uuid_transaksi = '$uuid' AND transaksi.uuid_produk = add_produk.uuidProduk AND transaksi_add.nama_add = add_produk.nama")->result_array();
		/*
		$result = $this->db->query("select transaksi_add.id as 'id', transaksi_add.nama_add as 'nama_add', transaksi_add.harga_add as 'harga_add', 
		transaksi_add.qty_add as 'qty_add' , transaksi_add.uuid_transaksi as 'uuid_add'
			from transaksi, transaksi_add where 
			transaksi_add.uuid_transaksi = transaksi.uuid AND transaksi_add.uuid_transaksi like '$uuid' ")->result_array();
		*/
		return $result;
	}
	/* untuk mengambil data additional yang sudah di daftarkan sesuai uuid*/
	public function get_add_produk_by_uuid($uuid){
		$result = $this->db->query("select * 
			from add_produk 
			where 
			uuidProduk like '$uuid' ")->result_array();

		return $result;
	}
	/* untuk mengambil jumlah data additional yang sudah di daftarkan sesuai uuid*/
	public function get_sum_add_produk_by_uuid($uuid){
		$result = $this->db->query("select * 
			from add_produk 
			where 
			uuidProduk like '$uuid' ")->num_rows();

		return $result;
	}
	public function get_produk_by_uuid2($vendor, $produk){
		$vendor = str_replace("_", " ", $vendor);
		
		$result = $this->db->query("select produk.uuid as 'uuid', users.uuid as 'uuid_vendor', users.namaVendor as 'nama_vendor',users.aktifVendor as 'aktifVendor', vendor_kategori.nama as 'nama_kategori', parameter.nama as 'nama', produk_detail.value as 'value' from produk, produk_detail, parameter, vendor_kategori, users, trx_kategori_parameter where 
			produk.uuid_vendor = users.uuid and 
			produk.kategori_id = vendor_kategori.id and 
			produk_detail.uuid_produk = produk.uuid and
			produk_detail.kategori_parameter_id = trx_kategori_parameter.id and
			vendor_kategori.id = trx_kategori_parameter.kategori_id and 
			parameter.id = trx_kategori_parameter.parameter_id and users.namaVendor like '$vendor' and produk.slug like '$produk' and (users.aktifVendor like '1' OR users.aktifVendor like '5') and produk.status_produk='2'")->result_array();
		return $result;

	}
	
	public function get_id_product($kategori){
		$result = $this->db->query("select DISTINCT(produk.uuid) as 'uuid', users.namaVendor as 'nama vendor' from produk, produk_detail, parameter, vendor_kategori, users, trx_kategori_parameter where 
			produk.uuid_vendor = users.uuid and 
			produk.kategori_id = vendor_kategori.id and 
			produk_detail.uuid_produk = produk.uuid and
			produk_detail.kategori_parameter_id = trx_kategori_parameter.id and
			vendor_kategori.id = trx_kategori_parameter.kategori_id and 
			parameter.id = trx_kategori_parameter.parameter_id and vendor_kategori.nama like '$kategori' and (users.aktifVendor like '1' OR users.aktifVendor like '5') and produk.status_produk='2'")->result_array();

		return $result;
	}

public function get_produk_search($key, $page){
		$offset = ($page-1) * 12;
		$result = $this->db->query("select DISTINCT(produk.uuid) as 'uuid' from produk, produk_detail, parameter, vendor_kategori, users, trx_kategori_parameter where 
			produk.uuid_vendor = users.uuid and 
			produk.kategori_id = vendor_kategori.id and 
			produk_detail.uuid_produk = produk.uuid and
			produk_detail.kategori_parameter_id = trx_kategori_parameter.id and
			vendor_kategori.id = trx_kategori_parameter.kategori_id and 
			parameter.id = trx_kategori_parameter.parameter_id and parameter.nama = 'Nama' and produk_detail.value like '%$key%' and (users.aktifVendor like '1' OR users.aktifVendor like '5') limit 12 offset $offset")->result_array();

		return $result;
	}
public function get_produk_search_count($key){
		
		$result = $this->db->query("select count(DISTINCT(produk.uuid)) as 'uuid' from produk, produk_detail, parameter, vendor_kategori, users, trx_kategori_parameter where 
			produk.uuid_vendor = users.uuid and 
			produk.kategori_id = vendor_kategori.id and 
			produk_detail.uuid_produk = produk.uuid and
			produk_detail.kategori_parameter_id = trx_kategori_parameter.id and
			vendor_kategori.id = trx_kategori_parameter.kategori_id and 
			parameter.id = trx_kategori_parameter.parameter_id and parameter.nama = 'Nama' and produk_detail.value like '%$key%' and (users.aktifVendor like '1' OR users.aktifVendor like '5')")->result_array()[0]['uuid'];

		return $result;
	}

	public function get_id_product_page($kategori, $page){
		$offset = ($page-1) * 12;
		$result = $this->db->query("select DISTINCT(produk.uuid) as 'uuid', users.namaVendor as 'nama vendor' from produk, produk_detail, parameter, vendor_kategori, users, trx_kategori_parameter where 
			produk.uuid_vendor = users.uuid and 
			produk.kategori_id = vendor_kategori.id and 
			produk_detail.uuid_produk = produk.uuid and
			produk_detail.kategori_parameter_id = trx_kategori_parameter.id and
			vendor_kategori.id = trx_kategori_parameter.kategori_id and 
			parameter.id = trx_kategori_parameter.parameter_id and vendor_kategori.nama like '$kategori' and (users.aktifVendor like '1' OR users.aktifVendor like '5') and produk.status_produk='2' limit 12 offset $offset")->result_array();

		return $result;
	}


	public function get_id_product_vendor($vendor, $page){
		$offset = ($page-1) * 10;
		$result = $this->db->query("select DISTINCT(produk.uuid) as 'uuid' from produk, produk_detail, parameter, vendor_kategori, users, trx_kategori_parameter where 
			produk.uuid_vendor = users.uuid and 
			produk.kategori_id = vendor_kategori.id and 
			produk_detail.uuid_produk = produk.uuid and
			produk_detail.kategori_parameter_id = trx_kategori_parameter.id and
			vendor_kategori.id = trx_kategori_parameter.kategori_id and 
			parameter.id = trx_kategori_parameter.parameter_id and users.uuid like '$vendor' limit 10 offset $offset")->result_array();

		return $result;
	}

	public function get_produk_count($vendor){
		$result = $this->db->query("select DISTINCT(produk.uuid) as 'uuid' from produk, produk_detail, parameter, vendor_kategori, users, trx_kategori_parameter where 
			produk.uuid_vendor = users.uuid and 
			produk.kategori_id = vendor_kategori.id and 
			produk_detail.uuid_produk = produk.uuid and
			produk_detail.kategori_parameter_id = trx_kategori_parameter.id and
			vendor_kategori.id = trx_kategori_parameter.kategori_id and 
			parameter.id = trx_kategori_parameter.parameter_id and users.uuid like '$vendor' and (users.aktifVendor like '1' OR users.aktifVendor like '5') and produk.status_produk='2'");

		return $result->num_rows();
	}

	public function countKategori($kategori){
		$result = $this->db->query("select * from produk, vendor_kategori, users where users.uuid = produk.uuid_vendor and produk.kategori_id = vendor_kategori.id and vendor_kategori.nama like '$kategori' and (users.aktifVendor like '1' OR users.aktifVendor like '5')");
		return $result->num_rows();
	}

	public function cekMinimal($uuid){

		$result = $this->db->query("SELECT produk_detail.value as 'value' from produk_detail, trx_kategori_parameter, parameter where trx_kategori_parameter.parameter_id = parameter.id and parameter.nama = 'Minimal Order' and produk_detail.kategori_parameter_id = trx_kategori_parameter.id and uuid_produk = '$uuid'");

		$a = $result->result_array();
		if($result->num_rows() >= 1){
			$rs = (int)$a[0]["value"];
		}else{
			$rs = 1;
		}
		return $rs;
	}
		
	public function get_produk_all(){
		$result = $this->db->query("select uuid, status_produk from produk order by created_at desc")->result_array();

		return $result;
	}


}

?>