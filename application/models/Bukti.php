<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Bukti extends CI_Model{
	
	public function add($data){
		json_encode($data);
		$this->db->insert('bukti_transfer', $data);
		return ($this->db->affected_rows() >= 1) ? true : false;
	}
	public function addMasalah($data){
		json_encode($data);
		$this->db->insert('masalah', $data);
		return ($this->db->affected_rows() >= 1) ? true : false;
	}
}

?>