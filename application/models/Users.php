<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Model{
	
	public $table = "users";
	public function input_data($data){
		$this->db->insert("users", $data);
	}

	public function verifikasi_email($data){
		$cek_data = $this->db->query("SELECT * FROM users where authEmail like '$data'");
		if($cek_data->num_rows() >= 1){
			$this->db->query("UPDATE users SET statusEmail = 1, status=1, aktifUser=1 where authEmail like '$data'");
			return 1;
		}else{
			return 0;
		}

	}

	public function verifikasi_forgetPass($data){
		$cek_data = $this->db->query("SELECT * FROM users where authForget like '$data'");
		if($cek_data->num_rows() >= 1){
			return 1;
		}else{
			return 0;
		}

	}

	public function cek_user($email){
		$hasil = $this->db->query("SELECT * FROM users where email = '$email' AND statusEmail = 1 ");
		return $hasil;
	}

	public function get_client($data){
		$hasil = $this->db->query("SELECT * FROM users where uuid = '$data'")->result_array()[0];
		return $hasil;
	}

	public function get_client_forget($email){
		$hasil = $this->db->query("SELECT uuid FROM users where authForget = '$email'")->result_array()[0];
		return $hasil['uuid'];
	}

	public function update_client($data, $uuid){
		$this->db->where('uuid',$uuid);
		$this->db->update('users', $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function update_client_img($data){
		$img = $data['img'];
		$uuid = $data['uuid'];
		$this->db->query("UPDATE users set img = '$img' where uuid like '$uuid'");
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function cek_account($email){
		$hasil = $this->db->query("SELECT * FROM users where email like '$email'");
		if($hasil->num_rows() >= 1){
			return true;
		}else{
			return false;
		}
	}

	public function cek_account_regis($email){
		$hasil = $this->db->query("SELECT * FROM users where email like '$email'");
		if($hasil->num_rows() >= 1){
			$status = $hasil->result_array()[0]['statusEmail'];
			if($status == "1"){
				return 2;
			}else{
				return 1;
			}
		}else{
			return 0;
		}
	}

	public function update_auth_email($email, $auth){
		$data = array(
			'authForget' => $auth
		);

		$this->db->query("UPDATE users set authForget = '$auth' where email like '$email'");
		return ($this->db->affected_rows() != 1) ? false : true;

	}

	public function get_name_by_email($email)
	{
		$this->db->where('email', $email);
		return $this->db->get('users')->result_array()[0]['namaDepan'];
	}


}

?>