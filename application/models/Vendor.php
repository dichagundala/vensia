<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends CI_Model{
	
	public function get($uuid){
		$this->db->where("uuid", $uuid);
		$this->db->select("namaVendor, bioVendor, kategori, publicId, uuid, vKabupaten");
		return $this->db->get("users")->result_array()[0];
	}
	public function get_by_name($name){
		$name = str_replace("_", " ", $name);
		
		return $this->db->query("Select uuid from users where namaVendor like '$name'")->result_array()[0];
	}

	public function get_search($name){
		return $this->db->query("Select * from users where namaVendor like '%$name%'")->result_array();
	}

	public function get_email_invoice($invoice){
		/*return $this->db->query("select vendor.namaVendor as 'nama', vendor.email as 'email', 
		transaksi.date_order as 'waktu', transaksi.time_order as 'jam', transaksi.harga_normal 'normal', 
		transaksi.harga_promo as 'promo', transaksi.qty as 'qty', invoice.created_at as 'tgl_pesan', 
		t_user.namaDepan as 'nama_client',t_user.namaBelakang as 'nama_client_belakang', t_user.email as 'useremail' 
		from invoice, transaksi, users as vendor, produk, users as t_user 
		where 'transaksi'. 'uuid_user' = t_user.uuid and invoice.uuid = transaksi.uuid_invoice 
			and transaksi.uuid_produk = produk.uuid and produk.uuid_vendor = vendor.uuid and invoice.kode_invoice = '$invoice'")->result_array();*/
		return $this->db->query("select vendor.namaVendor as 'nama', vendor.email as 'email', transaksi.date_order as 'waktu', 
		transaksi.time_order as 'jam', transaksi.harga_normal 'normal', transaksi.harga_promo as 'promo', transaksi.qty as 'qty', 
		invoice.created_at as 'tgl_pesan', t_user.namaDepan as 'nama_client',t_user.namaBelakang as 'nama_client_belakang',
		t_user.email as 'useremail' from invoice, transaksi, users as vendor, produk, users as t_user 
		where transaksi.uuid_user = t_user.uuid and invoice.uuid = transaksi.uuid_invoice 
		and transaksi.uuid_produk = produk.uuid and produk.uuid_vendor = vendor.uuid and invoice.kode_invoice = '$invoice'")->result_array();
	
}
	
	public function get_album($uuid){
		$this->db->where("uuid_vendor", $uuid);
		return $this->db->get("vendor_album")->result_array();
	}
	
	public function get_foto_by_album($uuid){
		return $this->db->query("select public_id,ket_foto from vendor_foto where uuid_album='$uuid'")->result_array();
	}
}

?>