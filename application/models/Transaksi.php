<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Model{
	
	public function add($data){
		$this->db->insert('transaksi', $data);

		return ($this->db->affected_rows() != 1) ? false : true;
	}
	//add transaksi additional
	public function add_additional($data2){
		$this->db->insert('transaksi_add', $data2);

		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function get_cart($uuid){
		$where = array('uuid_user' => $uuid, 'status' => 'CART');
		$this->db->where($where);
		return $this->db->get('transaksi')->result_array();
	}
	
	
	public function delete_cart($uuid){
		$this->db->where('uuid',$uuid);
		$this->db->delete('transaksi');
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function qty($uuid, $qty){
		$this->db->query("UPDATE transaksi set qty = $qty where uuid like '$uuid' ");
	}
	//untuk update transaksi_add
	public function qty_add($uuid, $qty, $id){
		$this->db->query("UPDATE transaksi_add set qty_add = $qty where id like '$id' AND uuid_transaksi like '$uuid' ");
	}

	public function inc_invoice(){
		$date = new dateTime('Now');
		$dateStr = $date->format('Y-m')."%";
		$result = $this->db->query("SELECT kode_invoice FROM invoice where created_at like '$dateStr' order by id desc");
		if($result->num_rows() >= 1){
			return $result->result_array()[0];
		}else{
			return "";
		}
	}

	public function update_note($note, $uuid){
		$this->db->query("Update transaksi set note = '$note' where uuid like '$uuid'");
	}

	public function update_real($uuid_invoice, $uuid, $nama_jasa, $harga_normal, $harga_promo){
		$this->db->where('uuid', $uuid);
		$data = array(
			"status" => "INVOICE",
			"nama_jasa" => $nama_jasa,
			"harga_normal" => $harga_normal,
			"harga_promo" => $harga_promo,
			"uuid_invoice" => $uuid_invoice
		);

		$this->db->update("transaksi", $data);
	}

	public function read($table){
     $this->db->select('*');
	 $this->db->from($table); 
	 $this->db->order_by('id','DESC');
	 $query = $this->db->get();
	 return $query->result_array();
   }

   public function readbyid($table,$id){
     $this->db->select('*');
	 $this->db->from($table); 
	 $this->db->where('id', $id);
	 $query = $this->db->get();
	 // return $query->result_array();
	 return $query->row_array();
   }

}


?>