<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Model{

	public function add($data){
		$this->db->insert('invoice', $data);
		return ($this->db->affected_rows() >= 1) ? true : false;
	}

	public function get($uuid){
		
		return $this->db->query("select transaksi.qty as 'qty', transaksi.uuid as 'Transaksi Uuid', produk.uuid as 'Produk Uuid', 
			transaksi.status as 'Status Transaksi', transaksi.harga_normal as 'Harga Normal', transaksi.harga_promo as 'Harga Promo', 
			users.namaDepan as 'Nama', users.telp as 'No Telp', users.email as 'email', invoice.kode_invoice as 'Invoice', 
			invoice.id_ipaymu as 'Ipaymu', invoice.metode_pembayaran as 'Metode Pembayaran', invoice.status as 'Status', 
			invoice.cicilan as 'Angsuran', invoice.confirm_at as 'Tgl Konfirm', invoice.created_at as 'Tgl Pemesanan', 
			invoice.updated_at as 'Tgl Pembayaran', transaksi.date_order as 'Tgl Event', invoice.total_biaya as 'Total Biaya', 
			invoice.biaya_dp as 'DP', invoice.alamat as 'Alamat', transaksi.note as 'Note' from 
			transaksi, invoice, users, produk where  
			transaksi.uuid_produk = produk.uuid and 
			transaksi.uuid_invoice = invoice.uuid and 
			transaksi.uuid_user = users.uuid and 
			invoice.uuid like '$uuid' ORDER BY transaksi.date_order desc")->result_array();
	}

	public function get_invoice($uuid){
		return $this->db->query("Select distinct(invoice.uuid) as 'Invoice Uuid', invoice.kode_invoice as 'Kode Invoice',  invoice.status as 'Status Invoice', invoice.created_at as 'Tgl Pesan', cicilan from invoice, transaksi where invoice.uuid = transaksi.uuid_invoice and transaksi.uuid_user like '$uuid'")->result_array();

	}

	public function cancel($uuid){

		$this->db->where('uuid' , $uuid);
		$this->db->update('invoice', array('status' => 'CANCEL'));
		return ($this->db->affected_rows() >= 1) ? true : false;
	}

	public function confirm($uuid){

		$this->db->where('uuid' , $uuid);
		$this->db->update('invoice', array('status' => 'KONFIRMASI TRANSFER'));
		return ($this->db->affected_rows() >= 1) ? true : false;
	}

	public function ipaymu($uuid, $ipaymu){

		$this->db->where('uuid' , $uuid);
		$this->db->update('invoice', array('id_ipaymu' => $ipaymu));
		return ($this->db->affected_rows() >= 1) ? true : false;
	}

	public function auto_update(){

		$this->db->query("update invoice set status = 'CANCEL' where status = 'MENUNGGU PEMBAYARAN' and Now() >= (confirm_at + INTERVAL 1 day)");
		return ($this->db->affected_rows() >= 1) ? true : false;
	}
	public function get_total_add($uuid){
		return $this->db->query("SELECT sum(qty_add*harga_add) as total FROM transaksi_add WHERE uuid_transaksi='$uuid'")->result()[0];
	}

}


?>