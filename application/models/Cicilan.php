<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Cicilan extends CI_Model{
	
	public function get($uuid){
		return $this->db->query("Select distinct(invoice.uuid) as 'Invoice Uuid', invoice.kode_invoice as 'Kode Invoice',  invoice.status as 'Status Invoice', invoice.created_at as 'Tgl Pesan', cicilan from invoice, transaksi, cicilan where invoice.uuid = cicilan.uuid_invoice and invoice.uuid = transaksi.uuid_invoice and transaksi.uuid_user like '$uuid'")->result_array();
	}

	public function get_by_invoice($uuid){
		 $this->db->where("uuid_invoice", $uuid);
		 return $this->db->get("cicilan")->result_array();
	}

}

?>