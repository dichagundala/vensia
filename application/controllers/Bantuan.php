<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bantuan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model("transaksi");
		$this->load->model('Question_model');
		
	}
	public function index()
	{
		$data = array();
		$page = "Bantuan";
		$title = $page. " | "."YEPS Indonesia - Your Event Partner Solution";
		$desc = "Your Event Partner Solution";
		$data['desc'] = $desc;
		
		$data['title'] = $title;
		if ($this->ion_auth->logged_in()){
			// set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$data['users'] = $this->ion_auth->users()->result();
			foreach ($data['users'] as $k => $user)
			{
				$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			$data['login'] = $this->ion_auth->is_admin();

		}
		$this->load->view('blog', $data);
	}

	public function faq()
	{
		$data = array();
		if ($this->ion_auth->logged_in()){
			// set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$data['users'] = $this->ion_auth->users()->result();
			foreach ($data['users'] as $k => $user)
			{
				$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			$data['login'] = $this->ion_auth->is_admin();

		}

		$page = "Faq";
		$title = $page. " | "."YEPS Indonesia - Your Event Partner Solution";
		$desc = "Your Event Partner Solution";
		$data['desc'] = $desc;
		
		$data['title'] = $title;
		//isi FAQ
		$data['data'] = $this->Question_model->read('q_and_a');
		
		$this->load->view('footer/bantuan/FAQ', $data);
	}

	public function panduan_keamanan()
	{
		$data = array();

		$page = "Panduan Kemanan";
		$title = $page. " | "."YEPS Indonesia - Your Event Partner Solution";
		$desc = "Your Event Partner Solution";
		$data['desc'] = $desc;
		
		$data['title'] = $title;
		if ($this->ion_auth->logged_in()){
			// set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$data['users'] = $this->ion_auth->users()->result();
			foreach ($data['users'] as $k => $user)
			{
				$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			$data['login'] = $this->ion_auth->is_admin();

		}
		$this->load->view('footer/yeps/keamanan', $data);
	}

	public function kebijakan_privasi()
	{
		$data = array();

		$page = "Kebijakan Privasi";
		$title = $page. " | "."YEPS Indonesia - Your Event Partner Solution";
		$desc = "Your Event Partner Solution";
		$data['desc'] = $desc;
		
		$data['title'] = $title;
		if ($this->ion_auth->logged_in()){
			// set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$data['users'] = $this->ion_auth->users()->result();
			foreach ($data['users'] as $k => $user)
			{
				$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			$data['login'] = $this->ion_auth->is_admin();

		}
		$this->load->view('footer/bantuan/kebijakan-privasi', $data);
	}

	public function contact_us()
	{
		$data = array();

		$page = "Contact Us";
		$title = $page. " | "."YEPS Indonesia - Your Event Partner Solution";
		$desc = "Your Event Partner Solution";
		$data['desc'] = $desc;
		
		$data['title'] = $title;
		if ($this->ion_auth->logged_in()){
			// set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$data['users'] = $this->ion_auth->users()->result();
			foreach ($data['users'] as $k => $user)
			{
				$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			$data['login'] = $this->ion_auth->is_admin();

		}
		$this->load->view('footer/bantuan/hubungi-kami', $data);
	}

	public function registt()
	{
		$data = array();
		if ($this->ion_auth->logged_in()){
			// set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$data['users'] = $this->ion_auth->users()->result();
			foreach ($data['users'] as $k => $user)
			{
				$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			$data['login'] = $this->ion_auth->is_admin();

		}
		$this->load->view('email/registration', $data);
	}
	public function addMasalah(){
		$this->load->model('bukti');

		$config = array(
			'file_name' => $this->input->post('email')."-".$this->input->post('tipe').date("Y-m-d-H-i-s").".".substr($_FILES['foto']['type'],6),
			'upload_path' => "./uploads/masalah/",
			'allowed_types' => "jpg|gif|png|jpeg|JPG|PNG",
			'overwrite' => TRUE,
			'max_size' => "2048000"
		);
		$this->upload->initialize($config);
		$this->load->library('upload', $config);
		$file_name= $this->input->post('email')."-".$this->input->post('tipe').date("Y-m-d-H-i-s").".".substr($_FILES['foto']['type'],6);
		if($this->upload->do_upload('foto')){

			$data = array(
				"jenis_pertanyaan" => $this->input->post('tipe'),
				"nama_lengkap" => $this->input->post('name'),
				"email" => $this->input->post('email'),
				"detail_masalah" => $this->input->post('content'),
				"foto" => $config['file_name']
			);

			if($this->bukti->addMasalah($data)){
				//$this->invoice->confirm($this->input->post('uuid'));
				$icon = "success";
				$text = "Konfirmasi sudah berhasil dilakukan";
				$direct = base_url('profil');
			}else{
				$icon = "info";
				$text = "Oops, sepertinya ada kesalahan, coba lagi nanti";
				$direct = "";
			}
		}else{
			$icon = "info";
			$text = $this->upload->display_errors();
			$direct = "";
		}
		
		$data = array(
			"icon" => $icon,
			"text" => $text,
			"direct" => $direct
		);

		echo json_encode($data);
	
	}
	public function addBantuan(){
		$this->load->model('bukti');

		$config = array(
			'file_name' => $this->input->post('uuid')."-".$this->input->post('tipe'),
			'upload_path' => "./uploads/tagihan/",
			'allowed_types' => "jpg|png|jpeg",
			'overwrite' => TRUE,
			'max_size' => "2048000"
		);

		$this->load->library('upload', $config);
		
		if($this->upload->do_upload('file')){


			$data = array(
				"tipe" => $this->input->post('tipe'),
				"email" => $this->input->post('email'),
				"content" => $this->input->post('content'),
				"img" => $config['file_name'],
				"transfer_date" => $this->input->post('transfer_date'),
				"transfer_note" => $this->input->post('transfer_note')
			);

			if($this->bukti->add($data)){
				$this->invoice->confirm($this->input->post('uuid'));
				$icon = "success";
				$text = "Konfirmasi sudah berhasil dilakukan";
				$direct = base_url('welcome');
			}else{
				$icon = "info";
				$text = "Oops, sepertinya ada kesalahan, coba lagi nanti";
				$direct = "";
			}
		}else{
			$icon = "info";
			$text = $this->upload->display_errors();
			$direct = "";
		}
		
		$data = array(
			"icon" => $icon,
			"text" => $text,
			"direct" => $direct
		);

		echo json_encode($data);
	}
}
