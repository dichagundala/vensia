<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();

/*		if($this->session->userdata('type') != "client"){
			redirect(base_url("index.php/Authss/login"));
		}else{*/
			$this->load->model("users");
			$this->load->model("produk");
			$this->load->model("transaksi");

			/*}*/
		}
		public function index($kategori)
		{
			$this->category($kategori);
		}

		public function tipe($kategori, $page_number = 1, $filter = "normal"){
			$data = array();
			if ($kategori == "Venue") {
				$desc = "Temukan tempat terbaik untuk mewujudkan event kamu hanya di Vensia.";
				$keyword = "Vensia, solusi acara, solusi event, event organizer jakarta, nikah murah, sewa gedung jakarta, foto prewedding, foto pernikahan, jasa foto video, souvenir murah, dekorasi pesta, sewa baju pernikahan, sewa gaun pengantin, katering murah jakarta, makeup jakarta, kartu undangan pernikahan, wedding cake jakarta, event cake jakarta";
			}
			elseif ($kategori == "Photography") {
				$desc = "Semua fotografi untuk mengabdikan momen-momen penting acara kamu ada di Vensia";
				$keyword = "Vensia, solusi acara, solusi event, event organizer jakarta, nikah murah, sewa gedung jakarta, foto prewedding, foto pernikahan, jasa foto video, souvenir murah, dekorasi pesta, sewa baju pernikahan, sewa gaun pengantin, katering murah jakarta, makeup jakarta, kartu undangan pernikahan, wedding cake jakarta, event cake jakarta";
			}
			elseif ($kategori == "Wardrobe") {
				$desc = "Mencari wardrobe yang sesuai untuk event kamu? Atau kostum yang cocok untuk acara kamu? Vensia menyediakan bermacam-macam wardrobe yang tepat untuk kamu! ";
				$keyword = "Vensia, solusi acara, solusi event, event organizer jakarta, nikah murah, sewa gedung jakarta, foto prewedding, foto pernikahan, jasa foto video, souvenir murah, dekorasi pesta, sewa baju pernikahan, sewa gaun pengantin, katering murah jakarta, makeup jakarta, kartu undangan pernikahan, wedding cake jakarta, event cake jakarta";
			}
			elseif ($kategori == "Decoration") {
				$desc = "Mendekorasi untuk pernikahan dan pesta kamu bukan hal sulit lagi. Hanya di Vensia yang menyediakan dekorasi acara dari berbagai vendor.";
				$keyword = "Vensia, solusi acara, solusi event, event organizer jakarta, nikah murah, sewa gedung jakarta, foto prewedding, foto pernikahan, jasa foto video, souvenir murah, dekorasi pesta, sewa baju pernikahan, sewa gaun pengantin, katering murah jakarta, makeup jakarta, kartu undangan pernikahan, wedding cake jakarta, event cake jakarta";
			}
			elseif ($kategori == "Souvenir") {
				$desc = "Hanya di Vensia yang menyediakan list dari berbagai vendor souvenir murah.";
				$keyword = "Vensia, solusi acara, solusi event, event organizer jakarta, nikah murah, sewa gedung jakarta, foto prewedding, foto pernikahan, jasa foto video, souvenir murah, dekorasi pesta, sewa baju pernikahan, sewa gaun pengantin, katering murah jakarta, makeup jakarta, kartu undangan pernikahan, wedding cake jakarta, event cake jakarta";
			}
			elseif ($kategori == "Catering") {
				$desc = "Bingung mencari katering yang murah dan enak? Di Vensia tersedia katering-katering enak dari berbagai vendor yang sesuai dengan kantong kamu!";
				$keyword = "Vensia, solusi acara, solusi event, event organizer jakarta, nikah murah, sewa gedung jakarta, foto prewedding, foto pernikahan, jasa foto video, souvenir murah, dekorasi pesta, sewa baju pernikahan, sewa gaun pengantin, katering murah jakarta, makeup jakarta, kartu undangan pernikahan, wedding cake jakarta, event cake jakarta";
			}
			elseif ($kategori == "Make Up") {
				$desc = "Di Vensia kamu dapat memilih make up artist favoritmu.";
				$keyword = "Vensia, solusi acara, solusi event, event organizer jakarta, nikah murah, sewa gedung jakarta, foto prewedding, foto pernikahan, jasa foto video, souvenir murah, dekorasi pesta, sewa baju pernikahan, sewa gaun pengantin, katering murah jakarta, makeup jakarta, kartu undangan pernikahan, wedding cake jakarta, event cake jakarta";
			}
			elseif ($kategori == "Invitation") {
				$desc = "Kamu tak perlu lagi datang ke percetakan untuk membuat kartu undangan pernikahanmu. Di Vensia kamu hanya perlu memilih desain undangan kesukaanmu, memesannya dan menunggu hasil jadinya saja.";
				$keyword = "Vensia, solusi acara, solusi event, event organizer jakarta, nikah murah, sewa gedung jakarta, foto prewedding, foto pernikahan, jasa foto video, souvenir murah, dekorasi pesta, sewa baju pernikahan, sewa gaun pengantin, katering murah jakarta, makeup jakarta, kartu undangan pernikahan, wedding cake jakarta, event cake jakarta";
			}
			elseif ($kategori == "Entertainment") {
				$desc = "Perlu hiburan di event atau acara kamu? Di Vensia tersedia berbagai hiburan yang cocok untuk event atau acara kamu.";
				$keyword = "Vensia, solusi acara, solusi event, event organizer jakarta, nikah murah, sewa gedung jakarta, foto prewedding, foto pernikahan, jasa foto video, souvenir murah, dekorasi pesta, sewa baju pernikahan, sewa gaun pengantin, katering murah jakarta, makeup jakarta, kartu undangan pernikahan, wedding cake jakarta, event cake jakarta";
			}
			elseif ($kategori == "Transportation") {
				$desc = "Vensia menyediakan sewa mobil dari berbagai vendor untuk pernikahan kamu.";
				$keyword = "Vensia, solusi acara, solusi event, event organizer jakarta, nikah murah, sewa gedung jakarta, foto prewedding, foto pernikahan, jasa foto video, souvenir murah, dekorasi pesta, sewa baju pernikahan, sewa gaun pengantin, katering murah jakarta, makeup jakarta, kartu undangan pernikahan, wedding cake jakarta, event cake jakarta";
			}
			elseif ($kategori == "Event Cake") {
				$desc = "Di Vensia tersedia vendor-vendor event cake yang siap untuk meriahkan acara kamu.";
				$keyword = "Vensia, solusi acara, solusi event, event organizer jakarta, nikah murah, sewa gedung jakarta, foto prewedding, foto pernikahan, jasa foto video, souvenir murah, dekorasi pesta, sewa baju pernikahan, sewa gaun pengantin, katering murah jakarta, makeup jakarta, kartu undangan pernikahan, wedding cake jakarta, event cake jakarta";
			}
			elseif ($kategori == "Videography") {
				$desc = "Vensia menyediakan list video grafi dari berbagai vendor untuk merekam setiap momen special acara kamu";
				$keyword = "Vensia, solusi acara, solusi event, event organizer jakarta, nikah murah, sewa gedung jakarta, foto prewedding, foto pernikahan, jasa foto video, souvenir murah, dekorasi pesta, sewa baju pernikahan, sewa gaun pengantin, katering murah jakarta, makeup jakarta, kartu undangan pernikahan, wedding cake jakarta, event cake jakarta";
			}
			$data['desc'] = $desc;
			$data['keyword'] = $keyword;

			$title = $kategori." | Vensia";
			$data['title'] = urldecode($title);
			$kategori = urldecode($kategori);
			$produk_count = count($this->produk->get_id_product($kategori));
			$id_product = $this->produk->get_id_product_page($kategori, $page_number);
			$array_dummy = array();
			$data_json['result'] = array();
			$result = array();
			if(count($id_product) >= 1){

				foreach($id_product as $key1){
					$a = array();
					$b = array();
					$result = $this->produk->get_produk_by_uuid($key1['uuid']);

					foreach($result as $key2){
						if($key2['nama'] == "Foto" || $key2['nama'] == "Harga Promosi" || $key2['nama'] == "Harga Normal" || $key2['nama'] == "Nama" || $key2['nama'] == "Lokasi"){
							
							array_push($a, $key2['nama']);
							array_push($b, $key2['value']);
						}
					}
					$array_dummy = array_combine($a, $b);
					array_push($data_json['result'], array("uuid" => $key1['uuid'], "nama vendor" => $key1['nama vendor'], "slug" => $key2['slug'], "parameter" => $array_dummy));

				}

				if(null !== ($this->input->post('sort'))){
					$method = $this->input->post('sort');
					if($method == "ph"){
						usort($data_json['result'], function($a, $b){
							return $a['parameter']['Harga Promosi'] - $b['parameter']['Harga Promosi'];
						});	
					}
					elseif($method == "p")
					{
						usort($data_json['result'], function($a, $b){
							return $b['parameter']['Harga Promosi'] - $a['parameter']['Harga Promosi'];
						});	
					}
					elseif($method == "n")
					{
						usort($data_json['result'], function($a, $b){
							return strcmp($a['slug'], $b['slug']);
						});	

					}
					elseif($method == "nh")
					{
						usort($data_json['result'], function($a, $b){
							return strcmp($b['slug'],$a['slug']);
						});		
					}
				}

				$data['countKategori'] = $this->countKategori();
				$data['kategori'] = $kategori;
				$data['produk_count'] = $produk_count;
				$data['produk'] = $data_json;
				
				$data['page'] = $page_number;
				$this->load->view('kategori', $data);
			} else{
				redirect(base_url());
			}
		}

		public function search(){

		}

		public function countKategori(){
			$kategori = array('Venue','Photography','Souvenir','Decoration','Wardrobe','Catering','Make Up','Invitation','Entertainment','Transportation','Event Cake','Videography');
			$count = array();
			$result = array();
			foreach ($kategori as $value) {
				array_push($count,$this->produk->countKategori($value));				
			}
			$result = array_combine($kategori, $count);
			return $result;
		}

		
		public function detail($uuid_produk){
			$data = array();
			if ($this->ion_auth->logged_in()){
			// set the flash data error message if there is one
				$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
				$data['users'] = $this->ion_auth->users()->result();
				foreach ($data['users'] as $k => $user)
				{
					$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
				}
				$data['login'] = $this->ion_auth->is_admin();

			}

			$result = $this->produk->get_produk_by_uuid($uuid_produk);
			$a = array();
			$b = array();
			foreach($result as $key2){
				array_push($a, $key2['nama']);
				array_push($b, $key2['value']);
			}
			$data_json['result'] = array();
			$array_dummy = array();
			$array_dummy = array_combine($a, $b);
			array_push($data_json['result'], array("uuid" => $uuid_produk, "parameter" => $array_dummy));

			$data['produk'] = $data_json['result'][0];
			
			$this->load->view('shop-detail', $data);
			
		}

		public function jadwal(){
			$data = array();
			if ($this->ion_auth->logged_in()){
			// set the flash data error message if there is one
				$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
				$data['users'] = $this->ion_auth->users()->result();
				foreach ($data['users'] as $k => $user)
				{
					$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
				}
				$data['login'] = $this->ion_auth->is_admin();

			}
			$this->load->view('jadwal', $data);
		}
	}