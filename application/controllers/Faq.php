<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();

			$this->load->model("transaksi");
		
	}
	public function index()
	{
		$data = array();
		$page = "Faq";
		$title = $page. " | "."YEPS Indonesia - Your Event Partner Solution";
		$desc = "Your Event Partner Solution";
		$data['desc'] = $desc;
		
		$data['title'] = $title;
		if ($this->ion_auth->logged_in()){
			// set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$data['users'] = $this->ion_auth->users()->result();
			foreach ($data['users'] as $k => $user)
			{
				$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			$data['login'] = $this->ion_auth->is_admin();

		}
		$this->load->view('faq', $data);
	}
}
