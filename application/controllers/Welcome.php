<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	function __construct(){
		// Construct the parent class
        parent::__construct();
        $this->load->library('bcrypt');
		$this->load->model('api');
		$this->load->model('transaksi');
		$this->load->model("produk");
		$this->load->library('uuid');

		//cara pakai uuid
		//$this->uuid->v4();



		//Cara pakai

		//$password = $this->input->post('password');
		//Fungsi Generate password
		//$passwordHash = $this->bcrypt->hash($password);
		//Fungsi Cek password
		//$this->bcrypt->verify($password, $passwordHash);

	}


	public function index()
	{
		// $data = array();
		// $title = "VENSIA";
		$desc = "YEPS (Your Event Partner Solution) Indonesia adalah perusahaan digital teknologi yang memberikan solusi untuk pelaksanaan event/acara di Indonesia.";
		$keyword = "yeps indonesia, solusi acara, solusi event, event organizer jakarta, nikah murah, sewa gedung jakarta, foto prewedding, foto pernikahan, jasa foto video, souvenir murah, dekorasi pesta, sewa baju pernikahan, sewa gaun pengantin, katering murah jakarta, makeup jakarta, kartu undangan pernikahan, wedding cake jakarta, event cake jakarta";
		$data['desc'] = $desc;
		$data['keyword'] = $keyword;		
		// if ($this->ion_auth->logged_in()){
		// 	// set the flash data error message if there is one
		// 	$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

		// 	//list the users
		// 	$data['users'] = $this->ion_auth->users()->result();
		// 	foreach ($data['users'] as $k => $user)
		// 	{
		// 		$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
		// 	}
		// 	$data['login'] = $this->ion_auth->is_admin();

		// }
		// $kategori = array();
		// $kategori = $this->db->query('select nama, public_id from vendor_kategori')->result_array();

		// $data['kategori'] = $kategori;
		// $data['title'] = $title;
		// $data['Venue'] = $this->countKategori();
		$data['Photography'] = $this->tipe('Photography');
		// print_r($data['Photography']);
		$data['Decoration'] = $this->tipe('Decoration');
		$data['Catering'] = $this->tipe('Catering');
		$data['Invitation'] = $this->tipe('Invitation');
		$this->load->view('index', $data);
	}

	public function kat(){
	  return "hello world";
	 }

	public function tipe($kategori){
		    $page_number = 1;
			$data = array();
			$array_dummy = array();
			$result = array();
			$data_json['result'] = array();
			
			// $kategori = urldecode($kategori);
			// $produk_count = count($this->produk->get_id_product($kategori));
			$id_product = $this->produk->get_id_product_page($kategori, $page_number);
			
			
			
			if(count($id_product) >= 1){

				foreach($id_product as $key1){
					$a = array();
					$b = array();
					$result = $this->produk->get_produk_by_uuid($key1['uuid']);

					foreach($result as $key2){
						if($key2['nama'] == "Foto" || $key2['nama'] == "Harga Promosi" || $key2['nama'] == "Harga Normal" || $key2['nama'] == "Nama" || $key2['nama'] == "Lokasi"){
							
							array_push($a, $key2['nama']);
							array_push($b, $key2['value']);
						}
					}
					$array_dummy = array_combine($a, $b);
					array_push($data_json['result'], array("uuid" => $key1['uuid'], "nama vendor" => $key1['nama vendor'], "slug" => $key2['slug'], "parameter" => $array_dummy));

				}

				// if(null !== ($this->input->post('sort'))){
				// 	$method = $this->input->post('sort');
				// 	if($method == "ph"){
				// 		usort($data_json['result'], function($a, $b){
				// 			return $a['parameter']['Harga Promosi'] - $b['parameter']['Harga Promosi'];
				// 		});	
				// 	}
				// 	elseif($method == "p")
				// 	{
				// 		usort($data_json['result'], function($a, $b){
				// 			return $b['parameter']['Harga Promosi'] - $a['parameter']['Harga Promosi'];
				// 		});	
				// 	}
				// 	elseif($method == "n")
				// 	{
				// 		usort($data_json['result'], function($a, $b){
				// 			return strcmp($a['slug'], $b['slug']);
				// 		});	

				// 	}
				// 	elseif($method == "nh")
				// 	{
				// 		usort($data_json['result'], function($a, $b){
				// 			return strcmp($b['slug'],$a['slug']);
				// 		});		
				// 	}
				// }

				// $data['countKategori'] = $this->countKategori();
				// $data['kategori'] = $kategori;
				// $data['produk_count'] = $produk_count;
				// $data['produk'] = $data_json;
				// $data['page'] = $page_number;
				// $this->load->view('kategori', $data);
				return $data_json;
			} else{
				redirect(base_url());
			}
		}

		public function countKategori(){
			$kategori = array('Venue','Photography','Souvenir','Decoration','Wardrobe','Catering','Make Up','Invitation','Entertainment','Transportation','Event Cake','Videography');
			$count = array();
			$result = array();
			foreach ($kategori as $value) {
				array_push($count,$this->produk->countKategori($value));				
			}
			$result = array_combine($kategori, $count);
			return $result;
		}

	public function profil(){

		$this->load->view('index2');

	}
}
