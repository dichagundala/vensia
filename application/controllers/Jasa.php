<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jasa extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		$this->load->model("users");
		$this->load->model("produk");
		$this->load->model("transaksi");
		$this->load->model("invoice");
		$this->load->model("vendor");
		$this->load->library('uuid');


	}
	public function transQty($uuid, $qty){
		if($uuid == null){
			$this->load->view("404");
		}else{

			$this->transaksi->qty($uuid, $qty);
		}
	}
	public function addQty($uuid, $qty){
		if($uuid == null){
			$this->load->view("404");
		}else{

			$this->transaksi->qty($uuid, $qty);
		}
	}
	public function _remap($method, $args){

		if (method_exists($this, $method))
		{
			$this->$method($args);
		}
		else
		{
			$this->index($method,$args);
		}
	}
	
	public function index($method = "", $args = array())
	{
		$temp = str_replace("_", " ", $method);
		$title = $temp." | Vensia";
		$desc = "Vendor Indonesia";
		$cek = array_filter($args);

		if(empty($cek)){
			$uuid_vendor = $this->vendor->get_by_name($method);
			$this->profilvendor($uuid_vendor['uuid'], "1");
		}elseif(strpos($args[0], 'page=') !== false){
			if($args[0] == ""){
				$args[0] = "page=1";
			}
			$page = explode('=', $args[0]);
			if(is_numeric($page[1])){

				$uuid_vendor = $this->vendor->get_by_name($method);
				if(validation_errors() != false) {
					$this->load->view("404");

				}else{

					$this->profilvendor($uuid_vendor['uuid'], $page[1]);
				}
			}	

		}
		elseif(strpos($args[0], 'portfolio') !== false){
			$uuid_vendor = $this->vendor->get_by_name($method);
			$this->profilvendor($uuid_vendor['uuid'], 1, "portfolio");
		}
		elseif(strpos($args[0], 'about') !== false){
			$uuid_vendor = $this->vendor->get_by_name($method);
			$this->profilvendor($uuid_vendor['uuid'], 1, "about");
		}
		else{
			$data = array();
			$result = $this->produk->get_produk_by_uuid2($method, $args[0]);
			
			
			$a = array();
			$b = array();
			foreach($result as $key2){
				array_push($a, $key2['nama']);
				array_push($b, $key2['value']);
			}
			$data_json['result'] = array();
			$array_dummy = array();
			$array_dummy = array_combine($a, $b);
			array_push($data_json['result'], array("uuid" => $result[0]['uuid'], "vendor" => $result[0]['nama_vendor'],"aktif vendor" => $result[0]['aktifVendor'], "uuid vendor" => $result[0]['uuid_vendor'], "parameter" => $array_dummy));

			// print_r($data_json['result']);

			$data['vendor'] = $method;
			$data['data_vendor'] = $this->vendor->get($result[0]['uuid_vendor']);

			$data['produk'] = $data_json['result'][0];
			$data['title'] = $title;
			$data['desc'] = $data['produk']['parameter']['Deskripsi'];
			/* memanggil data additional yang terdaftar  */
			$data['add_produk'] = $this->produk->get_add_produk_by_uuid($result[0]['uuid']);
			/* memanggil jumlah data additional yang terdaftar  */
			$data['add_sum_produk'] = $this->produk->get_sum_add_produk_by_uuid($result[0]['uuid']);
		
			if(count($data['data_vendor']) < 1 || count($data_json['result']) < 1) {
				$this->load->view("404");

			}else{

				$data['keyword'] = "Vensia, solusi acara, solusi event, event organizer jakarta, nikah murah, sewa gedung jakarta, foto prewedding, foto pernikahan, jasa foto video, souvenir murah, dekorasi pesta, sewa baju pernikahan, sewa gaun pengantin, katering murah jakarta, makeup jakarta, kartu undangan pernikahan, wedding cake jakarta, event cake jakarta";
				$this->load->view('shop-detail', $data);
			}
		}
	}

	public function profilvendor($vendor,$page = 1, $tab = "produk")
	{
		$produk_count = $this->produk->get_produk_count($vendor);
		$data = array();
		$desc = "Your Event Partner Solution";
		$data_vendor = $this->vendor->get($vendor);
		$id_product = $this->produk->get_id_product_vendor($vendor,$page);


		$array_dummy = array();
		$data_json['result'] = array();
		$result = array();
		foreach($id_product as $key1){
			$a = array();
			$b = array();
			$result = $this->produk->get_produk_by_uuid($key1['uuid']);
			foreach($result as $key2){
				if($key2['nama'] == "Foto" || $key2['nama'] == "Harga Promosi" || $key2['nama'] == "Harga Normal" || $key2['nama'] == "Nama" || $key2['nama'] == "Lokasi"){
					array_push($a, $key2['nama']);
					array_push($b, $key2['value']);
				}
			}
			$array_dummy = array_combine($a, $b);
			array_push($data_json['result'], array("uuid" => $key1['uuid'], "slug" => $key2['slug'], "vendor" => $result[0]['nama_vendor'], "uuid vendor" => $result[0]['uuid_vendor'], "parameter" => $array_dummy));
		}

		//album
	
		$data_album = $this->vendor->get_album($vendor);
		$data['album'] = $data_album;
		$data['produk'] = $data_json;
		$data['produk_count'] = $produk_count;
		$data['vendor'] = $data_vendor;
		$data['page'] = $page;
		$data['desc'] = $data['vendor']['deskripsi'];
		$data['keyword'] = "Vensia, solusi acara, solusi event, event organizer jakarta, nikah murah, sewa gedung jakarta, foto prewedding, foto pernikahan, jasa foto video, souvenir murah, dekorasi pesta, sewa baju pernikahan, sewa gaun pengantin, katering murah jakarta, makeup jakarta, kartu undangan pernikahan, wedding cake jakarta, event cake jakarta";
		$data['tab'] = $tab; 

		$this->load->view('profil-vendor', $data);

	}

	public function portfolio()
	{
		/*$produk_count = $this->produk->get_produk_count($vendor);
		$data = array();
		$desc = "Your Event Partner Solution";
		$data_vendor = $this->vendor->get($vendor);
		$id_product = $this->produk->get_id_product_vendor($vendor,$page);


		$array_dummy = array();
		$data_json['result'] = array();
		$result = array();
		foreach($id_product as $key1){
			$a = array();
			$b = array();
			$result = $this->produk->get_produk_by_uuid($key1['uuid']);
			foreach($result as $key2){
				if($key2['nama'] == "Foto" || $key2['nama'] == "Harga Promosi" || $key2['nama'] == "Harga Normal" || $key2['nama'] == "Foto" || $key2['nama'] == "Nama" || $key2['nama'] == "Lokasi"){
					array_push($a, $key2['nama']);
					array_push($b, $key2['value']);
				}
			}
			$array_dummy = array_combine($a, $b);
			array_push($data_json['result'], array("uuid" => $key1['uuid'], "slug" => $key2['slug'], "vendor" => $result[0]['nama_vendor'], "uuid vendor" => $result[0]['uuid_vendor'], "parameter" => $array_dummy));
		}

		$data['produk'] = $data_json;
		$data['produk_count'] = $produk_count;
		$data['vendor'] = $data_vendor;
		$data['page'] = $page;
		$data['desc'] = $desc;*/
		$this->load->view('profil-vendor-2', $data);

	}



}