<?php
use Mailgun\Mailgun;
defined('BASEPATH') OR exit('No direct script access allowed');

class Authss extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library("bcrypt");
		$this->load->model("users");
		$this->load->library('email');
		$this->load->library('uuid');
	}

	public function index()
	{
		redirect('/auth/login/', 'refresh');
	}

	public function login($action = "", $uuid = ""){
		$this->load->view('login');
		
	}
	public function cek_login(){
		$email = $this->input->post("email");
		$password = $this->input->post("password");

		$hasil = $this->users->cek_user($email);

		
		$rs = $hasil->result_array();
		if($hasil->num_rows() >= 1 && $this->bcrypt->verify($password, $rs[0]['password']) ){
			$datauser = array(
				'namaDepan' => $rs[0]['namaDepan'],
				'namaBelakang' => $rs[0]['namaBelakang'],
				'uuid' => $rs[0]['uuid'],
				'email' => $rs[0]['email'],
				'aktifVendor' => $rs[0]['aktifVendor'],
				'type' => "client"
			);
			

			$this->session->set_userdata($datauser);
			$icon = "success";
			$text = "Selamat datang ".$datauser['namaDepan']." ".$datauser['namaBelakang'];
			$direct = base_url('welcome');
		}else{
			$icon = "error";
			$text = "Password atau email Anda salah";
			$direct = base_url();
			$cek = $this->users->cek_account_regis($this->input->post('email')); 
			if($cek == 1 || $cek == 2){
				if($cek == 1){
					$text = "Email ini sudah terdaftar namun belum di verifikasi, silahkan cek email terlebih dahulu.";
				}
			}


		}

		$result = array(
			'icon' => $icon,
			'text' => $text,
			'direct' => $direct
		);

		echo json_encode($result);
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url(''));
	}

	public function register(){
		$this->load->view('register');
	}

	public function forgotpassword(){
		$this->load->view('forgot-password');
	}

	public function verification($verifikasi){
		$auth_email = $verifikasi;
		if($this->users->verifikasi_email($auth_email) == 1){
			// redirect(base_url('Authss/login')); awal
			redirect(base_url());
		}
	}

	public function verification_forgetPass($verifikasi){
		$auth_email = $verifikasi;
		if($this->users->verifikasi_forgetPass($auth_email) == 0){
			redirect(base_url('Authss/login'));
		}else{
			$data['uuid'] = $this->users->get_client_forget($auth_email);
			$this->load->view('new_pass', $data);
		}
	}

	public function generateEmail($subject, $tpl, $data){
		$mgClient = new Mailgun('54c915413905fdcbe811213e8f59be3f-7efe8d73-1855dae2');
		$domain = "mg.yepsindonesia.com";
        # Make the call to the client.
		$result = $mgClient->sendMessage($domain, array(
			'from'    => 'YEPS Indonesia no-reply@yepsindonesia.com',
			'to'      => $data['email'],
			'subject' => $subject,
            //'text'    => 'Do Not Reply Email from This!',
			'html'    => $this->load->view($tpl,$data,TRUE)
		));

		if($result->http_response_code == 200){
			return true;
		}else{
			return false;
		}

	}

	public function change_pass(){
		$uuid = $this->input->post('uuid');
		$password = $this->input->post('pass');
		$password = $this->bcrypt->hash($password);
		$this->db->query("UPDATE users set password = '$password' where uuid = '$uuid'");
		if(($this->db->affected_rows() >= 1)){
			$text = "Yeps, password berhasil diubah, silahkan login menggunakan password baru Anda";
			$icon = "success";
			$direct = base_url("Authss/login");
		}else{
			$text = "Oops, sepertinya ada kesalahan, coba lagi nanti";
			$icon = "info";
			$direct = "";
		}

		$data = array(
			"text" => $text,
			"icon" => $icon,
			"direct" => $direct
		);

		echo json_encode($data);
	}

	public function generateForgot(){
		$email = $this->input->post('email');
		$direct = "";
		$auth_forget = md5(date("Y-m-d H:i:s").$this->input->post('email'));
		$icon = "error";
		$text = "Email belum terdaftar";
		$direct = "";
		if( $this->users->cek_account($email)){

			if($this->users->update_auth_email($email, $auth_forget)){
				$nama = $this->users->get_name_by_email($email);
				$email = $nama." ".$email;
				$data = array(
					"name" => $nama,
					"email" => $email,
					"authForget" => $auth_forget
				);

				if ( $this->generateEmail("Forgot Password","email/forgot", $data))
				{
					//$report = $this->email->print_debugger();
					$icon = "success";
					$text = "Silahkan cek email Anda.";
					$direct = "";
				}else{
					$icon = "warning";
					$text = "Oops, sepertinya ada kesalahan. Silahkan coba lagi nanti";
					$direct = "";
				}
			}else{
			}
		}else{
		}

		$data_register = array(
			"icon" => $icon,
			"text" => $text,
			"direct" => $direct
		);

		echo json_encode($data_register);
		return true;
	}

	public function clientRegister(){
		if($this->input->post('email') != null){

			$auth_email = md5(date("Y-m-d H:i:s").$this->input->post('email'));
			$data = array(
				"namaDepan" => $this->input->post('firstName'),
				"namaBelakang" => $this->input->post('secondName'),
				"email" => $this->input->post('firstName')." ".$this->input->post('email'),
				"authEmail" => $auth_email
			);
			$cek = $this->users->cek_account_regis($this->input->post('email')); 
			if($cek == 1 || $cek == 2){
				if($cek == 1){
					$text = "Email ini sudah terdaftar namun belum di verifikasi, silahkan cek email terlebih dahulu.";
					$icon = "info";
					$direct = base_url();
				}else{
					$text = "Email ini sudah terdaftar.<br> Jika anda lupa password, silahkan klik <a href='".base_url('authss/forgotpassword')."' style='color : #107ADE'>disini</a>.";
					$icon = "info";
					$direct = base_url();
				}
			}else{
				if (  $this->generateEmail("Registrasi YEPS Indonesia","email/registration", $data))
				{
					$data_register = array(
						"namaDepan" => $this->input->post('firstName'),
						"namaBelakang" => $this->input->post('secondName'),
						"email" => $this->input->post('email'),
						"telp" => $this->input->post('phone'),
						"password" => $this->bcrypt->hash($this->input->post('password')),
						"authEmail" => $auth_email,
						"uuid" => md5($this->input->post('email').rand())
					);
					$this->users->input_data($data_register);
					$icon = "success";
					$text = "Silahkan cek email Anda untuk melakukan aktifasi akun";
					$direct = base_url();
				}else{
					$icon = "error";
					$text = "Oops, sepertinya ada kesalahan, silahkan coba lagi nanti";
					$direct = base_url();
				}
			}

			$rs = array(
				"icon" => $icon,
				"text" => $text,
				"direct" => $direct
			);

			echo json_encode($rs);
		}else{
			$this->load->view("404");
		}

	}
}
