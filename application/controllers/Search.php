<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		$this->load->model("users");
		$this->load->model("produk");
		$this->load->model("transaksi");
		$this->load->model("invoice");
		$this->load->model('vendor');
		$this->load->library('uuid');

	}

	public function produk($keyword = "", $page = "1")
	{	

		$temp = str_replace("_", " ", $keyword);
		$title = $temp." | YEPS Indonesia";
		$desc = "Your Event Partner Solution";

		$keyword = urldecode($keyword);
		$temp = explode(" ", $keyword);
		$result = $this->produk->get_produk_search($keyword, $page);

		$temp_uuid = array();
		foreach ($result as $key => $value) {
			array_push($temp_uuid, $value['uuid']);
			
		}
		$vendor = "";
		if(count($temp) > 1){
			$vendor = $this->vendor->get_search($keyword);

			foreach ($temp as $key) {
				$temp2_uuid = $this->produk->get_produk_search($key,"1");
				foreach ($temp2_uuid as $key2 => $value) {
					array_push($temp_uuid, $value['uuid']);
				}				
			}
		}

		$temp_uuid = array_unique($temp_uuid);
		
		
		$data = array();

		$array_dummy = array();
		$data_json['result'] = array();
		$result = array();
		$produk_count = $this->produk->get_produk_search_count($keyword);
		$page_number = $page;
		foreach($temp_uuid as $key1){
			$a = array();
			$b = array();
			$result = $this->produk->get_produk_by_uuid($key1);
			foreach($result as $key2){
				array_push($a, $key2['nama']);
				array_push($b, $key2['value']);
			}
			$array_dummy = array_combine($a, $b);
			array_push($data_json['result'], array("uuid" => $key1, "nama vendor" => $result[0]['nama_vendor'], "slug" => $key2['slug'], "parameter" => $array_dummy));

		}


		function sortByOrderPrice($a, $b) {
			return $a['parameter']['Harga Promosi'] - $b['parameter']['Harga Promosi'];
		}

		function sortByOrderPriceH($a, $b) {
			return $b['parameter']['Harga Promosi'] - $a['parameter']['Harga Promosi'];
		}

		function sortByOrderName($a, $b) {
			return $a['parameter']['Nama'] - $b['parameter']['Nama'];
		}

		function sortByOrderNameH($a, $b) {
			return $b['parameter']['Nama'] - $a['parameter']['Nama'];
		}

		if(null !== ($this->input->post('sort'))){
			$method = $this->input->post('sort');
			if($method == "ph"){
				usort($data_json['result'], 'sortByOrderPriceH');	
			}
			elseif($method == "p")
			{
				usort($data_json['result'], 'sortByOrderPrice');	
			}
			elseif($method == "nh")
			{
				usort($data_json['result'], 'sortByOrderNameH');	
			}
			elseif($method == "n")
			{
				usort($data_json['result'], 'sortByOrderName');	
			}
		}

		$data['countKategori'] = $this->countKategori();
		$data['produk_count'] = $produk_count;
		$data['produk'] = $data_json;
		$data['page'] = $page_number;
		$data['vendor'] = $vendor;
		$data['desc'] = $desc;
		$data['keyword'] = $keyword;
		$this->load->view('search', $data);
		
	}
	public function countKategori(){
		$kategori = array('Venue','Photography','Souvenir','Decoration','Wardrobe','Catering','Make Up','Invitation','Entertainment','Transportation','Event Cake','Videography');
		$count = array();
		$result = array();
		foreach ($kategori as $value) {
			array_push($count,$this->produk->countKategori($value));				
		}
		$result = array_combine($kategori, $count);
		return $result;
	}
}