<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profilvendor extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model("users");
		$this->load->model("produk");
		$this->load->model("transaksi");
		$this->load->model("vendor");

	}

	public function index($vendor,$page_number = 1)
	{
		$produk_count = $this->produk->get_produk_count($vendor);
		$data = array();
		$desc = "Your Event Partner Solution";
		$data['desc'] = $desc;
		$page = "Vendor";
		$title = $page. " | "."YEPS Indonesia - Your Event Partner Solution";
		$data['title'] = $title;
		$data_vendor = $this->vendor->get($vendor);
		$id_product = $this->produk->get_id_product_vendor($vendor,$page_number;


		$array_dummy = array();
		$data_json['result'] = array();
		$result = array();
		foreach($id_product as $key1){
			$a = array();
			$b = array();
			$result = $this->produk->get_produk_by_uuid($key1['uuid']);
			foreach($result as $key2){
				if($key2['nama'] == "Foto" || $key2['nama'] == "Harga Promosi" || $key2['nama'] == "Harga Normal" || $key2['nama'] == "Foto" || $key2['nama'] == "Nama" || $key2['nama'] == "Lokasi"){
					array_push($a, $key2['nama']);
					array_push($b, $key2['value']);
				}
			}
			$array_dummy = array_combine($a, $b);
			array_push($data_json['result'], array("uuid" => $key1['uuid'], "parameter" => $array_dummy));
		}

		$data['produk'] = $data_json;
		$data['produk_count'] = $produk_count;
		$data['vendor'] = $data_vendor;
		$data['page'] = $page_number;
		$this->load->view('profil-vendor', $data);

	}

	public function search(){
		$kategori = $this->input->post('kategori');
		$sort = $this->input->post('sort');
	}

}
