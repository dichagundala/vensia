<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends CI_Controller {

    function index()
    {
        $this->load->model("produk");
        $data['result'] = $this->db->get('sitemap')->result_array();
        
        $vendor = $this->produk->get_produk_all();
        $hasil = array();
        
        foreach($vendor as $key1){
            $result = $this->produk->get_produk_by_uuid($key1['uuid']);
            
            $link = "https://www.yepsindonesia.com/jasa/".$result[0]['nama_vendor']."/".$result[0]['slug'];
            $link = str_replace("&", " ", $link);
            $link = str_replace(" ", "_", $link);
            
            array_push($hasil, array("link" => $link, "priority" => "1.0"));
        }
        
        $data['produk'] = $hasil;
        

        header("Content-Type: text/xml;charset=iso-8859-1");

        $this->load->view("sitemap", $data);
    }

    function add()
    {
        $link = $this->input->post('link');
        $prior = $this->input->post('prior');
        $produk = array(
            'link' => $link,
            'priority' => $prior
        );

        $this->db->insert('sitemap', $produk);

        if($this->db->affected_rows() == 1){
            ?><script type="text/javascript"> alert("berhasil ditambah")</script> 
            <?php
            redirect(base_url('sitemap/form'));
        }
    }

    function form()
    {
        $this->load->view("sitemap_form");
    }

}
?>