<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio extends CI_Controller {

	function __construct(){
		// Construct the parent class
        parent::__construct();
		$this->load->model('transaksi');

	}
	public function index()
	{
		$data = array();
		$page = "Portfolio";
		$title = $page. " | "."YEPS Indonesia - Your Event Partner Solution";
		$desc = "Your Event Partner Solution";
		$data['desc'] = $desc;
		
		$data['title'] = $title;
		/*if ($this->ion_auth->logged_in()){
			// set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$data['users'] = $this->ion_auth->users()->result();
			foreach ($data['users'] as $k => $user)
			{
				$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			$data['login'] = $this->ion_auth->is_admin();
		}*/
		$data['data'] = $this->transaksi->read('portfolio');
		$this->load->view('portfolio', $data);
		
	}

	public function detail()
	{
		$data = array();
		$page = "Portfolio";
		$title = $page. " | "."YEPS Indonesia - Your Event Partner Solution";
		$desc = "Your Event Partner Solution";
		$data['desc'] = $desc;
		
		$data['title'] = $title;
		if ($this->ion_auth->logged_in()){
			// set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$data['users'] = $this->ion_auth->users()->result();
			foreach ($data['users'] as $k => $user)
			{
				$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			$data['login'] = $this->ion_auth->is_admin();

		}
		$data = $this->transaksi->readbyid('portfolio', $this->uri->segment('3'));
		$data['prevpage'] = $this->transaksi->readbyid('portfolio', ($this->uri->segment('3') - 1));
		$data['nextpage'] = $this->transaksi->readbyid('portfolio', ($this->uri->segment('3') + 1));
		$this->load->view('portfolio-detail', $data);
	}
}
