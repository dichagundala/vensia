<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

	/** 
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('type') != "client"){
			redirect(base_url("index.php/Authss/login"));
		}else{
			$this->load->model("users");
			$this->load->model("transaksi");
			$this->load->model("invoice");
			$this->load->model("cicilan");
		}
	}
	public function index()
	{
		$data = array();
		$page = "Profil";
		$title = $page. " | "."YEPS Indonesia - Your Event Partner Solution";
		$desc = "Your Event Partner Solution";
		$data['desc'] = $desc;
		
		$data['title'] = $title;
		if ($this->ion_auth->logged_in()){
			// set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$data['users'] = $this->ion_auth->users()->result();
			foreach ($data['users'] as $k => $user)
			{
				$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			$data['login'] = $this->ion_auth->is_admin();

		}

		$data['users'] = $this->users->get_client($this->session->userdata("uuid"));

		$data['invoice'] = $this->invoice->get_invoice($this->session->userdata("uuid"));
		$data['tagihan'] = $this->cicilan->get($this->session->userdata("uuid"));
		$this->load->view('profil', $data);
	}

	public function update_profil()
	{
		$namaDepan = $this->input->post('namaDepan');
		$namaBelakang = $this->input->post('namaBelakang');
		$kelamin = $this->input->post('kelamin');
		$telp = $this->input->post('telp');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$tgl_lahir = date_format(date_create($tgl_lahir), 'Y-m-d');
		$alamat = $this->input->post('alamat');
		$email = $this->input->post('email');
		$img = $this->input->post('img');
		$uuid = $this->session->userdata('uuid');

		$data = array(
			'namaDepan' => $namaDepan,
			'namaBelakang' => $namaBelakang,
			'gender' => $kelamin,
			'telp' => $telp,
			'tglLahir' => $tgl_lahir,
			'address' => $alamat,
			'email' => $email
		);

		if($this->users->update_client($data,$uuid)){
			$icon = "success";
			$text = "Berhasil update profile";
			$direct = base_url("index.php/profil");
		}else{
			$icon = "error";
//			$text = $this->db->last_query();
			$text = "Oops, sepertinya ada kesalahan, silahkan coba lagi nanti";
			$direct = base_url();
		}

		$result = array(
			"icon" => $icon,
			"text" => $text,
			"direct" => $direct
		);
		echo json_encode($result);
	}

	public function update_img()
	{
		$uuid = $this->session->userdata('uuid');

		$config = array(
			'upload_path' => "./uploads/",
			'allowed_types' => "jpg|png|jpeg",
			'overwrite' => TRUE,
			'max_size' => "2048000"
		);

		$this->load->library('upload', $config);
		if($this->upload->do_upload('file'))
		{
			$data = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

			$image= $data['upload_data']['file_name'];

			$data = array(
				'img' => $image,
				'uuid' => $uuid
			);

			if($this->users->update_client_img($data)){
				$icon = "success";
				$text = "Berhasil update profile image";
				$direct = base_url("index.php/profil");
			}else{
				$icon = "error";
//			$text = $this->db->last_query();
				$text = "Oops, sepertinya ada kesalahan, silahkan coba lagi nanti";
				$direct = base_url();
			}

		}
		else
		{
			$icon = "error";
//			$text = $this->db->last_query();
			if($this->upload->display_errors() == "<p>You did not select a file to upload.</p>"){
				$text = "Anda belum memilih file untuk diunggah";
			}else{
				$text = "Oops, sepertinya ada kesalahan, silahkan coba lagi nanti";
			}
			$direct = base_url();
		}

		$result = array(
			"icon" => $icon,
			"text" => $text,
			"direct" => $direct
		);
		echo json_encode($result);
	}

	public function detailTagihan($uuid){
		$cicilan = $this->cicilan->get_by_invoice($uuid);
		$data['cicilan'] = $cicilan;
		$this->load->view('detail-tagihan', $data);
	}

	public function konfirmasi($tipe, $uuid){
		
		$page = "Profil";
		$title = $page. " | "."YEPS Indonesia - Your Event Partner Solution";
		$desc = "Your Event Partner Solution";
		$data['desc'] = $desc;
		
		$data['title'] = $title;
		$data['tipe'] = $tipe;
		$data['uuid'] = $uuid;

		if($tipe == "cicilan"){
			$temp = $this->db->query("Select uuid_invoice from cicilan where uuid like '$uuid'")->result_array()[0]['uuid_invoice'];
			$temp = $this->db->query("Select cicilan.uuid as 'uuid cicilan', invoice.kode_invoice as 'nama invoice' from cicilan, invoice where invoice.uuid = cicilan.uuid_invoice and invoice.uuid like '$temp'")->result_array();
			$i = 0;
			foreach ($temp as $key) {
				$i++;
				if($key['uuid cicilan'] == $uuid){
					$name = $key['nama invoice']." - Cicilan ke ".$i;
				}
			}
		}else{
			$temp = $this->db->query("Select * from invoice where uuid like '$uuid'")->result_array();
			$name = $temp[0]['kode_invoice'];
			$data['price'] = $temp[0]['biaya_dp'];

			$temp = explode("-", $temp[0]['metode_pembayaran']);
			$data['bank'] = $this->db->query("Select * from manual_bank_account where bank_name like '".$temp[1]."'")->result_array()[0];
		}
		$data['nama'] = $name;

		$this->load->view('konfirmasi-angsuran',$data);
	}

	public function addKonfirmasi(){
		$this->load->model('bukti');

		$config = array(
			'file_name' => $this->input->post('uuid')."-".$this->input->post('tipe'),
			'upload_path' => "./uploads/tagihan/",
			'allowed_types' => "jpg|png|jpeg",
			'overwrite' => TRUE,
			'max_size' => "2048000"
		);

		$this->load->library('upload', $config);
		
		if($this->upload->do_upload('file')){


			$data = array(
				"tipe" => $this->input->post('tipe'),
				"uuid" => $this->input->post('uuid'),
				"bank_name_fr" => $this->input->post('bank_name_fr'),
				"bank_no_fr" => $this->input->post('bank_no_fr'),
				"bank_account_fr" => $this->input->post('bank_account_fr'),
				"bank_name_to" => $this->input->post('bank_name_to'),
				"transfer_price" => $this->input->post('transfer_price'),
				"img" => $config['file_name'],
				"transfer_date" => $this->input->post('transfer_date'),
				"transfer_note" => $this->input->post('transfer_note')
			);

			if($this->bukti->add($data)){
				$this->invoice->confirm($this->input->post('uuid'));
				$icon = "success";
				$text = "Konfirmasi sudah berhasil dilakukan";
				$direct = base_url('profil');
			}else{
				$icon = "info";
				$text = "Oops, sepertinya ada kesalahan, coba lagi nanti";
				$direct = "";
			}
		}else{
			$icon = "info";
			$text = $this->upload->display_errors();
			$direct = "";
		}
		
		$data = array(
			"icon" => $icon,
			"text" => $text,
			"direct" => $direct
		);

		echo json_encode($data);
	}

	public function riwayat()
	{
		$this->load->view('riwayat');
	}


}
