<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dummy extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model("users");
		$this->load->model("produk");
		$this->load->model("transaksi");
		$this->load->model("invoice");
		$this->load->library('uuid');
		
	}
	public function index()
	{
		$data['desc'] = "a";
		$data['kategori'] = $this->db->query("Select id as 'id', nama as 'nama' from vendor_kategori")->result_array();

		$this->load->view('dummy', $data);
	}

	public function getParam($id){
		$data['param'] = $this->db->query("Select trx_kategori_parameter.id as 'id', parameter.nama as 'nama' from parameter, vendor_kategori, trx_kategori_parameter where parameter.id = trx_kategori_parameter.parameter_id and vendor_kategori.id = trx_kategori_parameter.kategori_id and vendor_kategori.id = $id ")->result_array();
		$data['kategori'] = $id;	
		
		$data['vendor'] = $this->db->query("Select uuid as 'uuid', nama as 'nama' from vendor")->result_array();
		$this->load->view('dataParam', $data);
	}

	public function addDummy(){

		print_r($this->input->post());
		$trx_id = $this->input->post('trx');
		$value = $this->input->post('value');
		$vendor = $this->input->post('vendor');
		$kategori = $this->input->post('kategori');
		$uuid = json_decode(file_get_contents("http://api.yepsindonesia.com/auth/uuid"))->uuid;
		$produk = array(
			'vendor_uuid' => $vendor,
			'kategori_id' => $kategori,
			'label' => "none",
			'status' => "0",
			'uuid' => $uuid
		);
		$this->db->insert('produk', $produk);

		if($this->db->affected_rows() == 1){
			$n = count($value);
			for ($i=0; $i < $n; $i++) { 
				$detail = array(
					'produk_uuid' => $uuid,
					'kategori_parameter_id' => $trx_id[$i],
					'value' => $value[$i]
				);
				
				$this->db->insert('produk_detail', $detail);
			}
			?><script type="text/javascript"> alert("berhasil ditambah")</script> 
			<?php
			redirect(base_url('dummy'));
		}

	}

	public function form(){
		$this->load->view('contoh_form');
	}

	public function payment(){      
$url = 'https://my.ipaymu.com/payment.htm';  // URL Payment iPaymu 

$params = array(   // Prepare Parameters            
	"key" => "RwcnGa7MC9A15jvJ2xufU50uQrtuU1",
	"action" => "payment",
	"produk" => "INV/001",
	"price" => "10000",
	"quantity" => "1",
	"comment" => "test",
	"ureturn" => "http://localhost/api15/return.php?q=return",
	"unotify" => "http://localhost/api15/notify.php",
	"ucancel" => "http://localhost/api15/cancel.php",
	"format" => "json"
);

$params_string = http_build_query($params);

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, count($params));
curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

//execute post
$request = curl_exec($ch);

if ( $request === false ) {
	echo 'Curl Error: ' . curl_error($ch);
} else {

	$result = json_decode($request, true);

	if( isset($result['url']) )
		header('location: '. $result['url']);
	else {
		echo "Request Error ". $result['Status'] .": ". $result['Keterangan'];
	}
}

//close connection
curl_close($ch);
}
}
