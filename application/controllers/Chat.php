<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
        $this->load->model("model_chat");	
	}
	
	public function index()
	{
		/*if(!$this->session->userdata("loggin")){
			redirect("index.php/welcome/login","refresh");
		}*/
		$this->load->model("model_chat");
		$data['user']	= $this->model_chat->getAll(array("ip_user1 !=" => getUserIpAddr()));
		
		$this->load->view('footer',$data);
	}
	function getUserIpAddr(){
		if(!empty($_SERVER['HTTP_CLIENT_IP'])){
			//ip from share internet
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
			//ip pass from proxy
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
		
	}
	function login(){
		if($this->session->userdata('loggin')){
			redirect('index.php/welcome');
		}
		$this->load->model('model_user');
		$data['user']	= $this->model_chat->getAll(array('ip_user1 !=' => 
		getUserIpAddr()));
		$this->load->view('footer',$data);
	}
	
	function getChat(){
		//$this->load->model("model_user");
		$this->load->model("model_chat");
		
		$id_user	= $this->input->post("id_user",true); //tujuan
		//$id			= $this->session->userdata('id'); //dari
		$id			= $this->input->post("id_ip",true); //dari/user_1
		$id_max		= $this->input->post('id_max'); //dari
		$where	= "(((ip_user1 = '$id_user' AND admin = '$id') OR (admin = '$id_user' AND ip_user1 = '$id')) AND id > '$id_max')";
		$chat	= $this->model_chat->getAll($where);
		$data['id_max']		= $id_max;
		$data['ip_user1']	= $id_user;
		$data['chat'] 		= $chat;
		$this->load->view("vwChatBox",$data);
	}
	
	function getChatAll(){
		//$this->load->model("model_user");
		$this->load->model("model_chat");
		
		$id_user	= $this->input->post("id_user",true); //tujuan
		//$id			= $this->session->userdata('id'); //dari
		$id			= $this->input->post("id_ip",true); //dari/user_1
		$id_max		= $this->input->post('id_max'); //dari

		//$where	= "(((user_1 = '$id_user' AND user_2 = '$id') OR (user_2 = '$id_user' AND user_1 = '$id')))";
		$where	= "(((ip_user1 = '$id' AND admin = '$id_user') OR (admin = '$id_user' AND ip_user1 = '$id')))";
		$chat	= $this->model_chat->getAll($where);
		
		//$where2	= "(((user_1 = '$id_user' AND user_2 = '$id') OR (user_2 = '$id_user' AND user_1 = '$id')) AND id_chat > '$id_max')";
		$where2	= "(((ip_user1 = '$id' AND admin = '$id_user') OR (admin = '$id_user' AND ip_user1 = '$id')) AND id > '$id_max')";
		$get_id = $this->model_chat->getLastId($where2);
		
		$data['id_max']		= $get_id['id'];
		$data['ip_user1']	= $id_user;
		$data['chat'] 		= $chat;
		$this->load->view("footer",$data);
	}
	
	function getLastId(){
		//$this->load->model("model_user");
		$this->load->model("model_chat");
		
		$id_user	= $this->input->post("id_user",true); //tujuan
		//$id			= $this->session->userdata('id'); //dari
		$id			= $this->input->post("id_ip",true); //dari/user_1
		$id_max		= $this->input->post('id_max'); //dari
		
		$where	= "(((ip_user1 = '$id' AND admin = '$id_user') OR (admin = '$id_user' AND ip_user1 = '$id')) AND id > '$id_max')";
		$get_id = $this->model_chat->getLastId($where);
		
		echo json_encode(array("id" => $get_id['id'] != '' ?  $get_id['id'] : $id_max ));
	}
	
	function sendMessage(){
		$this->load->model("model_chat");
		$id_user	= $this->input->post("id_user",true); //tujuan/user_2
		//$id			= $this->session->userdata('id'); //dari/user_1
		$id			= $this->session->userdata('id'); //dari/user_1
		$pesan		= ($this->input->post("pesan",true));
		
		$data	= array(
			'ip_user1' => $id,
			'admin' => $id_user,
			'chat' => $pesan,
		);
		
		$query	=	$this->model_chat->getInsert($data);
		
		if($query){
			$rs = 1;
		}else{
			$rs	= 2;
		}
		
		echo json_encode(array("result"=>$rs));
		
	}
	
	function masuk(){
		$this->load->model('model_chat');
		
		$id		= $this->input->post('id_user',true);
		$data	= $this->model_chat->getAll(array('ip_user1' => $id))->row_array();
		
		$session = array('id' => $id, 'ip_user1' => $data['ip_user1'], 'loggin' => true);
		$this->session->set_userdata($session);
		redirect('index.php/welcome');
	}
	
	function keluar(){
		session_destroy();
		redirect("index.php/welcome/login");
	}
}
