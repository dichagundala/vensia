<?php
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

defined('BASEPATH') OR exit('No direct script access allowed');

class Request2 extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$client = new Client([
    // Base URI is used with relative requests
			'base_uri' => '',
    // You can set any number of default request options.
			'timeout'  => 2.0,
		]);
		$response = $client->request('POST', 'https://api.sandbox.midtrans.com/v2', 
			['verify' => false,
			'headers' => ['Authorization' => "ApiKey"]
		]);
		
	}

}