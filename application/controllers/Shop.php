<?php
use Mailgun\Mailgun;
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		$this->load->model("users");
		$this->load->model("produk");
		$this->load->model("transaksi");
		$this->load->model("invoice");
		$this->load->model("vendor");
		$this->load->library('uuid');
		$this->load->library('email');
		
		$params = array('server_key' => 'SB-Mid-server-IalaLlr_GXaUaVpYoziUfJLr', 'production' => false);
		$this->load->library('midtrans');
		$this->midtrans->config($params);

	}
	public function index()
	{
		$this->cart();
	}

	public function cart(){
		if($this->session->userdata('type') != "client"){
			redirect(base_url("index.php/Authss/login"));
		}else{

			$data = array();
			$page = "Cart";
			$user = $this->users->get_client($this->session->userdata('uuid'));
			
			$title = $page. " | "."Vensia";
			$data['title'] = $title;
			$desc = "Vendor Terbaik Indonesia";
			$data['desc'] = $desc;

			if ($this->ion_auth->logged_in()){
			// set the flash data error message if there is one
				$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
				$data['users'] = $this->ion_auth->users()->result();
				foreach ($data['users'] as $k => $user)
				{
					$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
				}
				$data['login'] = $this->ion_auth->is_admin();

			}

			$transaksi = $this->transaksi->get_cart($this->session->userdata('uuid'));

			$data['result'] = array();
			$result = array();
			$produk = array();
			foreach ($transaksi as $key1) {
				$data['alamat'] = $key1['address_order'];
				//untuk data transaksi additional
				$a = array();		
				$b = array();
				$result = $this->produk->get_produk_by_uuid($key1['uuid_produk']);
				array_push($a, 'uuid');
				array_push($b, $key1['uuid_produk']);
				foreach ($result as $key2) {
					$nama_vendor = $key2['nama_vendor'];
					$nama_kategori = $key2['nama_kategori'];
					$uuid_vendor = $key2['uuid_vendor'];
					array_push($a, $key2['nama']);
					array_push($b, $key2['value']);
				}
				$result2 = $this->produk->get_cart_add($key1['uuid']);
				$harga_total_add = $this->invoice->get_total_add($key1['uuid'])->total;
				$produk = array_combine($a, $b);
				// array_push($data['result'], array('uuid' => $key1['uuid'],'total_harga' => $key1['total_harga'], 'vendor' => $nama_vendor, 'slug' => $key2['slug'], 
				// 'uuid_vendor' => $uuid_vendor, 'kategori' => $nama_kategori, 'date' => $key1['date_order'], 'time' => $key1['time_order'], 'qty' => $key1['qty'],'result2' => $result2, 'note' => $key1['note'],'address' => $key1['address_order'], 'total_add' =>  $harga_total_add,
				// 'parameter' => $produk)); old code

				array_push($data['result'], array('uuid' => $key1['uuid'], 'vendor' => $nama_vendor, 'slug' => $key2['slug'], 
				'uuid_vendor' => $uuid_vendor, 'kategori' => $nama_kategori, 'date' => $key1['date_order'], 'time' => $key1['time_order'], 'qty' => $key1['qty'],'result2' => $result2, 'note' => $key1['note'],'address' => $key1['address_order'], 'total_add' =>  $harga_total_add,
				'parameter' => $produk));
			}
			$this->load->view('shop-chart', $data);
		}
	}

	public function update_invoice(){
		$this->invoice->auto_update();
	}

	public function delete_cart($uuid){
		if($this->transaksi->delete_cart($uuid)){
			$icon = "success";
			$text = "Berhasil dihapus";
		}else{
			$icon = "info";
			$text = "Oops, coba lagi nanti";
		}

		$data = array(
			'icon' => $icon,
			'text' => $text
		);

		echo json_encode($data);
	}

	public function note_save(){
		$dataNote = $this->input->post('note');
		$dataUuid = $this->input->post('uuid');

		for ($i=0; $i < count($dataNote); $i++) { 
			$this->transaksi->update_note($dataNote[$i], $dataUuid[$i]);
		}
	}

	public function checkout($alamat = ""){
		$data = array();
		$page = "Checkout";
		$title = $page. " | "."Vensia";
		$desc = "Vendor Terbaik Indonesia";
		$data['desc'] = $desc;
		
		$data['alamat'] = urldecode($alamat);
		
		if ($this->ion_auth->logged_in()){
			// set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$data['users'] = $this->ion_auth->users()->result();
			foreach ($data['users'] as $k => $user)
			{
				$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			$data['login'] = $this->ion_auth->is_admin();

		}

		$transaksi = $this->transaksi->get_cart($this->session->userdata('uuid'));
		$data['result'] = array();
		$result = array();
		$produk = array();
		foreach ($transaksi as $key1) {
			$a = array();		
			$b = array();
			$result = $this->produk->get_produk_by_uuid($key1['uuid_produk']);
			
			array_push($a, 'uuid');
			array_push($b, $key1['uuid_produk']);
			foreach ($result as $key2) {
				$nama_vendor = $key2['nama_vendor'];
				$nama_kategori = $key2['nama_kategori'];
				array_push($a, $key2['nama']);
				array_push($b, $key2['value']);
			}
			//untuk checkout tanpa pajak & additional
			/* awal */
			$transaksi_add = $this->produk->get_cart_add($key1['uuid']);
			$add_produk = $this->produk->get_add_produk_by_uuid($key1['uuid_produk']);
				
			foreach ($transaksi_add as $key4) {
				$id_add[] 		=  $key4['id'];
				$nama_add[] 	=  $key4['nama_add'];
				$harga_add[] 	=  $key4['harga_add'];
				$qty_add[] 		=  $key4['qty_add'];
				
			}
			foreach ($add_produk as $key6) {
				$id_add2[] 		=  $key6['id'];
				$nama_add2[] 	=  $key6['nama'];
				$harga_add2[] 	=  $key6['harga'];
				$qty_add2[] 	=  $key6['qty'];
				
			}
			$produk = array_combine($a, $b);
			// $produk2 = array_combine($c, $d, $e);
			//$produk2 = array_combine($c, $d, $e);
			array_push($data['result'], array('uuid' => $key1['uuid'], 'vendor' => $nama_vendor,  
			'kategori' => $nama_kategori, 'date' => $key1['date_order'], 
			'time' => $key1['time_order'], 'qty' => $key1['qty'], 'Note' => $key1['note'],
			'id_add' => $id_add, 'nama_add' => $nama_add, 'harga_add' => $harga_add, 'qty_add' => $qty_add,
			'id_add2' => $id_add2, 'nama_add2' => $nama_add2, 'harga_add2' => $harga_add2, 'qty_add2' => $qty_add2,'parameter' => $produk,'Alamat' => $key1['address_order'],'Note' => $key1['note'],'slug' => $key2['slug']));
			
			/* akhir */
		}

		$this->load->view('shop-checkout', $data);	
	}

	public function checkout_completed($uuid){
		$data = array();
		$title = "Checkout | Vensia";
		$desc = "Vendor Terbaik Indonesia";
		$data['desc'] = $desc;
		if ($this->ion_auth->logged_in()){
			// set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$data['users'] = $this->ion_auth->users()->result();
			foreach ($data['users'] as $k => $user)
			{
				$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}

			$data['login'] = $this->ion_auth->is_admin();

		}

		$data['invoice'] = $this->invoice->get($uuid);
		
		if($data['invoice'][0]['Metode Pembayaran'] == "transfer-Mandiri" || $data['invoice'][0]['Metode Pembayaran'] == "transfer-Mandiri Syariah" ){

			$temp = explode("-", $data['invoice'][0]['Metode Pembayaran']);
			$data['bank'] = $this->db->query("Select * from manual_bank_account where bank_name like '".$temp[1]."'")->result_array()[0];
		}
		$data['result'] = array();
		$result = array();
		$produk = array();
		
		foreach ($data['invoice'] as $key1) {
			$a = array();		
			$b = array();
			$result = $this->produk->get_produk_by_uuid($key1['Produk Uuid']);
			array_push($a, 'uuid');
			array_push($b, $key1['Produk Uuid']);
			foreach ($result as $key2) {
				$nama_vendor = $key2['nama_vendor'];
				$nama_kategori = $key2['nama_kategori'];
				array_push($a, $key2['nama']);
				array_push($b, $key2['value']);
			}
			$produk = array_combine($a, $b);
			array_push($data['result'], array('uuid' => $key1['Transaksi Uuid'], 'vendor' => $nama_vendor, 'kategori' => $nama_kategori, 'Status Transaksi' => $key1['Status Transaksi'], 'date' => $key1['Tgl Event'], 'qty' => $key1['qty'], 'Note' => $key1['Note'], 'Alamat' => $key1['Alamat'], 'parameter' => $produk));
		}
		$data['invoice_uuid'] = $uuid;
		$this->load->view('shop-checkout-completed', $data);
	}
	
	//gk dipakai, data yang sebenarnya di controler jasa.php
	public function detail($uuid_produk){
		$data = array();
		if ($this->ion_auth->logged_in()){
			// set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$data['users'] = $this->ion_auth->users()->result();
			foreach ($data['users'] as $k => $user)
			{
				$data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			$data['login'] = $this->ion_auth->is_admin();

		}
		$result = $this->produk->get_produk_by_uuid($uuid_produk);
		$a = array();
		$b = array();
		foreach($result as $key2){
			array_push($a, $key2['nama']);
			array_push($b, $key2['value']);
		}
		$data_json['result'] = array();
		$array_dummy = array();
		$array_dummy = array_combine($a, $b);
		array_push($data_json['result'], array("uuid" => $uuid_produk, "vendor" => $result[0]['nama_vendor'], "uuid vendor" => $result[0]['uuid_vendor'], "aktif vendor" => $result[0]['aktif_vendor'], "parameter" => $array_dummy));

		$data['produk'] = $data_json['result'][0];
		$this->load->view('shop-detail', $data);
	}

	//untuk harga & qty additional
	function sub_add_qty(){
        $id=$this->input->post('id');
        $data=$this->produk->get_subkategori($id);
        echo json_encode($data);
	}
	
	//untuk mengambil add produk dengan uuidProduk
	function get_sub_add_qty_byuuid(){
        $uuid=$this->input->post('uuid');
        $data=$this->produk->get_subkategori_byuuid($uuid);
        echo json_encode($data);
	}

	public function add_to_cart(){
		$uuid_produk = $this->input->post('produk_uuid');
		$time = $this->input->post('time');
		$qty = $this->input->post('qty');
		$note = $this->input->post('note');
		$alamat = $this->input->post('alamat');
		$name_add = $this->input->post('name_add3');
		$qty_add = $this->input->post('qty3');
		$harga_add = $this->input->post('harga4');
		$temp = explode(" - ", $time);
		$tanggal = new DateTime($temp[0]);

		if($this->session->userdata('type') != 'client'){
			$icon = "info";
			$text = "Oops, Kamu belum login, silahkan login dulu";
			$direct = base_url('Authss/login/');
		}else{
			$nilai_uuid=$this->uuid->v4();
			$data = array(
				'uuid_produk' => $uuid_produk,
				'uuid_user' => $this->session->userdata('uuid'),
				'uuid_invoice' => "",
				'date_order' => $tanggal->format('Y-m-d'),
				'time_order' => $temp[1],
				/*'qty' => $this->produk->cekMinimal($uuid_produk),*/
				'qty' => $qty,
				'note' => $note,
				'address_order' => $alamat,
				'status' => "CART",
				'uuid' => $nilai_uuid
			);
			$icon = "success";
						$text = $name_add;
						$direct = base_url('Shop/cart');
			$n2 = count($name_add);
                for($i2=0;$i2<$n2;$i2++){
                     /* untuk isi array additional input*/
                        /* awal */
						$data2 = array(
							'nama_add' => $name_add[$i2],
							'qty_add' => $qty_add[$i2],
							'harga_add' => $harga_add[$i2],
							'uuid_transaksi' => $nilai_uuid
						);
                        
                    //insert kedalam database add_produk
                    $this->db->insert('transaksi_add', $data2);
                    //$this->transaksi->add_additional($data2);
					/*if($this->db->insert('transaksi_add', $data2)){
						$icon = "success";
						$text = $name_add;
						$direct = base_url('Shop/cart');
					}else{
						$icon = "warning";
						$text = "Oops, silahkan coba lagi";
						$direct = "";
					}*/
					/* Akhir */
            }
			

			if($this->transaksi->add($data)){
				$icon = "success";
				$text = "Berhasil di tambahkan ke cart";
				$direct = base_url('Shop/cart');
			}else{
				$icon = "warning";
				$text = "Oops, silahkan coba lagi";
				$direct = "";
			}
		}

		$result = array(
			"icon" => $icon,
			"text" => $text,
			"direct" => $direct
		);

		echo json_encode($result);

	}

	public function detail_cart($uuid){
		$result = $this->produk->get_produk_by_uuid($uuid);
		$a = array();
		$b = array();
		foreach ($result as $key2) {
			array_push($a, $key2['nama']);
			array_push($b, $key2['value']);
		}
		$data['result'] = array_combine($a, $b);
		$this->load->view('cart-detail',$data);
	}

	public function transQty($uuid, $qty){
		if($uuid == null){
			$this->load->view("404");
		}else{

			$this->transaksi->qty($uuid, $qty);
		}
	}
	//untuk update transaksi additional
	public function transQty_add($uuid, $qty, $id){
		if($uuid == null){
			$this->load->view("404");
		}else{

			$this->transaksi->qty_add($uuid, $qty, $id);
		}
	}

	public function generateInvoice(){
		
		$date = new dateTime('Now');

		//real
		$transaksi = $this->transaksi->get_cart($this->session->userdata('uuid'));
		
		$results = array();
		$result = array();
		$produk = array();
		$nama = array();
		$harga_normal = array();
		$harga_promo = array();

		foreach ($transaksi as $key1) {
			$a = array();		
			$b = array();
			$c = array();
			$result = $this->produk->get_produk_by_uuid($key1['uuid_produk']);
			array_push($a, 'uuid');
			array_push($b, $key1['uuid_produk']);
			$alamat=$key1['address_order'];
			//array_push($c, $key1['address_order']);
			foreach ($result as $key2) {
				$nama_vendor = $key2['nama_vendor'];
				$nama_kategori = $key2['nama_kategori'];
				array_push($a, $key2['nama']);
				array_push($b, $key2['value']);
			}

			$produk = array_combine($a, $b);
			array_push($results, array('uuid' => $key1['uuid'], 'vendor' => $nama_vendor, 'kategori' => $nama_kategori, 'date' => $key1['date_order'], 'time' => $key1['time_order'], 'qty' => $key1['qty'], 'note' => $key1['note'], 'parameter' => $produk));
		}

		$subTotal = 0;
		$real = array();

		$uuid_invoice = $this->uuid->v4();
		$data_email_client = array();
		$biaya_satuan = "";
		foreach ($results as $key) {
			if($key['parameter']['Harga Promosi'] == "0"){
				$subTotal += (int)$key['parameter']['Harga Normal']*$key['qty'];
				$biaya_satuan = $key['parameter']['Harga Normal'];
			}else{
				$subTotal += (int)$key['parameter']['Harga Promosi']*$key['qty'];	
				$biaya_satuan = $key['parameter']['Harga Promosi'];
			}

			$this->transaksi->update_real($uuid_invoice, $key['uuid'], $key['parameter']['Nama'], $key['parameter']['Harga Normal'],$key['parameter']['Harga Promosi']);
			array_push($data_email_client, array( "vendor" => $key['vendor'], "kategori" => $key['kategori'], "satuan" => $biaya_satuan, "qty" => $key['qty'], "note" => $key['note']));
		}
		//disini untuk cek data status users,
		//jika status users '1' maka diubah jadi 2,
		//jika status user '3' maka diuabh jadi 4.
		//belum dikerjakan

		//$grantTotal = $subTotal + $subTotal/10 + $subTotal/20;
		$grantTotal = $subTotal + $subTotal/20;
		$dp = "0";


		$metode = 'transfer-Mandiri'; //$this->input->post('metode');
		$angsuran = 1; //$this->input->post('angsuran');
		$alamat = 'alamat'; //$this->input->post('alamat');

		$inc = $this->getInc();
		$invoice = "INV/".$date->format('dmY')."/C/".$angsuran."/".$inc;


		$data= array(
			"uuid" => $uuid_invoice,
			"kode_invoice" => $invoice,
			"metode_pembayaran" => $metode,
			"total_biaya" => $grantTotal,
			"biaya_dp" => $dp,
			"cicilan" => $angsuran,
			"alamat" => $alamat,
			"status" => "KONFIRMASI"
		);

		if($this->invoice->add($data)){
			
			$data_email = $this->vendor->get_email_invoice($invoice);
			$total_price = 0;
			foreach ($data_email as $key) {
				if($key['promo'] != "0"){
					$price = $key['promo'] * $key['qty'];
				}else{
					$price = $key['normal'] * $key['qty'];
				}
				$total_price += (int)$price;
				$date =  new dateTime($key['tgl_pesan']);
				$tgl_event = date_format(date_create($key['waktu']), "d M Y")." ".$key['jam']." WIB";
				//$nama_client = $key['nama_client'];
				$nama_client = $key['nama_client']." ".$key['nama_client_belakang'];
				$user_email = $key['useremail'];
				//data email kirim ke vendor
				$data_vendor = array(
					"nama" => $key['nama'],
					//"client" => $key['nama_client'],
					"client" => $key['nama_client']." ".$key['nama_client_belakang'],
					"email" => $key['email'],
					"invoice" => $invoice,
					"tgl_pesan" => $date->format('d M Y'),
					"tgl_event" => $tgl_event,
					"harga" => $price,
					"tempo" => $date->modify('+1 day')->format('(d M Y) (H:i)')
				);

				//generate dan kirim email vendor
				$this->generateEmail("Pesanan Baru - ".$invoice, "email/pesananbaru-vendor.php", $data_vendor);

			}

			$date =  new dateTime($key['tgl_pesan']);
				//data email kirim ke cro
			$data_cro = array(
				"nama" => $key['nama'],
				"client" => $nama_client,
				"invoice" => $invoice,
				"tgl_pesan" => $date->format('d M Y'),
				"harga" => $total_price,
				"tempo" => $date->modify('+1 day')->format('(d M Y) (H:i)')
			);

				//data email kirim ke client
			$data_client = array(
				"email" => $user_email,
				"client" => $nama_client,
				"invoice" => $invoice,
				"transaksi" => $data_email_client
			);

				//generate dan kirim email cro
			$this->generateEmailCRO("Pesanan Baru - ".$invoice, "email/pesananbaru-cro.php", $data_cro);
			
				//generate dan kirim email client
			$this->generateEmail("Menunggu Konfirmasi Pesanan - ".$invoice, "email/menunggukonfirmasi-client.php", $data_client);

			$text = "Pesanan Anda akan Kami proses untuk ketersediaannya, harap menunggu maksimal 1x24 jam, Terima kasih";
			$icon = "success";
			$direct = base_url("Shop/checkout_completed/").$uuid_invoice;


		}else{
			$text = "Oops, silahkan coba lagi";
			$icon = "info";
			$direct = "";
		}

		$data = array(
			"text" => $text,
			"icon" => $icon,
			"direct" => $direct
		);

		echo json_encode($data);
	}

	public function getInc(){
		$text =  $this->transaksi->inc_invoice();
		if($text == ""){
			$invID = 0;
		}else{
			$invID = (int)substr($text['kode_invoice'], -3);
		}
		$invID++;
		$invID = str_pad($invID, 3, '0', STR_PAD_LEFT);
		return $invID;
	}

	public function cancel($uuid){
		$this->invoice->cancel($uuid);
	}

	public function payment(){      
		$url = 'https://my.ipaymu.com/payment.htm';  

		$params = array(   
			"key" => "RwcnGa7MC9A15jvJ2xufU50uQrtuU1",
			"action" => "payment",
			"product" => $this->input->post('invoice'),
			"price" => $this->input->post('price'),
			"quantity" => "1",
			"comments" => $this->input->post('comment'),
			"ureturn" => "https://private.yepsindonesia.com/profil/",
			"unotify" => "http://localhost/api15/notify.php",
			"ucancel" => "http://localhost/api15/cancel.php",
			"format" => "json"
		);

		$params_string = http_build_query($params);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, count($params));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$request = curl_exec($ch);

		if ( $request === false ) {
			echo 'Curl Error: ' . curl_error($ch);
		} else {

			$result = json_decode($request, true);

			if( isset($result['url']) )
				header('location: '. $result['url']);
			else {
				echo "Request Error ". $result['Status'] .": ". $result['Keterangan'];
			}
		}

		curl_close($ch);
	}

	public function va(){      
		$url = 'https://my.ipaymu.com/api/getva';  

		$params = array(   
			"key" => "RwcnGa7MC9A15jvJ2xufU50uQrtuU1",
			"uniqid" => $this->input->post('invoice'),
			"price" => $this->input->post('price'),
			"unotify" => base_url('shop/notifyVA')
		);

		$params_string = http_build_query($params);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, count($params));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$request = curl_exec($ch);

		if ( $request === false ) {
			echo 'Curl Error: ' . curl_error($ch);
		} else {
			$result = json_decode($request, true);
			$this->invoice->ipaymu($this->input->post('uuid'), $result['va']);
			echo "<h2>No. Virtual Account : ".$result['va']."</h2>";
		}

		curl_close($ch);
	}

	//function generate
	//parameter : subject = subject dari email
	//			  tpl = view/template email yang dipakai
	//			  data = data yang dimasukkan ke email
	public function generateEmail($subject, $tpl, $data){
		$mgClient = new Mailgun('54c915413905fdcbe811213e8f59be3f-7efe8d73-1855dae2');
		$domain = "mg.yepsindonesia.com";
        # Make the call to the client.
		$result = $mgClient->sendMessage($domain, array(
			'from'    => 'Vensia no-reply@yepsindonesia.com',
			'to'      => $data['email'],
			'subject' => $subject,
            'text'    => 'Do Not Reply Email from This!',
			'html'    => $this->load->view($tpl,$data,TRUE)
		));
		if($result->http_response_code == 200){
			return true;
		}else{
			return false;
		}

	}

	public function generateEmailCRO($subject, $tpl, $data){
		$mgClient = new Mailgun('54c915413905fdcbe811213e8f59be3f-7efe8d73-1855dae2');
		$domain = "mg.yepsindonesia.com";
        # Make the call to the client.
		$result = $mgClient->sendMessage($domain, array(
			'from'    => 'Vensia no-reply@yepsindonesia.com',
			//'to'      => "imam.9216@gmail.com",
			'to'      => "adhika.trisnadp@gmail.com",
			'cc'      => "gandhiwicaks@gmail.com",
			'cc' 	  => "ronnifirmansyah17@gmail.com",
			//'to' =>  "a.kilua_1@yahoo.co.id",
			'subject' => $subject,
            'text'    => 'Do Not Reply Email from This!',
			'html'    => $this->load->view($tpl,$data,TRUE)
		));
		if($result->http_response_code == 200){
			return true;
		}else{
			return false;
		}

	}


}