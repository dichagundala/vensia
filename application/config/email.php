<?php defined('BASEPATH') OR exit('No direct script access allowed.');

//$config['useragent']        = 'PHPMailer';              // Mail engine switcher: 'CodeIgniter' or 'PHPMailer'
$config['protocol']         = 'smtp';                   // 'mail', 'sendmail', or 'smtp'
$config['smtp_host']        = 'mail.yepsindonesia.com';
$config['smtp_user']        = 'test@yepsindonesia.com';
$config['smtp_pass']        = 's3rv3ry3ps1nd0n3s14';
/*$config['protocol']         = 'smtp';                   // 'mail', 'sendmail', or 'smtp'
$config['smtp_host']        = 'smtp.mailgun.org';
$config['smtp_user']        = 'postmaster@mg.yepsindonesia.com';
$config['smtp_pass']        = '1d64eda7007d38e5f706e6611f7085e7-7efe8d73-ab607150';*/
$config['smtp_port']        = 465;
$config['smtp_timeout']     = 30;                       // (in seconds)
$config['smtp_crypto']      = 'ssl';                    // '' or 'tls' or 'ssl'
$config['smtp_debug']       = 0;                        // PHPMailer's SMTP debug info level: 0 = off, 1 = commands, 2 = commands and data, 3 = as 2 plus connection status, 4 = low level data output.
$config['mailtype']         = 'html';                   // 'text' or 'html'

$config['crlf']             = "\n";                     // "\r\n" or "\n" or "\r"
$config['newline']          = "\n";                     // "\r\n" or "\n" or "\r"
$config['bcc_batch_mode']   = false;
$config['bcc_batch_size']   = 200;
$config['encoding']         = '8bit';                   // The body encoding. For CodeIgniter: '8bit' or '7bit'. For PHPMailer: '8bit', '7bit', 'binary', 'base64', or 'quoted-printable'.