<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="YEPS" />
    <meta name="description" content="Your Event Partner Solution">
    <!-- Document title -->
    <title>YEPS - Lupa Password</title>
    <!-- Stylesheets & Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,800,700,600|Montserrat:400,500,600,700|Raleway:100,300,600,700,800" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets'); ?>/css/plugins.css" rel="stylesheet">
    <link href="<?php echo base_url('assets'); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url('assets'); ?>/css/responsive.css" rel="stylesheet"> 
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets'); ?>/images/favicon-16x16.png">
</head>
<body>
    <!-- Wrapper -->
    <div id="wrapper">
        <!-- Section -->
        <section class="fullscreen" style="background-image: url(<?php echo base_url('assets'); ?>/images/pages/backg-forgot.jpg); background-repeat: no-repeat;">
            <div class="container container-fullscreen">
                <div class="text-middle">
                    <div class="text-center m-b-30">
                        <a href="index.html" class="logo">
                            <img src="<?php echo base_url('assets'); ?>/images/logo-dark.png" alt="Polo Logo" style="max-height: 100px">
                        </a>
                    </div>
                    <div class="row">
                        <div class="col-md-3 center p-30 background-white b-r-6">
                            <h3>Insert Your New Password</h3>
                            <form id="form-changePass" class="form-transparent-grey">
                                <div class="form-group">
                                    <input type="hidden" name="uuid" value="<?php echo $uuid;?>">
                                    <input type="password" name="pass" class="form-control form-white placeholder" placeholder="Enter your Password..." required id="password-field">
                                    <span toggle="#password-field" class="field-icon fa fa-fw fa-eye toggle-password"></span>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-default">Recover your Password</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end: Section -->
    </div>
    <!-- end: Wrapper -->

    <!-- Go to top button -->
    <a id="goToTop"><i class="fa fa-angle-up top-icon"></i><i class="fa fa-angle-up"></i></a>

    <!--Plugins-->
    <script src="<?php echo base_url('assets'); ?>/js/jquery.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/plugins.js"></script>

    <!--Template functions-->
    <script src="<?php echo base_url('assets'); ?>/js/functions.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.11/dist/sweetalert2.all.min.js"></script>
    <script type="text/javascript">
        $('form#form-changePass').on('submit', function(e){
            e.preventDefault();

            var mydata = $('form#form-changePass').serialize();
            $.ajax({
                type        : "POST",
                url         : "<?php echo base_url('/Authss/change_pass');?>",
                data        : mydata,
                datatype    : "json",
                success     : function(hasil){
                    console.log(hasil);                       
                    var rs = $.parseJSON(hasil); 
                    swal({
                        type : rs['icon'],
                        text : rs['text']
                    }).then( function() {
                        if(rs['icon'] == "success"){

                            location.replace(rs['direct']);
                        }
                    });
                } 
            });
        });
    </script>
    <script type="text/javascript">
        /*for show and hidden password*/
        $(".toggle-password").click(function() {

            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    </script>
</body>

</html>
