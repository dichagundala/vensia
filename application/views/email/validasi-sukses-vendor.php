<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>

body {
  width: 100% !important;
  min-width: 100%;
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%;
  margin: 0;
  padding: 0;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  font-family: Roboto, Helvetica, Arial, sans-serif;
}

.img {
  width: 100%;
  height: auto;
}

.box{
  background-color: white;
  border-radius: 10px;
  padding: 0px;
  margin: auto;
  width: 50%;
  border-collapse: collapse; border-radius: 6px; border-spacing: 0; box-shadow: 0 1px 8px 0 rgba(28,35,43,0.15); float: none;
}

.my-backg{
  background: #FFFFFF; border-collapse: collapse; border-radius: 6px; border-spacing: 0; box-shadow: 0 1px 8px 0 rgba(28,35,43,0.15); float: none; margin: auto; overflow: hidden; padding: 0; text-align: center; vertical-align: top; width: 580px
}

.my-backg2{
  background: #FFFFFF; border-collapse: collapse; border-spacing: 0; hyphens: none; margin: auto; max-width: 100%; padding: 0; text-align: inherit; vertical-align: top; width: 320px !important
}

.title{
  font-weight: bold;
  text-transform: uppercase;
  text-align: center;
  color: white;
}

.my-h2{
  color: #6F7881; font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 300; line-height: 22px; margin: 0; padding: 0; text-align: left; width: 100%; word-wrap: normal
}

.my-h1{
  color: inherit; font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 24px; font-weight: 600; hyphens: none; line-height: 30px; margin: 0 0 24px; padding-top: 30px; text-align: left; width: 100%; word-wrap: normal
}

.my-txt{
  color: #6F7881; font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.5; margin: 0; padding: 0; text-align: center
}


</style>

<body style="width: 100% !important;
min-width: 100%;
-webkit-text-size-adjust: 100%;
-ms-text-size-adjust: 100%;
margin: 0;
padding: 0;
-moz-box-sizing: border-box;
-webkit-box-sizing: border-box;
box-sizing: border-box;
font-family: Roboto, Helvetica, Arial, sans-serif;
background-color: #fafafa">
<div  style="padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;">
  <div  style="margin-right: -15px; margin-left: -15px;">

    <div style="margin-top: 30px" align="center">
      <img width="250" src="<?php echo base_url('assets'); ?>/images/email/c.png" style="margin-top: 30px">
    </div>
    <div style="margin-top: 30px;"></div>
    <div  style=" background-color: white;border-radius: 10px;padding: 0px; margin: auto; width: 50%;border-collapse: collapse; border-radius: 6px; border-spacing: 0; float: none; border: 3px solid #d3d3d3;">

      <div  style="padding: 20px; background: #FFFFFF; border-collapse: collapse; border-spacing: 0; hyphens: none; margin: auto; max-width: 100%; padding: 0; text-align: inherit; vertical-align: top; width: 450px !important">


        <h1 align="center" style="color: inherit; font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 20px; font-weight: 600; hyphens: none; line-height: 30px; margin: 0 0 24px; padding-top: 30px; text-align: left; width: 100%; word-wrap: normal">
          Selamat Akun Anda Telah di Validasi
        </h1>

       <!-- <h2 align="left" style=" color: #6F7881; font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 300; line-height: 22px; margin: 0; margin-bottom: 10px; padding: 0; text-align: left; width: 100%; word-wrap: normal">
          Berikut informasi login anda :
        </h2>

        <h2 align="left" style="padding-top: 10px;  color: #6F7881; font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 300; line-height: 22px; margin: 0; padding: 0; text-align: left; width: 100%; word-wrap: normal">
          url : <a href="https://vendor.yepsindonesia.com" target="_blank"><span style="color: #107ade; text-decoration: underline; font-weight: bold;">https://vendor.yepsindonesia.com</span></a>
        </h2>

        <h2 align="left" style="padding-top: 10px;  color: #6F7881; font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 300; line-height: 22px; margin: 0; padding: 0; text-align: left; width: 100%; word-wrap: normal">
          username : <span style="font-weight: bold;">email</span>
        </h2>

        <h2 align="left" style="padding-top: 10px;  color: #6F7881; font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 300; line-height: 22px; margin: 0; padding: 0; text-align: left; width: 100%; word-wrap: normal; margin-bottom: 20px;">
          password : <span style="font-weight: bold;">rahasiavendor</span>
        </h2>

        <h2 align="left" style="padding-top: 10px;  color: #6F7881; font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 300; line-height: 22px; margin: 0; padding: 0; text-align: left; width: 100%; word-wrap: normal">
          Harap reset password Anda untuk mencegah agar akun Anda tidak dapat digunakan oleh orang lain.
        </h2>-->
        <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: center; vertical-align: top; width: 100%">
          <tbody>
            <tr style="padding: 0; text-align: center; vertical-align: top" align="center">
              <td style="-moz-hyphens: auto; -webkit-hyphens: auto; background: #1375d1; border: 2px none #1375d1; border-collapse: collapse !important; border-radius: 6px; color: #FFFFFF; font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: center; vertical-align: top; word-wrap: break-word"
                align="center" bgcolor="#1375d1" valign="top">
                <a href="<?php echo base_url('index.php/Authss/login/');?>" rel="noopener noreferrer" target="_blank" style="border: 0 solid #1375d1; border-radius: 6px; color: #FFFFFF; display: inline-block; font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: bold; line-height: 1.3; margin: 0; padding: 13px 0; text-align: center; text-decoration: none; width: 100%"
                  target="_blank">
                  <p class="text-center" style="color: white; font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 300; letter-spacing: 1px; line-height: 1.3; margin: 0; padding: 0; text-align: center" align="center">
                    MASUK AKUN
                  </p>
                </a>
              </td>
            </tr>
          </tbody>
        </table>
        <br>
        <br>
      </div>
    </div>

    <center>
      <div style="width: 564px; margin-top: 30px">
        <p  align="center" style="color: #6F7881; font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.5; padding: 0; text-align: center">
          Your Event Partner Solution
        </p>
        <p align="center" style="color: #6F7881; font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.5; margin: 0; padding: 0; text-align: center">
         Ariobimo Sentral Lantai 4. 
         Jl. H. R. Rasuna Said Kuningan Timur, Kecamatan Setiabudi, 
         Jakarta Selatan, DKI Jakarta 12950
       </p>
       <p  align="center" style="color: #6F7881; font-family: Roboto, Helvetica, Arial, sans-serif; font-size: 14px; font-weight: normal; line-height: 1.5; margin: 0; padding: 0; text-align: center">
        <span>
          <a href="#" style="color: #1375d1; text-decoration: none" target="_blank">yepsindonesia.com</a>
        </span> 
      </p>
    </div>
  </center>

</div>
</div>
</body>
</html>