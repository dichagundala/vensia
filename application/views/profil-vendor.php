<?php $this->load->view('header'); ?>

<!-- Our numbers -->
<section class="p-t-40 p-b-40" style="background-image: linear-gradient(to right, #05519a , #107ade, #4091de);">
    <div class="container xs-text-center sm-text-center">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
             <div class="equalize testimonial testimonial-box" style="box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2), 0 2px 10px 0 rgba(0, 0, 0, 0.19);">
                <div class="p-b-5 p-t-5 testimonial-item">

            <?php
                if($vendor['publicId'] == ""){
                    $img = base_url('./assets/images/profil.jpg');
                }else{
                    $img = "https://res.cloudinary.com/yepsindo/image/upload/w_380,h_507/".$vendor['publicId'].".jpg";
                }
            ?>
                    <img alt="Vendor img" class="yeps-avatar m-t-10" src="<?php echo $img; ?>">
                    <span><h1 class="yeps-vendor-name">
                        <?php echo $vendor['namaVendor'];?><br>
                        <!-- <?php echo $vendor['deskripsi'];?> -->
                        
                    </h1></span>
                    <p class="yeps-vendor-location">
                        <i class="fas fa-map-marker-alt" style="color: black"></i> 
                        &nbsp;<?php echo $vendor['vKabupaten'];?>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
        <!-- <div class="col-md-6">
           <h4 class="m-b-10 text-light">Pilih Kategori</h4>

           <select class="selectpicker" multiple>
            <?php
            $kategori = explode(",", $vendor['kategori']);
            foreach ($kategori as $key) {
                echo "<option value='".$key."'>".$key."</option>";
            }
            ?>
        </select>
        <h4 class="m-b-10 text-light">Atur Berdasarkan</h4>

        <select class="selectpicker m-b-20">
            <option value="popularity">Popularitas</option>
            <option value="asc">Nama: A - Z</option>
            <option value="dsc">Nama: Z - A</option>
            <option value="price">Harga Tinggi ke Rendah</option>
            <option value="price-desc">Harga Rendah ke Tinggi</option>
            <option value="disc">Diskon</option>
        </select>

        <div class="btn-group bootstrap-select">
            <a href="" class="btn btn-danger" style="width: 100%">CARI</a>
        </div>
    </div>
    <div class="col-md-1"></div> -->
    
</div>
</div>
</section>
<!-- end: Our numbers -->

<!-- Content -->
<section id="page-content" style="background-color: #f7f7f7;">
    <div class="container clearfix container-fullscreen">
        <div class="row">
            <!-- post content -->
            <?php
            $link_vendor = str_replace(" ", "_", $vendor['namaVendor']);
            ?>
            <div id="tabs-04" class="tabs simple justified no-border tab-margin">
                <ul class="tabs-navigation">
                    <li <?php if($tab == "produk"){ echo "class='active'"; }?> style="text-align: center"><a href="<?php echo base_url('jasa/'.$link_vendor.'/');?>"><i class="fa fa-home"></i>Produk</a> </li>
                    <li <?php if($tab == "portfolio"){ echo "class='active'"; }?> style="text-align: center"><a href="<?php echo base_url('jasa/'.$link_vendor.'/portfolio');?>"><i class="fa fa-images"></i>Portfolio</a> </li>
                    <li <?php if($tab == "about"){ echo "class='active'"; }?> style="text-align: center"><a href="<?php echo base_url('jasa/'.$link_vendor.'/about');?>"><i class="fa fa-book"></i>Tentang Kami</a> </li>
                </ul>
                <div class="tabs-content" style="background-color: #f7f7f7">
                    <div class="tab-pane <?php if($tab == "produk"){ echo "active"; }?>" id="About4">
                        <div class="row m-b-20">
                            <div class="col-md-6 p-t-10 m-b-20"></div>
                            <div class="col-md-3 p-t-5" style="text-align: right;"><h5>Atur Berdasarkan</h5></div>
                            <div class="col-md-3">
                                <div class="order-select">
                                    <!-- <form action="<?php echo base_url('kategori/tipe/'.$kategori.'/'.$page)?>" method="post" class="form-inline"> -->
                                        <form action="#" method="post" class="form-inline">
                                            <div class="form-group">
                                                <select name="sort">
                                                    <option selected="selected" value="order">Pengaturan Default</option>
                                                    <option value="n">Nama: A - Z</option>
                                                    <option value="nh">Nama: Z - A</option>
                                                    <option value="ph">Harga Tinggi ke Rendah</option>
                                                    <option value="p">Harga Rendah ke Tinggi</option>
                                                </select>
                                            </div>
                                            <button type="submit" class="btn btn-yeps btn-sm">Filter</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--Product list-->
                            <div class="shop container">
                                <div class="grid-layout grid-5-columns" data-item="grid-item">
                                    <?php 
                                    function exist($url){
                                        $file_headers = @get_headers($url);
                                        if($file_headers[0] == "HTTP/1.0 200 OK"){
                                            return true;
                                        }else{
                                            return false;
                                        }
                                    }
                                    foreach ($produk['result'] as $key){
                                     $nama_vendor = str_replace(" ","_",$key['vendor']);
                                     $produk = $key['slug'];
                                     $foto = array_filter(explode(";", $key['parameter']['Foto']));
                                     if(count($foto) > 0){
                                        foreach ($foto as $keyFoto) {
                                            $image = "https://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/".$keyFoto.".jpg";
                                            if(empty($image) || $key['parameter']['Foto'] == "" || $keyFoto == ""){
                                                $image = base_url('assets/images/pages/blank.jpg');
                                            }
                                        }
                                    }else{
                                        $image = "https://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/".$key['parameter']['Foto'].".jpg";

                                        if(empty($image) || $key['parameter']['Foto'] == "" || $key['parameter']['Foto'] == ""){
                                            $image = base_url('assets/images/pages/blank.jpg');
                                        }
                                    }
                                    ?>
                                    <div class="grid-item">
                                        <div class="product yeps-vendor-card">
                                            <div class="product-image">
                                                <a href="<?php echo base_url('jasa/'.$nama_vendor.'/'.$produk); ?>">

                                                    <img alt="Shop product image!" src="<?php echo $image; ?>"> 
                                                </a>
                                                <?php
                                                if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal']){
                                                    ?>
                                                    <div class="product-harga" style="display: none">
                                                    </div>
                                                    <?php
                                                }else{
                                                    ?>
                                                    <span class="product-sale-off"><?php echo ceil((((int)$key['parameter']['Harga Normal'] - (int)$key['parameter']['Harga Promosi'])/(int)$key['parameter']['Harga Normal'])*100); echo "%";?> Off</span>
                                                    <?php
                                                }
                                                ?>
                                                <div class="product-overlay">
                                                    <a href="<?php echo base_url('jasa/'.$nama_vendor.'/'.$produk); ?>">Lihat Detail</a>
                                                </div>
                                            </div>

                                            <div class="product-description" style="padding: 10px">
                                                <div class="product-category"><?php $key['parameter']['Lokasi']; ?></div>

                                                <div class="product-title product-height">
                                                    <h3><a href="<?php echo base_url('jasa/'.$nama_vendor.'/'.$produk); ?>"><?php echo $key['parameter']['Nama']?></a></h3>
                                                </div>
                                                <?php
                                                if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal']){
                                                    ?>
                                                    <div class="product-harga">Rp. <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?>
                                                </div>
                                                <?php
                                            }else{

                                                ?>
                                                <div class="product-harga" style="color: #107ade">Rp. <?php echo number_format($key['parameter']['Harga Promosi'],0,",","."); ?>
                                            </div>
                                            <div class="diskon-harga">Rp. <strike> <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?></strike>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                    </div>
                    <hr>
                    <!-- Pagination -->
                    <center>
                        <nav class="text-center">
                          <ul class="pagination pagination-fancy">
                            <li> <a href="#" aria-label="Previous"> <span aria-hidden="true"><i class="fa fa-angle-left"></i></span> </a> </li>
                            <?php
                            $active = "";
                            if($produk_count%10 > 0){
                                $produk_count = ((int)($produk_count/10))+1;
                            }else{
                                $produk_count = ((int)($produk_count/10));
                            }

                            for ($i=1; $i <= $produk_count ; $i++) { 
                                $nama_vendor = str_replace(" ", "_", $vendor['nama']);
                                if($i == $page){
                                    $active = "class='active'";
                                }
                                echo "<li ".$active."><a href='".base_url('jasa/'.$nama_vendor.'/page='.$i)."'>".$i."</a></li>";
                                $active = "";
                            }
                            ?>
                            <li> <a href="#" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-angle-right"></i></span> </a> </li>
                        </ul>
                    </nav>
                </center>
                <!-- end: Pagination -->
            </div>
            <!--End: Product list-->
        </div>

        <div class="tab-pane <?php if($tab == "portfolio"){ echo "active"; }?>" id="Choose4">
           <div class="container">
            <div class="row">
                <!-- Blog -->
                <div id="blog" class="grid-layout post-4-columns m-b-30" data-item="post-item">
                    <!-- Post item-->
                    <?php foreach( $album as $key2){ ?>      
                        <div class="post-item border">
                            <div class="post-item-wrap yeps-vendor-card">
                                <div class="post-image">
                                    <?php $result= $this->vendor->get_foto_by_album($key2['uuid']); ?>
                                    <div data-lightbox="gallery" data-items="10" data-loop="true">
                                        <?php $panjang = count($result); ?>
                                        <?php for($i=0;$i<$panjang;$i++){ 
                                            if($i==0){?>
                                                <a  title="<?php echo $result[$i]['ket_foto'];?>" href="<?php echo 'https://res.cloudinary.com/yepsindo/image/upload/'.$result[$i]['public_id'].'.jpg';?>" data-lightbox="gallery-item">
                                                    <img alt="" src="<?php echo 'https://res.cloudinary.com/yepsindo/image/upload/w_595,h_397,c_fill/'.$result[$i]['public_id'].'.jpg';?>"></a>
                                                <?php }
                                                else { ?>
                                                    <a title="<?php echo $result[$i]['ket_foto'];?>" href="<?php echo 'https://res.cloudinary.com/yepsindo/image/upload/'.$result[$i]['public_id'].'.jpg';?>" data-lightbox="gallery-item"></a>
                                                <?php }
                                            } ?>
                                        </div>
                                    </div>
                                    <center>
                                        <div class="post-item-description">
                                           
                                           
                                            <span class="post-meta-date"><i class="fa fa-calendar-o"></i> <?php echo date_format(date_create($key2['tgl_kegiatan']), 'd M Y');?></span>
                                            <h2><a data-toggle="tooltip" data-placement="bottom" title="<?php echo $key2['deskripsi']; ?>"><?php echo $key2['judul'];?>
                                        </a></h2>
                                    </div>
                                </center>
                            </div>
                        </div>
                    <?php } ?>

                    <!-- end: Post item-->

                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane <?php if($tab == "about"){ echo "active"; }?>" id="Assets4">
        <div class="container">
            <div class="row">
                <div style="padding: 20px">
                    <p>
                        <?php echo $vendor['bioVendor']?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- end: post content -->
</div>
</div>
</section>
<!-- end: Content -->

<?php $this->load->view('footer'); ?>

<script type="text/javascript">
    function formatDate(date){
      var today = new Date(date);
      var dd = today.getDate();
      var mm = today.getMonth()+1;

      var month = ['', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

      var yyyy = today.getFullYear();
      if(dd<10){
        dd='0'+dd;
    } 
    if(mm<10){
        mm='0'+mm;
    } 
    var today = dd+' '+month[mm]+' '+yyyy;
    return today;
}

$.ajax({
 url: 'http://localhost/api15/vendors/album_with_sampul',
 type: 'POST',
 data: {
   uuid_vendor : '<?php echo $vendor['uuid']; ?>'
},
dataType: 'json',
success: function(result){
    console.log(result);
    for(var i = 0; i < result.length; i++){
        /*var template = "<div class='grid-item'>"+result[i].judul+"<br>"+result[i].total+"<br>"+result[i].deskripsi+"<br>"+result[i].tgl_kegiatan+"<br>"+result[i].uuid+"</div><div class='grid-item'>"+result[i].judul+"<br>"+result[i].total+"<br>"+result[i].deskripsi+"<br>"+result[i].tgl_kegiatan+"<br>"+result[i].uuid+"</div>";*/
        var template = '<div class="post-item border"><div class="post-item-wrap yeps-vendor-card"><div class="post-image"><div data-lightbox="gallery" data-items="10" data-loop="true"><a href="https://res.cloudinary.com/yepsindo/image/upload/w_255,h_170,c_pad,b_auto/'+result[i].public_id+'.jpg" data-lightbox="gallery-item"><img alt="" src="https://res.cloudinary.com/yepsindo/image/upload/w_255,h_170,c_pad,b_auto/'+result[i].public_id+'.jpg"></a></div></div><div class="post-item-description"><span class="post-meta-date"><i class="fa fa-calendar-o"></i>'+formatDate(result[i].tgl_kegiatan)+'</span><h2><a href="#">'+result[i].judul+'</a></h2><p>'+result[i].deskripsi+'</p></div></div></div>';
        $("#gallery-album").append(template);
    }
}
});
</script>