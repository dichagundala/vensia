<div class="row">
	<div class="col-sm-6">	<img alt="Shop product image!" src="<?php echo $result['Foto'];?>"></div>
	<div class="col-sm-6">
		<p>Deskripsi</p>
		<p><?php echo $result['Deskripsi']; ?></p>
	</div>

</div>
<div class="row">
	<div class="col-sm-12">
		<div class="box-services-a">
			<h3 align="center">Additional Info</h3>
			<table class="table table-striped table-bordered">
				<tbody>
					<?php 
					foreach ($result as $key => $value) {
						if($key != "Deskripsi"){
							?>
							<tr>
								<td><?php echo $key;?></td>
								<?php
								if($key == "Harga"){
									echo "<td>Rp. ". number_format($value,0,",",".") ."</td>";

								}else
								{

									echo "<td>".$value."</td>";
								}
								?>
							</tr>
							<?php 
						} 
					} 
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>