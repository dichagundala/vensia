<!DOCTYPE html>
<html lang="en">

<head>

    <?php if(ENVIRONMENT == 'production'){ ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126567510-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-126567510-1');
      </script>
  <?php } ?>

  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="author" content="Vensia" />
  <meta name="description" content="<?php echo $desc;?>" />
  <?php
  if(isset($keyword)) { ?>
    <meta name="keyword" content="<?php echo $keyword;?>" />
<?php } ?>

<!-- Document title -->
<title><?php if(isset($title)){ echo $title;} else{ echo "Vensia.id";};?></title>
<!-- Stylesheets & Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto%3A400%2C500%2C700%2C900&#038;subset=latin%2Clatin-ext" rel="stylesheet" type="text/css" />
<!-- ui kit -->
<link href="<?php echo base_url('assets'); ?>/uikit/css/uikit.css" rel="stylesheet">  
<link href="<?php echo base_url('assets'); ?>/css/plugins.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/style.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/responsive.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/css/custom.css" rel="stylesheet">  
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/timepicker/dist/bootstrap-clockpicker.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<!-- Favicon -->
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets'); ?>/images/favicon-16x16.png">

<?php
if(isset($produk['parameter']['Nama'])){
 if($data_vendor['publicId'] == ""){
    $img = base_url('./assets/images/profil.jpg');
}else{
    $img = "https://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/".$data_vendor['publicId'].".jpg";
}
if($produk['parameter']['Harga Promosi'] == "0" || $produk['parameter']['Harga Promosi'] == $produk['parameter']['Harga Normal']){
    $harga = $produk['parameter']['Harga Normal'];
}else{
    $harga = $produk['parameter']['Harga Promosi'];
}
?>

<!-- Markup JSON-LD dibuat oleh Pemandu Markup Data Terstruktur Google. -->
<script type="application/ld+json">
    {
      "@context" : "http://schema.org",
      "@type" : "Product",
      "name" : "<?php echo $produk['parameter']['Nama'];?>",
      "image" : "<?php echo 'https://res.cloudinary.com/yepsindo/image/upload/w_380,h_500,c_fill,q_auto:good/'.$produk['parameter']['Foto'].'.jpg';?>",
      "description" : "<?php echo $produk['parameter']['Deskripsi'];?>",
      "brand" : {
      "@type" : "Brand",
      "name" : "<?php echo $produk['vendor'];?>",
      "logo" : "<?php echo $img;?>"
  },
  "offers" : {
  "@type" : "Offer",
  "price" : "<?php echo $harga;?>",
  "priceCurrency" : "IDR"
}
}
</script>

<?php
}
?>

</head>
<body>

 <div id="loading" class="loader" style="position: absolute; margin: auto; height: auto; width: 100%; z-index: 9999; margin-top: 300px; display: none" align="center">
    <div class="loader-inner line-scale">
       <div></div>
       <div></div>
       <div></div>
       <div></div>
       <div></div>
   </div>
</div>
<!-- Wrapper -->
<div id="wrapper">

    <!-- TOPBAR -->
    <div id="topbar" class="topbar-fullwidth mobile">
        <div class="container">

            <div class="topbar-dropdown hidden-xs">
                <div class="title">
                    <i class="fa fa-phone"></i>Ada Pertanyaan? Hubungi Kami 
                    <a href="#"><b>(021) 3972-2372</b></a>
                </div>
            </div>

            <div class="topbar-dropdown float-right mobile">

                <?php if($this->session->userdata('type') == ""){ ?>
                    <div class="title">
                        <i class="fa fa-user"></i>
                        <a href="<?php echo base_url('authss/login');?>">Masuk</a>
                    </div>
                    <?php 
                } 
                else {
                    ?>
                    <div class="title">
                        <i class="fa fa-user"></i>
                        <a href="<?php echo base_url('index.php/profil'); ?>">Profil</a>
                        &nbsp;&nbsp;|&nbsp;
                        <i class="fa fa-power-off"></i>
                        <a href="<?php echo base_url('index.php/authss/logout'); ?>">&nbsp; Keluar</a>
                    </div>
                <?php } ?> 
            </div>
        </div>
    </div>
    <!-- end: TOPBAR -->

    <!-- Header -->
    <header id="header" class="header-fullwidth">
        <div id="header-wrap">
            <div class="container">
                <!--Logo-->
                <div id="logo">
                    <a href="<?php echo base_url(); ?>" class="logo" data-dark-logo="<?php echo base_url('assets'); ?>/images/logo-dark.png">
                        <img src="<?php echo base_url('assets'); ?>/images/logo.png" alt="YEPS Indonesia Logo">
                    </a>
                </div>
                <!--End: Logo-->

                 <!--Top Search Form
                <div id="top-search">
                    <form id="form-search">
                        <input type="text" id="keyword" name="keyword" class="form-control" value="" placeholder="Cari vendor / jasa yang Anda butuhkan" required>
                    </form>
                </div>
                end: Top Search Form-->

                <!--Header Extras-->
                <div class="header-extras" style="line-height: 80px">
                    <ul>
                        <li>
                            <!--top search-->
                            <a id="top-search-trigger" href="#" class="toggle-item">
                                <i class="fa fa-search"></i>
                                <i class="fa fa-close"></i>
                            </a>
                            <!--end: top search-->
                        </li>
                        <li >
                            <!--shopping cart-->
                            <div id="shopping-cart" style="font-size: ">
                                <a href="<?php echo base_url('shop/cart'); ?>/">
                                    <?php 
                                    if($this->session->userdata('type') != ""){
                                        $cart_count = count($this->transaksi->get_cart($this->session->userdata('uuid')));
                                        echo "<span class='shopping-cart-items'>".$cart_count."</span>";
                                    }else{
                                        echo "<span class='shopping-cart-items'>0</span>";
                                    }
                                    ?>
                                    <i class="fa fa-shopping-cart"></i>
                                </a>
                            </div>
                            <!--end: shopping cart-->
                        </li>
                        <li class="hidden-xs">
                            <?php if($this->session->userdata('type') == ""){ ?>
                                <div class="topbar-dropdown float-right">

                                    <div class="title">
                                        <i class="fa fa-user"></i>
                                        <a href="#">Masuk</a>
                                    </div>

                                    <div class="topbar-form dropdown-invert" style="margin-top: 40px">
                                        <form method="post" id="form-login">
                                            <div class="form-group">
                                                <label class="sr-only">Username or Email</label>
                                                <input placeholder="Email" class="form-control" name="email" type="text">
                                            </div>
                                            <div class="form-group">
                                                <label class="sr-only">Password</label>
                                                <input type="password" class="form-control" name="password" placeholder="Password" required id="password-field">
                                                <span toggle="#password-field" class="field-icon fa fa-fw fa-eye toggle-password" style="padding-top: 5px"></span>
                                            </div>
                                            <div class="form-inline form-group" style="margin-top: -25px;">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox">
                                                        <small> Remember me</small> 
                                                    </label>
                                                </div>
                                                <div style="float: right; margin-top: -5px">
                                                    <a href="<?php echo base_url('authss/forgotpassword');?>/">Lupa Kata Sandi?</a>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-vensia btn-block" style="margin-top: -35px; margin-bottom: -20px">Masuk ke Vensia</button>
                                        </form>
                                        <center>
                                            <a href="<?php echo base_url('authss/register'); ?>/">Belum punya Akun? Daftar Sekarang</a>
                                        </center>
                                    </div>
                                </div>

                                <?php 
                            } 
                            else {
                                ?>
                                <!-- <li class="hidden-xs"> -->
                                    <div class="topbar-dropdown float-right">
                                        <div class="title">
                                            <i class="fa fa-user"></i>
                                            <a href="#"><?php echo $this->session->userdata('namaDepan')." ".$this->session->userdata('namaBelakang') ; ?></a>
                                        </div>
                                        <div class="dropdown-list">
                                            <a class="list-entry" href="<?php echo base_url('index.php/profil'); ?>">
                                                Akun
                                            </a>
                                            <a class="list-entry" href="<?php echo base_url('profil/riwayat'); ?>">
                                                Transaksi
                                            </a>
                                            <a class="list-entry" style="padding-top: 15px" href="<?php echo base_url('index.php/authss/logout'); ?>">
                                                <i class="fa fa-power-off"></i> Keluar
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <!--end: Header Extras-->

                    <!--Navigation Resposnive Trigger-->
                    <div id="mainMenu-trigger">
                        <button class="lines-button x"> <span class="lines"></span> </button>
                    </div>
                    <!--end: Navigation Resposnive Trigger-->

                    <!--Navigation-->
                    <div id="mainMenu" class="light">
                        <div class="container">
                             <form>
        <div class="inner-form">
          <div class="input-field second-wrap">
            <input id="search" type="text" placeholder="Enter Keywords?" style="z-index: 10000001;"/>
          </div>
          <div class="input-field third-wrap">
            <button class="btn-search" type="submit">
              <i class="fa fa-search" style="margin-top: -50px"></i>
            </button>
          </div>
        </div>
      </form>
                            <nav style="line-height: 80px">
                                <ul>
                                    <li><a href="<?php echo base_url(); ?>">Beranda</a></li>
                                    <!-- <li><a href="<?php echo base_url('portfolio'); ?>">Portfolio</a></li>
                                        <li><a href="<?php echo base_url('blog'); ?>">BLog</a></li> -->
                                        <li>
                                            <a href="<?php echo base_url('bantuan/contact_us'); ?>">Hubungi Kami</a>
                                        </li>
                                        <?php if($this->session->userdata('type') != "")
                                        { ?>
                                            <li class="dropdown"> 
                                                <a href="#" class="menu-vdr">
                                                    <i class="fas fa-warehouse"></i> Vendor
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <?php 
                                                    $validasi = $this->session->userdata('aktifVendor');
                                                    if ($validasi == 0) { ?>
                                                        <li>
                                                            <a href="http://localhost/beta15/vendors/daftar"><i class="fa fa-registered"></i>Daftar Vendor</a>
                                                        </li>
                                                    <?php }
                                                    else if ($validasi == 2) { ?>
                                                        <li>
                                                            <a href="#" onclick="alert1()">
                                                                <i class="fa fa-archive"></i>Dashboard Vendor
                                                            </a>
                                                        </li>
                                                    <?php }
                                                    else if ($validasi == 3) { ?>
                                                        <li>
                                                            <a href="#" onclick="alert2()">
                                                                <i class="fa fa-archive"></i>Dashboard Vendor
                                                            </a>
                                                        </li>
                                                    <?php }
                                                    else if ($validasi == 4) { ?>
                                                        <li>
                                                            <a href="#" onclick="alert3()">
                                                                <i class="fa fa-archive"></i>Dashboard Vendor
                                                            </a>
                                                        </li>
                                                    <?php }
                                                    else if ($validasi == 6) { ?>
                                                        <li>
                                                            <a href="#" onclick="alert4()">
                                                                <i class="fa fa-archive"></i>Dashboard Vendor
                                                            </a>
                                                        </li>
                                                    <?php }
                                                    else { ?>
                                                        <li>
                                                            <form method="post" action="#" id="login_form">
                                                                <input type="hidden" name="email" value="<?php echo $this->session->userdata('email');?>">                                                        
                                                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                            </form>
                                                            <a href="#" onclick="alert5()">
                                                                <i class="fa fa-archive"></i>Dashboard Vendor
                                                            </a> 
                                                        </li>
                                                    <?php } ?>

                                                </ul>
                                            </li> 
                                        <?php } ?>                               
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <!--end: Navigation-->
                    </div>
                </div>
            </header>
            <!-- end: Header -->



