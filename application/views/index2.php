<?php $this->load->view('header'); ?>
<style type="text/css">


</style>
<!-- Inspiro Slider -->
<div id="slider" class="inspiro-slider arrows-large arrows-creative dots-creative" data-height-xs="360" data-autoplay-timeout="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    <!-- Slide 1 -->
    <div class="slide" style="background-image:url('<?php echo base_url('assets'); ?>/homepages/shop-v3/images/b1.jpg');">
        <div class="container">
            <div class="slide-captions text-right">
                <!-- Captions -->
                <h2 class="text-large"">YEPS HADIR UNTUK<br/> MEMBANTU ACARA ANDA</h2>
                <a class="btn btn-light btn-outline" href="#kategori">Lihat Kategori</a>
                <!-- end: Captions -->
            </div>
        </div>
    </div>
    <!-- end: Slide 1 -->
    <!-- Slide 2 -->
    <div class="slide" style="background-image:url('<?php echo base_url('assets'); ?>/homepages/shop-v3/images/b2.jpg');">
        <div class="container">
            <div class="slide-captions">
                <!-- Captions -->
                <h2 class="text-large text-dark">DISKON SAMPAI<br /> DENGAN 50%</h2>
                <a class="btn btn-dark" href="<?php echo base_url('photography'); ?>">Lihat Koleksi</a>
                <!-- end: Captions -->
            </div>
        </div>
    </div>
    <!-- end: Slide 2 -->
</div>
<!--end: Inspiro Slider -->

<!-- SUMMER SALE -->
<section class="section-pattern p-t-30 p-b-30 text-center" style="background: url('<?php echo base_url('assets'); ?>/images/pattern/pattern22.png')">
    <div class="container clearfix">
            <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                <div class="promo-light image-right animated visible fadeInLeft" data-animation="slideInLeft">
                    <a href="<?php echo base_url('maintenance'); ?><?php echo base_url('maintenance'); ?>">
                        <img src="<?php echo base_url('assets'); ?>/images/pages/fitur.png" class="img-promo">
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                <div class="promo-light image-left animated visible fadeInRight" data-animation="slideInRight">
                    <a href="<?php echo base_url('maintenance'); ?><?php echo base_url('maintenance'); ?>">
                        <img src="<?php echo base_url('assets'); ?>/images/pages/fitur2.png" class="img-promo">
                    </a>
                </div>
            </div>
    </div>
</section>
<!-- end: SUMMER SALE -->

<!-- end: SHOP CATEGORIES -->
<section>
    <div class="container" id="kategori">
        <div class="text-center">
            <?php
            echo "asdfd";
            var_dump($data);
            ?>
            <div class="text-medium-light m-b-20">Temukan yang Cocok dengan Anda. <br />Cari berbagai <b><span class="text-rotator" data-rotate-effect="flipInX">Venue
                ,Photography
                ,Wardrobe
                ,Decoration
                ,Souvenir
                ,Catering
                ,Makeup
                ,Invitation
                ,Entertainment
                ,Transportation
                ,Wedding Cake
            ,Videography</span></b> di YEPS
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="hr-title hr-long center"><abbr>Browser our categories </abbr> </div>
        </div>
    </div>

    <div class="shop-category">
        <div class="row">
            <div class="col-md-3">
                <div class="shop-category-box">
                    <a href="<?php echo base_url('maintenance'); ?>"><img class="img-rounded" alt="" src="<?php echo base_url('assets'); ?>/images/shop/shop-category/a1.jpg">
                        <!-- <div class="shop-category-box-title text-center">
                            <h6>Venue</h6><small>64 Venue</small>
                        </div> -->
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="shop-category-box">
                    <a href="<?php echo base_url('maintenance'); ?>"><img class="img-rounded" alt="" src="<?php echo base_url('assets'); ?>/images/shop/shop-category/a2.jpg">
                        <!-- <div class="shop-category-box-title text-center">
                            <h6>Photography</h6><small>36 Photography</small>
                        </div> -->
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="shop-category-box">
                    <a href="<?php echo base_url('maintenance'); ?>"><img class="img-rounded" alt="" src="<?php echo base_url('assets'); ?>/images/shop/shop-category/a3.jpg">
                        <!-- <div class="shop-category-box-title text-center">
                            <h6>Wardrobe</h6><small>64 Wardrobe</small>
                        </div> -->
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="shop-category-box">
                    <a href="<?php echo base_url('maintenance'); ?>"><img class="img-rounded" alt="" src="<?php echo base_url('assets'); ?>/images/shop/shop-category/a4.jpg">
                        <!-- <div class="shop-category-box-title text-center">
                            <h6>Decoration</h6><small>80 Decoration</small>
                        </div> -->
                    </a>
                </div>
            </div>
        </div>
        <!-- End Row -->
        <div class="row">
            <div class="col-md-3">
                <div class="shop-category-box">
                    <a href="<?php echo base_url('maintenance'); ?>"><img class="img-rounded" alt="" src="<?php echo base_url('assets'); ?>/images/shop/shop-category/a5.jpg">
                        <!-- <div class="shop-category-box-title text-center">
                            <h6>Souvenir</h6><small>25 Souvenir</small>
                        </div> -->
                    </a>
                </div>
            </div> 
            <div class="col-md-3">
                <div class="shop-category-box">
                    <a href="<?php echo base_url('maintenance'); ?>"><img class="img-rounded" alt="" src="<?php echo base_url('assets'); ?>/images/shop/shop-category/a6.jpg">
                        <!-- <div class="shop-category-box-title text-center">
                            <h6>Catering</h6><small>36 Cathering</small>
                        </div> -->
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="shop-category-box">
                    <a href="<?php echo base_url('maintenance'); ?>"><img class="img-rounded" alt="" src="<?php echo base_url('assets'); ?>/images/shop/shop-category/a7.jpg">
                        <!-- div class="shop-category-box-title text-center">
                            <h6>Makeup</h6><small>25 Makeup</small>
                        </div> -->
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="shop-category-box">
                    <a href="<?php echo base_url('maintenance'); ?>"><img class="img-rounded" alt="" src="<?php echo base_url('assets'); ?>/images/shop/shop-category/a8.jpg">
                        <!-- <div class="shop-category-box-title text-center">
                            <h6>Invitation</h6><small>80 Invitation</small>
                        </div> -->
                    </a>
                </div>
            </div>

        </div>
        <!-- End Row -->
        <div class="row">
            <div class="col-md-3">
                <div class="shop-category-box">
                    <a href="<?php echo base_url('maintenance'); ?>"><img class="img-rounded" alt="" src="<?php echo base_url('assets'); ?>/images/shop/shop-category/a9.jpg">
                        <!-- <div class="shop-category-box-title text-center">
                            <h6>Entertainment</h6><small>64 Entertainment</small>
                        </div> -->
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="shop-category-box">
                     <a href="<?php echo base_url('maintenance'); ?>"><img class="img-rounded" alt="" src="<?php echo base_url('assets'); ?>/images/shop/shop-category/a10.jpg">
                        <!-- <div class="shop-category-box-title text-center">
                            <h6>Videography</h6><small>36 Videography</small>
                        </div> -->
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="shop-category-box">
                    <a href="<?php echo base_url('maintenance'); ?>"><img class="img-rounded" alt="" src="<?php echo base_url('assets'); ?>/images/shop/shop-category/a11.jpg">
                        <!-- <div class="shop-category-box-title text-center">
                            <h6>Transportation</h6><small>25 Transportation</small>
                        </div> -->
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="shop-category-box">
                    <a href="<?php echo base_url('maintenance'); ?>"><img class="img-rounded" alt="" src="<?php echo base_url('assets'); ?>/images/shop/shop-category/a12.jpg">
                        <!-- <div class="shop-category-box-title text-center">
                            <h6>Wedding Cake</h6><small>80 Wedding Cake</small>
                        </div> -->
                    </a>
                </div>
            </div>
        </div>
        <!-- End Row -->
        
    </div>
</div>
</section>
<!-- end: SHOP CATEGORIES -->

<!-- DELIVERY INFO -->
<section class="background-grey p-t-40 p-b-0">
    <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="icon-box medium fancy">
                <div class="icon" data-animation="pulse infinite"> <a href="#"><i class="fa fa-smile-o"></i></a> </div>
                <h3>Support 24/7</h3>
                <p>Kami siap melayani anda 24 jam setiap hari</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="icon-box medium fancy">
              <div class="icon" data-animation="pulse infinite"> <a href="#"><i class="fa fa-lock"></i></a> </div>
              <h3>Data Privacy</h3>
              <p>Sistem Kami menjamin data pelanggan agar tidak tersebar ke pihak lain.</p>
          </div>
      </div>
      <div class="col-md-4">
        <div class="icon-box medium fancy">
          <div class="icon" data-animation="pulse infinite"> <a href="#"><i class="fa fa-angellist"></i></a> </div>
          <h3>Friendly User</h3>
          <p>Sistem ini dibuat untuk memudahkan pelanggan untuk melakukan aktivitasnya</p>
      </div>
  </div>
</div>
</div>
</section>
<!-- end: DELIVERY INFO -->

<!-- SHOP WIDGET PRODUTCS -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="heading-fancy heading-line">
                    <h4>Top Rated</h4>
                </div>

                <!-- <div class="widget-shop">
                    <div class="product">
                        <div class="product-image">
                            <img alt="Shop product image!" src="<?php echo base_url('assets'); ?>/images/shop/products/vendor_gedung/9.jpg">
                        </div>
                        <div class="product-description">
                            <div class="product-category">Jakarta Pusat</div>
                            <div class="product-title">
                                <h3><a href="<?php echo base_url('venue/detail'); ?>" data-lightbox="ajax">Atria Hotel | ENDLESS LOVE</a></h3>
                            </div>
                            
                            <div class="product-price"><ins>Rp.328.888.000</ins>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="product-image">
                            <img alt="Shop product image!" src="<?php echo base_url('assets'); ?>/images/shop/products/vendor_photography/6.jpg">
                        </div>

                        <div class="product-description">
                            <div class="product-category">Jakarta Barat</div>
                            <div class="product-title">
                                <h3><a href="<?php echo base_url('photography/detail'); ?>" data-lightbox="ajax">JC ART House</a></h3>
                            </div>
                            <div class="product-price"><ins>Rp.5.000.000</ins>
                            </div>
                        </div>

                    </div>
                    <div class="product">
                        <div class="product-image">
                            <img alt="Shop product image!" src="<?php echo base_url('assets'); ?>/images/shop/products/vendor_gedung/7.jpg">
                        </div>

                        <div class="product-description">
                            <div class="product-category">Jakarta Pusat</div>
                            <div class="product-title">
                                <h3><a href="<?php echo base_url('venue/detail'); ?>" data-lightbox="ajax">Atria Hotel | TRUE LOVE</a></h3>
                            </div>
                            <div class="product-price"><ins>Rp.235.888.000</ins>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="col-md-3">
                <div class="heading-fancy heading-line">
                    <h4>On Sale</h4>
                </div>

                            <!-- <div class="widget-shop">
                                <div class="product">
                                    <div class="product-image">
                                        <img alt="Shop product image!" src="<?php echo base_url('assets'); ?>/images/shop/products/vendor_photography/1.jpg">
                                    </div>

                                    <div class="product-description">
                                        <div class="product-category">Tangerang</div>
                                        <div class="product-title">
                                            <h3><a href="<?php echo base_url('photography/detail'); ?>" data-lightbox="ajax">JC ART House</a></h3>
                                        </div>
                                        <div class="product-price"><ins>Rp.5.000.000</ins>
                                        </div>
                                    </div>
                                </div>
                                <div class="product">
                                    <div class="product-image">
                                     <img alt="Shop product image!" src="<?php echo base_url('assets'); ?>/images/shop/products/vendor_gedung/9.jpg">
                                 </div>

                                 <div class="product-description">
                                    <div class="product-category">Bekasi</div>
                                    <div class="product-title">
                                        <h3><a href="<?php echo base_url('decoration/detail'); ?>" data-lightbox="ajax">Evlin | Paket Standart</a></h3>
                                    </div>
                                    <div class="product-price"><ins>Rp.45.000.000</ins>
                                    </div>
                                </div>
                            </div>
                            <div class="product">
                                <div class="product-image">
                                 <img alt="Shop product image!" src="<?php echo base_url('assets'); ?>/images/shop/products/vendor_gedung/3.jpg">
                             </div>

                             <div class="product-description">
                                <div class="product-category">Bogor</div>
                                <div class="product-title">
                                    <h3><a href="<?php echo base_url('wardrobe/detail'); ?>"  data-lightbox="ajax">Sanggar Tien Santoso</a></h3>
                                </div>
                                <div class="product-price"><ins>Rp.8.500.000</ins>
                                </div>
                            </div>

                        </div>
                    </div> -->
                </div>
                <div class="col-md-3">
                    <div class="heading-fancy heading-line">
                        <h4>Recommended</h4>
                    </div>

                    <!-- <div class="widget-shop">
                        <div class="product">
                            <div class="product-image">
                                <img alt="Shop product image!" src="<?php echo base_url('assets'); ?>/images/shop/products/vendor_gedung/1.jpg">
                            </div>
                            <div class="product-description">
                                <div class="product-category">Jakarta Utara</div>
                                <div class="product-title">
                                    <h3><a href="<?php echo base_url('decoration/detail'); ?>" data-lightbox="ajax">Sanggar Minang Chandra Sutan Sati</a></h3>
                                </div>
                                <div class="product-price"><ins>Rp.15.000.000</ins>
                                </div>
                            </div>
                        </div>
                        <div class="product">
                            <div class="product-image">
                             <img alt="Shop product image!" src="<?php echo base_url('assets'); ?>/images/shop/products/vendor_gedung/2.jpg">
                         </div>

                         <div class="product-description">
                            <div class="product-category">Jakarta Pusat</div>
                            <div class="product-title">
                                <h3><a href="<?php echo base_url('cathering/detail'); ?>" data-lightbox="ajax">Vista Devy</a></h3>
                            </div>
                            <div class="product-price"><ins>Rp.108.000.000</ins>
                            </div>
                        </div>

                    </div>
                    <div class="product">
                        <div class="product-image">
                            <img alt="Shop product image!" src="<?php echo base_url('assets'); ?>/images/shop/products/vendor_gedung/5.jpg">
                        </div>

                        <div class="product-description">
                            <div class="product-category">Depok</div>
                            <div class="product-title">
                                <h3><a href="<?php echo base_url('venue/detail'); ?>" data-lightbox="ajax">Atria Hotel | ETERNAL LOVE</a></h3>
                            </div>
                            <div class="product-price"><ins>Rp.168.888.000</ins>
                            </div>
                        </div>

                    </div>

                </div> -->
            </div>
            <div class="col-md-3">
                <div class="heading-fancy heading-line">
                    <h4>Popular</h4>
                </div>

                <!-- <div class="widget-shop">
                    <div class="product">
                        <div class="product-image">
                            <img alt="Shop product image!" src="<?php echo base_url('assets'); ?>/images/shop/products/vendor_gedung/4.jpg">
                        </div>
                        <div class="product-description">
                            <div class="product-category">Depok</div>
                            <div class="product-title">
                                <h3><a href="<?php echo base_url('venue/detail'); ?>" data-lightbox="ajax">Atria Hotel | ENDLESS LOVE</a></h3>
                            </div>
                            <div class="product-price"><ins>Rp.300.000.000</ins>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="product-image">
                            <img alt="Shop product image!" src="<?php echo base_url('assets'); ?>/images/shop/products/vendor_gedung/3.jpg">
                        </div>

                        <div class="product-description">
                            <div class="product-category">Bekasi</div>
                            <div class="product-title">
                                <h3><a href="<?php echo base_url('venue/detail'); ?>" data-lightbox="ajax">Swiss BelHotel Pondok Indah</a></h3>
                            </div>
                            <div class="product-price"><ins>Rp.300.000.000</ins>
                            </div>
                        </div>

                    </div>
                    <div class="product">
                        <div class="product-image">
                            <img alt="Shop product image!" src="<?php echo base_url('assets'); ?>/images/shop/products/vendor_gedung/8.jpg">
                        </div>

                        <div class="product-description">
                            <div class="product-category">Jakarta Barat</div>
                            <div class="product-title">
                                <h3><a href="<?php echo base_url('cathering/detail'); ?>" data-lightbox="ajax">VESSA CATERING</a></h3>
                            </div>
                            <div class="product-price"><ins>Rp.68.900.000</ins>
                            </div>

                        </div>

                    </div>

                </div> -->
            </div>
        </div>
    </div>
</section>
<!-- end: SHOP WIDGET PRODUTCS -->
<?php $this->load->view('footer'); ?>



