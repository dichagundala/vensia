<?php $this->load->view('header'); ?>

<section class="section-pattern p-t-60 p-b-30 text-center" style="background: url(<?php echo base_url('assets'); ?>/images/pages/bg.jpg)">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 class="text-medium" style="color: white">YEPS Career</h3>
            </div>
        </div>
    </div>
</section>

<section class="background-grey">
  <div class="container">
    <!-- Career -->
    <div id="portfolio" class="grid-layout portfolio-4-columns" data-margin="20">
        <!-- Konten Karir -->
        <div class="portfolio-item light-bg no-overlay img-zoom pf-illustrations pf-uielements pf-media">
            <div class="portfolio-item-wrap shadow-sm">
                <div class="portfolio-image">
                    <a href="#"><img src="<?php echo base_url('assets/images/karir/1.jpg'); ?>" alt=""></a>
                </div>
                <div class="portfolio-description">
                    <a href="<?php echo base_url('career/detail'); ?>">
                        <h3>Digital Marketing</h3>
                        <span>Marketing</span>
                    </a>
                </div>
            </div>
        </div>   
        <!-- end: Konten Karir -->                 
    </div>
    <!-- end Career -->
</div>
</section>

<?php $this->load->view('footer'); ?>
