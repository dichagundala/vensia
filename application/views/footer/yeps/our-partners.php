 <?php $this->load->view('header'); ?>

 <!-- Inspiro Slider -->
 <div id="slider" class="inspiro-slider" data-height-xs="360">
    <!-- Slide 1 -->
    <div class="slide" style="background-image:url('<?php echo base_url('assets'); ?>/homepages/shop-v3/images/b1.jpg');">
        <div class="container">
            <div class="slide-captions text-right text-dark">
                
                <h1 data-caption-animation="rotateInUpRight">Create with Ease</h1>
                <h3 data-caption-animation="rotateInUpRight">And be proud of your own work!</h3>
                
            </div>
        </div>
    </div>
    <!-- end: Slide 1 -->
</div>
<!--end: Inspiro Slider -->

<!-- Content -->
<section>
    <div class="container">
        <div class="heading heading-center">
            <span class="lead">Our awesome partners we've had the pleasure to work with! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus. </span>
        </div>
        <ul class="grid grid-4-columns">
            <li>
                <a data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." title="" data-placement="top" data-toggle="popover" data-container="body" data-original-title="Popover title" data-trigger="hover">
                    <img src="<?php echo base_url('assets'); ?>/images/partners/1.png" alt=""></a>
                </li>
                <li>
                    <a data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." title="" data-placement="top" data-toggle="popover" data-container="body" data-original-title="Popover title" data-trigger="hover"><img src="<?php echo base_url('assets'); ?>/images/partners/2.png" alt="">
                    </a>
                </li>
                <li>
                    <a data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." title="" data-placement="top" data-toggle="popover" data-container="body" data-original-title="Popover title" data-trigger="hover"><img src="<?php echo base_url('assets'); ?>/images/partners/3.png" alt="">
                    </a>
                </li>
                <li>
                    <a data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." title="" data-placement="top" data-toggle="popover" data-container="body" data-original-title="Popover title" data-trigger="hover"><img src="<?php echo base_url('assets'); ?>/images/partners/4.png" alt="">
                    </a>
                </li>
                <li>
                    <a data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." title="" data-placement="top" data-toggle="popover" data-container="body" data-original-title="Popover title" data-trigger="hover"><img src="<?php echo base_url('assets'); ?>/images/partners/5.png" alt="">
                    </a>
                </li>
                <li>
                    <a data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." title="" data-placement="top" data-toggle="popover" data-container="body" data-original-title="Popover title" data-trigger="hover"><img src="<?php echo base_url('assets'); ?>/images/partners/6.png" alt="">
                    </a>
                </li>
                <li>
                    <a data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." title="" data-placement="top" data-toggle="popover" data-container="body" data-original-title="Popover title" data-trigger="hover"><img src="<?php echo base_url('assets'); ?>/images/partners/7.png" alt="">
                    </a>
                </li>
                <li>
                    <a data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." title="" data-placement="top" data-toggle="popover" data-container="body" data-original-title="Popover title" data-trigger="hover"><img src="<?php echo base_url('assets'); ?>/images/partners/8.png" alt="">
                    </a>
                </li>
                <li>
                    <a data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." title="" data-placement="top" data-toggle="popover" data-container="body" data-original-title="Popover title" data-trigger="hover"><img src="<?php echo base_url('assets'); ?>/images/partners/9.png" alt="">
                    </a>
                </li>
                <li>
                    <a data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." title="" data-placement="top" data-toggle="popover" data-container="body" data-original-title="Popover title" data-trigger="hover"><img src="<?php echo base_url('assets'); ?>/images/partners/10.png" alt="">
                    </a>
                </li>
                <li>
                    <a data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." title="" data-placement="top" data-toggle="popover" data-container="body" data-original-title="Popover title" data-trigger="hover"><img src="<?php echo base_url('assets'); ?>/images/partners/3.png" alt="">
                    </a>
                </li>
                <li>
                    <a data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." title="" data-placement="top" data-toggle="popover" data-container="body" data-original-title="Popover title" data-trigger="hover"><img src="<?php echo base_url('assets'); ?>/images/partners/5.png" alt="">
                    </a>
                </li>
            </ul>
        </div>
    </section>



    <section class="background-grey">
       <div class="container">
          <div class="text-center">
             <h2>Aliquam enim enim, pharetra in sodales</h2>
             <p class="lead">
                Aliquam enim enim, pharetra in sodales at, interdum sit amet dui. Nullam vulputate euis od urna non pharetra. Phasellus bland matt is ipsum, ac laoreet lorem lacinia et. interum sit amet dui.
            </p>
            <a class="btn btn-default icon-left" href="#"><span>Apply now!</span></a>
        </div>
    </div>
</section>

<!-- end: Content -->


<?php $this->load->view('footer'); ?>
