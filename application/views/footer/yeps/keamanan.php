<?php $this->load->view('header'); ?>

<!-- PHISHING -->
<section>
    <div class="container">
        <div class="text-center m-b-60">
            <h1 class="text-medium">KEAMANAN</h1>
            <!-- <p>Lorem ipsum dolor sit amet, consecte adipiscing elit. Suspendisse condimentum porttitor cursumus.</p> -->
        </div>
        <div class="row">
            <div class="col-md-6 m-b-30" style="height:556px">
                <div class="image-absolute" data-animation="fadeInLeft" data-animation-delay="800">
                    <img src="<?php echo base_url('assets');?> /images/keamanan/app-iphone-dark.png" alt="">
                </div>
                <div class="image-absolute" data-animation="fadeInLeft" data-animation-delay="400">
                    <img src="<?php echo base_url('assets'); ?>/images/keamanan/app-iphone-white.png" alt="">
                </div>
            </div>

            <div class="col-md-6" data-animation="fadeInUp" data-animation-delay="1000">
                <div class="m-b-40">
                    <h3>Phishing  (Serupa Tapi Tak Sama)</h3>
                    <p>Phishing merupakan bentuk penipuan online yang bertujuan untuk mendapatkan informasi pribadi pengguna seperti username, password, email dan sebagainya melalui website yang tampilannya menyerupai website yepsindonesia.com. 
                    </p>
                    <p>Berikut beberapa cara untuk mencegah phishing;</p>
                    <ol>
                        <li class="m-b-10">Pastikan alamat yang anda akses menggunakan domain yepsindonesia.com dan diawali <b>http://</b> , contoh : <b>https://yepsindonesia.com</b>. Apabila menemui situs yang tampilannya sama seperti yeps tapi berbeda domain atau alamat maka segera tutup situs tersebut.</li>
                        <li class="m-b-10">Lindungi akun dengan tidak memberi informasi username, password,  kode otp dan data lain yang bersifat pribadi kepada siapapapun.</li>
                        <li class="m-b-10">Apabila Anda menerima link/akun external yang dikirim melalu pesan singkat atau personal message.</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- PHISHING -->

<!-- SCAMMING -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-7" data-animation="fadeInUp" data-animation-delay="400">
                <div class="m-b-40">
                    <h3>Scamming (Pihak yang Mengaku Sebagai Pihak dari YEPS)</h3>
                    <p>Scamming merupakan modus penipuan yang menyamar sebagai pihak tertentu yang bertujuan untuk mendapatkan data / keuntungann dari pihak lain. Modus ini juga sering dilakukan di media on line.
                    </p>
                    <p>Berikut beberapa cara untuk mencegah scamming;</p>
                    <ol>
                        <li class="m-b-10">Abaikan apabila Anda mendapat telpon, sms, pm ataupun email dari pihak yang mengatasnamakan  YEPS dan meminta transfer sejumlah uang kepada pihak lain selain nomor rekenig resmi YEPS.</li>
                        <li class="m-b-10">Waspadai informasi dan promo palsu dari luar pihak YEPS, untuk informasi upda te dan promo hanya ada di situs resmi yepsindonesia.com</li>
                        <li class="m-b-10">Abaikan ketika ada pihak lain yang meminta informasi pribadi dan rahasia Anda seperti password, kode OTP dan data pribadi lainnya.</li>
                        <li>Data berupa Nomor Rekening, bukti bayar atau bukti transfer hanya akan diminta oleh pihak resmi YEPS.</li>
                    </ol>
                    <p>Apabila Anda mengalami hal tersebut dan ragu untuk mengambil tindakan, segera hubungi customer support kami di (021) 3972-2372 atau <a href="mailto:support@yepsindonesia.com">support@yepsindonesia.com</a></p>
                </div>
            </div>

            <div class="col-md-5 m-b-30" style="height:556px">
                <div class="image-absolute" data-animation="fadeInLeft" data-animation-delay="800">
                    <img src="<?php echo base_url('assets');?> /images/keamanan/app-iphone-dark.png" alt="">
                </div>
                <div class="image-absolute" data-animation="fadeInLeft" data-animation-delay="1000">
                    <img src="<?php echo base_url('assets'); ?>/images/keamanan/app-iphone-white.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- SCAMMING -->

<div class="seperator"><i class="fa fa-chevron-down"></i></div>

<!-- Testimonials -->
<section>
    <div class="container">
        <div class="row">
            <div class="text-center m-b-60">
                <h3>Panduan Keamanan</h3>
            </div>
            <div class="carousel testimonial testimonial-border" data-margin="20" data-arrows="false" data-items="3" data-equalize-item=".testimonial-item">

                <!-- Testimonials item -->
                <div class="testimonial-item shadows">
                    <center>
                        <p>
                            <span class="dropcap dropcap-colored" style="float: none !important;"> 1 </span>
                        </p>
                    </center>
                    <p style="font-size: 14px; line-height: 22px;">Periksa & Pastikan alamat yang anda akses menggunakan domain yepsindonesia.com dan diawali http:// , contoh : https://yepsindonesia.com</p>
                </div>
                <!-- end: Testimonials item-->

                <!-- Testimonials item -->
                <div class="testimonial-item shadows">
                    <center>
                        <p>
                            <span class="dropcap dropcap-colored" style="float: none !important;"> 2 </span>
                        </p>
                    </center>
                    <p style="font-size: 14px; line-height: 22px;">Sistem YEPS Indonesia meminta para pengguna untuk memasukkan nama akun dan kata sandi (username and password) ketika login, perubahan data rekening, transaksi via Mitrans</p>
                </div>
                <!-- end: Testimonials item-->

                <!-- Testimonials item -->
                <div class="testimonial-item shadows">
                    <center>
                        <p>
                            <span class="dropcap dropcap-colored" style="float: none !important;"> 3 </span>
                        </p>
                    </center>
                    <p style="font-size: 14px; line-height: 22px;">Pihak YEPS Indonesia tidak pernah meminta data pribadi, nama akun (username), kode OTP, dan kata sandi (password) melalui email, telepon, ataupun pesan pribadi. Jika ada pihak yang mengatasnamakan Yepsindonesia dan meminta data-data tersebut mohon abaikan.</p>
                </div>
                <!-- end: Testimonials item-->

                <!-- Testimonials item -->
                <div class="testimonial-item shadows">
                    <center>
                        <p>
                            <span class="dropcap dropcap-colored" style="float: none !important;"> 4 </span>
                        </p>
                    </center>
                    <p style="font-size: 14px; line-height: 22px;">Sistem transaksi di YEPS Indonesia hanya melalui Mitrans (Payment Gateway) dan Transfer langsung ke rekening Atas Nama YEPS Indonesia.</p>
                </div>
                <!-- end: Testimonials item-->

                <!-- Testimonials item -->
                <div class="testimonial-item shadows">
                    <center>
                        <p>
                            <span class="dropcap dropcap-colored" style="float: none !important;"> 5 </span>
                        </p>
                    </center>
                    <p style="font-size: 14px; line-height: 22px;">Abaikan jika menerima telepon ancaman yang mengaku dari vendor , bahkan yang mengatasnamakan YEPS Indonesia dan meminta untuk transfer sejumlah uang. <b>YEPS Indonesia tidak pernah meminta biaya tambahan di luar tagihan yang tertera dalam transaksi.</b></p>
                </div>
                <!-- end: Testimonials item-->

                <!-- Testimonials item -->
                <div class="testimonial-item shadows">
                    <center>
                        <p>
                            <span class="dropcap dropcap-colored" style="float: none !important;"> 6 </span>
                        </p>
                    </center>
                    <p style="font-size: 14px; line-height: 22px;">Anda harus bersikap waspada terhadap informasi-informasi via pesan pribadi. YEPS Indonesia tidak akan menjamin keamanan transaksi anda jika mengakses di luar sistem https://yepsindonesia.com.
                        <!-- end: Testimonials item-->

                    </div>
                </div>
            </div>
        </section>
        <!-- end: Testimonials -->


        <!-- Nnti Pindah ke footer -->
        <script type="text/javascript">
          var tpj=jQuery;
          tpj.noConflict();
          var revapi6;
          tpj(document).ready(function() {
            if(tpj("#rev_slider_6_1").revolution == undefined){
               revslider_showDoubleJqueryError("#rev_slider_6_1");
           }else{
               revapi6 = tpj("#rev_slider_6_1").show().revolution({
                  sliderType:"standard",
                  jsFileLocation:"js/plugins/revolution/js/",
                  sliderLayout:"fullwidth",
                  dottedOverlay:"none",
                  delay:9000,
                  navigation: {
                     keyboardNavigation:"off",
                     keyboard_direction: "horizontal",
                     mouseScrollNavigation:"off",
                     onHoverStop:"on",
                     arrows: {
                        style:"hades",
                        enable:true,
                        hide_onmobile:true,
                        hide_under:0,
                        hide_onleave:true,
                        hide_delay:200,
                        hide_delay_mobile:1200,
                        tmp:'<div class="tp-arr-allwrapper">    <div class="tp-arr-imgholder"></div></div>',
                        left: {
                           h_align:"left",
                           v_align:"center",
                           h_offset:40,
                           v_offset:0
                       },
                       right: {
                           h_align:"right",
                           v_align:"center",
                           h_offset:40,
                           v_offset:0
                       }
                   }
               },
               responsiveLevels:[1240,1024,778,480],
               visibilityLevels:[1240,1024,778,480],
               gridwidth:[1170,1024,778,480],
               gridheight:[700,768,960,720],
               lazyType:"none",
               shadow:0,
               spinner:"spinner2",
               stopLoop:"off",
               stopAfterLoops:-1,
               stopAtSlide:-1,
               shuffle:"off",
               autoHeight:"off",
               disableProgressBar:"on",
               hideThumbsOnMobile:"on",
               hideSliderAtLimit:0,
               hideCaptionAtLimit:481,
               hideAllCaptionAtLilmit:0,
               debugMode:false,
               fallbacks: {
                 simplifyAll:"off",
                 nextSlideOnWindowFocus:"off",
                 disableFocusListener:false,
             }
         });
           }
       });  /*ready*/
   </script>
   <?php $this->load->view('footer'); ?>