<?php $this->load->view('header'); ?>

<section class="background-grey">
  <div class="container">
    <div class="card-group card" style="margin-top: -50px">
        <div class="col-md-4">
            <div class="card-body">
                <a href="#"><img style="height: 200px" src="<?php echo base_url('assets/images/karir/1.jpg'); ?>" alt=""></a>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card-body">
                <h4 style="text-transform: uppercase;">Digital Marketing</h4>
                <hr>
                <p style="line-height: 15px !important">Divisi : Marketing</p>
                <p style="line-height: 15px !important; color: #1375d1">IDR 4.000.000 - 8.000.000</p>
                <small class="text-muted"><i class="fa fa-clock-o"></i> Exp: 20 Maret 2019</small>
                <hr>
                <span style="color: #c2c2c5">Type :</span> Full Time &nbsp;&nbsp;&nbsp; <span style="color: #c2c2c5">Experience :</span> Less than 1 year
            </div>
        </div>
    </div>

    <div class="card m-t-20">
        <div class="card-header" style="background-color: #ffffff">
            <h4> Job description & requirements </h4>
        </div>
        <div class="card-body">
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae libero lacinia, gravida purus ac, venenatis leo. Phasellus id tincidunt massa. Sed elit eros, auctor sed lacus sit amet, pulvinar dapibus justo. Nulla a urna id purus faucibus elementum. Nulla facilisi. Ut lobortis eget eros in condimentum. Quisque sem augue, consequat aliquet sapien in, imperdiet congue arcu. Praesent gravida, justo at efficitur pellentesque, lectus ligula tristique lacus, nec commodo nisl risus eget nisl.

                Integer varius pretium scelerisque. In bibendum vulputate accumsan. Duis commodo neque at nibh dictum rutrum ac a mi. Praesent suscipit eu lorem fermentum suscipit. In ornare id ex et tincidunt. Mauris malesuada vitae lectus et iaculis. Etiam consequat feugiat velit vitae egestas. Nunc ut dui massa. Aliquam nec fermentum purus, eu consectetur lorem. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum in pretium purus, ut dictum erat. Cras ac fringilla metus, vel fermentum mauris. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed in vulputate urna.

                Praesent placerat massa diam, at vestibulum quam sollicitudin ac. Quisque ligula urna, porttitor at nisl a, lobortis commodo sem. Etiam volutpat metus ac dolor tincidunt, ultricies luctus mauris imperdiet. Fusce id elit justo. Nulla sed lacus vitae ante dapibus sodales. Integer ac massa at tellus efficitur mollis. Nulla rutrum malesuada laoreet. Donec consectetur diam vitae est tristique, et posuere massa suscipit. Suspendisse ac turpis sit amet justo iaculis porta. Maecenas at orci magna. Duis condimentum, lacus vitae euismod lobortis, eros orci imperdiet massa, a suscipit dui arcu feugiat velit. Praesent varius ornare laoreet.

                Fusce volutpat sollicitudin mattis. Mauris iaculis eros non feugiat venenatis. Fusce vitae orci sed nisl condimentum dignissim in vitae quam. Ut nec semper nibh. Aenean arcu leo, maximus non ultricies gravida, tempor non tellus. Vestibulum cursus lorem a tellus bibendum, vel pellentesque nisi luctus. Sed rhoncus sapien at sapien ornare, vitae mattis augue finibus.

                Proin luctus nisi purus, in pulvinar felis maximus sit amet. Vivamus pretium lacus nec felis scelerisque, ac facilisis diam blandit. Aliquam erat volutpat. Cras porta magna et lacus interdum fermentum. Vivamus nec viverra eros. Fusce rutrum massa massa, nec gravida leo malesuada id. Donec efficitur nisl eget luctus convallis. 
            </p>
            <hr>
            <span style="color: #c2c2c5">Send your email to :</span> <span style="font-weight: bold;">putriamalia@yepsindonesia.com</span> 
        </div>
    </div>
    
</div>
</section>

<?php $this->load->view('footer'); ?>
