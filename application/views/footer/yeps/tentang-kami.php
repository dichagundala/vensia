<?php $this->load->view('header'); ?>

<!-- Inspiro Slider -->
<div id="slider" class="inspiro-slider arrows-large arrows-creative dots-creative" data-height-xs="100">
    <div class="slide kenburns" style="background-image:url('<?php echo base_url('assets'); ?>/images/pages/slider-other/tentang-kami-1.jpg');">   
    </div>
</div>
<!--end: Inspiro Slider -->

<section>
 <div class="container">
  <div class="row text-center">
    <div>
        <img src="<?php echo base_url('assets'); ?>/images/pages/icon/icon-4.png" alt="YEPS INDONESIA" style="width: 40%">
    </div>
    <div class="p-40">
        <h4>YEPS (Your Event Partner Solution) Indonesia adalah perusahaan digital teknologi<br>
        yang memberikan solusi untuk pelaksanaan event event di Indonesia.</h4>
    </div>

</div>
</div>
</section>

<section style="background-color: #f0f4f7">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1 style="font-weight: bold">We Are the One</h1>
                <p style="font-size: 18px">YEPS Indonesia merupakan marketplace event manajemen pertama yang 
                    memberikan solusi atas permasalahan klien dalam pelaksanaan suatu event 
                    dalam satu platform yang menyediakan vendor vendor kebutuhan pelaksanaan 
                    event mulai dari UKM hingga korporasi, dengan layanan event manajemen 
                    yang berperan aktif untuk memastikan event klien berjalan efektif, 
                efisien dan optimal.</p>
            </div>
            <div class="col-md-4">
             <img src="<?php echo base_url('assets'); ?>/images/pages/icon/icon-5.png" alt="YEPS INDONESIA" style="width: 60%; margin: auto; padding: 30px 0px 0px 10px">
         </div>
     </div>
 </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
             <img src="<?php echo base_url('assets'); ?>/images/pages/icon/icon-6.png" alt="YEPS INDONESIA" style="width: 60%; margin: auto; padding-top: 170px">
         </div>
         <div class="col-md-8">
            <h1 style="font-weight: bold">Why YEPS? Why Not!</h1>
            <ul style="font-size: 18px">
                <li class="m-b-10">YEPS memudahkan klien mencari informasi vendor kebutuhan event dengan harga 
                yang transparan dan fasilitas yang diberikan </li>
                <li class="m-b-10">YEPS mengoptimalkan waktu klien yang terbuang untuk perencanaan dan persiapan 
                event</li>
                <li class="m-b-10">YEPS menghemat biaya klien dalam seluruh proses pelaksanaan event dengan layanan 
                YEPS event manajemen </li>
                <li class="m-b-10">YEPS memberikan layanan pembayaran dengan 30% pembayaran untuk memulai 
                event impian klien </li>
                <li class="m-b-10">YEPS memastikan setiap proses pelaksanaan event klien berjalan optimal dengan 
                dukungan legalitas dan layanan klien </li>
                <li class="m-b-10">YEPS memberikan laporan akhir event kepada klien dan klien dapat memberikan 
                ulasan </li>
                <li class="m-b-10">YEPS membuat klien lebih santai dan nyaman saat melaksanakan event </li>
            </ul>
        </div>
    </div>
    <div class="row">
     <div class="col-md-8">
        <ul style="font-size: 18px; margin-top: 50px">
            <li class="m-b-10">Promosi</li>
            <li class="m-b-10">Memberikan peluang pelanggan yang baru untuk bisnis vendor </li>
            <li class="m-b-10">Meningkatkan pendapatan dan kemajuan bisnis vendor </li>
        </ul>
    </div>
    <div class="col-md-4">
       <img src="<?php echo base_url('assets'); ?>/images/pages/icon/icon-7.png" alt="YEPS INDONESIA" style="width: 60%; margin: auto; padding-top: 20px">
   </div>
</div>
</div>
</section>

<section style="background-color: #b6d0fb">
    <div class="container">
        <div class="row">
            <h4 class="text-center p-l-40 p-r-40">Melaksanakan event bersama YEPS sebagai partner event yang akan memberikan
            solusi untuk kelangsungan event anda menjadi sangat mudah dan juga banyak keuntungannya</h4>
        </div>
    </div>
</section>


<section class="box-fancy center section-fullwidth p-b-0">
    <div class="container">
        <h1 style="font-weight: bold; text-align: center; margin-bottom: 60px">Apa Saja Fitur di YEPS</h1>
        <div class="row">
            <center>
               <div class="col-md-4">
                <img src="<?php echo base_url('assets'); ?>/images/pages/icon/icon-8.png" alt="YEPS INDONESIA" style="width: 50%; margin-bottom: 20px">
                <span>
                    <ul class="list-icon">
                        <li class="m-b-0">Kategori vendor</li>
                        <li class="m-b-0">Detail fasilitas dan harga vendor</li>
                        <li class="m-b-0">Informasi profil vendor dan portofolio</li>
                        <li class="m-b-0">Review dan rating</li>
                    </ul>
                </span>
            </div>

            <div class="col-md-4">
                <img src="<?php echo base_url('assets'); ?>/images/pages/icon/icon-9.png" alt="YEPS INDONESIA" style="width: 50%; margin-bottom: 20px">
                <span>
                    <ul class="list-icon">
                        <li class="m-b-0">Layanan pembayaran</li>
                        <li class="m-b-0">Metode pembayaran</li>
                        <li class="m-b-0">Pengembalian dana</li>
                        <li class="m-b-0">Sisa waktu pembayaran</li>
                        <li class="m-b-0">Jangka waktu pembaran & Cicilan</li>
                    </ul>
                </span>
            </div>

            <div class="col-md-4">
                <img src="<?php echo base_url('assets'); ?>/images/pages/icon/icon-10.png" alt="YEPS INDONESIA" style="width: 50%; margin-bottom: 20px">
                <span>
                    <ul class="list-icon">
                        <li class="m-b-0">Pusat Informasi</li>
                        <li class="m-b-0">Pelayanan pelanggan</li>
                    </ul>
                </span>
            </div>
        </center>
    </div>
    <div class="row" style="z-index: 1000">
        <center>
           <div class="col-md-4">
            <img src="<?php echo base_url('assets'); ?>/images/pages/icon/icon-11.png" alt="YEPS INDONESIA" style="width: 50%; margin-bottom: 20px">
            <span>
                <ul class="list-icon">
                    <li class="m-b-0">Panduan dan pusat bantuan </li>
                    <li class="m-b-0">Syarat dan ketentuan </li>
                    <li class="m-b-0">Cara pemesanan dan transaksi </li>
                    <li class="m-b-0">FAQ</li>
                </ul>
            </span>
        </div>

        <div class="col-md-4">
            <img src="<?php echo base_url('assets'); ?>/images/pages/icon/icon-12.png" alt="YEPS INDONESIA" style="width: 50%; margin-bottom: 20px">
            <span>
                <ul class="list-icon">
                    <li class="m-b-0">Manajemen acara dan kontrol</li>
                    <li class="m-b-0">Konfirmasi booking order juga ada dari masing vendor</li>
                </ul>
            </span>
        </div>
    </center>
</div>
</div>
</section>
<div class="seperator">
    <i class="fas fa-layer-group"></i>
</div>
<section class="section-fullwidth p-b-0" style="margin-top: -20px">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-4">
            <h2 style="font-weight: bold; margin-bottom: 40px">Kategori Vendor di YEPS</h2>
            <img src="<?php echo base_url('assets'); ?>/images/pages/picture/cate.jpg" alt="YEPS INDONESIA" style="width: 90%; margin-bottom: 20px">
        </div>
        <div class="col-md-7">
            <h2 style="font-weight: bold; margin-bottom: 40px">Event Bersama YEPS</h2>
            <img src="<?php echo base_url('assets'); ?>/images/pages/picture/eventwithyeps.jpg" alt="YEPS INDONESIA" style="width: 90%; margin-bottom: 20px">
        </div>
    </div>
</section>

<section style="background-color: #f0f4f7">
	<div class="container">
		<div class="heading heading-center">
			<h1 style="font-weight: bold; text-align: center;">Our Team</h1>
			<!-- <span class="lead">Create amaLorem ipsum dolor sit amet, consectetur adipiscing elit.</span> -->
		</div>
		
        <div class="carousel team-members team-members-shadow" data-arrows="false" data-margin="20" data-items="4" style="margin-top: -30px">

            <div class="team-member">
                <div class="team-image">
                    <img src="<?php echo base_url('assets'); ?>/images/team/redho.jpg">
                </div>
                <div class="team-desc">
                    <h3>Redho Octaviano ZP</h3>
                    <span>Direktur Utama</span>
                </div>
            </div>

            <div class="team-member">
                <div class="team-image">
                    <img src="<?php echo base_url('assets'); ?>/images/team/rama.jpg">
                </div>
                <div class="team-desc">
                    <h3>Rama Perdana</h3>
                    <span>Direktur Keuangan dan Strategi</span>
                </div>
            </div>

            <div class="team-member">
                <div class="team-image">
                    <img src="<?php echo base_url('assets'); ?>/images/team/gandhi.jpg">
                </div>
                <div class="team-desc">
                    <h3>Gandhi Wicaksono</h3>
                    <span>Direktur Relasi Bisnis</span>
                </div>
            </div>

            <div class="team-member">
                <div class="team-image">
                    <img src="<?php echo base_url('assets'); ?>/images/team/putri.jpg">
                </div>
                <div class="team-desc">
                    <h3>Putri Rahmani Amalia</h3>
                    <span>Direktur Operasional dan SDM</span>
                </div>
            </div>
            
            <div class="team-member">
                <div class="team-image">
                    <img src="<?php echo base_url('assets'); ?>/images/team/atdp.jpg">
                </div>
                <div class="team-desc">
                    <h3>Adhika Trisna DP</h3>
                    <span>Direktur Teknologi</span>
                </div>
            </div>

            <div class="team-member">
                <div class="team-image">
                    <img src="<?php echo base_url('assets'); ?>/images/team/zaki.jpg">
                </div>
                <div class="team-desc">
                    <h3>Muhammad Zaki</h3>
                    <span>Direktur Riset dan Pengembangan</span>
                </div>
            </div>

            <div class="team-member">
                <div class="team-image">
                    <img src="<?php echo base_url('assets'); ?>/images/team/ramiz.jpg">
                </div>
                <div class="team-desc">
                    <h3>Ramiz Azhar</h3>
                    <span>Marketing Manager</span>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- <section>
    <div class="container">
        <h1 class="heading-jumbo text-center">What people are saying!</h1>
        <p class="lead text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo.</p>
        <hr class="space">
        <div class="carousel arrows-visibile testimonial testimonial-single testimonial-left" data-items="1">

            <div class="testimonial-item">
                <img src="<?php echo base_url('assets'); ?>/images/team/9.jpg" alt="">
                <p>Polo is by far the most amazing template out there! I literally could not be happier that I chose to buy this template!</p>
                <span>Alan Monre</span>
                <span>CEO, Square Software</span>
            </div>

            <div class="testimonial-item">
                <img src="<?php echo base_url('assets'); ?>/images/team/9.jpg" alt="">
                <p>The world is a dangerous place to live; not because of the people who are evil, but because of the people who don't do anything about it.</p>
                <span>Alan Monre</span>
                <span>CEO, Square Software</span>
            </div>

        </div>
    </div>
</section> -->

<section class="text-center">
    <div class="container">
        <div class="row">
            <h2>- Say Yes to <span style="color: #1375d1">YEPS</span> -</h2>
            <p style="font-weight: bold; font-size: 14px; margin-top: -15px">Ariobimo Sentral, South Jakarta, RT.9/RW.4, <br>
            East Kuningan, Jakarta, 12950</p>
        </div>
    </div>
    
</section>

<?php $this->load->view('footer'); ?>