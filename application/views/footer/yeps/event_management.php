 <?php $this->load->view('header'); ?>

 <!-- Page title -->
 <section id="page-title" data-parallax-image="<?php echo base_url('assets'); ?>/images/footer/header-01.jpg" style="height: 250px">
   <div class="container">
      <div class="page-title header-a">
        <h1>Event Management</h1>
        <span>YEPS Indonesia</span>
    </div>
    <div class="breadcrumb">
     <ul>
        <li><a href="<?php echo base_url(''); ?>">Beranda</a>
        </li>
        <li class="active"><a href="#">Event Management</a>
        </li>
    </ul>
</div>
</div>
</section>
<!-- end: Page title -->

<!-- Content -->
<section>
    <div class="container">
        <center>
            <img src="<?php echo base_url('assets');?>/images/footer/yeps/em/em-01.jpg" alt="" style="width: 80%; margin-bottom: 15px"> 
            <img src="<?php echo base_url('assets');?>/images/footer/yeps/em/em-02.jpg" alt="" style="width: 80%; margin-bottom: 15px"> 
            <img src="<?php echo base_url('assets');?>/images/footer/yeps/em/em-03.jpg" alt="" style="width: 80%;">
        </center> 
    </div>
</section>
<!-- end: Content -->

<?php $this->load->view('info-yeps'); ?>

<?php $this->load->view('footer'); ?>
