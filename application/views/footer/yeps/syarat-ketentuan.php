 <?php $this->load->view('header'); ?>

 <!-- Page title -->
 <section id="page-title" data-parallax-image="<?php echo base_url('assets'); ?>/images/footer/header-01.jpg" style="height: 250px">
     <div class="container">
      <div class="page-title header-a">
       <h1>Syarat dan Ketentuan</h1>
       <span>YEPS Indonesia</span>
   </div>
   <div class="breadcrumb">
       <ul>
        <li><a href="<?php echo base_url(''); ?>">Beranda</a>
        </li>
        <li><a href="#">Bantuan</a>
        </li>
        <li class="active"><a href="#">Syarat dan Ketentuan</a>
        </li>
    </ul>
</div>
</div>
</section>
<!-- end: Page title -->

<!-- Content -->
<section id="page-content" class="sidebar-left">
    <div class="container">
        <div class="row">
            <!-- post content -->
            <div class="content col-md-9 justify">

                <h1 class="text-center" style="font-weight: bold;">Syarat dan Ketentuan</h1>
                <h4 id="section-1">Pendahuluan</h4>
                <p>Selamat datang di <b>YEPS Indonesia</b>. Syarat dan Ketentuan ini mengatur Pengguna terkait dengan penggunaan layanan YEPS. Pengguna diharapkan membaca Syarat dan Ketentuan ini dengan cermat sebelum menggunakan layanan YEPS agar Anda mengetahui Hak, Kewajiban, dan tanggung jawab Anda selama menggunakan layanan YEPS.</p>
                <p>Dengan menggunakan layanan YEPS, maka Pengguna dianggap telah membaca, dan menyetujui semua isi dalam Syarat dan Ketentuan ini. Jika Pengguna tidak menyetujui salah satu, sebagian, atau seluruh isi Syarat dan Ketentuan ini, maka Pengguna tidak diperkenankan menggunakan layanan atau membuka akun di YEPS Indonesia.</p>
                <p>Pengguna harus berusia sekurang-kurangnya 18 tahun untuk menggunakan layanan kami. Apabila Pengguna berusia di bawah 18 tahun, Pengguna harus mendapatkan izin dari orang tua atau wali tersebut harus menyetujui Syarat dan Ketentuan ini. Dengan menggunakan layanan YEPS maka Pengguna mengakui bahwa Pengguna adalah orang yang cakap dan mampu untuk mengikatkan dirinya dalam sebuah perjanjian yang sah menurut Hukum Negara Republik Indonesia.</p>
                <hr>
                <h4 id="section-2">A. Informasi Umum</h4>
                <ol>
                    <li class="p-b-10"><b>YEPS</b> adalah platform online yang memberikan akses pada Klien agar Klien dapat menelusuri berbagai jenis layanan/produk event yang disediakan oleh Vendor. Pengguna dapat melakukan pemesanan layanan/produk yang disediakan oleh Vendor yang telah bekerja sama dengan YEPS.</li>
                    <li class="p-b-10"><b>Syarat dan Ketentuan</b> ini adalah salah satu dari keseluruhan Perjanjian antara YEPS dengan Pengguna yang terkait dengan Layanan dan menghapus serta menggantikan perjanjian tertulis atau lisan, representasi, atau kesepahaman sebelumnya di antara YEPS dengan Pengguna. (YEPS akan memberikan perjanjian lain yang berisikan tentang batasan tanggung jawab YEPS saat event diselenggarakan).</li>
                    <li class="p-b-10"><b>Pengguna</b> adalah pihak yang menggunakan layanan YEPS Indonesia (Klien, Vendor atau pihak yang sekadar berkunjung ke situs YEPS Indonesia).</li>
                    <li class="p-b-10"><b>Klien</b> adalah pihak yang menggunakan layanan/produk pada platform YEPS.</li>
                    <li class="p-b-10"><b>Vendor</b> adalah pihak yang menggunakan layanan YEPS untuk menawarkan jasa/produknya dalam platform YEPS.</li>
                    <li class="p-b-10"><b>Layanan/Produk</b> adalah layanan yang berwujud/tidak berwujud yang disediakan oleh Vendor dalam platform YEPS.</li>
                    <li class="p-b-10"><b>YEPS Event Management</b>  adalah tim yang melakukan pengorganisasian,  monitoring, dan kontrol dalam suatu kegiatan/event klien yang dikelola secara professional, sistematis, efisien, dan efektif.</li>
                    <li class="p-b-10"><b>Rekening resmi YEPS</b> adalah rekening bersama yang disepakati YEPS dan Pengguna untuk proses transaksi di situs YEPS.</li>
                    <li class="p-b-10">YEPS memiliki kewenangan untuk mengambil keputusan atas segala permasalahan yang terjadi setiap transaksi.</li>
                    <li class="p-b-10">YEPS berhak meminta Data Pribadi Pengguna apabila diperlukan.</li>
                </ol>
                <!--  <p>Vestibulum consectetur scelerisque lacus, <a class="scroll-to" href="#section-5"><strong>Scroll to Section Five</strong></a> ac fermentum lorem convallis sed. Nam odio tortor, dictum quis malesuada at, pellentesque vitae orci. Vivamus elementum, felis eu auctor lobortis, diam velit egestas lacus, quis fermentum metus ante quis urna. Sed at facilisis libero. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum bibendum blandit dolor. Nunc orci dolor, molestie nec nibh in, hendrerit tincidunt ante. Vivamus sem augue, hendrerit non sapien in, mollis ornare augue.</p> -->
                <hr>
                <h4 id="section-3">B. Akun dan Keamanan Akun</h4>
                <ol>
                    <li class="p-b-10">Layanan kami membutuhkan Akun dengan identitas pengguna dan password. Setelah mendaftar di situs YEPS, Pengguna akan mendapatkan akun yang dapat diakses dengan password yang telah Pengguna buat.</li>
                    <li class="p-b-10">Vendor dapat memilih kategori layanan/produk yang akan ditawarkan kepada Klien dalam situs YEPS.</li>
                    <li class="p-b-10">YEPS dapat melakukan tindakan terhadap akun yang terindikasi melakukan pelanggaran Syarat dan Ketentuan yang berlaku. Tindakan dapat berupa penghapusan layanan/produk Vendor, suspensi akun, dan/atau penghapusan akun Pengguna.</li>
                    <li class="p-b-10">YEPS dapat menutup akun Pengguna untuk sementara atau permanen atas tindakan kecurangan dalam bertransaksi atau pelanggaran terhadap Syarat dan Ketentuan yang berlaku.</li>
                    <li class="p-b-10">YEPS dapat menutup akun Vendor apabila terindikasi adanya tindakan kecurangan yang dilakukan oleh Vendor yang bertujuan untuk menguntungkan Vendor dan merugikan pihak lain.</li>
                    <li class="p-b-10">YEPS dapat menahan dana Pengguna apabila ditemukan tindak kecurangan dalam bertransaksi atau pelanggaran terhadap Syarat dan Ketentuan YEPS.</li>
                    <li class="p-b-10">Pengguna tidak dapat mengubah nama akun dan domain Pengguna.</li>
                    <li class="p-b-10">Pengguna setuju untuk menjaga kerahasiaan akun untuk semua aktifitas yang terjadi dalam akun Pengguna.</li>
                    <li class="p-b-10">YEPS TIDAK PERNAH meminta username, password dan kode verifikasi milik Pengguna dengan alasan apapun. Karena itu, Pengguna diharap tidak memberikan username, password dan kode verifikasi kepada pihak yang mengatasnamakan YEPS Indonesia atau pihak lain yang tidak terjamin keamanannya.</li>
                    <li class="p-b-10">Pengguna diharap logout di setiap akhir sesi.</li>
                    <li class="p-b-10">Pengguna diharap segera melapor kepada YEPS apabila terjadi penggunaan tanpa izin atas akun Pengguna. YEPS tidak bertanggung jawab atas kerugian ataupun kendala yang timbul atas penyalahgunaan akun Pengguna yang diakibatkan oleh kelalaian Pengguna, termasuk namun tidak terbatas pada meminjamkan atau memberikan akses akun kepada pihak lain, mengakses link atau tautan yang diberikan oleh pihak lain, memberikan atau memperlihatkan kode verifikasi, password atau email kepada pihak lain, maupun kelalaian Vendor lainnya yang mengakibatkan kerugian ataupun kendala pada akun Vendor.</li>
                    <li class="p-b-10">Pengguna dapat meminta YEPS untuk menghapus akun milik Pengguna dengan memberi tahu secara tertulis kepada YEPS (atau melalui email ke <span style="text-decoration: underline; color: #107ade">support@yepsindonesia.com</span>). Pengguna tetap bertanggung jawab dan berkewajiban menyelesaikan atas setiap transaksi yang belum selesai. YEPS tidak akan bertanggung jawab atas segala kerugian yang terjadi akibat penghapusan akun Pengguna.</li>
                    <li class="p-b-10">YEPS memiliki fitur keamanan tambahan TFA (Two Factor Authentication). Apabila Pengguna mengaktifkan fitur tersebut, maka YEPS akan mengirimkan kode OTP (One Time Password) via SMS ke nomor handphone yang sudah terdaftar dalam akun Pengguna. Kode OTP tersebut adalah rahasia. Pengguna diharapkan tidak memberitahukan kepada orang lain/pihak yang mengatasnamakan YEPS.</li>
                    <li class="p-b-10">Pengguna menerima bahwa Layanan ini mengandung materi yang rahasia dan sangat sensitif. Oleh karena itu Pengguna setuju untuk menjaga kerahasiaan yang tinggi ini dan informasi serta rahasia bisnis lainnya (termasuk, untuk menghindari keraguan, persyaratan Perjanjian ini) dan mengambil tindakan keamanan yang tidak kalah ketatnya dengan tindakan yang Pengguna lakukan untuk melindungi informasi milik sendiri, namun tidak kurang cermatnya untuk melindungi pengungkapan dan penggunaan informasi rahasia yang tidak sah. Pengungkapan hanya dibolehkan setelah mendapatkan izin sebelumnya secara tegas atau sebagaimana yang diwajibkan oleh hukum.</li>
                </ol>
                <hr>

                <h4 id="section-4">C. Klien</h4>
                <ol>
                    <li class="p-b-10">Untuk melakukan transaksi, Klien harus berusia sekurang-kurangnya 18 (delapan belas) tahun dan memiliki kapasitas hukum penuh untuk membuat transaksi.</li>
                    <li class="p-b-10">Klien melakukan  transaksi sesuai tahapan prosedur yang telah ditetapkan oleh YEPS. Klien melakukan proses pembayaran menggunakan metode transfer bank. YEPS akan meneruskan dana ke Vendor setiap kali Klien melakukan pembayaran.</li>
                    <li class="p-b-10">Saat melakukan permintaan maka Klien telah membaca dan menyetujui informasi layanan/produk sebelum bertransaksi. </li>
                    <li class="p-b-10">Apabila layanan/produk tidak tersedia, maka Vendor dapat menolak permintaan Klien dan pembayaran akan dikembalikan kepada Klien.</li>
                    <li class="p-b-10">Klien akan mendapatkan konfirmasi  atas layanan/produk Vendor yang telah dipesan maksimal dalam waktu <b>1x24 jam</b>. Lebih dari itu pesanan Klien dianggap telah ditolak oleh Vendor.</li>
                    <li class="p-b-10">Pembayaran DP sebesar 30% oleh Klien dilakukan setelah Vendor mengkonfirmasi ketersediaan layanan/produk yang diinginkan Klien. Setelah Vendor melakukan konfirmasi, Klien wajib melakukan pembayaran selambat-lambatnya <b>1x24 JAM</b> setelah Vendor melakukan konfirmasi atas pemesanan layanan/produk yang diinginkan. Sisa pembayaran berikutnya dilakukan mengikuti termin pembayaran yang dipilih oleh Klien.</li>
                    <li class="p-b-10">Pengguna menyetujui bahwa segala transaksi yang dilakukan melalui rekening resmi YEPS.</li>
                    <li class="p-b-10">Setiap masalah yang disebabkan keterlambatan pembayaran adalah merupakan tanggung jawab Klien. YEPS akan mengambil tindakan agar permasalahan tidak berkelanjutan.</li>
                    <li class="p-b-10">Setiap masalah pembayaran dan biaya tambahan yang disebabkan oleh perbedaan bank Klien dengan bank resmi YEPS adalah tanggung jawab Klien.</li>
                    <li class="p-b-10">YEPS dapat mengambil keputusan dengan merujuk pada bukti-bukti yang tersedia terhadap permasalah-permasalahan transaksi yang terjadi apabila tidak terciptanya kesepakatan penyelesaian antara Klien dengan Vendor. Keputusan YEPS tidak dapat diganggu gugat dan mengikat pihak Klien dengan Vendor.</li>
                    <li class="p-b-10">Invoice yang diterbitkan atas nama Vendor dan dilengkapi dengan booking order dari masing-masing Vendor.</li>
                    <li class="p-b-10">Klien setuju untuk memberikan ulasan yang akurat dan tidak mengandung penggambaran yang keliru. Klien tidak menyebabkan cedera pada pihak manapun. Klien bertanggung jawab atas ulasan yang Klien berikan.</li>
                </ol>
                <hr>

                <h4 id="section-5">D. Vendor</h4>
                <ol>
                    <li class="p-b-10">Vendor wajib memberikan informasi layanan/produk dengan lengkap, jelas dan akurat sesuai dengan keadaan dan kualitas layanan yang ditawarkan. Apabila terdapat ketidaksesuaian antara kategori dengan informasi yang diberikan oleh Vendor kepada Klien, maka YEPS berhak menahan dana transaksi.</li>
                    <li class="p-b-10">Dalam menggunakan fitur foto layanan/produk, dan informasi layanan/produk, Vendor dilarang membuat peraturan-peraturan yang tidak memenuhi peraturan perundang-undangan yang berlaku di Indonesia. Termasuk : 
                        <ul>
                            <li>tidak menerima komplain; dan</li>
                            <li>tidak menerima refund.</li>
                        </ul>
                    </li>
                    <li class="p-b-10">Vendor wajib memberikan konfirmasi untuk menerima atau menolak pesanan Klien dalam batas waktu <b>1x24 jam</b> terhitung sejak adanya notifikasi pesanan Klien dari YEPS. Jika dalam batas waktu tersebut tidak ada balasan dari Vendor maka secara otomatis pesanan dianggap dibatalkan.</li>
                    <li class="p-b-10" style="font-weight: bold">Ketika dalam proses waktu tunggu pembayaran, Vendor dilarang membatalkan orderan.</li>
                    <li class="p-b-10">Vendor wajib memberikan booking order sejak menerima pesanan Klien dari YEPS.</li>
                    <li class="p-b-10">Vendor memahami dan menyetujui bahwa pembayaran atas harga layanan/ produk (diluar biaya transfer / administrasi) akan dikembalikan sepenuhnya ke Klien apabila transaksi dibatalkan atau transaksi tidak berhasil.</li>
                    <li class="p-b-10">YEPS dapat menahan dana di Rekening Resmi YEPS sampai waktu yang tidak terbatas apabila terdapat permasalahan dan klaim dari Klien terkait proses pengadaan dan kualitas layanan/produk. Pembayaran dari Klien baru akan dikirimkan kepada Vendor apabila permasalahan tersebut telah selesai.</li>
                    <li class="p-b-10">YEPS berwenang untuk membatalkan dan/atau menahan dana transaksi dalam hal: 
                        <ul>
                            <li>Nama layanan/produk dan informasi layanan/produk tidak sesuai/tidak jelas dengan layanan/produk yang diadakan;</li>
                            <li>Jika ditemukan adanya kecurangan dalam transaksi.</li>
                        </ul>    
                    </li>
                    <li class="p-b-10">Pajak Penghasilan Vendor diurus sendiri oleh masing-masing Vendor sesuai dengan ketentuan pajak yang berlaku di peraturan perundang-undangan di Indonesia.</li>
                    <li class="p-b-10">YEPS berwenang mengambil keputusan atas permasalahan-permasalahan transaksi yang belum terselesaikan akibat tidak adanya kesepakatan penyelesaian, baik antara Vendor dan Klien, dengan melihat bukti-bukti yang ada. Keputusan YEPS adalah keputusan akhir yang tidak dapat diganggu gugat dan mengikat pihak Vendor dan Klien untuk mematuhinya.</li>
                </ol>
                <hr>

                <h4 id="section-6">E. YEPS Event Management</h4>
                <ol>
                    <li class="p-b-10">Event Management akan melakukan controlling di lapangan saat event dilaksanakan untuk melaporkan/memberikan evaluasi setelah event selesai.</li>
                    <li class="p-b-10">Event Management bukan event organizer acara.</li>
                    <li class="p-b-10">Segala keluhan mengenai layanan/produk yang telah dipesan oleh Klien dapat melalui tim Event Management YEPS.</li>
                </ol>
                <hr>
                
                <h4 id="section-7">F. Harga</h4>
                <ol>
                    <li class="p-b-10">Harga layanan/produk dalam situs YEPS adalah harga yang ditetapkan oleh Vendor. Vendor memastikan bahwa harga produk/layanan di situs Yeps sekurang-kurangnya dan tidak lebih dari harga yang Vendor tawarkan diluar platform YEPS.</li>
                    <li class="p-b-10">Vendor harus memperbaharui data apabila ada perubahan terhadap harga, rincian, ketersediaan produk/layanan Vendor secara berkala.</li>
                    <li class="p-b-10">Kerugian yang disebabkan oleh kesalahan ketik pada kolom harga/informasi sehingga harga/informasi menjadi tidak benar/sesuai adalah tanggung jawab Vendor. </li>
                    <li class="p-b-10">Apabila terjadi kesalahan pengetikan keterangan harga layanan/produk yang tidak disengaja, Vendor dapat menolak pesanan yang dilakukan oleh Klien.</li>
                    <li class="p-b-10">YEPS hanya melayani transaksi dalam mata uang Rupiah.</li>
                </ol>
                <hr>
                <h4 id="section-8">G. Komisi</h4>
                <ol>
                    <li class="p-b-10">Komisi adalah sistem pembagian pendapatan yang diterima oleh YEPS dari Vendor.</li>
                    <li class="p-b-10">Pendapatan yang diterima Vendor otomatis terpotong sebesar 10% sebagai wujud komisi dari Vendor untuk YEPS.</li>
                    <li class="p-b-10">Sistem komisi ini berlaku setelah Vendor bekerja sama dengan YEPS selama 3 bulan sejak Vendor memiliki akun dalam platform YEPS.</li>
                    <li class="p-b-10">Pada 3 bulan awal sejak Vendor memiliki akun dalam platform YEPS, sistem Komisi ini belum berlaku.</li>
                </ol>
                <hr>

                <h4 id="section-9">H. Pembayaran</h4>
                <ol>
                    <li class="p-b-10">Klien mengikuti tahapan pembayaran yang telah dipilih oleh Klien. Klien tidak dapat merubah tahapan pembayaran yang telah dipilih.</li>
                    <li class="p-b-10">Setelah mendapatkan konfirmasi persetujuan dari Vendor, Klien diharapkan membayar DP sebesar 30%.
                    Sebelum hari diselenggarakannya event, Klien diharap membayar sebesar 50% sesuai dengan termin yang telah dipilih oleh Klien.</li>
                    <li class="p-b-10">Setelah event selesai diselenggarakan, Klien membayar 20% sisanya kepada YEPS.</li>
                    <li class="p-b-10">YEPS akan meneruskan dana ke Vendor setiap kali klien melakukan pembayaran.</li>
                    <li class="p-b-10">Apabila ada acara yang batal dilaksanakan setelah DP dibayar, YEPS sebagai penanggungjawab akan berusaha menyelesaikan masalah ke Vendor. </li>
                </ol>
                <hr>

                <h4 id="section-10">I. Ganti Rugi</h4>
                <p>Jika terjadi pemesanan yang salah atau pemesanan tidak sesuai yang disebabkan oleh kegagalan Vendor dalam mematuhi kewajiban, Vendor akan mencari akomodasi alternatif untuk Klien dengan standar yang sama atau lebih baik, dan dengan jarak yang wajar dari kapasitas awal, menyediakan transportasi gratis dan menanggung selisih apapun atas biaya diatas tarif bersih yang telah disetujui pada saat pemesanan.</p>
                <hr>

                <h4 id="section-11">J. Promo</h4>
                <ol>
                    <li class="p-b-10">YEPS dapat mengadakan kegiatan promo dengan Syarat dan Ketentuan yang mungkin akan berbeda pada tiap kegiatan promo. Pengguna disarankan untuk membaca dengan cermat Syarat dan Ketentuan yang terkait dengan promo tersebut.</li>
                    <li class="p-b-10">YEPS berhak tanpa pemberitahuan sebelumnya melakukan tindakan-tindakan yang diperlukan seperti pencabutan Promo, membatalkan transaksi, menahan dana, menurunkan reputasi Vendor, menutup akun, serta hal-hal lainnya jika ditemukan adanya manipulasi, pelanggaran maupun pemanfaatan Promo untuk keuntungan pribadi Pengguna, maupun indikasi kecurangan atau pelanggaran pelanggaran Syarat dan Ketentuan YEPS dan ketentuan Hukum yang berlaku di wilayah negara Indonesia.</li>
                    <li class="p-b-10">Apabila ada promo yang diberikan oleh Vendor secara langsung, maka hak dan wewenang atas promo tersebut sepenuhnya berada pada Vendor.</li>
                </ol>
                <hr>

                <h4 id="section-12">K. Promosi</h4>
                <p>Vendor memberikan izin pada YEPS untuk mempromosikan layanan/produk perusahaan Vendor menggunakan nama YEPS melalui promosi secara online, termasuk pemasaran melalui email dan/atau periklanan. YEPS dapat menjalankan promosi atas pertimbangan YEPS serta YEPS akan membayar biaya periklanan pada promosi online YEPS. Vendor dapat meminta pembatasan atas jenis channel promosi yang YEPS pilih.</p>
                <hr>

                <h4 id="section-13">L. Penerusan Dana</h4>
                <ol>
                    <li class="p-b-10">Penerusan dana sesama bank akan diproses dalam waktu 1x24 jam pada hari kerja, sedangkan penerusan dana antar bank akan diproses dalam waktu 2x24 jam hari kerja.</li>
                    <li class="p-b-10">Untuk penerusan dana dengan tujuan nomor rekening di luar bank BCA dan Mandiri apabila ada biaya tambahan yang dibebankan akan menjadi tanggungan dari Pengguna.</li>
                    <li class="p-b-10">Dalam hal ditemukan adanya dugaan pelanggaran terhadap Syarat dan Ketentuan YEPS, kecurangan, manipulasi atau kejahatan, Pengguna memahami dan menyetujui bahwa YEPS berhak melakukan tindakan pemeriksaan, pembekuan, penundaan dan/atau pembatalan terhadap penerusan dana yang dilakukan oleh Pengguna.</li>
                    <li class="p-b-10">Pemeriksaan, pembekuan atau penundaan penerusan dana sebagaimana dimaksud dalam poin 3 dapat dilakukan dalam jangka waktu selama yang diperlukan oleh pihak YEPS.</li>
                </ol>
                <hr>

                <h4 id="section-14">M. Larangan</h4>
                <ol>
                    <li class="p-b-10">Pengguna setuju untuk tidak :</li>
                    <ul>
                        <li>Menggunakan situs ini atau konten untuk tujuan komersil tanpa izin dari YEPS;</li>
                        <li>Menggunakan situs ini atau konten untuk promosi ke halaman situs lain diluar situs YEPS;</li>
                        <li>Memberikan data kontak pribadi untuk bertransaksi diluar layanan YEPS;</li>
                        <li>Vendor mengarahkan Klien untuk bertransaksi diluar layanan YEPS;</li>
                        <li>Membuat konten yang mengandung materi yang dapat dianggap berbahaya, cabul, pornografi, tidak senonoh, kekerasan, rasis, diskriminasi, hinaan, ancaman, pelecehan, penindasan, kebencian atau yang tidak menyenangkan lainnya;</li>
                        <li>Mengunggah atau mempergunakan kata-kata, komentar, gambar, atau konten apapun yang mengandung unsur SARA, diskriminasi, merendahkan atau menyudutkan orang lain, vulgar, bersifat ancaman, atau hal-hal lain yang dapat dianggap tidak sesuai dengan nilai dan norma sosial;</li>
                        <li>Membuat peraturan bersifat klausula baku yang tidak memenuhi peraturan perundang-undangan yang berlaku di Indonesia, termasuk namun tidak terbatas pada (i) tidak menerima komplain, (ii) tidak menerima refund (pengembalian dana), (iii) layanan/produk tidak bergaransi, dan (iv) penyusutan nilai harga;</li>
                        <li>Melanggar Hak Kekayaan Intelektual. Vendor dilarang mempergunakan foto/gambar yang memiliki watermark yang menandakan hak kepemilikan orang lain;</li>
                        <li>Menggunakan identitas palsu dengan tujuan menyesatkan, menipu dan memperdaya;</li>
                        <li>Memalsukan informasi untuk menyamarkan asal-usul pernyataan yang Pengguna berikan;</li>
                        <li>Melakukan duplikasi layanan-layanan/produk-produk atau tindakan lain yang diindikasikan sebagai persaingan tidak sehat;</li>
                        <li>Mengunggah, mengirim email, memposting, mengirimkan atau menyediakan iklan, materi promosi yang tidak diinginkan atau tidak sah, "surat sampah", "spam", "surat berantai", "skema piramida", atau bentuk ajakan lainnya yang tidak sah; </li>
                        <li>Mengunggah, mengirim email, memposting, mengirimkan atau menyediakan materi yang berisikan virus, worm, Trojan-horse perangkat lunak atau kode, rutin, file maupun program komputer lainnya yang dirancang untuk secara langsung atau tidak langsung memengaruhi, memanipulasi, mengganggu, menghancurkan atau membatasi fungsionalitas atau integritas perangkat lunak atau perangkat keras komputer atau data atau perlengkapan telekomunikasi apapun;</li> 
                        <li>Tindakan apapun yang dapat memberi pengaruh negatif dan mengakibatkan kerusakan pada situs, YEPS dan Karyawan, atau reputasi YEPS.</li>
                        <li>Membuat tautan ke halaman lain dengan tujuan komersial tanpa persetujuan YEPS.</li>
                        <li>Menciptakan atau menggunakan perangkat, software, fitur dan alat lainnya yang bertujuan untuk : </li>
                        <ul>
                            <li>Memanipulasi data Pengguna;</li>
                            <li><i>Crawling/Scraping;</i></li>
                            <li>Kecurangan dalam transaksi, promosi, dsb; dan</li>
                            <li>Aktifitas lain yang dapat dinilai sebagai tindakan manipulasi system</li>
                        </ul>
                    </ul>
                    <li class="p-b-10">YEPS tidak bertanggung jawab penuh atas semua Konten yang Pengguna unggah, posting, kirim melalui email, kirimkan atau sediakan melalui Situs. Pengguna memahami bahwa dengan menggunakan layanan YEPS, Pengguna mungkin akan menemukan Konten yang dianggap menyinggung, tidak sopan atau tidak pantas. Dalam situasi apapun YEPS tidak akan bertanggung jawab dengan cara apapun untuk setiap Konten, termasuk, tetapi tidak terbatas pada, setiap kesalahan atau pembiaran dalam Konten, atau kehilangan maupun kerusakan dalam bentuk apapun yang terjadi sebagai akibat penggunaan, kepercayaan pada Konten yang diposting, diemail, dikirimkan atau disediakan di Situs YEPS.</li>
                </ol>
                <hr>

                <h4 id="section-15">N. Sanksi</h4>
                <p>Pelanggaran terhadap Syarat dan Ketentuan ini dapat mengakibatkan hal-hal berikut ini :</p>
                <ul>
                    <li>Penghapusan akun</li>
                    <li>Suspensi akun</li>
                    <li>Tuntutan Pidana</li>
                    <li>Tuntutan Perdata</li>
                    <li>Penghapusan, pemotongan, dan penyembunyian konten atau materi yang dianggap tidak sesuai.</li>

                </ul>
                <hr>

                <h4 id="section-16">O. Bantuan</h4>
                <ol>
                    <li class="p-b-10">Bantuan adalah fitur yang disediakan oleh YEPS untuk memfasilitasi penyelesaian permasalahan teknis yg dialami Pengguna dan permasalahan transaksi antara Klien dan Vendor.</li>
                    <li class="p-b-10">Fitur ini akan mengarahkan Pengguna kepada live support YEPS apabila Pengguna mengalami keluhan-keluhan/permasalah teknis yg dialami. Apabila terjadi permasalahan transaksi antara Klien dengan Vendor, fitur ini dapat secara otomatis menahan dana pembayaran ke Vendor sampai dengan permasalahan yang dilaporkan ke Bantuan selesai.</li>
                    <li class="p-b-10">Ketika Klien menggunakan fitur ini maka Klien akan memberikan bukti-bukti transaksi, invoice, dan bukti penunjang lainnya yang dapat menjadi dasar pembenar atas setiap argument yang dikeluarkan oleh pihak Klien. Selanjutnya tim Event Management YEPS akan melakukan evaluasi atas keluhan yang dikeluarkan oleh Klien.</li> 
                    <li class="p-b-10">Penyelesaian permasalahan teknis yang dialami Pengguna berupa Solusi yang ditentukan oleh YEPS, sedangkan penyelesaian permasalahan transaksi melalui Bantuan dapat berupa solusi yang dihasilkan berdasarkan kesepakatan bersama antara Vendor dan Klien.</li>
                    <li class="p-b-10">Dengan menggunakan fitur Bantuan, maka Pengguna paham dan setuju bahwa YEPS berwenang untuk mengambil keputusan/solusi atas permasalah tersebut.</li>

                </ol>
                <hr>

                <h4 id="section-17">P. Pelepasan</h4>
                <p>Pengguna melepaskan YEPS (termasuk Induk Perusahaan, Direktur, dan karyawan) dari klaim dan tuntutan atas kerusakan dan kerugian (aktual dan tersirat) dari setiap jenis dan sifatnya yang dikenal dan tidak dikenal, yang timbul dari atau dengan cara apapun berhubungan dengan sengketa tersebut.</p>
                <hr>

                <h4 id="section-18">Q. Ganti Rugi</h4>
                <ol>
                    <li class="p-b-10">Pengguna akan melepaskan YEPS (termasuk Induk Perusahaan, direktur, dan karyawan) dari tuntutan ganti rugi, klaim, kewajiban, kerusakan termasuk biaya hukum yang dilakukan pihak ketiga akibat dari : </li>
                    <ul>
                        <li>Penggunaan terhadap situs YEPS yang tidak semestinya;</li>
                        <li>Konten yang diberikan, disediakan, atau diakses dari situs YEPS;</li>
                        <li>Pelanggaran Pengguna terhadap Syarat dan Ketentuan ini;</li>
                        <li>Pelanggaran hak dan kewajiban lainnya; </li>
                        <li>Setiap tindakan atau kelalaian oleh Pengguna, baik lalai, melanggar hukum, dan lainnya.</li>
                    </ul>
                    <li class="p-b-10">Tidak ada salah satu pihak pun yang akan bertanggung jawab atas setiap penundaan atau wanprestasi dalam melaksanakan perjanjian di bawah ini jika penundaan atau wanprestasi tersebut disebabkan oleh kondisi yang berada di luar kendalinya termasuk, namun tidak terbatas pada, bencana alam, pembatasan pemerintah, perang, terorisme, pemberontakan, insiden nuklir, dan/atau penyebab lainnya mana pun yang penyebabnya di luar kendali pihak yang kinerjanya terpengaruh.</li>
                </ol>
                <hr>

                <h4 id="section-19">R. Hukum</h4>
                <ol>
                    <li class="p-b-10">Syarat dan Ketentuan ini dilaksanakan dan tunduk sesuai dengan Peraturan Perundang-undangan Republik Indonesia.</li>
                    <li class="p-b-10">Apabila terjadi perselisihan yang timbul dari/berhubungan dengan perjanjian ini, Pengguna akan menghubungi YEPS secara langsung terlebih dahulu untuk melakukan perundingan atau musyawarah sebelum beralih ke alternatif lain.</li>
                    <li class="p-b-10">Apabila dalam waktu 1 (satu) bulan setelah dimulainya perundingan atau muswarah tidak mencapai titik penyelesaian, maka para Pihak akan menyelesaikan permasalahan yang berhubungan dengan perjanjian ini secara eksklusif dalam yurisdiksi pengadilan Republik Indonesia.</li>
                </ol>
                <hr>

                <h4 id="section-20">S. Penutup</h4>
                <ol>
                    <li class="p-b-10">Syarat dan ketentuan dapat diperbaharui sewaktu-waktu tanpa pemberitahuan terlebih dahulu. YEPS menyarankan agar Pengguna membaca secara seksama dan memeriksa kembali halaman Syarat dan Ketentuan ini. <b>Dengan mengakses dan menggunakan layanan YEPS, maka Pengguna dianggap menyetujui perubahan-perubahan dalam Syarat dan ketentuan YEPS.</b></li>
                    <li class="p-b-10">Yeps akan menampilkan perubahan Syarat dan Ketentuan ini dalam situs Yeps untuk informasi Pengguna.</li>
                    <li class="p-b-10">Versi terbaru dari Syarat dan Ketentuan ini menggantikan semua versi sebelumnya.</li>
                    <li class="p-b-10">Apabila Pengguna memiliki pertanyaan atau masalah sehubungan dengan Syarat dan Ketentuan ini, silakan hubungi kami di : <span style="text-decoration: underline; color: #107ade">support@yepsindonesia.com</span> <br>
                        Silakan kirim semua pemberitahuan hukum ke : <span style="text-decoration: underline; color: #107ade">legal@yepsindonesia.com </span>
                    </li>
                </ol>
                <hr>

                <a href="html" class="btn scroll-to"><span><i class="fa fa-arrow-up"></i>Kembali Keatas</span></a>

                <p style="float: right">Efektif sejak : <span style="background-color: #107ade; color: white; font-weight: bold; padding: 5px">Agustus 2018</span></p>

            </div>
            <!-- end: post content -->

            <!-- Sidebar-->
            <div class="sidebar col-md-3">
                <div class="pinOnScroll">

                    <!--Navigation-->

                    <div class="widget" >
                        <h4 class="widget-title">Syarat dan Ketentuan</h4>
                        <div id="mainMenu" class="menu-vertical">
                            <div class="container">
                                <nav>
                                    <ul>
                                        <li class="active">
                                            <a class="scroll-to" href="#section-1">Pendahuluan</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-2">A. Informasi Umum</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-3">B. Akun dan Keamanan Akun</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-4">C. Klien</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-5">D. Vendor</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-6">E. YEPS Event Management</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-7">F. Harga</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-8">G. Komisi</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-9">H. Pembayaran</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-10">I. Ganti Rugi</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-11">J. Promo</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-12">K. Promosi</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-13">L. Penerusan Dana</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-14">M. Larangan</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-15">N. Sanksi</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-16">O. Bantuan</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-17">P. Pelepasan</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-18">Q. Ganti Rugi</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-19">R. Hukum</a>
                                        </li>
                                        <li><a class="scroll-to p-l-10" href="#section-20">S. Penutup</a>
                                        </li>

                                    </ul>
                                </nav>
                            </div>
                        </div>
                        
                    </div>
                    <!--end: Navigation-->


                </div>
            </div>
            <!-- end: Sidebar-->
        </div>
    </div>
</section>
<!-- end: Content -->

<?php $this->load->view('info-yeps'); ?>

<?php $this->load->view('footer'); ?>
