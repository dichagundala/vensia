<?php $this->load->view('header'); ?>
<style>
.search {
  width: 70%;
  box-sizing: border-box;
  border: 2px solid #ccc;
  border-radius: 4px;
  font-size: 16px;
  background-color: white;
  background-image: url('<?php echo base_url('assets'); ?>/images/pages/icon/searchicon.png');
  background-position: 10px 12px; 
  background-repeat: no-repeat;
  padding: 12px 20px 12px 40px;
  -webkit-transition: width 0.4s ease-in-out;
  transition: width 0.4s ease-in-out;
}

.search:focus {
  width: 100%;
}

.form-box{
  border: 2px solid #ece6e6; background-color: #ffffff; border-radius: 5px; padding: 20px;
}

.custom-file {
  position: relative;
  display: inline-block;
  width: 97%;
  height: calc(2.25rem + 2px);
  margin-bottom: 10px;
}

.custom-file-input {
  position: relative;
  z-index: 2;
  width: 100%;
  height: calc(2.25rem + 2px);
  margin: 0;
  opacity: 0;
}

.custom-file-label {
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  z-index: 1;
  height: calc(2.25rem + 2px);
  padding: .375rem .75rem;
  line-height: 1.5;
  color: #495057;
  background-color: #fff;
  border: 1px solid #ced4da;
  border-radius: .25rem;
}

</style>

<!-- Page title -->
<section id="page-title" data-parallax-image="<?php echo base_url('assets'); ?>/images/footer/header-01.jpg" style="height: 250px">
 <div class="container">
  <div class="page-title header-f">
   <h1>Hallo, </h1>
   <span>Apa yang Bisa Kami Bantu ?</span>
   <form style="margin-top: 20px">
    <input class="search" type="text" name="search" placeholder="Cari form Bantuan">
  </form>
</div>
<div class="breadcrumb">
 <ul>
  <li><a href="<?php echo base_url(''); ?>">Beranda</a>
  </li>
  <li><a href="#">Bantuan</a>
  </li>
  <li class="active"><a href="#">Hubungi Kami</a>
  </li>
</ul>
</div>
</div>
</section>
<!-- end: Page title -->
<!-- Section -->
<section style="background-color: #f7f7f7">
  <h2 class="text-center">Form Bantuan</h2>
  <div class="container form-box">
    <h4>Lengkapi Detail Masalah</h4>
    <hr>
    <form id="bantuan">
      <div class="row">
        <div class="col-md-12">
         <div class="form-group">
          <label class="upper">Pilih Jenis Pertanyaan</label>
          <select name="tipe">
            <option value="akun">Akun</option>
           <option value="transaksi">Transaksi</option>
           <option value="pembayaran">Pembayaran</option>
           <option value="Refund">Refund</option>
         </select>
       </div>
     </div>
   </div>
   <div class="row">
    <div class="col-md-6">
     <div class="form-group">
      <label class="upper">Nama Lengkap</label>
      <input class="form-control required" name="name" placeholder="Enter name" type="text">
    </div>
  </div>
  <div class="col-md-6">
   <div class="form-group">
    <label class="upper">Email</label>

    <input class="form-control required email" name="email" placeholder="Enter email" type="email">
  </div>
</div>
</div>
<div class="row">
  <div class="col-md-12">
   <div class="form-group">
    <label class="upper"">Detail Masalah</label>
    <textarea class="form-control required" name="content" rows="9" placeholder="Enter comment" id="comment" aria-required="true"></textarea>
  </div>
</div>
</div>
<div class="row">
  <center>
    <div class="custom-file">
      <input type="file" name="foto" class="custom-file-input">
      <p class="custom-file-label">Pilih File Pendukung</p>
    </div>
    <br>
    <small class="form-text text-muted">
      Sertakan bukti pendukung untuk mempercepat respon kami. 
    </small>
  </center>
</div>
<div class="row" style="margin-top: 15px">
  <div class="col-md-12">
   <div class="form-group text-center">
    <button class="btn btn-default" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Kirim</button>
  </div>
</div>
</div>
</form>
</div>
</section>
<!-- end: Section -->

<?php $this->load->view('footer'); ?>

<script type="text/javascript">
  $('#bantuan').on('submit', function(e){
    e.preventDefault();
    var mydata = $("form#bantuan")[0];
    var data = new FormData(mydata);

    $.ajax({
      url : '<?php echo base_url('bantuan/addMasalah')?>',
      data : data,
      enctype: 'multipart/form-data',
      processData: false,
      contentType: false,
      type : 'POST',
      success : function(hasil){
        console.log(hasil);
        var rs = $.parseJSON(hasil);
        swal({
          type : rs['icon'],
          text : rs['text']
        }).then( function(e) {
          if(rs['icon'] == "success"){
            location.replace(rs['direct']);
          }
        });
      },
    })
  })
</script>
