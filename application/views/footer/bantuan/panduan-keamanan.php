<?php $this->load->view('header'); ?>

    <!-- Page title -->
    <section id="page-title" data-parallax-image="images/parallax/5.jpg">
     <div class="container">
      <div class="page-title">
       <h1>Panduan Keamanan</h1>
       <span>Frequently Asked Questions</span>
   </div>
   <div class="breadcrumb">
       <ul>
        <li><a href="#">AAAAAAAHome</a>
        </li>
        <li><a href="#">Pages</a>
        </li>
        <li class="active"><a href="#">FAQs</a>
        </li>
    </ul>
</div>
</div>
</section>
<!-- end: Page title -->
<!-- Section -->
<section>
	<div class="container">
        <!--Accordion default-->
        <h3>Accordion</h3>
        <p>Adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam auam quaerat voluptatem.</p>
        <div class="accordion">
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-rocket"></i>Suscipit laboriosam</h5>
                <div class="ac-content">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.

                </div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-heart"></i>Aliquam voluptatem</h5>
                <div class="ac-content">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-shopping-cart"></i>Labore et dolore</h5>
                <div class="ac-content">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
            </div>
        </div>
        <!--END: Accordion default-->
        <hr class="space">

        <!--Accordion default-->
        <h3>Accordion</h3>
        <p>Adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam auam quaerat voluptatem.</p>
        <div class="accordion">
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-rocket"></i>Suscipit laboriosam</h5>
                <div class="ac-content">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.

                </div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-heart"></i>Aliquam voluptatem</h5>
                <div class="ac-content">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-shopping-cart"></i>Labore et dolore</h5>
                <div class="ac-content">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
            </div>
        </div>
        <!--END: Accordion default-->
        <hr class="space">
    </div>
</section>
<!-- end: Section -->

<?php $this->load->view('footer'); ?>
