<?php $this->load->view('header'); ?>

<!-- Page title -->
<section id="page-title" data-parallax-image="<?php echo base_url('assets'); ?>/images/footer/header-01.jpg" style="height: 250px">
   <div class="container">
      <div class="page-title header-a">
        <h1>FAQs</h1>
        <span>YEPS Indonesia</span>
    </div>
    <div class="breadcrumb">
     <ul>
        <li><a href="<?php echo base_url(''); ?>">Beranda</a>
        </li>
        <li><a href="#">Bantuan</a>
        </li>
        <li class="active"><a href="#">FAQs</a>
        </li>
    </ul>
</div>
</div>
</section>
<!-- end: Page title -->
<!-- Section -->
<section>
	<div class="container">
        <!--Accordion default-->
        <h3>Tanya Jawab</h3>
        <div class="accordion">
        <?php foreach ($data as $d) {?>
            <div class="ac-item">
                <h5 class="ac-title"><i class="fab fa-bandcamp"></i><?php echo $d['question']; ?></h5>
                <div class="ac-content"><?php echo $d['answer']; ?></div>
            </div>
        <?php } ?>
        </div>
        <!-- <p>Adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam auam quaerat voluptatem.</p> -->
        <!--<div class="accordion">
            <div class="ac-item ac-active">
                <h5 class="ac-title"><i class="fas fa-user-tie"></i>Apa itu YEPS?</h5>
                <div class="ac-content">YEPS adalah Platform digital yang memudahkan klien untuk membuat suatu acara/event dan juga wadah serta pasar bagi para vendor/penyedia jasa event. YEPS bukan penyedia layanan/produk melainkan hanya sebagai perantara. YEPS merupakan solusi untuk Anda yang ingin menyelanggarakan event ataupun untuk para vendor yang ingin mengembangkan usaha bisnisnya.
                </div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title"><i class="fab fa-bandcamp"></i>Bagaimana saya dapat bergabung menjadi Vendor YEPS?</h5>
                <div class="ac-content">Jika Anda sebagai orang yang mempunyai jasa pelayanan sesuai dengan kategori kami, maka silakan klik halaman vendor untuk daftar menjadi vendor. Untuk informasi selengkapnya Anda dapat menghubungi<span style="text-decoration: underline; color: #107ade"> support@YEPSindonesia.com</span></div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title"><i class="fab fa-bandcamp"></i>Bagaimana cara menggunakan YEPS?</h5>
                <div class="ac-content">Anda hanya perlu buka website kami di YEPSindonesia.com kemudian log in dan pilih Vendor sesuai dengan keinginan Anda. Anda juga dapat mengunduh aplikasi YEPS Indonesia melalui Google Play Store untuk pengguna Android dan Apps Store untuk pengguna ios. Anda dapat menggunakan aplikasi YEPS dengan cara mengunduh aplikasi YEPS Indonesia di Google Play Store untuk pengguna Android dan Apps Store untuk pengguna ios.</div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title"><i class="fab fa-bandcamp"></i>Apakah ada biaya tambahan lain apabila saya ingin membooking vendor yang ada di YEPS?</h5>
                <div class="ac-content">Tidak Ada, namun jika dalam hal dan situasi tertentu diperlukan maka kami akan melakukan konfirmasi terlebih dahulu kepada Anda.
                </div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title"><i class="fab fa-bandcamp"></i>Bagaimana sistem pembayaran di YEPS?</h5>
                <div class="ac-content">Untuk saat ini system pembayaran hanya dapat menggunakan metode transfer bank atau setor tunai.</div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title"><i class="fab fa-bandcamp"></i>Apakah ada jaminan dana saya aman apabila melakukan transaksi di YEPS?</h5>
                <div class="ac-content">Kami dapat menjamin dana Anda aman karena transparansi dari setiap transaksi yang berlangsung. Kami juga mempunyai security system untuk mengontrol setiap transaksi yang berlangsung. Mohon untuk selalu melampirkan bukti pembayaran Anda dan menyimpan bukti pembayaran tersebut agar tidak disalah gunakan.</div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title"><i class="fab fa-bandcamp"></i>Apakah semua vendor yang ada di YEPS dapat dijamin kualitasnya?</h5>
                <div class="ac-content">Untuk kualitas dari setiap vendor, Anda dapat melihat langsung profil dan portofolio dari tiap vendor. Namun Anda tidak perlu khawatir, karena vendor yang ada di YEPS sudah kami review sebelum bergabung bersama kami.
                </div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title"><i class="fab fa-bandcamp"></i>Kalo event tersebut gagal apakah ada garansi atau ganti rugi dari YEPS?</h5>
                <div class="ac-content">Untuk kerugian non material kami tidak dapat menjamin dikarenakan kualitas dari setiap vendor berbeda-beda. Untuk kerugian secara material kami siap bertanggung jawab apabila ada kecurangan/kelalaian dari pihak vendor.</div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title"><i class="fab fa-bandcamp"></i>Dapatkah saya melakukan penggantian/cancel vendor yang saya inginkan?</h5>
                <div class="ac-content">Anda dapat melakukan penggantian/ melakukan pembatalan vendor yang sudah Anda pesan apabila Anda belum melakukan konfirmasi pembayaran.</div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title"><i class="fab fa-bandcamp"></i>Mengapa saya harus menggunakan YEPS untuk menyelenggarakan event, bukankah lebih mudah jika menggunakan jasa event organizer?</h5>
                <div class="ac-content">Karena kami memberikan kemudahan, keamanan, kenyamanan dan kepuasan untuk Anda. Ketika Anda menggunakan YEPS untuk pelaksanaan event Anda, harga layanan vendor yang ada di situs YEPSindonesia.com sama dan tidak lebih dari yang vendor pasang diluar situs YEPS. Kami memiliki dukungan teknologi untuk memastikan keamanan pembayaran dan transparansi setiap transaksi yang berlangsung. Kami memiliki tim event manajement sebagai Quality Control dari setiap event yang di selenggarakan. Sebelum dan setelah event selesai, tim event manajemen kami akan melakukan review sebelum meneruskan dana ke pihak vendor.
                </div>
            </div>
        </div>-->
        <!--END: Accordion default-->
        <hr class="space">

        <!--Accordion default-->
        <!-- <h3>Accordion</h3>
        <p>Adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam auam quaerat voluptatem.</p>
        <div class="accordion">
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-rocket"></i>Suscipit laboriosam</h5>
                <div class="ac-content">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.

                </div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-heart"></i>Aliquam voluptatem</h5>
                <div class="ac-content">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title"><i class="fa fa-shopping-cart"></i>Labore et dolore</h5>
                <div class="ac-content">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
            </div>
        </div> -->
        <!--END: Accordion default-->
        <hr class="space">
    </div>
</section>
<!-- end: Section -->

<?php $this->load->view('footer'); ?>
