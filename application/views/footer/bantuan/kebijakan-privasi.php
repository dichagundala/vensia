<?php $this->load->view('header'); ?>

<!-- Page title -->
<section id="page-title" data-parallax-image="<?php echo base_url('assets'); ?>/images/footer/header-01.jpg" style="height: 250px">
   <div class="container">
      <div class="page-title header-a">
        <h1>Kebijakan Privasi</h1>
         <span>YEPS Indonesia</span>
     </div>
     <div class="breadcrumb">
         <ul>
            <li><a href="<?php echo base_url(''); ?>">Beranda</a>
            </li>
            <li><a href="#">Bantuan</a>
            </li>
            <li class="active"><a href="#">Kebijakan Privasi</a>
            </li>
        </ul>
    </div>
</div>
</section>
<!-- end: Page title -->

<section>
	<div class="container">
        <!--Accordion default-->
        <h2 class="text-center">Kebijakan Privasi</h2>
        <p style="font-weight: bold; color: black" class="text-center">Privasi Anda adalah Prioritas kami</p>
        <p class="justify">Selamat datang di situs resmi <a href="https://yepsindonesia.com" target="_blank">www.yepsindonesia.com</a> yang dimiliki dan dikelola oleh PT. YEPS Solusi Acara Indonesia. Halaman ini menerangkan kepada Anda tentang bagaimana kami mengumpulkan, menggunakan, membagikan dan melindungi Data Pribadi yang kami terima dari Pengguna Layanan YEPS. Privasi anda sangat penting kami. Kami hanya akan mengumpulkan data yang relevan dengan transaksi antara kami dengan Anda.</p>

        <p class="justify">Anda dapat mengunjungi platform YEPS tanpa harus memasukan Data Pribadi Anda. Selama kunjungan Anda ke situs YEPS, Privasi Anda akan tetap terjaga dan kami tidak dapat mengidentifikasi Anda kecuali Anda mendaftar dan memiliki akun dalam situs YEPS dan masuk ke situs kami menggunakan username dan password Anda.</p>

        <p class="justify">Data Pribadi adalah data Anda yang benar dan akurat ataupun tidak. Anda dapat teridentifikasi dari data tersebut atau dari data-data lainnya. Data Pribadi dapat mencakup nama, nomor telepon, alamat, dll.</p>

        <p class="justify">Dengan menggunakan layanan kami berarti Anda menyetujui Data Pribadi Anda untuk kami Kumpulkan, gunakan, bagikan, dan lindungi seperti yang diatur dalam Kebijakan Privasi ini. Apabila Anda tidak memberikan izin kepada kami untuk mengolah data seperti yang diatur dalam Kebijakan Privasi ini, tolong jangan melanjutkan menggunakan layanan kami atau mengakses situs kami. </p>

        <p class="justify">Kami sewaktu-waktu dapat mengubah Kebijakan Privasi tanpa pemberitahuan terlebih dahulu sebelumnya. Kami akan memposting segala perubahan Kebijakan Privasi di situs kami untuk informasi Anda.</p>
        <div class="accordion">
            <div class="ac-item">
                <h5 class="ac-title">1. PENGUMPULAN DATA PRIBADI</h5>
                <div class="ac-content">Kami akan mengumpulkan Data Pribadi Anda saat : 
                    <ol class="alpa">
                        <li class="m-t-10">Saat Anda melakukan registrasi akun di situs yepsindonesia.com, serta saat Anda menggunakan layanan kami;</li>
                        <li class="m-t-10">Saat Anda berinteraksi dengan kami melalui telepon, surat, pertemuan tatap muka, media sosial, dan email;</li>
                        <li class="m-t-10">Saat Anda menggunakan layanan kami atau berinteraksi dengan kami melalui aplikasi atau menggunakan layanan website kami. Termasuk dengan tidak terbatas pada melalui cookies yang mungkin kami gunakan saat Anda berinteraksi dengan aplikasi atau situs web kami;</li>
                        <li class="m-t-10">Saat Anda melakukan transaksi melalui layanan kami;</li>
                        <li class="m-t-10">Saat Anda melakukan kritik dan saran atau keluhan untuk kami;</li>
                        <li class="m-t-10">Saat Anda mengirimkan Data Pribadi Anda dengan alasan apapun.</li>
                    </ol>
                    <p class="color-d justify">Aplikasi mobile kami mengumpulkan Data Pribadi tentang lokasi perangkat mobile Anda melalui GPS, Wi-Fi, dsb. Kami mengumpulkan, menggunakan, dan mengolah data ini untuk memberikan layanan sesuai lokasi yang Anda inginkan dan untuk mengirimkan konten-konten yang sesuai dengan lokasi Anda atau perangkat mobile Anda. Anda dapat tidak member izin kepada kami untuk memperoleh Data Pribadi terkait lokasi Anda melalui pengaturan perangkat Anda.</p>
                </div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title">2. DATA PRIBADI YANG DIKUMPULKAN</h5>
                <div class="ac-content">Saat Anda membuat akun YEPS, atau memberikan Data Pribadi melalui Platform, data yang kami kumpulkan dapat berupa :
                    <ol class="alpa">
                        <div class="row">
                            <div class="col-md-6">
                                <li class="m-t-10">Nama</li>
                                <li class="m-t-10">Alamat Anda</li>
                                <li class="m-t-10">Alamat tempat acara diselenggarakan</li>
                                <li class="m-t-10">Alamat email</li>
                                <li class="m-t-10">Nomor Telepon</li>
                            </div>
                            <div class="col-md-6">
                                <li class="m-t-10">Nomor Ponsel</li>
                                <li class="m-t-10">Jenis Kelamin</li>
                                <li class="m-t-10">Tanggal Lahir</li>
                                <li class="m-t-10">Nomor Rekening</li>
                            </div>
                        </div>
                    </ol>
                    <p>Kami dapat meminta Anda untuk memberikan data ulasan Anda setelah menggunakan layanan YEPS, termasuk alamat protokol internet (“IP”) Anda, data lokasi geografis, jenis sistem pengoperasian, kewarganegaan, preferensi pencarian, dan lainnya terkait penggunaan layanan dalam platform kami.</p>

                    <p>Anda mungkin/harus menyerahkan Data Pribadi Anda yang akurat kepada kami, vendor, dan dalam platform. Anda harus memperbaharui Data Pribadi Anda apabila ada perubahaan. Kami berhak memverifikasi kesesuaian/kebenaran Data yang Anda berikan.</p>

                    <p>Kami hanya akan mengumpulkan Data Pribadi Anda jika Anda memberikannya secara sukarela kepada kami. Jika Anda memilih untuk tidak menyerahkan Data Pribadi Anda kepada kami atau kemudian menarik persetujuan menggunakan Data Pribadi Anda, maka hal tersebut dapat menyebabkan kami tidak dapat memberikan layanan kami pada Anda. </p>

                </div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title">3. PENGGUNAAN DATA PRIBADI</h5>
                <div class="ac-content">
                    <p>Kami dapat mengumpulkan, menggunakan, membagikan, dan melindungi Data Pribadi yang kami terima dari Anda untuk satu atau lebih dari tujuan-tujuan berikut ini :
                    </p>
                    <ol class="alpa">
                        <li class="m-t-10">Mengolah aplikasi/transaksi Anda dengan kami atau transaksi Anda dengan pihak ketiga melalui Layanan;</li>
                        <li class="m-t-10">Mendaftar, mengelola, mengurus akses ke Situs kami;</li>
                        <li class="m-t-10">Mengoperasikan dan memberikan Anda layanan yang ditawarkan di Situs;</li>
                        <li class="m-t-10">Menghubungi Anda mengenai hal-hal yang berkaitan dengan penggunaan/akses ke situs kami serta layanan di situs kami, dan setiap pertanyaan/permintaan yang diajukan oleh Anda melalui situs kami atau sebaliknya; </li>
                        <li class="m-t-10">memberi tahu Anda tentang masalah layanan dan tindakan akun yang tidak lazim;</li>
                        <li class="m-t-10">menyesuaikan pengalaman Anda saat menggunakan layanan;</li>
                        <li class="m-t-10">mengukur dan meningkatkan pengalaman dan kepuasan pengguna;</li>
                        <li class="m-t-10">mempublikasikan ulasan pengguna, dalam format digital dan/atau cetak untuk akses publik;</li>
                        <li class="m-t-10">menegakkan Syarat dan Ketentuan;</li>
                        <li class="m-t-10">menyelesaikan sengketa atau pengaduan, mengumpulkan pembayaran atau biaya, atau memecahkan masalah; dan/atau</li>
                        <li class="m-t-10">mempertahankan dan memberikan setiap pembaruan perangkat lunak dan/atau pembaruan lainnya serta dukungan yang mungkin diperlukan dari waktu ke waktu untuk memastikan kelancaran Layanan kami;</li>
                        <li class="m-t-10">untuk tujuan lain yang diberitahukan kepada Anda pada saat pengumpulan.</li>
                    </ol>
                    <p>Kami mungkin akan menggunakan, membagikan dan mengolah Data Pribadi Anda tergantung tujuan tersebut dan tujuan yang mungkin tidak tercantum diatas. Kami akan mengatakan tujuan tersebut kepada Anda untuk memperoleh persetujuan dari Anda. Kecuali diizinkan oleh Undang –undang yang berlaku di Republik Indonesia.</p>
                </div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title">4. BERBAGI DATA PRIBADI </h5>
                <div class="ac-content">
                    <p>Data Pribadi Anda dapat dibagikan oleh YEPS kepada perusahaan yang bekerja sama dengan YEPS. YEPS juga dapat menyingkapkan Data Pribadi Anda kepada penyedia layanan pihak ketiga dari waktu ke waktu. Layanan pihak ketiga termasuk namun tidak terbatas pada hosting situs web, analisis data, pemasaran, memproses transaksi kartu kredit, dan penyedia layanan.
                    </p>
                    <p>YEPS dapat membagikan Data Pribadi Anda dalam situasi berikut :</p>
                    <ol class="alpa">
                        <li class="m-t-10">Mengolah aplikasi/transaksi Anda dengan kami atau transaksi Anda dengan pihak ketiga melalui Layanan;</li>
                        <li class="m-t-10">Untuk mengadakan atau membela terhadap klaim atau gugatan apapun;</li>
                        <li class="m-t-10">Untuk mematuhi perintah pengadilan, proses peradilan, permintaan yang sah, surat perintah atau setara oleh pejabat penegak hukum atau pihak yang berwenang;</li>
                        <li class="m-t-10">Untuk menyelidiki penipuan atau kesalahan lainnya, atau seperti yang dipersyaratkan lainnya atau diperlukan dalam rangka mematuhi hukum yang berlaku, atau untuk melindungi kepentingan sah kami;</li>
                        <li class="m-t-10">Untuk menegakkan atau menerapkan syarat dan ketentuan yang berlaku bagi produk dan layanan kami;</li>
                        <li class="m-t-10">Untuk melindungi hak, properti atau keamanan YEPS, Pengguna Situs lainnya, atau orang lain manapun sesuai dengan kebijaksanaan YEPS; dan</li>
                        <li class="m-t-10">Untuk setiap situasi lain yang diperbolehkan menurut hukum.</li>
                    </ol>
                </div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title">5. PERSETUJUAN PENGGUNA</h5>
                <div class="ac-content">
                    <p>Dengan menjelajah, menggunakan layanan dari situs dan ketika Anda membuat akun baru pada situs, maka Anda memberikan izin kepada YEPS untuk mengumpulkan, menggunakan, membagikan, dan mengolah Data Pribadi yang disebutkan diatas untuk tujuan seperti yang telah dijelaskan diatas.</p>
                </div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title">6. PENARIKAN PERSETUJUAN</h5>
                <div class="ac-content">
                    <p>Anda dapat menarik persetujuan atas pengumpulan, penggunaan, pembagian dan pengelolaan Data Pribadi Anda yang kami lakukan dengan memberikan pemberitahuan yang beralasan kepada kami setiap saat. Kami akan berhenti mengumpulkan, menggunakan, membagi dan mengelola Data Pribadi Anda kecuali diwajibkan oleh hukum. 
                    </p>
                    <p>Apabila Anda menarik persetujuan atas pengumpulan, penggunaan, pembagian dan pengelolaan kami atas Data Anda, mungkin kami tidak dapat meneruskan memberi layanan kami kepada Anda dan Anda setuju bahwa kami tidak akan bertanggung jawab atas kerugian yang timbul akibat penghentian layanan tersebut.
                    </p>
                </div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title">7. COOKIES</h5>
                <div class="ac-content">
                    <p>Kami mungkin akan mengimplementasikan cookies atau fitur lainnya untuk membantu kami mengumpulkan atau berbagi data yang akan membantu kami meningkatkan layanan yang kami tawarkan dan membantu kami menawarkan fitur dan layanan baru. Cookies adalah pengidentifikasi yang kami transfer ke computer atau perangkat lunak Anda untuk mengenali komputer atau perangkat Anda dan memberitahukan  kepada kami bagaimana dan kapan layanan atau situs web digunakan atau di kunjungi, oleh berapa banyak orang serta melacak pergerakan di dalam situs web kami. Kami dapat memasang cookies di Data Pribadi Anda. Cookies juga terpasang pada data layanan yang telah Anda pilih dan halaman yang telah Anda lihat. Data ini berguna untuk memantau keranjang (cart) Anda. Cookies juga berguna untuk mengirim konten yang sesuai dengan minat Anda.</p>
                    <p>Anda dapat menonaktifkan penggunaan cookies pada pengaturan browser Anda. Tapi harap diperhatikan bahwa apabila Anda mematikan penggunaan cookies Anda mungkin tidak dapat menggunakan fungsi situs atau layanan kami.</p>
                </div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title">8. MENGUBAH DATA PRIBADI YANG TELAH DIBERIKAN</h5>
                <div class="ac-content">
                    <p>Saat Anda memberikan Data Pribadi kepada kami, pastikan bahwa data yang Anda berikan benar dan tepat. Jika terdapat kesalahan dalam Data Pribadi Anda, Anda dapat memperbaikinya dengan login ke dalam situs kami dan memperbaikinya. </p>
                    <p>Jika terdapat Data Pribadi yang tidak dapat diperbaiki melalui situs, harap hubungi kami pada kontak yang tertera diakhir Kebijakan Privasi ini untuk mengakses Data Pribadi Anda yang dibawah kendali kami. </p>
                    <p>Untuk melindungi privasi Anda, kami akan melakukan verifikasi identitas Anda sebelum memberikan akses untuk merubah Data Pribadi Anda. Kami akan membalas permintaan Anda untuk akses ke Data Pribadi Anda sesegera mungkin.</p>
                </div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title">9. MELINDUNGI DATA PRIBADI</h5>
                <div class="ac-content">
                    <p>Kami melindungi Data Pribadi di bawah kepemilikan atau kendali kami dengan mempertahankan pengaturan keamanan yang wajar, termasuk prosedur fisik, teknis dan organisasi, untuk mencegah akses, pengumpulan, penggunaan, pembagian, penyalinan, modifikasi, penghapusan yang tidak sah atau risiko yang sama.</p>
                </div>
            </div>
            <div class="ac-item">
                <h5 class="ac-title">10. PENDAFTARAN</h5>
                <div class="ac-content">
                    <p>Kami akan memberikan Anda 1 akun apabila Anda telah mendaftar di situs kami. Dengan mendaftar di situs kami, maka Anda menjamin bahwa sekurang-kurangnya Anda berusia 18 tahun dan data yang Anda berikan benar, akurat dan lengkap seperti yang diperlukan dalam formulir pendaftaran di situs.</p>
                    <p>Khusus untuk Vendor, YEPS akan memberikan akun setelah Verifikasi Vendor selesai sesuai dengan tahapan tang ditentukan. Data akun akan dikirimkan YEPS kepada Vendor via Email atau Nomor Handphone milik Vendor yang terdaftar di sistem YEPS.</p>
                    <p>YEPS tidak bertanggung jawab atas kerugian atau kerusakan yang diderita oleh kami, Vendor ataupun klien yang kerugian atau kerusakannya tersebut disebabkan oleh ketidakakuratan atau tidak lengkapnya data yang diberikan oleh Anda.</p>
                    <p>Setelah mendaftar, Anda akan menerima username dan password. Anda bertanggung jawab untuk menjaga kerahasiaan username dan password milik Anda, dan Anda bertanggung jawab atas semua aktifitas akun Anda. Anda setuju akan segera melapor kepada YEPS apabila terjadi penyalahgunaan akun atau kata sandi atau pelanggaran keamanan lainnya. Pastikan Anda logout di setiap akhir sesi.</p>
                </div>
            </div>
            <div class="ac-item ac-active">
                <h5 class="ac-title">11. PERUBAHAN KEBIJAKAN PRIVASI</h5>
                <div class="ac-content">
                    <p>Dengan menggunakan situs dan layanan yang kami sediakan, artinya Anda setuju dan memberikan izin kepada kami atas pengumpulan, penggunaan, pembagian dan perlindungan data pribadi Anda sebagaimana diatur di dalam kebijakan privasi ini. Dari waktu ke waktu YEPS dapat merubah isi kebijakan privasi ini. Kami akan memposting segala perubahan Kebijakan Privasi di situs Kami untuk informasi Anda.</p>
                    <p>Jika Anda memiliki pertanyaan atau permintaan apapun yang berhubungan dengan Kebijakan Privasi YEPS, silakan hubungi Petugas Perlindungan Data di : <a href="mailto:privacy@yepsindonesia.com"><b>privacy@yepsindonesia.com</b></a></p>
                    <p>Silakan kirimkan semua pemberitahuan hukum di : <a href="mailto:legal@yepsindonesia.com"><b>legal@yepsindonesia.com</b></a></p>
                    <p>Kami akan merespon saran dan keluhan Anda sesegera mungkin.</p>
                </div>
            </div>
        </div>

        <p style="float: right">Efektif sejak : <span style="background-color: #107ade; color: white; font-weight: bold; padding: 5px">Juli 2018</span></p>

    </div>
</section>
<!-- end: Section -->

<?php $this->load->view('footer'); ?>
