 <?php $this->load->view('header'); ?>

 <!-- Page title -->
 <section id="page-title" data-parallax-image="<?php echo base_url('assets'); ?>/images/footer/header-01.jpg" style="height: 250px">
   <div class="container">
      <div class="page-title header-a">
        <h1>Registrasi Vendor</h1>
        <span>YEPS Indonesia</span>
    </div>
    <div class="breadcrumb">
     <ul>
        <li><a href="<?php echo base_url(''); ?>">Beranda</a>
        </li>
        <li><a href="#">Vendor</a>
        </li>
        <li class="active"><a href="#">Registrasi Vendor</a>
        </li>
    </ul>
</div>
</div>
</section>
<!-- end: Page title -->

<!-- Content -->
<section id="page-content" class="sidebar-left">
    <div class="container">
        <div class="row">
            <!-- post content -->
            <div class="content col-md-9">

                <h2 id="section-1">Registrasi Vendor</h2>
                <h5>Daftarkan Vendor Anda ke YEPS dan tingkatkan transaksi usaha Anda bersama kami dengan berbagai kemudahan.</h5>
                <center>
                    <a class="btn btn-default btn-sm" href="https://vendor.yepsindonesia.com/daftar">Daftar Vendor</a>
                    <a class="btn btn-warning btn-sm" href="<?php echo base_url('syarat_ketentuan#section-5'); ?>">Syarat dan Ketentuan</a>
                </center>
                <div class="accordion radius m-t-10">
                    <div class="ac-item ac-active">
                        <h5 class="ac-title"><i class="fa fa-rocket"></i>Registrasi Vendor</h5>
                        <div class="ac-content">
                            <img src="<?php echo base_url('assets');?>/images/footer/layananvendor/registrasi-vendor/regist-v-1.jpg" alt="" style="width: 90%;">
                            <p>Buka halaman <a href="https://vendor.yepsindonesia.com/daftar" target="_blank"><b>www.vendor.yepsindonesia.com/daftar</b></a> untuk melakukan pendaftaran sebagai Vendor</p>
                            <div class="col-md-12">
                                Kemudian lengkapi data Vendor Anda sebagai berikut;
                                <ol>
                                    <li class="p-b-10 p-t-10">Nama Vendor</li>
                                    <li class="p-b-10">Deskripsi Vendor (Penjelasan mengenai usaha/ layanan yang Anda tawarkan</li>
                                    <li class="p-b-10">Email (Alamat email yang Anda gunakan, digunakan untuk pemberitahuan informasi tentang kami dan verifikasi untuk akun Anda.</li>
                                    <li class="p-b-10">No Telepon</li>
                                    <li class="p-b-10">Alamat;
                                        <ul>
                                            <li class="p-b-10">Provinsi</li>
                                            <li class="p-b-10">Kabupaten</li>
                                            <li class="p-b-10">Kecamatan</li>
                                            <li class="p-b-10">Kelurahan</li>
                                        </ul>
                                    </li>
                                    <li class="p-b-10">Upload Logo/ Gambar Vendor Anda</li>
                                    <li class="p-b-10">Pilih Kategori apa saja yang tersedia dalam vendor Anda</li>
                                    <li class="p-b-10">Website</li>
                                    <li>Social Media</li>
                                </ol>
                                Apabila semua data sudah terisi dengan lengkap, selanjutnya silakan klik <button class="btn btn-default btn-xs">Submit</button>
                            </div>
                            <hr>

                            <img src="<?php echo base_url('assets');?>/images/footer/layananvendor/registrasi-vendor/regist-v-2.jpg" alt="" style="width: 90%;">
                            <p>Kemudian Anda akan mendapatkan email dari Kami setelah melakukan pendaftaran. Dan pendaftaran Anda akan divalidasi oleh tim Kami maksimal 3x2 jam</p>
                            <hr>

                            <img src="<?php echo base_url('assets');?>/images/footer/layananvendor/registrasi-vendor/regist-v-3.jpg" alt="" style="width: 90%;">
                            <p>Anda akan mendapat email Validasi yang berisi email dan password untuk akses login padahal halaman vendor. Dan anda diharuskan segera untuk mengganti password.</p>
                            <hr>

                            <img src="<?php echo base_url('assets');?>/images/footer/layananvendor/registrasi-vendor/regist-v-4.jpg" alt="" style="width: 90%;">
                            <p>Kemudian login menggunakan akun yang ada di email Anda.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end: post content -->

            <!-- Sidebar-->
            <div class="sidebar col-md-3">
                <div class="pinOnScroll">
                    <!--Navigation-->
                    <div class="widget ">
                        <h3>Layanan Vendor</h3>
                        <div id="mainMenu" class="menu-vertical">
                            <div class="container">
                                <nav>
                                    <ul>
                                        <li class="active">
                                            <a href="<?php echo base_url('layananvendor/registrasi_vendor'); ?>"><i class="fa fa-arrow-circle-right"></i>Registrasi Vendor</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url('layananvendor/kelola_layanan'); ?>"><i class="fa fa-arrow-circle-right"></i>Kelola Layanan</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <!--end: Navigation-->
                </div>
            </div>
            <!-- end: Sidebar-->
        </div>
    </div>
</section>
<!-- end: Content -->
<?php $this->load->view('footer'); ?>
