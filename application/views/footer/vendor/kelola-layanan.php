 <?php $this->load->view('header'); ?>

 <!-- Page title -->
 <section id="page-title" data-parallax-image="<?php echo base_url('assets'); ?>/images/footer/header-01.jpg" style="height: 250px">
   <div class="container">
      <div class="page-title header-a">
        <h1>Kelola Layanan</h1>
        <span>YEPS Indonesia</span>
    </div>
    <div class="breadcrumb">
     <ul>
        <li><a href="<?php echo base_url(''); ?>">Beranda</a>
        </li>
        <li><a href="#">Vendor</a>
        </li>
        <li class="active"><a href="#">Kelola Layanan</a>
        </li>
    </ul>
</div>
</div>
</section>
<!-- end: Page title -->

<!-- Content -->
<section id="page-content" class="sidebar-left">
    <div class="container">
        <div class="row">
            <!-- post content -->
            <div class="content col-md-9">

                <h2 id="section-1">Kelola Layanan</h2>
                <div class="accordion radius">
                    <div class="ac-item ac-active">
                        <h5 class="ac-title"><i class="fa fa-rocket"></i>Cara Menambahkan Data Produk Vendor</h5>
                        <div class="ac-content">
                            <!-- Setelah Anda selesai melakukan registrasi dan mengisi data tentang layanan Anda, selanjutnya silakan masukan produk/layanan yang Anda tawarkan. Apabila Anda memiliki produk/layanan lebih dari satu, maka silakan masukan data produk/layanan Anda satu per satu.
                             -->
                             <img src="<?php echo base_url('assets');?>/images/footer/layananvendor/kelolalayanan/PANDUAN-02.jpg" alt="" style="width: 80%;">
                             <p>Masukkan email dan password anda pada halaman <a href="https://vendor.yepsindonesia.com" target="_blank" style="font-weight: bold"> www.vendor.yepsindonesia.com</a> berdasarkan email yang kami kirimkan ke Anda</p>
                             <hr>

                             <img src="<?php echo base_url('assets');?>/images/footer/layananvendor/kelolalayanan/PANDUAN-03.jpg" alt="" style="width: 80%;">
                             <p>Jika sudah masuk kedalam dashboard klik <span style="background-color: #107ade; color: white; padding: 5px">Tambah Produk</span> di <span style="background-color: #107ade; color: white; padding: 5px">Pengaturan Toko</span> (yang dilingkari warna merah pada gambar)</p>
                             <hr>

                             <img src="<?php echo base_url('assets');?>/images/footer/layananvendor/kelolalayanan/PANDUAN-04.jpg" alt="" style="width: 80%;">
                             <p>Pilih kategori sesuai dengan kategori vendor Anda</p>
                             <hr>

                             <img src="<?php echo base_url('assets');?>/images/footer/layananvendor/kelolalayanan/PANDUAN-05.jpg" alt="" style="width: 80%;">
                             <p>Masukkan semuda data produk / jasa Vendor Anda selengkap - lengkapnya</p>
                             <hr>

                             <img src="<?php echo base_url('assets');?>/images/footer/layananvendor/kelolalayanan/PANDUAN-06.jpg" alt="" style="width: 80%;">
                             <p>Kemudian pilih tombol <button class="btn btn-success btn-xs">simpan</button></p>
                             <hr>
                             <h5>Lihat Video Tutorial</h5>
                             <iframe width="1280" height="720" src="https://www.youtube.com/embed/EylGGU1aqus" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="ac-item">
                        <h5 class="ac-title"><i class="fa fa-rocket"></i>Update Informasi/Deskripsi Layanan</h5>
                        <div class="ac-content">
                        </div>
                    </div>
                </div>
            </div>
            <!-- end: post content -->

            <!-- Sidebar-->
            <div class="sidebar col-md-3">
                <div class="pinOnScroll">
                    <!--Navigation-->
                    <div class="widget ">
                        <h3>Layanan Vendor</h3>
                        <div id="mainMenu" class="menu-vertical">
                            <div class="container">
                                <nav>
                                    <ul>
                                        <li class="active">
                                            <a href="<?php echo base_url('layananvendor/registrasi_vendor'); ?>"><i class="fa fa-arrow-circle-right"></i>Registrasi Vendor</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url('layananvendor/kelola_layanan'); ?>"><i class="fa fa-arrow-circle-right"></i>Kelola Layanan</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <!--end: Navigation-->
                </div>
            </div>
            <!-- end: Sidebar-->
        </div>
    </div>
</section>
<!-- end: Content -->
<?php $this->load->view('footer'); ?>
