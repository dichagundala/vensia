 <?php $this->load->view('header'); ?>

 <!-- Page title -->
 <section id="page-title" data-parallax-image="<?php echo base_url('assets'); ?>/images/footer/header-01.jpg" style="height: 250px">
     <div class="container">
      <div class="page-title header-a">
        <h1>Layanan Pembayaran</h1>
        <span>YEPS Indonesia</span>
    </div>
    <div class="breadcrumb">
       <ul>
        <li><a href="<?php echo base_url(''); ?>">Beranda</a>
        </li>
        <li><a href="#">Klien</a>
        </li>
        <li class="active"><a href="#">Layanan Pembayaran</a>
        </li>
    </ul>
</div>
</div>
</section>
<!-- end: Page title -->

<!-- Content -->
<section id="page-content" class="sidebar-left">
    <div class="container">
        <div class="row">
            <!-- post content -->
            <div class="content col-md-9">

             <h2>Layanan Pembayaran</h2>
             <p>Setelah proses checkout, selajutnya Anda akan masuk ke Halaman Pembayaran. Anda dapat memilih metode pembayaran dan layanan angsuran pembayaran.</p>
             <div class="accordion radius">
                <div class="ac-item ac-active">
                    <h5 class="ac-title">1. Pilih Angsuran Pembayaran</h5>
                    <div class="ac-content">
                        <div class="col-md-6">
                            <img style="max-width: 350px" src="<?php echo base_url('assets'); ?>/images/footer/layananklien/pemesanan/pemesanan-1.jpg">
                        </div>
                        <div class="col-md-6">
                            Klien di permudah dengan layanan pembayaran DP sebesar 30% dari total pembayaran. Kemudian silakan pilih Angsuran Pembayaran yang Anda inginkan, tersedia 1 hingga 5 tahap pembayaran.
                        </div>
                    </div>
                </div>
                <div class="ac-item">
                    <h5 class="ac-title">2.  Pilih Metode Pembayaran</h5>
                    <div class="ac-content">
                        <div class="col-md-6">
                            <img style="max-width: 350px" src="<?php echo base_url('assets'); ?>/images/footer/layananklien/pemesanan/pemesanan-1.jpg">
                        </div>
                        <div class="col-md-6">
                            Kami menyediakan beberapa metode pembayaran untuk transaksi Anda;
                            <ol>
                                <li class="p-b-10">Payment Gateway : Mitrans</li>
                                <li class="p-b-10">Transfer Bank PT YEPS SOLUSI ACARA INDONESIA</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="ac-item">
                    <h5 class="ac-title">3. Minta konfirmasi Ketersediaan Pesanan</h5>
                    <div class="ac-content">
                        <div class="col-md-6">
                            <img style="max-width: 350px" src="<?php echo base_url('assets'); ?>/images/footer/layananklien/pemesanan/pemesanan-1.jpg">
                        </div>
                        <div class="col-md-6">
                            Setelah klien memproses dengan klik lanjut ke pembayaran aka nada fitur “Tunggu konfirmasi ketersediaan pesanan” sebelum Time remaining pembayaran aktif.<br><br>
                            Proses tunggu konfirmasi ketersediaan pesanan berdurasi maksimal 3 jam setelah klien melanjutkan ke pembayaran dari menu metode pembayaran sebelumnya.<br><br>
                            Setelah semua vendor melakukan konfirmasi ketersediaan, maka status Tunggu konfirmasi ketersediaan pesanan berubah dari <span style="background-color: red; color: white">WAITING</span> menjadi <span style="background-color: green; color: white">OK</span>.
                        </div>
                    </div>
                </div>
                <div class="ac-item">
                    <h5 class="ac-title">4. Vendor Menolak Pesanan </h5>
                    <div class="ac-content">
                        <div class="row m-b-25">
                            <div class="col-md-6">
                                <img style="max-width: 350px" src="<?php echo base_url('assets'); ?>/images/footer/layananklien/pemesanan/pemesanan-1.jpg">
                            </div>
                            <div class="col-md-6">
                                Tidak semua vendor dapat menerima pesanan Anda dalam keadaan tertentu. Misalkan anda memesan 10 layanan/produk baik dalam satu order di kategori vendor yang berbeda atau mungkin masih dalam satu vendor yang sama, maka setelah vendor mengonfirmasi ketersediaan pesanan akan tampil detail pesanan yang tersedia maupun tidak tersedia melalui email atau pada boarder pemesanan Anda. 
                            </div>
                        </div>
                        <div class="row m-b-25">
                            <div class="col-md-6">
                                <img style="max-width: 350px" src="<?php echo base_url('assets'); ?>/images/footer/layananklien/pemesanan/pemesanan-1.jpg">
                            </div>
                            <div class="col-md-6">
                                Pada halaman tersebut, manajemen YEPS akan menampilkan rekomendasi vendor sejenis yang mendekati harga, model layanan/produk dan fasilitas dari pesanan anda sebelumnya. 
                            </div>
                        </div>
                        <div class="col-md-12 m-b-25 text-center">
                            Kami juga akan menghubungi anda untuk melakukan konfirmasi perihal tersebut.
                        </div>
                        <div class="row m-b-25">
                            <div class="col-md-6">
                                <img style="max-width: 350px" src="<?php echo base_url('assets'); ?>/images/footer/layananklien/pemesanan/pemesanan-1.jpg">
                            </div>
                            <div class="col-md-6">
                                Jika Anda menerima hasil ketersediaan vendor dan rekomendasi pengganti vendor dari kami, Anda dapat menyetujui nya dengan meng klik tombol setuju kemudian lanjutkan.
                            </div>
                        </div>
                        <div class="row m-b-25">
                            <div class="col-md-6">
                                <img style="max-width: 350px" src="<?php echo base_url('assets'); ?>/images/footer/layananklien/pemesanan/pemesanan-1.jpg">
                            </div>
                            <div class="col-md-6">
                                Namun jika anda tidak bersedia dengan hasil ketersediaan vendor dan rekomendasi pengganti vendor dari kami tersebut anda dapat membatalkan vendor pilihan dari kami dan melanjutkannya dengan perintah <span style="background-color: yellow;">setuju & lanjutkan</span>. Atau anda dapat <span style="background-color: red; color: white">membatalkan semua pesanan</span> dengan cara klik Batalkan semua pesanan dan anda dapat mengulangi pemesanan dari awal.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ac-item">
                    <h5 class="ac-title">5. Time Remaining Pembayaran Aktif</h5>
                    <div class="ac-content">
                        Setelah vendor sudah mengonfirmasi ketersediaan pesanan Anda, silakan klik konfirmasi pesanan maka time remaining pada halaman akan aktif. Lama durasi time remaining pembayaran adalah 24 jam. Anda diwajibkan menyelesaikan pembayaran sesuai instruksi dan perintah yang ada selama masa time remaining berlaku, dalam masa ini YEPS layanan klien akan melakukan remainder dan follow up agar proses pemesanan berjalan dengan baik.
                    </div>
                </div>
                <div class="ac-item">
                    <h5 class="ac-title">6. Layanan Pembayaran</h5>
                    <div class="ac-content">
                        Jika Anda sudah melakukan pembayaran sesuai dengan nominal dan ke nomor rekening tujuan yang tertera, pembayaran anda akan terkonfirmasi secara otomatis oleh sistem dan disertai bukti pemesanan yang akan dikirim ke email anda dan konfirmasi via telfon oleh layanan klien YEPS.
                    </div>
                </div>
                <div class="ac-item">
                    <h5 class="ac-title">6. Layanan Pembayaran</h5>
                    <div class="ac-content">
                        Jika Anda sudah melakukan pembayaran sesuai dengan nominal dan ke nomor rekening tujuan yang tertera, pembayaran anda akan terkonfirmasi secara otomatis oleh sistem dan disertai bukti pemesanan yang akan dikirim ke email anda dan konfirmasi via telfon oleh layanan klien YEPS.
                    </div>
                </div>
                <div class="ac-item">
                    <h5 class="ac-title">7. Konfirmasi Pembayaran Anda Secara Manual</h5>
                    <div class="ac-content">
                        <div class="col-md-6">
                            <img style="max-width: 350px" src="<?php echo base_url('assets'); ?>/images/footer/layananklien/pemesanan/pemesanan-1.jpg">
                        </div>
                        <div class="col-md-6">
                            Setelah anda melakukan pembayaran anda juga dapat mengkonfirmasi pembayaran anda secara manual dengan mengklik “konfirmasi pembayaran” dan unggah foto bukti transaksi pembayaran anda secara manual.
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- end: post content -->

        <!-- Sidebar-->
        <div class="sidebar col-md-3">
            <div class="pinOnScroll">
                <!--Navigation-->
                <div class="widget ">
                    <h3>Layanan Klien</h3>
                    <div id="mainMenu" class="menu-vertical">
                        <div class="container">
                            <nav>
                                <ul>
                                    <li class="active">
                                        <a href="<?php echo base_url('layananklien/akun'); ?>"><i class="fa fa-arrow-circle-right"></i>Akun</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('layananklien/panduanpemesanan'); ?>"><i class="fa fa-arrow-circle-right"></i>Panduan Pemesanan</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('layananklien/layananpembayaran'); ?>"><i class="fa fa-arrow-circle-right"></i>Layanan Pembayaran</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('layananklien/refund'); ?>"><i class="fa fa-arrow-circle-right"></i>Refund</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('layananklien/eventmanagement'); ?>"><i class="fa fa-arrow-circle-right"></i>Layanan Management Event</a>
                                    </li>

                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--end: Navigation-->
            </div>
        </div>
        <!-- end: Sidebar-->
    </div>
</div>
</section>
<!-- end: Content -->

<?php $this->load->view('footer'); ?>
