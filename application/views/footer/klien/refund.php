 <?php $this->load->view('header'); ?>

 <!-- Page title -->
 <section id="page-title" data-parallax-image="<?php echo base_url('assets'); ?>/images/footer/header-01.jpg" style="height: 250px">
     <div class="container">
      <div class="page-title header-a">
        <h1>Refund</h1>
        <span>YEPS Indonesia</span>
    </div>
    <div class="breadcrumb">
       <ul>
        <li><a href="<?php echo base_url(''); ?>">Beranda</a>
        </li>
        <li><a href="#">Klien</a>
        </li>
        <li class="active"><a href="#">Refund</a>
        </li>
    </ul>
</div>
</div>
</section>
<!-- end: Page title -->

<!-- Content -->
<section id="page-content" class="sidebar-left">
    <div class="container">
        <div class="row">
            <!-- post content -->
            <div class="content col-md-9">

                <h2>Refund</h2>
                <p><b>Refund adalah</b> Sejumlah uang yang akan dikembalikan kepada Anda setelah Anda melakukan pembatalan atas pesanan yang telah Anda lakukan tergantung pada pesanan yang bersifat dapat di refund.</p>
                <p>Refund yang telah diajukan tidak dapat dibatalkan.</p>
                <div class="accordion radius">
                    <div class="ac-item ac-active">
                        <h5 class="ac-title"><i class="fa fa-rocket"></i>Ketentuan Refund</h5>
                        <div class="ac-content">
                            <!-- <div class="col-md-6">
                                <img style="max-width: 350px" src="<?php echo base_url('assets'); ?>/images/footer/layananklien/pemesanan/pemesanan-1.jpg">
                            </div> -->
                            <div class="col-md-12">
                                Klien hanya dapat mengajukan Refund dengan ketentuan sebagai berikut :
                                <ul>
                                    <li class="p-b-10">Klien baru menyelesaikan pembayaran DP sebesar 30%, terhitung : </li>
                                    <ul>
                                        <li class="p-b-10">Maksimal 3 hari setelah masa pembayaran DP maka uang akan dikembalikan sebesar 20% dari DP yang telah dibayar oleh Klien.</li>
                                        <li class="p-b-10">Maksimal 1 minggu setelah masa pembayaran DP maka uang akan dikembalikan sebesar 10% dari DP yang telah dibayar oleh Klien.</li>
                                    </ul>
                                    <li>Proses refund hanya dapat dilakukan dalam platform YEPS.</li>
                                </ul>
                                YEPS akan meninjau permintaan refund dari Klien untuk memberikan konfirmasi apakah proses refund diterima atau ditolak.
                            </div>
                        </div>
                    </div>
                    <div class="ac-item">
                        <h5 class="ac-title"><i class="fa fa-heart"></i>Proses Refund</h5>
                        <div class="ac-content">
                            <ol>
                                <li class="p-b-10">Klien akan mendapatkan konfirmasi persetujuan atas refund yang dilakukan.</li>
                                <li class="p-b-10">Proses refund akan dilakukan ke kartu debit Klien sesuai dengan yang Klien gunakan untuk bertransaksi dalam situs YEPS.</li>
                                <li class="p-b-10">Refund sesama bank diproses dalam waktu 1x24 jam kerja</li>
                                <li class="p-b-10">Refund antar bank akan diproses dalam waktu 2x24 jam kerja</li>
                            </ol>
                        </div>
                    </div>

                    <div class="ac-item">
                        <h5 class="ac-title"><i class="fa fa-heart"></i>Cara Refund</h5>
                        <div class="ac-content">
                            <div class="row m-b-25">
                                <div class="col-md-6">
                                    <img style="max-width: 350px" src="<?php echo base_url('assets'); ?>/images/footer/layananklien/pemesanan/pemesanan-1.jpg">
                                </div>
                                <div class="col-md-6">
                                    1.  Log In kedalam Akun YEPS Anda
                                </div>
                            </div>
                            <div class="row m-b-25">
                                <div class="col-md-6">
                                    <img style="max-width: 350px" src="<?php echo base_url('assets'); ?>/images/footer/layananklien/pemesanan/pemesanan-1.jpg">
                                </div>
                                <div class="col-md-6">
                                    2.  Klik submenu <b>PESANAN SAYA</b> pada halaman dashboard Anda 
                                </div>
                            </div>
                            <div class="row m-b-25">
                                <div class="col-md-6">
                                    <img style="max-width: 350px" src="<?php echo base_url('assets'); ?>/images/footer/layananklien/pemesanan/pemesanan-1.jpg">
                                </div>
                                <div class="col-md-6">
                                    3.  Cek detail pesanan Anda lalu pilih pesanan yang ingin Anda refund. Lalu klik tombol detail.
                                </div>
                            </div>
                            <div class="row m-b-25">
                                <div class="col-md-6">
                                    <img style="max-width: 350px" src="<?php echo base_url('assets'); ?>/images/footer/layananklien/pemesanan/pemesanan-1.jpg">
                                </div>
                                <div class="col-md-6">
                                    4.  Pada detail pesanan Anda, klik tombol <b>Refund </b>
                                </div>
                            </div>
                            <div class="row m-b-25">
                                <div class="col-md-6">
                                    <img style="max-width: 350px" src="<?php echo base_url('assets'); ?>/images/footer/layananklien/pemesanan/pemesanan-1.jpg">
                                </div>
                                <div class="col-md-6">
                                    5.  Pada detail pesanan Anda, klik tombol Refund. Kemudian isi formulir dengan mengikuti keterangan yang ada.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- end: post content -->

            <!-- Sidebar-->
            <div class="sidebar col-md-3">
                <div class="pinOnScroll">
                    <!--Navigation-->
                    <div class="widget ">
                        <h3>Layanan Klien</h3>
                        <div id="mainMenu" class="menu-vertical">
                            <div class="container">
                                <nav>
                                    <ul>
                                        <li class="active">
                                            <a href="<?php echo base_url('layananklien/akun'); ?>"><i class="fa fa-arrow-circle-right"></i>Akun</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url('layananklien/panduanpemesanan'); ?>"><i class="fa fa-arrow-circle-right"></i>Panduan Pemesanan</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url('layananklien/layananpembayaran'); ?>"><i class="fa fa-arrow-circle-right"></i>Layanan Pembayaran</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url('layananklien/refund'); ?>"><i class="fa fa-arrow-circle-right"></i>Refund</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url('layananklien/eventmanagement'); ?>"><i class="fa fa-arrow-circle-right"></i>Layanan Management Event</a>
                                        </li>

                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <!--end: Navigation-->
                </div>
            </div>
            <!-- end: Sidebar-->
        </div>
    </div>
</section>
<!-- end: Content -->
<?php $this->load->view('footer'); ?>
