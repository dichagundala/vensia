 <?php $this->load->view('header'); ?>

 <!-- Page title -->
 <section id="page-title" data-parallax-image="<?php echo base_url('assets'); ?>/images/footer/header-01.jpg" style="height: 250px">
     <div class="container">
      <div class="page-title header-a">
        <h1>Akun</h1>
        <span>YEPS Indonesia</span>
    </div>
    <div class="breadcrumb">
       <ul>
        <li><a href="<?php echo base_url(''); ?>">Beranda</a>
        </li>
        <li><a href="#">Klien</a>
        </li>
        <li class="active"><a href="#">Akun</a>
        </li>
    </ul>
</div>
</div>
</section>
<!-- end: Page title -->

<!-- Content -->
<section id="page-content" class="sidebar-left">
    <div class="container">
        <div class="row">
            <!-- post content -->
            <div class="content col-md-9">

             <h2>Akun</h2>
             <div class="accordion radius">
                <div class="ac-item ac-active">
                    <h5 class="ac-title"><i class="fa fa-rocket"></i>Panduan Registrasi</h5>
                    <div class="ac-content">Masuk ke halaman <a href="https://private.yepsindonesia.com/authss/register/" style="color: #107ade">Registrasi akun</a>
                        <img src="<?php echo base_url('assets');?>/images/footer/layananklien/akun/regist-u-1.jpg" alt="" style="width: 90%;">
                        <p>Lengkapi data Anda sesuai dengan form yang ada.</p>
                        <hr>

                        <img src="<?php echo base_url('assets');?>/images/footer/layananklien/akun/regist-u-2.jpg" alt="" style="width: 90%;">
                        <p>Kemudian Anda akan menerima email dari Kami lalu aktifkan akun Anda dengan menekan tombol <button class="btn btn-default btn-xs">Aktifkan Akun</button></p>
                    </div>
                </div>
                <div class="ac-item">
                    <h5 class="ac-title"><i class="fa fa-heart"></i>Pengaturan Akun</h5>
                    <div class="ac-content">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</div>
                </div>
            </div>

            <hr class="space">

            <a href="html" class="btn scroll-to"><span><i class="fa fa-arrow-up"></i>Kembali Keatas</span></a>

        </div>
        <!-- end: post content -->

        <!-- Sidebar-->
        <div class="sidebar col-md-3">
            <div class="pinOnScroll">
                <!--Navigation-->
                <div class="widget ">
                    <h3>Layanan Klien</h3>
                    <div id="mainMenu" class="menu-vertical">
                        <div class="container">
                            <nav>
                                <ul>
                                    <li class="active">
                                        <a href="<?php echo base_url('layananklien/akun'); ?>"><i class="fa fa-arrow-circle-right"></i>Akun</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('layananklien/panduanpemesanan'); ?>"><i class="fa fa-arrow-circle-right"></i>Panduan Pemesanan</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('layananklien/layananpembayaran'); ?>"><i class="fa fa-arrow-circle-right"></i>Layanan Pembayaran</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('layananklien/refund'); ?>"><i class="fa fa-arrow-circle-right"></i>Refund</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('layananklien/eventmanagement'); ?>"><i class="fa fa-arrow-circle-right"></i>Layanan Management Event</a>
                                    </li>

                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--end: Navigation-->
            </div>
        </div>
        <!-- end: Sidebar-->
    </div>
</div>
</section>
<!-- end: Content -->
<?php $this->load->view('footer'); ?>
