 <?php $this->load->view('header'); ?>

 <!-- Page title -->
 <section id="page-title" data-parallax-image="<?php echo base_url('assets'); ?>/images/footer/header-01.jpg" style="height: 250px">
   <div class="container">
      <div class="page-title header-a">
        <h1>Panduan Pemesanan</h1>
        <span>YEPS Indonesia</span>
    </div>
    <div class="breadcrumb">
     <ul>
        <li><a href="<?php echo base_url(''); ?>">Beranda</a>
        </li>
        <li><a href="#">Klien</a>
        </li>
        <li class="active"><a href="#">Panduan Pemesanan</a>
        </li>
    </ul>
</div>
</div>
</section>
<!-- end: Page title -->

<!-- Content -->
<section id="page-content" class="sidebar-left">
    <div class="container">
        <div class="row">
            <!-- post content -->
            <div class="content col-md-9">

             <h2>Panduan Pemesanan</h2>
             <div class="accordion radius">
                <div class="ac-item ac-active">
                    <h5 class="ac-title"><i class="fa fa-rocket"></i>Registrasi dan Masuk</h5>
                    <div class="ac-content">
                        <img src="<?php echo base_url('assets');?>/images/footer/layananklien/pemesanan/order-u-1.jpg" alt="" style="width: 90%;">
                        <p>Sebelum melakukan pemesanan, anda di wajibkan untuk melakukan login.</p>
                        <hr>
                    </div>
                </div>
                <div class="ac-item">
                    <h5 class="ac-title"><i class="fa fa-heart"></i>Cara Memilih Vendor dan Cart Order</h5>
                    <div class="ac-content">
                        <img src="<?php echo base_url('assets');?>/images/footer/layananklien/pemesanan/order-u-2.jpg" alt="" style="width: 90%;">
                        <p>Pilih vendor yang Anda inginkan di daftar Kategori atau cari pada kolom Search (Anda bisa menyesuaikan deskripsi vendor berdasarkan deskripsi vendor yang anda inginkan).</p>
                        <hr>

                        <img src="<?php echo base_url('assets');?>/images/footer/layananklien/pemesanan/order-u-3.jpg" alt="" style="width: 90%;">
                        <p>Setelah memilih kategori Anda dapat memfilter Vendor berdasarkan waktu ketersediaan, tingkat performa (Rating), harga, dan Lokasi.</p>
                        <hr>

                        <img src="<?php echo base_url('assets');?>/images/footer/layananklien/pemesanan/order-u-4.jpg" alt="" style="width: 90%;">
                        <p>Klik pilih/tambahkan vendor kedalam keranjang pemesanan dengan menekan tombol <button class="btn btn-default btn-xs">add to cart</button></p>
                        <hr>

                        <img src="<?php echo base_url('assets');?>/images/footer/layananklien/pemesanan/order-u-5.jpg" alt="" style="width: 90%;">
                        <p>Pesanan klien tersedia di menu cart order. Pada tahap ini klien dapat melihat dan memastikan kembali vendor yang telah di pilih. Anda dapat menambah, mengubah dan membatalkan pesanan sebelum melakukan proses checkout.</p>
                        <hr>

                        <img src="<?php echo base_url('assets');?>/images/footer/layananklien/pemesanan/order-u-6.jpg" alt="" style="width: 90%;">
                        <p>Terdapat informasi jumlah total pembayaran atas pesanan Anda dengan rincian sub total, beban pajak, biaya event charge.</p>
                        <hr>
                        <p>Jika semua pesanan pada chart order sudah sesuai, anda dapat melanjutkan proses pemesanan dengan mengklik <button class="btn btn-default btn-xs">Konfirmasi Pesanan</button></p> <p>

                        <!-- <ol style="list-style-type: lower-alpha;">
                            <li class="p-b-10">Pilih vendor yang Anda inginkan di daftar Kategori atau cari pada kolom Search (Anda bisa menyesuaikan deskripsi vendor berdasarkan deskripsi vendor yang anda inginkan).</li>
                            <li class="p-b-10">Pilih Vendor berdasarkan waktu ketersediaan, tingkat performa (Rating), harga, dan Lokasi (Anda bisa mempelajari Profil Vendor).</li>
                            <li class="p-b-10">Klik pilih/tambahkan vendor kedalam keranjang pemesanan (chart order) </li>
                            <li class="p-b-10">Pesanan klien tersedia di menu chart order. Pada tahap ini klien dapat melihat dan memastikan kembali vendor yang telah di pilih. Anda dapat menambah, mengubah dan membatalkan pesanan sebelum melakukan proses checkout.</li>
                            <li class="p-b-10">Terdapat informasi jumlah total pembayaran atas pesanan Anda dengan rincian sub total, beban pajak, biaya event charge.</li>
                            <li class="p-b-10">Jika semua pesanan pada chart order sudah sesuai, anda dapat melanjutkan proses pemesanan dengan mengklik “proses untuk checkout”.</li>
                        </ol> -->
                    </div>
                </div>
            </div>
            
        </div>
        <!-- end: post content -->

        <!-- Sidebar-->
        <div class="sidebar col-md-3">
            <div class="pinOnScroll">
                <!--Navigation-->
                <div class="widget ">
                    <h3>Layanan Klien</h3>
                    <div id="mainMenu" class="menu-vertical">
                        <div class="container">
                            <nav>
                                <ul>
                                    <li class="active">
                                        <a href="<?php echo base_url('layananklien/akun'); ?>"><i class="fa fa-arrow-circle-right"></i>Akun</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('layananklien/panduanpemesanan'); ?>"><i class="fa fa-arrow-circle-right"></i>Panduan Pemesanan</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('layananklien/layananpembayaran'); ?>"><i class="fa fa-arrow-circle-right"></i>Layanan Pembayaran</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('layananklien/refund'); ?>"><i class="fa fa-arrow-circle-right"></i>Refund</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('layananklien/eventmanagement'); ?>"><i class="fa fa-arrow-circle-right"></i>Layanan Management Event</a>
                                    </li>

                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <!--end: Navigation-->
            </div>
        </div>
        <!-- end: Sidebar-->
    </div>
</div>
</section>
<!-- end: Content -->

<?php $this->load->view('footer'); ?>
