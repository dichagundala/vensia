<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="Vensia" />
    <meta name="description" content="Your Event Partner Solution">
    <!-- Document title -->
    <title>Vensia - Daftar</title>
    <!-- Stylesheets & Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,800,700,600|Montserrat:400,500,600,700|Raleway:100,300,600,700,800" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets'); ?>/css/plugins.css" rel="stylesheet">
    <link href="<?php echo base_url('assets'); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url('assets'); ?>/css/responsive.css" rel="stylesheet"> 
    <link href="<?php echo base_url('assets'); ?>/css/custom.css" rel="stylesheet"> 
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets'); ?>/images/favicon-16x16.png">
</head>


<body>

   <div id="loading" class="loader" style="position: absolute; margin: auto; height: auto; width: 100%; z-index: 9999; margin-top: 300px; display: none" align="center">
    <div class="loader-inner line-scale">
       <div></div>
       <div></div>
       <div></div>
       <div></div>
       <div></div>
   </div>
</div>
<!-- Wrapper -->
<div id="wrapper">

    <!-- Section -->
    <section class="fullscreen" style="background-image: url(<?php echo base_url('assets'); ?>/images/pages/backg-login.gif); background-repeat: no-repeat;background-size: cover;">
        <div class="container container-fullscreen">
            <div class="text-middle">
                <div class="row" style="margin-top: -40px">
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-1"></div>
                    <div class="col-md-4 text-center m-b-30">
                        <a href="<?php echo base_url(); ?>" class="logo hidden-xs">
                            <img src="<?php echo base_url('assets'); ?>/images/logo.png" alt="Vensia logo" style="max-height: 100px">
                        </a>
                        <a href="<?php echo base_url(); ?>" class="logo hidden-lg hidden-md hidden-sm">
                            <img src="<?php echo base_url('assets'); ?>/images/logo-dark.png" alt="Vensia logo" style="max-height: 100px;">
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-1"></div>
                    <div class="col-md-4 p-10 background-white b-r-6" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                        <form class="form-transparent-grey" id="form-register">
                            <div class="row">
                                <div class="col-md-12 p-l-20">
                                    <h3>Daftar di Vensia</h3>
                                    <p style="margin-top: -20px">Sudah punya akun di Vensia? Masuk <a href="<?php echo base_url('authss/login'); ?>/" style="color: #dc2977; font-weight: bold">disini</a></p>
                                </div>

                                <div class="col-md-12 form-group">
                                    <label class="sr-only">Nama Depan</label>
                                    <input type="text" name="firstName" placeholder="Nama Depan" class="form-control input-lg" required>
                                </div>
								<div class="col-md-12 form-group">
                                    <label class="sr-only">Nama Belakang</label>
                                    <input type="text" name="secondName" placeholder="Nama Belakang" class="form-control input-lg" required>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label class="sr-only">Email</label>
                                    <input type="email" name="email" placeholder="Email" class="form-control input-lg" required>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label class="sr-only">Kata Sandi</label>
                                    <input type="password" name="password" placeholder="Kata Sandi" class="form-control input-lg" required id="password-field">
                                    <span toggle="#password-field" class="field-icon fa fa-fw fa-eye toggle-password"></span>
                                </div>

                                <div class="col-md-12 form-group">
                                    <label class="sr-only">Nomor Telepon</label>
                                    <input type="text" name="phone" placeholder="Nomor Telepon" class="form-control input-lg" required>
                                </div>
                                <div class="col-md-12 form-group">
                                    <input type="submit" class="btn btn-block btn-vensia" value="Daftar Akun" name="submit">
                                </div>
                            </div>
                        </form>
                        <p class="small" style="text-align: center;">Dengan menekan Daftar Akun, saya mengkonfirmasi telah menyetujui <a href="<?php echo base_url('syarat_ketentuan'); ?>" style="color: #107ade; font-weight: bold">Syarat dan Ketentuan</a> serta <a style="color: #107ade; font-weight: bold" href="<?php echo base_url('bantuan/kebijakan_privasi'); ?>">Kebijakan Privasi</a> Vensia. 
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end: Section -->


</div>
<!-- end: Wrapper -->

<!-- Go to top button -->
<a id="goToTop"><i class="fa fa-angle-up top-icon"></i><i class="fa fa-angle-up"></i></a><!--Plugins-->
<script src="<?php echo base_url('assets'); ?>/js/jquery.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins.js"></script>

<!--Template functions-->
<script src="<?php echo base_url('assets'); ?>/js/functions.js"></script> 

<!--SweetAlert-->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.11/dist/sweetalert2.all.min.js"></script>

<script type="text/javascript">
    $('form#form-register').on('submit', function(e){
        e.preventDefault();
        var password = $('form#form-register').find('input[name=password]').val();
        var mydata = $("form#form-register").serialize();
        $.ajax({
            type : "POST",
            url : "<?php echo base_url('index.php/Authss/clientRegister')?>",
            data : mydata,
            datatype : "json", 
            beforeSend  : function(){
                $("#submit").prop("disabled", true);
                $("#loading").show();
                $("#wrapper").css('opacity','0.5');  
            },
            success : function(hasil){
                var rs = $.parseJSON(hasil);
                $("#loading").hide();
                swal({
                    type : rs['icon'],
                    html : rs['text']
                }).then((result) =>{
                    if(rs['icon'] == "success"){
                      $("#submit").prop("disabled", false);
                      $("#wrapper").css('opacity','1.0');
                      location.replace(rs['direct']);
                  }
              }) 
            }
        });
    });
</script>
<script type="text/javascript">
    $(".toggle-password").click(function() {

      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});
</script>
</body>

</html>
