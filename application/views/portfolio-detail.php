<?php $this->load->view('header'); ?>

<!-- <section class="section-pattern p-t-60 p-b-30 text-center" style="background: url(<?php echo base_url('assets'); ?>/images/pages/bg.jpg)">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 class="text-medium" style="color: white">YEPS Portfolio</h3>
            </div>
        </div>
    </div>
</section> -->

<!-- Page title -->
<section id="page-title" class="page-title-center text-light" style="background-image:url(<?php echo base_url('assets'); ?>/images/pages/bg.jpg)" style="padding: 40px 0 !important">
    <div class="container">
        <div class="page-title">
            <h1><?php echo $judul ?></h1>
            <div class="small m-b-20"><?php echo date_format(date_create($date_event), 'd F Y');?></div>
        </div>
    </div>
</section>
<!-- end: Page title -->

<!-- Page Content -->
<section id="page-content" class="sidebar-right">
    <div class="container">
        <div id="blog" class="single-post col-md-10 center">
            <!-- Post single item-->
            <div class="post-item">
                <div class="post-item-wrap">
                    <div class="post-item-description">
                        <?php echo $konten ?>
                    </div>

                    <!--Gallery Carousel -->
                    <div class="hr-title hr-long center"><abbr>Foto - Foto Event</abbr> </div>
                    <div class="carousel" data-lightbox="gallery" data-margin="20">
                        <?php 
                            $exfoto = explode(",",$foto);
                            for ($i=0; $i < sizeof($exfoto); $i++) { 
                                if($exfoto[$i] != null){
                        ?>
                        <!-- portfolio item -->
                        <div class="portfolio-item img-zoom pf-illustrations pf-media pf-icons pf-Media">
                            <div class="portfolio-item-wrap">
                                <div class="portfolio-image">
                                    <a href="#"><img style="width: 215px; height: 215px;" src="<?php echo base_url('asset/images/portfolio/'.$exfoto[$i]); ?>" alt=""></a>
                                </div>
                                <div class="portfolio-description">
                                    <a title="Paper Pouch!" data-lightbox="image" href="<?php echo base_url('asset/images/portfolio/'.$foto); ?>" class="btn btn-light btn-rounded">Zoom</a>
                                </div>
                            </div>
                        </div>
                        <!-- end: portfolio item -->
                        <?php }} ?>
                    </div>

                    <div class="post-navigation">
                        <?php if ($prevpage != null) { ?>
                          <a href="<?php echo site_url("Portfolio/detail/".$prevpage['id']); ?>" class="post-prev">
                            <div class="post-prev-title"><span>Previous Post</span>Judul Sebelumnya</div>
                        </a>
                        <?php } ?>
                        <a href="<?php echo site_url('Portfolio')?>" class="post-all">
                            <i class="fa fa-th"></i>
                        </a>
                        <?php if ($nextpage != null) {?>
                        <a href="<?php echo site_url("Portfolio/detail/".$nextpage['id']); ?>" class="post-next">
                            <div class="post-next-title"><span>Next Post</span>Judul Selanjutnya</div>
                        </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <!-- end: Post single item-->
        </div>
    </div>
</section>


<?php $this->load->view('footer'); ?>