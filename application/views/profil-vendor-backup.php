<?php $this->load->view('header'); ?>

<!-- Content -->
<section id="page-content" class="sidebar-left">
    <div class="container">
        <div class="row" style="margin-right: -100px;">
            <!-- post content -->
            <div class="content col-md-9" style="padding-left: 0px;">
                <div id="tabs-04" class="tabs justified no-border" style="margin-top: -40px">
                    <ul class="tabs-navigation">
                        <li class="active"><a href="#About4"><i class="fa fa-home"></i>Produk</a> </li>
                        <li><a href="#Services4"><i class="fa fa-home"></i>Ulasan <span class="badge">140</span></a> </li>
                        <li><a href="#Choose4"><i class="fa fa-home"></i>Portfolio</a> </li>
                        <li><a href="#Assets4"><i class="fa fa-home"></i>Tentang Kami</a> </li>
                    </ul>
                    <div class="tabs-content">
                        <div class="tab-pane active" id="About4">
                            <!--Product list-->
                            <div class="shop">
                                <div class="grid-layout grid-4-columns" data-item="grid-item">
                                    <div class="grid-item">
                                        <div class="product">
                                            <div class="product-image">
                                                <a href="#"><img alt="Shop product image!" src="<?php echo base_url('assets/images/shop/products/vendor_gedung/1.jpg'); ?>" > 
                                                </a>
                                                <a href="#"><img alt="Shop product image!" src="<?php echo base_url('assets/images/shop/products/vendor_gedung/1.jpg'); ?>" > 
                                                </a>
                                                <span class="product-new">NEW</span>
                                                <span class="product-wishlist">
                                                    <a href="<?php echo base_url('venue/jadwal'); ?>" data-toggle="tooltip" data-placement="right" title="Lihat Jadwal"><i class="fa fa-calendar"></i></a>
                                                </span>
                                                <div class="product-overlay">
                                                    <a href="<?php echo base_url('venue/detail'); ?>" data-lightbox="ajax">Quick View</a>
                                                </div>
                                            </div>

                                            <div class="product-description">
                                                <div class="product-category">Jakarta Selatan</div>
                                                <div class="product-title">
                                                    <a href="<?php echo base_url('venue/detail'); ?>" data-lightbox="ajax">Swiss BelHotel Pondok Indah</a></h3>
                                                </div>
                                                <div class="product-harga">Rp. 328.888.000
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid-item">
                                        <div class="product">
                                            <div class="product-image">
                                                <a href="#"><img alt="Shop product image!" src="<?php echo base_url('assets/images/shop/products/vendor_gedung/1.jpg'); ?>" > 
                                                </a>
                                                <a href="#"><img alt="Shop product image!" src="<?php echo base_url('assets/images/shop/products/vendor_gedung/1.jpg'); ?>" > 
                                                </a>
                                                <span class="product-new">NEW</span>
                                                <span class="product-wishlist">
                                                    <a href="<?php echo base_url('venue/jadwal'); ?>" data-toggle="tooltip" data-placement="right" title="Lihat Jadwal"><i class="fa fa-calendar"></i></a>
                                                </span>
                                                <div class="product-overlay">
                                                    <a href="<?php echo base_url('venue/detail'); ?>" data-lightbox="ajax">Quick View</a>
                                                </div>
                                            </div>

                                            <div class="product-description">
                                                <div class="product-category">Jakarta Selatan</div>
                                                <div class="product-title">
                                                    <a href="<?php echo base_url('venue/detail'); ?>" data-lightbox="ajax">Swiss BelHotel Pondok Indah</a></h3>
                                                </div>
                                                <div class="product-harga">Rp. 328.888.000
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid-item">
                                        <div class="product">
                                            <div class="product-image">
                                                <a href="#"><img alt="Shop product image!" src="<?php echo base_url('assets/images/shop/products/vendor_gedung/1.jpg'); ?>" > 
                                                </a>
                                                <a href="#"><img alt="Shop product image!" src="<?php echo base_url('assets/images/shop/products/vendor_gedung/1.jpg'); ?>" > 
                                                </a>
                                                <span class="product-new">NEW</span>
                                                <span class="product-wishlist">
                                                    <a href="<?php echo base_url('venue/jadwal'); ?>" data-toggle="tooltip" data-placement="right" title="Lihat Jadwal"><i class="fa fa-calendar"></i></a>
                                                </span>
                                                <div class="product-overlay">
                                                    <a href="<?php echo base_url('venue/detail'); ?>" data-lightbox="ajax">Quick View</a>
                                                </div>
                                            </div>

                                            <div class="product-description">
                                                <div class="product-category">Jakarta Selatan</div>
                                                <div class="product-title">
                                                    <a href="<?php echo base_url('venue/detail'); ?>" data-lightbox="ajax">Swiss BelHotel Pondok Indah</a></h3>
                                                </div>
                                                <div class="product-harga">Rp. 328.888.000
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid-item">
                                        <div class="product">
                                            <div class="product-image">
                                                <a href="#"><img alt="Shop product image!" src="<?php echo base_url('assets/images/shop/products/vendor_gedung/1.jpg'); ?>" > 
                                                </a>
                                                <a href="#"><img alt="Shop product image!" src="<?php echo base_url('assets/images/shop/products/vendor_gedung/1.jpg'); ?>" > 
                                                </a>
                                                <span class="product-new">NEW</span>
                                                <span class="product-wishlist">
                                                    <a href="<?php echo base_url('venue/jadwal'); ?>" data-toggle="tooltip" data-placement="right" title="Lihat Jadwal"><i class="fa fa-calendar"></i></a>
                                                </span>
                                                <div class="product-overlay">
                                                    <a href="<?php echo base_url('venue/detail'); ?>" data-lightbox="ajax">Quick View</a>
                                                </div>
                                            </div>

                                            <div class="product-description">
                                                <div class="product-category">Jakarta Selatan</div>
                                                <div class="product-title">
                                                    <a href="<?php echo base_url('venue/detail'); ?>" data-lightbox="ajax">Swiss BelHotel Pondok Indah</a></h3>
                                                </div>
                                                <div class="product-harga">Rp. 328.888.000
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid-item">
                                        <div class="product">
                                            <div class="product-image">
                                                <a href="#"><img alt="Shop product image!" src="<?php echo base_url('assets/images/shop/products/vendor_gedung/1.jpg'); ?>" > 
                                                </a>
                                                <a href="#"><img alt="Shop product image!" src="<?php echo base_url('assets/images/shop/products/vendor_gedung/1.jpg'); ?>" > 
                                                </a>
                                                <span class="product-new">NEW</span>
                                                <span class="product-wishlist">
                                                    <a href="<?php echo base_url('venue/jadwal'); ?>" data-toggle="tooltip" data-placement="right" title="Lihat Jadwal"><i class="fa fa-calendar"></i></a>
                                                </span>
                                                <div class="product-overlay">
                                                    <a href="<?php echo base_url('venue/detail'); ?>" data-lightbox="ajax">Quick View</a>
                                                </div>
                                            </div>

                                            <div class="product-description">
                                                <div class="product-category">Jakarta Selatan</div>
                                                <div class="product-title">
                                                    <a href="<?php echo base_url('venue/detail'); ?>" data-lightbox="ajax">Swiss BelHotel Pondok Indah</a></h3>
                                                </div>
                                                <div class="product-harga">Rp. 328.888.000
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <hr>
                                <!-- Pagination -->
                                <center>
                                    <nav class="text-center">
                                      <ul class="pagination pagination-fancy">
                                        <li> <a href="#" aria-label="Previous"> <span aria-hidden="true"><i class="fa fa-angle-left"></i></span> </a> </li>
                                        <li><a href="#">1</a> </li>
                                        <li><a href="#">2</a> </li>
                                        <li class="active"><a href="#">3</a> </li>
                                        <li><a href="#">4</a> </li>
                                        <li><a href="#">5</a> </li>
                                        <li> <a href="#" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-angle-right"></i></span> </a> </li>
                                    </ul>
                                </nav>
                            </center>
                            <!-- end: Pagination -->
                        </div>
                        <!--End: Product list-->
                    </div>
                    <div class="tab-pane" id="Services4">
                        <h4>Services</h4>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>
                    </div>
                    <div class="tab-pane" id="Choose4">
                            <div class="container-fluid">
                                <!-- Gallery -->
                                <div class="grid-layout grid-3-columns" data-margin="20" data-item="grid-item" data-lightbox="gallery">
                                    <div class="grid-item">
                                        <a class="image-hover-zoom" href="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>" data-lightbox="gallery-item"><img src="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>"></a>
                                    </div>
                                    <div class="grid-item">
                                        <a class="image-hover-zoom" href="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>" data-lightbox="gallery-item"><img src="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>"></a>
                                    </div>
                                    <div class="grid-item">
                                        <a class="image-hover-zoom" href="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>" data-lightbox="gallery-item"><img src="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>"></a>
                                    </div>
                                    <div class="grid-item">
                                        <a class="image-hover-zoom" href="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>" data-lightbox="gallery-item"><img src="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>"></a>
                                    </div>
                                    <div class="grid-item">
                                        <a class="image-hover-zoom" href="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>" data-lightbox="gallery-item"><img src="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>"></a>
                                    </div>
                                    <div class="grid-item">
                                        <a class="image-hover-zoom" href="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>" data-lightbox="gallery-item"><img src="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>"></a>
                                    </div>
                                    <div class="grid-item">
                                        <a class="image-hover-zoom" href="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>" data-lightbox="gallery-item"><img src="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>"></a>
                                    </div>
                                    <div class="grid-item">
                                        <a class="image-hover-zoom" href="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>" data-lightbox="gallery-item"><img src="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>"></a>
                                    </div>
                                    <div class="grid-item">
                                        <a class="image-hover-zoom" href="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>" data-lightbox="gallery-item"><img src="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>"></a>
                                    </div>
                                    <div class="grid-item">
                                        <a class="image-hover-zoom" href="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>" data-lightbox="gallery-item"><img src="<?php echo base_url('assets/images/portfolio-vendor/1.jpg');?>"></a>
                                    </div>
                                </div>
                                <!-- end: Gallery -->
                            </div>
                    </div>
                    <div class="tab-pane" id="Assets4">
                        <h4>Services</h4>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: post content -->

        <!-- Sidebar-->
        <div class="sidebar col-md-3">
            <div class="pinOnScroll">
                <!--Tabs with Posts-->
                <div class="widget ">
                    <!-- <h4 class="widget-title">Recent Posts</h4> -->
                    <div class="post-thumbnail-list">
                        <div class="post-thumbnail-entry">
                            <img alt="" class="profil-vendor" src="<?php echo base_url('assets'); ?>/images/profil-vendor/ibis.png">
                            <div class="post-thumbnail-content">
                                <a href="#" style="text-transform: uppercase; font-family: Montserrat; font-weight: 600; color: #1375d1">Hotel Ibis</a>
                                <span class="post-date"><i class="fa fa-location-arrow"></i> Jakarta Selatan</span>
                                <span class="post-category"></span>

                            </div>
                            <div class="product-rate">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i>
                            </div>
                        </div>
                        <div class="post-thumbnail-entry">
                            <div class="social-icons social-icons-border social-icons-light  social-icons-colored-hover text-center">
                                <ul>
                                    <li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li class="social-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li class="social-instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li class="social-pinterest"><a href="#"><i class="fa fa-globe"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="post-thumbnail-entry">
                            <div class="filter-group active">
                                <h4 class="widget-title">Kategori</h4>
                                <div class="row">
                                    <div class="ex1">
                                        <label class="space">Venue
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="space">Photography
                                          <input type="checkbox">
                                          <span class="checkmark"></span>
                                      </label>
                                      <label class="space">Wardrobe
                                          <input type="checkbox">
                                          <span class="checkmark"></span>
                                      </label>
                                      <label class="space">Decoration
                                          <input type="checkbox">
                                          <span class="checkmark"></span>
                                      </label>
                                      <label class="space">Catering
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="space">Videography
                                      <input type="checkbox">
                                      <span class="checkmark"></span>
                                  </label>
                              </div>
                          </div> 
                      </div>
                  </div>
              </div>
          </div>
          <!--End: Tabs with Posts-->

      </div>
  </div>
  <!-- end: Sidebar-->
</div>
</div>
</section>
<!-- end: Content -->

<?php $this->load->view('footer'); ?>