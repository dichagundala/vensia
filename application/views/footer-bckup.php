<!-- Footer -->
<?php
//untuk get IP Address
function getUserIpAddr(){
    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
    
}
//echo 'User Real IP - '.getUserIpAddr();
?>
<footer id="footer" class="footer-light">
    <div class="footer-content">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <!-- Footer widget area 1 -->
                    <div class="widget clearfix widget-contact-us" style="background-image: url('<?php echo base_url('assets'); ?>/images/world-map-dark.png'); background-position: 30% 70px; background-repeat: no-repeat">
                        <h4>Vensia</h4>
                        <ul class="list-icon">
                            <li>
                                <i class="fa fa-map-marker"></i>Ariobimo Sentral Lantai 4. <br>Jl. H. R. Rasuna Said Kuningan Timur, Kecamatan Setiabudi, 
                                <br>Jakarta Selatan, DKI Jakarta 12950
                            </li>
                            <li><i class="fa fa-phone"></i> (021) 3972-2372 </li>
                            <li><i class="fa fa-envelope"></i> <a href="mailto:halo@yepsindonesia.com">halo@vensia.id</a>
                            </li>
                            <li>
                                <br><i class="fa fa-clock-o"></i>Senin - Jumat: <strong>08:00 - 17:00</strong>
                                <br>Sabtu, Minggu: <strong>Tutup</strong>
                            </li>
                        </ul>
                        <!-- Social icons -->
                        <div class="social-icons social-icons-border float-left m-t-20">
                            <ul>
                                <li class="social-instagram"><a href="https://www.instagram.com/yepsindonesia/"><i class="fab fa-instagram"></i></a></li>
                                <li class="social-facebook"><a href="https://www.facebook.com/yepsindo/"><i class="fab fa-facebook"></i></a></li>
                                <li class="social-twitter"><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li class="social-youtube"><a href="#"><i class="fab fa-youtube"></i></a></li>
                                <!-- <li class="social-gplus"><a href="#"><i class="fa fa-google-plus"></i></a></li> -->
                            </ul>
                        </div>
                        <!-- end: Social icons -->
                    </div>
                    <!-- end: Footer widget area 1 -->
                </div>
                <div class="col-md-2">
                    <!-- Footer widget area 2 --> 
                    <div class="widget">
                        <h4>LAYANAN KAMI</h4>
                        <ul class="list-icon list-icon-arrow">
                            <li><a href="<?php echo base_url('about_us'); ?>">Tentang Kami</a></li>
                            <li><a href="<?php echo base_url('syarat_ketentuan'); ?>">Syarat dan Ketentuan</a></li>
                            <li><a href="#">Partner Vensia</a></li>
                            <li><a href="<?php echo base_url('keamanan'); ?>">Keamanan</a></li>
                            <li><a href="<?php echo base_url('event_management'); ?>">Event Management</a></li>
                            <li><a href="<?php echo base_url('career'); ?>">Karir</a></li>
                            <li><a href="<?php echo base_url('portfolio'); ?>">Portfolio</a></li>
                        </ul>
                    </div>
                    <!-- end: Footer widget area 2 -->
                </div>
                <div class="col-md-2">
                    <!-- Footer widget area 3 -->
                    <div class="widget">
                        <div class="widget">
                            <h4>LAYANAN KLIEN</h4>
                            <ul class="list-icon list-icon-arrow">
                                <li><a href="<?php echo base_url('layananklien/akun'); ?>">Akun</a></li>
                                <li><a href="<?php echo base_url('layananklien/panduanpemesanan'); ?>">Panduan Pemesanan</a></li>
                                <li><a href="<?php echo base_url('layananklien/layananpembayaran'); ?>">Layanan Pembayaran</a></li>
                                <li><a href="<?php echo base_url('layananklien/refund'); ?>">Refund</a></li>
                                <li><a href="#">Layanan Event Management</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- end: Footer widget area 3 -->
                </div>
                <div class="col-md-2">
                    <!-- Footer widget area 4 -->
                    <div class="widget">
                        <div class="widget">
                            <h4>LAYANAN VENDOR</h4>
                            <ul class="list-icon list-icon-arrow">
                                <li><a href="<?php echo base_url('layananvendor/registrasi_vendor'); ?>">Registrasi Vendor</a></li>
                                <li><a href="<?php echo base_url('layananvendor/kelola_layanan'); ?>">Kelola Layanan</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- end: Footer widget area 4 -->
                </div>
                <div class="col-md-3">
                    <!-- Footer widget area 3 -->
                    <div class="widget">
                        <div class="widget">
                            <h4>BANTUAN</h4>
                            <ul class="list-icon list-icon-arrow">
                                <li><a href="<?php echo base_url('bantuan/faq'); ?>">FAQ</a></li>
                                <li><a href="<?php echo base_url('bantuan/panduan_keamanan'); ?>">Panduan Keamanan</a></li>
                                <li><a href="<?php echo base_url('bantuan/kebijakan_privasi'); ?>">Kebijakan Privasi</a></li>
                                <li><a href="<?php echo base_url('bantuan/contact_us'); ?>">Hubungi Kami</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- end: Footer widget area 3 -->
                </div>
            </div>
        </div>
        <!-- <div class="container">
            <div class="row">
                <center>
                    <h5 style="color: white">Payment Partners</h5>
                    <img class="img-h-30 m-r-5" src="<?php echo base_url('assets'); ?>/images/bank/ipaymu.png">
                    <img class="img-h-30 m-r-5" src="<?php echo base_url('assets'); ?>/images/bank/mandiri.png">
                    <img class="img-h-30 m-r-5" src="<?php echo base_url('assets'); ?>/images/bank/mandiri-syariah.png">
                </center>
            </div>
        </div> -->

    </div>
    <div class="copyright-content" style="background-color: #ca1d69">
        <div class="container">
            <div class="copyright-text text-center">&copy; 2019 Vensia. All Rights Reserved.
            </div>
        </div>
    </div>
</footer>
<!-- end: Footer -->

</div>
<!-- end: Wrapper -->

<!--Chat Box-->
<!--Awal-->
<link rel = "stylesheet" type = "text/css" 
href = "<?php echo base_url(); ?>asset/css/chat_box.css">  
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

<div class="container">
   <!--<h2 style="text-align:center">Chat-box Created By <span class="red" style="color:red">M</span>ilind <span class="red" style="color:red">K</span>amthe</h2>-->
   <!--<p style="text-align:center">creativemk22@gmail.com</p>-->
   <hr>
    <div class="row">
       <div class="chatbox chatbox22 chatbox--tray">
        <div class="chatbox__title">
            <h5><a href="javascript:void()" onClick="getChat2('<?php echo getUserIpAddr(); ?>','0')">Leave a message</a></h5>
        <!--<button class="chatbox__title__tray">
            <h5><a href="javascript:void()">Leave a message</a></h5>-->
        <button class="chatbox__title__tray">
            <span></span>
        </button>
        <button class="chatbox__title__close">
            <span>
                <svg viewBox="0 0 12 12" width="12px" height="12px">
                    <line stroke="#FFFFFF" x1="11.75" y1="0.25" x2="0.25" y2="11.75"></line>
                    <line stroke="#FFFFFF" x1="11.75" y1="11.75" x2="0.25" y2="0.25"></line>
                </svg>
            </span>
        </button>
    </div>
    <div class="chatbox__body">
        <!--dari orang lain-->
        <!--awal-->
        <!--<div class="panel-body" style="height:400px;overflow-y:auto" id="box">-->
            <div id="chat-box">
            <!--<div id="loading" style="display:none"><center><i class="fa fa-spinner fa-spin"></i> Loading...</center></div>-->
           <!-- </div>-->
        </div>
        
                 <!--awal-->
                 <?php  
                 //echo getUserIpAddr();?>
        <input type="hidden" id="id_ip" value="<?php echo getUserIpAddr(); ?>" />
        <input type="hidden" id="id_user" value="1" />
        <input type="hidden" id="id_max" value="<?php echo isset($id_max) ? $id_max : '' ; ?>" />
        
        <!--<div class="row">
						<div class="col-md-12">
							<textarea class="form-control " id="pesan" style="margin-right:10px;"></textarea>
							<button id="send" type="button" class="btn btn-primary pull-right" style="margin-top:10px;"  onClick="sendMessage()" ><i class="fa fa-send"></i> Send Message</button>
						</div>
					</div>-->
   
<!--</div>
<div class="panel-footer">
        
        <div class="chatbox__body__message chatbox__body__message--left">

          <div class="chatbox_timing">
              <ul>
                  <li><a href="#"><i class="fa fa-calendar"></i><?php //echo "hai";?> 22/11/2018</a></li>
                  <li><a href="#"><i class="fa fa-clock-o"></i> 7:00 PM</a></a></li>
              </ul>
          </div>
          <img src="https://www.gstatic.com/webp/gallery/2.jpg" alt="Picture">
          <div class="clearfix"></div>
          <div class="ul_section_full">
             <ul class="ul_msg">
                 <li><strong>Person Name</strong></li>
                 <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </li>
             </ul>
             <div class="clearfix"></div>
             <ul class="ul_msg2">
                 <li><a href="#"><i class="fa fa-pencil"></i> </a></li>
                 <li><a href="#"><i class="fa fa-trash chat-trash"></i></a></li>
             </ul>
         </div>

     </div>
     
     <div class="chatbox__body__message chatbox__body__message--right">

      <div class="chatbox_timing">
          <ul>
              <li><a href="#"><i class="fa fa-calendar"></i> 22/11/2018</a></li>
              <li><a href="#"><i class="fa fa-clock-o"></i> 7:00 PM</a></a></li>
          </ul>
      </div>

      <img src="https://www.gstatic.com/webp/gallery/2.jpg" alt="Picture">
      <div class="clearfix"></div>
      <div class="ul_section_full">
         <ul class="ul_msg">
             <li><strong>Person Name</strong></li>
             <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </li>
         </ul>
         <div class="clearfix"></div>
         <ul class="ul_msg2">
             <li><a href="#"><i class="fa fa-pencil"></i> </a></li>
             <li><a href="#"><i class="fa fa-trash chat-trash"></i></a></li>
         </ul>
     </div>

 </div>-->
 
</div> 
 <div class="panel-footer">
    <div class="input-group">
        <input id="btn-input" type="text" class="form-control input-sm chat_set_height" placeholder="Type your message here..." tabindex="0" dir="ltr" spellcheck="false" autocomplete="off" autocorrect="off" autocapitalize="off" contenteditable="true" />

        <span class="input-group-btn">
            <button class="btn bt_bg btn-sm" id="btn-chat" onClick="sendMessage()">
            Send</button>
        </span>
    </div>
</div> 
</div>

</div>
</div>

<!--Akhir-->

<!--untuk live chat akhir -->

<!-- Go to top button -->
<!--<a href="https://tawk.to/chat/5b3c386c6d961556373d623e/default" id="mybutton" target="_blank">
    <button class="btn btn-yeps">Live Chat</button>
</a>-->

<a id="goToTop"><i class="fa fa-angle-up top-icon"></i><i class="fa fa-angle-up"></i></a>
<!--Plugins-->
<script src="<?php echo base_url('assets'); ?>/js/jquery.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins.js"></script>

<!--Template functions-->
<script src="<?php echo base_url('assets'); ?>/js/functions.js"></script>

<script src="<?php echo base_url('assets'); ?>/js/custom.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/timepicker/dist/bootstrap-clockpicker.min.js"></script>

<!-- uikit -->
<script src="<?php echo base_url('assets'); ?>/uikit/js/uikit.js"></script>

<script>
//untuk js Chat Box
(function($) {
    $(document).ready(function() {
        var $chatbox = $('.chatbox'),
        $chatboxTitle = $('.chatbox__title'),
        $chatboxTitleClose = $('.chatbox__title__close'),
        $chatboxCredentials = $('.chatbox__credentials');
        $chatboxTitle.on('click', function() {
            $chatbox.toggleClass('chatbox--tray');
        });
        /*$chatboxTitleClose.on('click', function(e) {
            e.stopPropagation();
            $chatbox.addClass('chatbox--closed');
        });*/
        $chatbox.on('transitionend', function() {
            if ($chatbox.hasClass('chatbox--closed')) $chatbox.remove();
        });
        
    });
})(jQuery);
</script>
<script>
//untuk online chat
$(document).ready(function(){
	//getChat(0);
	$("#user").click(function(){
		$("#id_max").val('0');
	});
	
	setInterval(function(){ 
		if($("#id_user").val() > 0){
			getLastId($("#id_user").val(),$("#id_ip").val() ,$("#id_max").val()); 
			getChat($("#id_user").val(),$("#id_ip").val() ,$("#id_max").val()); 
			autoScroll();
		}else{
			
		}
	},3000);
});

function getChatAll(id_user,id_ip,id_max){
	
	$.ajax({
		url		: "<?php echo site_url('index.php/chat/getChatAll') ?>",
		type	: 'POST',
		dataType: 'html',
		data 	: {id_user:id_user,id_ip:id_ip,id_max:id_max},
		beforeSend	: function(){
			$("#loading").show();
		},
		success	: function(result){
			$("#loading").hide();
			$("#chat-box").html(result);
			//$(".panel-footer").show();
			
			autoScroll();
			//document.getElementById('btn-input').focus();
		}
	});
}
function getChat2(id_user,id_max){
	var id_user = $("#id_user").val();
    //var id_ip = $("#id_ip").val();
    //var id_max = $("#id_max").val();
	$.ajax({
		url		: "<?php echo site_url('index.php/chat/getChat') ?>",
		type	: 'POST',
		dataType: 'html',
		data 	: {id_user:id_user,id_max:id_max},
		beforeSend	: function(){
			//$("#loading").show();
		},
		success	: function(result){
			$("#loading").hide();
			if(id_user != $("#id_user").val() ){
				$("#chat-box").html(result);
			}else{
				$("#chat-box").append(result);
			}
			//$(".panel-footer").show();
			//document.getElementById('btn-input').focus();
		}
	});
}
function getChat(id_user,id_ip,id_max){
	var id_user = $("#id_user").val();
    var id_ip = $("#id_ip").val();
    var id_max = $("#id_max").val();
	$.ajax({
		url		: "<?php echo site_url('index.php/chat/getChat') ?>",
		type	: 'POST',
		dataType: 'html',
		data 	: {id_user:id_user,id_ip:id_ip,id_max:id_max},
		beforeSend	: function(){
		//	$("#loading").show();
		},
		success	: function(result){
			$("#loading").hide();
			if(id_user != $("#id_user").val() ){
				$("#chat-box").html(result);
			}else{
				$("#chat-box").append(result);
			}
			//$(".panel-footer").show();
			//document.getElementById('btn-input').focus();
		}
	});
}

function getLastId(id_user,id_ip,id_max){
	$.ajax({
		url		: "<?php echo site_url('index.php/chat/getLastId') ?>",
		type	: 'POST',
		dataType: 'json',
		data 	: {id_user:id_user,id_ip:id_ip,id_max:id_max},
		beforeSend	: function(){
			
		},
		success	: function(result){
			$("#id_max").val(result.id);
		}
	});
}

function sendMessage(){
	var pesan 	= $("#btn-input").val();
	var id_user = $("#id_user").val();
    var id_ip = $("#id_ip").val();
	
	if(pesan == ''){
		document.getElementById('btn-input').focus();
	}else{
		$.ajax({
			url		: "<?php echo site_url('index.php/chat/sendMessage') ?>",
			type	: 'POST',
			dataType: 'json',
			data 	: {id_user:id_user, id_ip:id_ip,  pesan:pesan},
			beforeSend	: function(){
			},
			success	: function(result){
				getChat($("#id_user").val(),$("#id_ip").val() ,$("#id_max").val());
				getLastId($("#id_user").val(),$("#id_ip").val() ,$("#id_max").val()); 
				$("#btn-input").val('');
				autoScroll();
			}
		});
	}
}

function autoScroll(){
	var elem = document.getElementById('box');
	//elem.scrollTop = elem.scrollHeight;
}

function aktifkan(i){
	$("li").removeClass("active");
	$("#aktif-"+i).addClass("active");
}
</script>
<script type="text/javascript">
    $('.clockpicker').clockpicker({
        placement: 'top',
        align: 'left',
        donetext: 'Done'
    });
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.11/dist/sweetalert2.all.min.js"></script>

<script type="text/javascript">
    $('form#form-login').on('submit', function(e){
        e.preventDefault();

        var mydata = $("form#form-login").serialize();
        $.ajax({
            type : "POST",
            url : "<?php echo base_url('index.php/Authss/cek_login')?>",
            data : mydata,
            datatype : "json",
            success : function(hasil){
                console.log(hasil);
                var rs = $.parseJSON(hasil);
                swal({
                    type : rs['icon'],
                    text : rs['text']
                }).then(function(e){
                    location.replace(rs['direct']);
                })
            }
        });
    });
</script>
<script type="text/javascript">
    $('form#form-search').on('submit', function(e){
        e.preventDefault();

        var keyword = $("#keyword").val();
        location.replace("<?php echo base_url('Search/produk/')?>"+keyword+"/1");
        
    });
</script>
<script type="text/javascript">
    $('#modal-3').on('show.bs.modal', function(e){
        var uuid = $(e.relatedTarget).data('uuid');
        var modal = $(this);

        modal.find('.modal-title').html("Pilihan Tanggal Keberangkatan : <u>" + name + "</u>");
        $('.modal-body').html("Getting Data...");
        $.ajax({
            type : 'post',
            url : '<?php echo base_url('Shop/detail_cart/')?>'+uuid,
            success : function(data){
                $('.modal-body').html(data);
            }
        });
    })
</script>
<script type="text/javascript">
	/*for show and hidden password*/
	$(".toggle-password").click(function() {

		$(this).toggleClass("fa-eye fa-eye-slash");
		var input = $($(this).attr("toggle"));
		if (input.attr("type") == "password") {
			input.attr("type", "text");
		} else {
			input.attr("type", "password");
		}
	});
</script>
<script type="text/javascript">
  /*setelah klik masuk*/
  $('#form-login').on('submit', function(e){
    e.preventDefault();

    var mydata = $('form#form-login').serialize();
    $.ajax({
        type        : "POST",
        url         : "<?php echo base_url('index.php/Authss/cek_login');?>",
        data        : mydata,
        datatype    : "json",
        success     : function(hasil){
            console.log(hasil);                       
            var rs = $.parseJSON(hasil); 
            swal({
                type : rs['icon'],
                text : rs['text']
            }).then( function() {
                if(rs['icon'] == "success"){

                    location.replace(rs['direct']);
                }
            });
        } 
    });
});
</script>
<!--Start of Tawk.to Script-->
<!-- <script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5b6bc6e4df040c3e9e0c6ddc/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script> -->
<!--End of Tawk.to Script-->

<!-- Partical Js files  -->
<!-- <script src="js/plugins/components/particles.js" type="text/javascript"></script>
<script src="js/plugins/components/particles-animation.js" type="text/javascript"></script>
-->
</body>

</html>