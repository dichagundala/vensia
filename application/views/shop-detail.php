<?php $this->load->view('header'); ?>
<link href="<?php echo base_url('assets'); ?>/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"
media="screen">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        // Add smooth scrolling to all links
        $("#info-service").on('click', function (event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function () {

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });
    });

</script>

<!-- SHOP PRODUCT PAGE -->
<section id="product-page" class="product-page">
    <div class="lyt-vnsia">
        <div class="product">
            <div class="product-title m-b-20 mobile">
                <a style="font-weight: bold; font-size: 20px;" class="capital"
                href="<?php echo base_url('jasa/'.$vendor.'/page=1'); ?>"><?php echo $produk['vendor']?></a>
            </div>
            <div class="row m-b-40">
                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4">
                    <div class="product-image">
                        <!-- Carousel slider -->
                        <div class="carousel dots-inside dots-dark arrows-visible arrows-only arrows-dark"
                        data-items="1" data-loop="true" data-autoplay="true" data-animate-in="fadeIn"
                        data-animate-out="fadeOut" data-autoplay-timeout="2500" data-lightbox="gallery">
                        <?php
                        function exist($url){
                            $file_headers = @get_headers($url);
                            if($file_headers[0] == "HTTP/1.0 200 OK"){
                                return true;
                            }else{
                                return false;
                            }
                        }

                        $foto = array_filter(explode(";", $produk['parameter']['Foto']));
                        if(count($foto) == 1){
                         foreach ($foto as $key) {
                            $image = 'https://res.cloudinary.com/yepsindo/image/upload/w_380,h_500,c_pad,b_rgb:CECECE,q_auto/'.$key.'.jpg';
                            $images = 'https://res.cloudinary.com/yepsindo/image/upload/'.$key.'.jpg';
                            if(empty($image)|| $key == ""){
                                $image = base_url('assets/images/pages/blank.jpg');
                            }
                            ?>
                            <div uk-grid uk-lightbox="animation: fade">
                                <a href="<?php echo $images;?>" data-caption="Foto Produk Vendor">
                                    <img src="<?php echo $image;?>">
                                </a>
                            </div>
                            <?php
                        }
                    } elseif (count($foto) < 1){
                        $image = 'https://res.cloudinary.com/yepsindo/image/upload/w_380,h_500,c_pad,b_rgb:CECECE,q_auto/'.$produk['parameter']['Foto'].'.jpg';
                        $images = 'https://res.cloudinary.com/yepsindo/image/upload/'.$key.'.jpg';
                        if(empty($image)){
                            $image = base_url('assets/images/pages/blank.jpg');
                        }
                        ?>
                        <div uk-grid uk-lightbox="animation: fade">
                            <a href="<?php echo $images;?>" data-caption="Foto Produk Vendor">
                                <img src="<?php echo $image;?>">
                            </a>
                        </div>
                        <?php
                    } else {
                        foreach ($foto as $key) {
                            if($key != null){
                                $image = 'https://res.cloudinary.com/yepsindo/image/upload/w_380,h_500,c_pad,b_rgb:CECECE,q_auto/'.$key.'.jpg';
                                $images = 'https://res.cloudinary.com/yepsindo/image/upload/'.$key.'.jpg';
                                if(empty($image)){
                                    $image = base_url('assets/images/pages/blank.jpg');
                                }
                                ?>
                                <div uk-grid uk-lightbox="animation: fade">
                                    <a href="<?php echo $images;?>" data-caption="Foto Produk Vendor">
                                        <img src="<?php echo $image;?>">
                                    </a>
                                </div>
                                <?php
                            }
                        }
                    }
                    ?>

                </div>
                <!-- Carousel slider -->
            </div>
        </div>


        <div class="col-md-6 col-xs-6 col-sm-6 col-lg-6">
            <div class="product-description">

                <div class="product-title product-width">
                    <h1 class="p-name"><?php echo $produk['parameter']['Nama']; ?></h1>
                </div>
                <div class="diskon-harga">
                    <a id="info-service" href="#tabs-1" class="btn-info-service" data-toggle="tooltip"
                    data-placement="top" data-original-title="Klik untuk melihat Informasi">Lihat
                Informasi</a>
            </div>
            <div class="product-title m-t-5" style="text-transform: Capitalize; color: #c1bfbf;font-size: 13px">
                <i class="fa fa-star star" style="color: #FFC300">&nbsp;</i>
                5.0 
                <span style="font-size: 10px">/ 5 Ulasan</span>
                <span style="margin: 0px 10px">|</span>
                <i class="fa fa-map-marker" style="margin-right: 10px"></i>
                <?php echo $produk['parameter']['Lokasi'];?>
            </div>

            <div class="seperator m-b-10 m-t-10"></div>
            <?php
            if($produk['parameter']['Harga Promosi'] == "0" || $produk['parameter']['Harga Promosi'] == $produk['parameter']['Harga Normal']){
                ?>
                <div class="harga-produk">
                    <ins>Rp. <?php echo number_format($produk['parameter']['Harga Normal'],0,",","."); ?></ins>
                </div>
                <?php
            }else{
                ?>
                <label style="color: grey; margin-top: 10px;">
                    <span class="produk-diskon">
                        <?php echo ceil((((int)$produk['parameter']['Harga Normal'] - (int)$produk['parameter']['Harga Promosi'])/(int)$produk['parameter']['Harga Normal'])*100); echo "% OFF";?>
                    </span>
                    <span style="color: #1375d1; font-size: 18px; font-weight: bold; padding-top: 10px">Rp.
                        <?php echo number_format($produk['parameter']['Harga Promosi'],0,",","."); ?></span>
                        <strike>Rp.
                            <?php echo number_format($produk['parameter']['Harga Normal'],0,",","."); ?></strike>
                        </label>
                        <?php
                    }
                    $date = new dateTime('Now');
                    ?>

                    <div class="seperator m-t-20 m-b-10"></div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <form action="" class="form-horizontal" role="form" id="form-addCart">
                            <input type="hidden" name="produk_uuid" value="<?php echo $produk['uuid'];?>">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-4 product-category">Kuantitas</label>
                                <div class="col-sm-8">
                                    <div class="cart-product-quantity"
                                    style="margin-bottom: 8px; margin-left: 3px !important;">
                                    <div class="quantity">
                                        <?php

                                        if( $produk['parameter']['Harga Promosi'] == "0"){
                                            echo "<input type='hidden' name='harga' value='". $produk['parameter']['Harga Normal']."'>";
                                        }else{
                                            echo "<input type='hidden' name='harga' value='". $produk['parameter']['Harga Promosi']."'>";
                                        }
                                        ?>
                                        <input type="button" class="minus" value="-"
                                        data-uuid="<?php echo  $produk['uuid'];?>">
                                        <input type="number" class="qty"
                                        data-uuid="<?php echo  $produk['uuid'];?>" <?php $c=1; if (isset( $produk['parameter']['Minimal Order'])) {

                                         echo "min='". $produk['parameter']['Minimal Order']."'";
                                     }else{
                                        echo "min='". $c."'"; 
                                    }
                                    $n=0;
                                    ?> <?php $q=isset($produk['parameter']['Minimal Order']);
                                    if($q >= 0 ) {
                                        $n=  isset($produk['parameter']['Minimal Order']);

                                    }if($q==""){ 
                                        $n= 1;
                                    }?> value="<?php echo $n; ?>" name="qty">
                                    <input type="button" class="plus" value="+"
                                    data-uuid="<?php echo  $produk['uuid'];?>">

                                </div>
                                <?php 
                                $h= isset($produk['parameter']['Minimal Order']) ? $produk['parameter']['Minimal Order'] : 0 ;
                                $subTotal = 0;
                                                //echo "<script type='text/javascript'>alert('$h');</script>";
                                if($produk['parameter']['Harga Promosi'] == "0"){
                                                       // $subTotal += (int)$produk['parameter']['Harga Normal']*$produk['qty'];
                                 $subTotal += isset($produk['parameter']['Minimal Order']) ? (int)$produk['parameter']['Harga Normal']*$produk['parameter']['Minimal Order'] : 0;
                             }else{
                                                        //$subTotal += (int)$produk['parameter']['Harga Promosi']*$produk['qty'];
                                $subTotal += isset($produk['parameter']['Minimal Order']) ? (int)$produk['parameter']['Harga Normal']*$produk['parameter']['Minimal Order'] : 0 ;   
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 product-category">Tanggal & Waktu<strong
                        style="color: red;">*</strong></label>
                        <div class="col-sm-8">
                            <div class="form-group" style="margin-bottom: 7px !important;">
                                <div style="margin-left: 13px; width: 400px;"
                                class="input-group date form_datetime col-md-3"
                                data-date="<?php echo $date->format('Y-m-d')?>"
                                data-date-format="dd MM yyyy - hh:ii" data-link-field="dtp_input1">
                                <input class="form-control" size="12" type="text" value="" readonly
                                name="time" required>
                                <span class="input-group-addon"><span
                                    class="fa fa-calendar"></span></span>
                                </div>
                                <input type="hidden" id="dtp_input1" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-4 product-category">Alamat Event<strong
                            style="color: red;">*</strong></label>
                            <div class="col-sm-8">
                                <textarea type="text" class="form-control noteInput"
                                placeholder="Masukkan alamat lengkap event anda" name="alamat" value=""
                                required="required"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 product-category">Catatan ke Vendor</label>
                            <div class="col-sm-8">
                                <textarea type="text" class="form-control noteInput"
                                placeholder="Masukkan catatan" name="note"
                                value="<?php echo isset($key['note']);?>"></textarea>
                                <!--<input type="hidden" name="uuid[]" value="<?php //echo  $produk['uuid'];?>">-->
                            </div>
                        </div>
                        <hr class="our-space">
                        <div class="form-group row" style="margin-top: 19px">
                            <!-- <h6 class="text-center">Tambahan Produk dari Vendor</h6> -->

                            <label for="inputEmail3" class="col-sm-4 product-category">Additional</label>
                            <div class="col-sm-8">
                                <select class="form-control select">
                                    <option value="0">Pilih Service</option>
                                    <?php

                                foreach($add_produk as $row) { ?>

                                        <option name="name_add2" id="name_add2" value="<?php echo $row['id']."|".$row['nama'] ?>"><?php echo $row['nama'] ?></option>';

                                <?php } if(isset($row)){ ?> <!-- endforeach -->

                                <input type="hidden" class="qty" name="name_add3[]" id="name_add3[]">

                                    <div class="quantity m-b-10" style="display: inline-block;">

                                        <?php $n2=0;

                                            if( $row['qty'] == "0"){

                                                echo "<input type='hidden' name='harga2' value='". $row['qty']."'>";

                                            }else{

                                                echo "<input type='hidden' name='harga2' value='". $row['qty']."'>";

                                            }

                                            ?>

                                            <p class="add-name" id="text-add">Qty : 

                                            <input type="text" class="ilang qty" size="3" data-uuid="<?php echo  $row['id'];?>"

                                         <?php $c2=1; if (isset( $row['qty'])) {

                                             echo "min='". $row['qty']."'";

                                         }else{

                                            echo "min='". $c2."'"; 

                                         } ?>

                                    <?php $q2=$row['qty'];

                                        if($q2 >= 0 ) {

                                          $n2=  $row['qty'];

                                      }?>

                                      value="<?php echo $n2; ?>" name="qty2" id="qty2" disabled>

                                       - <span class="add-price-satuan">Harga Satuan : Rp.  <input type="text" class="ilang qty"  name="harga3" id="harga3" disabled></span></p>



                                      <input type="hidden" class="qty" name="qty3[]" id="qty3[]">

                                     

                                      <input type="hidden" class="qty" name="harga4[]" id="harga4[]">

                                      <input class="button-remove btn btn-danger btn-xs" type="button" value="Remove">  

                                      <input class="button-add btn btn-default btn-xs" type="button" value="Tambah"> 

                                  </div>

                                  <?php }?>
                                </select>
                            </div>
                            <div id="additionalselects">
                            </div>
                            <p class="add-name" id="info-add" align="center"><span class="add-price-satuan"
                                style="font-style: italic">Kamu dapat mengubah quantity additional item di
                            halaman shop cart</span></p>
                        </div>

                    </form>
                    <div class="form-group row">
                        <?php if($produk['aktif vendor'] == 5){ ?>
                            <a href="#" class="btn btn-vensia btn-block disabled" id="addCart"><i
                                class="fa fa-shopping-cart"></i> Pesan</a>
                            <?php } else{ ?>
                                <div class="col-sm-4">
                                    <a href="#" class="btn btn-outline-vensia btn-block" id="addCart"><i
                                        class="fa fa-shopping-cart"></i> Chart</a>
                                    </div>
                                    <div class="col-sm-8">
                                        <a href="#" class="btn btn-vensia btn-block" id="addCart">Pesan Sekarang</a>
                                    </div>
                                <?php } 0?>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- end -->
                <?php
                if($data_vendor['publicId'] == ""){
                    $img = base_url('./assets/images/profil.jpg');
                }else{
                    $img = "https://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/".$data_vendor['publicId'].".jpg";
                }
                ?>
                <div class="col-md-2 col-xs-2 col-sm-2 col-lg-2">
                    <div class="uk-card uk-card-default uk-card-body hidden-xs">
                        <center>
                            <img alt="Vendor YEPS" class="vendor-img" src="<?php echo $img; ?>">
                            <a class="capital vendor-name-cart"
                            href="<?php echo base_url('jasa/'.$vendor.'/page=1'); ?>"><?php echo $produk['vendor']?></a>
                        </center>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- end: SHOP PRODUCT PAGE -->

<hr class="our-space">
<section id="product-page" class="product-page">
    <div class="lyt-vnsia">
        <div class="fasilitas">
            <h4 style="margin-bottom: 30px">Fasilitas yang di Dapat</h4>
            <?php echo $produk['parameter']['Fasilitas']; ?>
            <ul>

                <?php 
                foreach ($produk['parameter'] as $key => $value) {
                 if(!($key == "Deskripsi" || $key == "Maps" || $key == "Lokasi" || $key == "Nama" || $key == "Harga Promosi" || $key == "Harga Normal" || $key == "Foto" || $key == "Fasilitas" || $key == "Atribut" || $key == "Proses Pengerjaan" || $key == "Area Jangkauan")){
                    ?>
                    <li><?php echo $key;?>: &nbsp;
                        <?php
                        if($key == "Maps"){
                            echo "<img src='https://www.mapquestapi.com/staticmap/v5/map?key=cQuyDdVBSw3VhsGwxc0mKxQ4ruKURw2J&locations=".$value."&size=600,400&zoom=12'/></td>";
                        }else{
                            echo $value;
                        }
                        ?>
                        <?php 
                    } 
                } 
                ?>
            </ul>
        </div>
        <?php if(isset($produk['parameter']['Proses Pengerjaan'])){
            echo "Proses Pengerjaan : ". $produk['parameter']['Proses Pengerjaan'];
        } ?>
        <hr>
        <h4 style="margin-bottom: 30px">Deskripsi Service</h4>
        <p><?php echo $produk['parameter']['Deskripsi']?></p>
        <hr style="margin: 30px 0px">
        <div class="table-responsive">

            <table class="table table-striped table-bordered">
                <tbody>
                    <?php 
                    foreach ($produk['parameter'] as $key => $value) {
                     if($key == "Maps" || $key == "Lokasi"){
                        ?>
                        <tr>
                            <td><?php echo $key;?></td>
                            <?php
                            if($key == "Maps"){
                                echo "<td><img src='https://www.mapquestapi.com/staticmap/v5/map?key=cQuyDdVBSw3VhsGwxc0mKxQ4ruKURw2J&locations=".$value."&size=600,400&zoom=12'/></td>";
                            }else{
                                echo "<td>".$value."</td>";
                            }
                            ?>
                        </tr>
                        <?php 
                    } 
                } 
                ?>
            </tbody>
        </table>
    </div>
</div>
</section>

<hr class="our-space">
<section id="product-page" class="product-page">
    <div class="lyt-vnsia">
        <div class="comments" id="comments">
            <div class="comment_number">
                Ulasan Service <span>(3)</span>
            </div>

            <div class="comment-list">
                <!-- Comment -->
                <div class="comment uk-card uk-card-default uk-card-body" id="comment-1">
                    <div class="text">
                        <div class="product-rate">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                        <h5 class="name">John Doe</h5>
                        <span class="comment_date">Posted at 15:32h, 06 December</span>
                        <a class="comment-reply-link" href="#">Reply</a>
                        <div class="text_holder">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s.</p>
                        </div>
                    </div>
                </div>
                <!-- end: Comment -->
                <!-- Comment -->
                <div class="comment uk-card uk-card-default uk-card-body" id="comment-1-1">
                    <div class="text">
                        <div class="product-rate">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                        <h5 class="name">John Doe</h5>
                        <span class="comment_date">Posted at 15:32h, 06 December</span>
                        <a class="comment-reply-link" href="#">Reply</a>
                        <div class="text_holder">
                            <p>It is a long established fact that a reader will be distracted by the
                            readable content.</p>
                        </div>
                    </div>
                </div>
                <!-- end: Comment -->
            </div>
        </div>
    </div>
</section>


<script type="text/javascript" src="<?php echo base_url('assets'); ?>/datepicker/jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/datepicker/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/datepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/datepicker/js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>

<!-- ================================================================================================================= -->
<script type="text/javascript">
   $(document).ready(function() {
    var i = 0;
    $(".button-remove").hide() ;
    //$(".minus").hide() ;
    //$(".qty").hide() ;
    //$(".plus").hide() ;
    $("#qty2").hide();
    $("#qty2_plus").hide();
    $("#qty2_minus").hide();
    $("#harga3").hide();
    $(".button-add").hide();
    $("#text-add").hide();
    $("#info-add").hide();
    // alert($('.select option').length - 1);
    var opt = $('.select option').length - 1;

    $(document).on("change", ".select", function() {
        $(".minus").show() ;
        $(".qty").show() ;
        $("#qty2").show();
        $("#qty2_plus").show();
        $("#qty2_minus").show();
        $("#harga3").show();
        $(".plus").show() ;
        $(".button-add").show();
        $("#text-add").show();
        $("#info-add").show();
        if ($('.box').length == opt){
            $(".button-add").hide();
        }
        

        
        var id_add=$(this).val();
        var res = id_add.split("|");
        var id=res[0];
        var nama_add=res[1];
        var input_panjang=document.getElementsByName('harga3').length;
        var k ;


        $.ajax({
            url : "<?php echo base_url('Shop/sub_add_qty');?>",
            method : "POST",
            data : {id: id},
            dataType : 'json',
            success: function(data){
                var html = '';
                var i;
                var nil;
                var j;

                    //for(j=0;j<opt;j++){

                        for(i=0; i<data.length; i++){

                            document.getElementsByName('name_add3[]')[input_panjang-1].value=data[i].nama;
                            document.getElementsByName('harga4[]')[input_panjang-1].value=data[i].harga;
                            document.getElementsByName('qty3[]')[input_panjang-1].value=data[i].qty;
                      // $(".harga3["+i+"]").val=data[i].qty;
                      document.getElementsByName('harga3')[input_panjang-1].value=data[i].harga;
                      document.getElementsByName('qty2')[input_panjang-1].value=data[i].qty;
                        //nil=$(".harga3["+i+"]").val;
                        //alert(input_panjang-1+", "+i+"; "+document.getElementsByName('harga3')[input_panjang-1].value+", "+document.getElementsByName('qty2')[j].value);
                        //alert(document.getElementsByName('harga3')[i].value);
                       // html += '<input type="number" class="qty" value="'+data[i].qty+'" name="harga3["'+i+'"]" disabled>';

                        // html += '<option>'+data[i].harga+'</option>';
                    }
                        //alert(document.getElementsByName('name_add2')[0].value);
                        for(k=0;k<input_panjang; k++){

                        //alert(document.getElementsByName('harga3')[k].value);
                    }
                    //}
                   // $('.harga3').html(html);

               },
               error:function(){
               }
           });
        
    });


    $(document).on("click", ".button-add", function() {
        $('.box:first').clone().insertAfter(".box:last");
        $(".button-remove").show();
        i++;
        if ($('.box').length == opt){
            $(".button-add").hide();
        }
             //alert($('.box').length +" "+ opt);
         });
    
    $(document).on("click", ".button-remove", function() {
        if ($('.button-remove', '.box').length > 1){
            $(this).closest(".box").remove();}
            else{
                $(".button-remove").hide();}

                if ($('.box').length < opt){
                    $(".button-add").show() ;
                }

            });
});
</script>

<!-- ================================================================================================================= -->

<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker();
    });
</script>
<script type="text/javascript">
    $('.form_datetime').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        startDate: '<?php echo $date->format('Y-m-d')?>',
        forceParse: 0,
        showMeridian: false,
        pickerPosition: "bottom-left"
    });
    $('.form_date').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
    $('.form_time').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0
    });
</script>
<?php $this->load->view('footer'); ?>
<script type="text/javascript">
    $('#addCart').on('click', function(e){
        e.preventDefault();
        if($("form#form-addCart").valid()){
            var mydata = $("form#form-addCart").serialize();
            console.log(mydata);
            $.ajax({
                type    : 'POST',
                url     : "<?php echo base_url('Shop/add_to_cart');?>",
                data    : mydata,
                typedata : 'json',
                success : function(result){
                    console.log(result);
                    var rs = $.parseJSON(result);
                    swal({
                        type : rs['icon'],
                        text : rs['text']
                    }).then(function(){
                        location.replace(rs['direct']);

                    })

                }

            });

        }else{
           swal({
            type : "info",
            text : "Mohon untuk melengkapi data terlebih dahulu"
        });
       }

   });

</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script type="text/javascript">
    numeral.register('locale', 'id', {
        delimiters: {
            thousands: '.',
            decimal: ','
        },
        abbreviations: {
            thousand: 'k',
            million: 'm',
            billion: 'b',
            trillion: 't'
        },
        ordinal : function (number) {
            return number === 1 ? 'er' : 'ème';
        },
        currency: {
            symbol: '€'
        }
    });
    numeral.locale('id');
</script>
<script type="text/javascript">
    $('.qty').on('change', function(e){
        var a = $(this);
        var min = parseInt(a.closest('div').find('input[name^=qty]').attr('min'));
        var qty = a.val();
        if(qty < min){
            a.val(min);
            var qty = a.val();
            var harga = parseInt(a.closest('div').find('input[name=harga]').val());
            var subTotal = numeral(harga* qty).format('0,0');
            a.closest('.col-md-12').find('.subtotal-cart').html("Rp."+subTotal);
            swal({
                type : "info",
                text : "Minimal Order : " + min
            })
            /*$.ajax({
                url : '<?php echo base_url('Jasa/transQty/');?>'+a.data('uuid')+'/'+qty,
                success : function(hasil){*/
                    a.closest('.col-md-12').find('.subtotal-cart').html("Rp."+subTotal);
                    calculate();
            /*  }
        })*/
    }else if(qty <= 0){
     swal({
        text : "Hapus jasa isi?",
        icon : "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya Hapus'
    }).then((result) => {
        if (result.value) {
           $.ajax({
              type : 'POST',
              url : '<?php echo base_url('Jasa/delete_cart/')?>'+ a.data('uuid'),
              typedata : 'json',
              success : function(hasil){
                 var rs = $.parseJSON(hasil);
                 swal({
                    type : rs['icon'],
                    text : rs['text']
                }).then( function(e) {
                    location.reload();
                });
            }
        });
       }
   })
}else{
 var harga = parseInt(a.closest('div').find('input[name=harga]').val());
 var subTotal = numeral(harga* qty).format('0,0');
 a.closest('.col-md-12').find('.subtotal-cart').html("Rp."+subTotal);
 $.ajax({
    url : '<?php echo base_url('Jasa/transQty/');?>'+a.data('uuid')+'/'+qty,
    success : function(hasil){
       a.closest('div').find('input[name^=qty]').val(qty);
       a.closest('.col-md-12').find('.subtotal-cart').html("Rp."+subTotal);
       calculate();
   }
})
}
});

    function calculate(){
        var subTotal = numeral('0');
        var qty = [];
        var i = 0;

        $('input.qty').each(function(){
           qty.push($(this).val());

       });

        $('input[name=harga]').each(function(){
           subTotal.add(parseInt($(this).val()) * qty[i]);
           i++;
       });


        var pajak = numeral(subTotal.value()/10);
        var event = numeral(subTotal.value()/20);
        var grant = numeral(subTotal.value()+pajak.value()+event.value());
        $('#subTotal').html("Rp. "+subTotal.format('0,0'));
        $('#pajak').html("Rp. "+pajak.format('0,0'));
        $('#eventCharge').html("Rp. "+event.format('0,0'));
        $('#grantTotal').html("<strong>Rp. "+grant.format('0,0')+"</strong>");

    }
// =======================================================================================================================
    // $('.plus').on('click', function(e){
        $(document).on('click', ".plus", function () {
            numeral.locale('id');
            var a = $(this);
        //alert(a);
        var qty = parseInt(a.closest('div').find('input[name^=qty]').val());
        var harga = parseInt(a.closest('div').find('input[name=harga]').val());
        qty++; 
        var subTotal = numeral(harga* qty).format('0,0');
        /*$.ajax({
            url : '<?php// echo base_url('Jasa/transQty/');?>'+a.data('uuid')+'/'+qty,
            success : function(hasil){*/
                a.closest('div').find('input[name^=qty]').val(qty);
                a.closest('.col-md-12').find('.subtotal-cart').html("Rp."+subTotal);
                calculate();
            /*}
        })*/
    })

    // $('.minus').on('click', function(e){
        $(document).on('click', ".minus", function () {
            var a = $(this);
            var qty = parseInt(a.closest('div').find('input[name^=qty]').val());
            var harga = parseInt(a.closest('div').find('input[name=harga]').val());
            var min = parseInt(a.closest('div').find('input[name^=qty]').attr('min'));
            qty--;
            if(qty < min){
                swal({
                    type : "info",
                    text : "Minimal Order : " + min
                })
            }else{

                var subTotal = numeral(harga* qty).format('0,0');

                if(a.closest('div').find('input[name^=qty]').val() == "1"){
                    swal({
                        title: 'Hapus Jasa',
                        text: "Yakin akan hapus jasa ini dari Cart?",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya Hapus'
                    }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                type : 'POST',
                                url : '<?php echo base_url('Jasa/delete_cart/')?>'+ a.data('uuid'),
                                typedata : 'json',
                                success : function(hasil){
                                    var rs = $.parseJSON(hasil);
                                    swal({
                                        type : rs['icon'],
                                        text : rs['text']
                                    }).then( function(e) {
                                        location.reload();
                                    });
                                }
                            });
                        }
                    })
                }else{
                    a.closest('div').find('input[name^=qty]').val(qty);
                    var subTotal = numeral(harga* qty).format('0,0');
                //$.ajax({
                    //url : '<?php echo base_url('Jasa/transQty/');?>'+a.data('uuid')+'/'+qty,
                    //success : function(hasil){
                        a.closest('div').find('input[name^=qty]').val(qty);
                        a.closest('.col-md-12').find('.subtotal-cart').html("Rp."+subTotal);
                        calculate();
                    //}
                //})
            }
        }
        
    })
//untuk qty Additional Counting
//awal
function calculateadd(){
  var subTotal = numeral('0');
  var qty = [];
  var i = 0;
  $('input.qty').each(function(){
   qty.push($(this).val());

});
  $('input.qty2').each(function(){
   qty.push($(this).val());

});
  $('input[name=harga]').each(function(){
   subTotal.add(parseInt($(this).val()) * qty[i]);
   i++;
});
  $('input[name=harga3]').each(function(){
   subTotal.add(parseInt($(this).val()) * qty[i]);
   i++;
});
		//$('#main-harga').html("Rp. "+subTotal.format('0,0'));
		

	}
    // untuk tomol plus additional produk
    $('.plusadd').on('click', function(e){
      numeral.locale('id');
      var a = $(this);
		//var id_add = parseInt(a.closest('div').find('input[name^=id_add]').val());
		var qty = parseInt(a.closest('div').find('input[name^=harga2]').val(0));
		var harga = parseInt(a.closest('div').find('input[name=harga3]').val());
		alert(qty);
        qty++; 
        
        var subTotal = numeral(harga* qty).format('0,0');
		/*$.ajax({
			url : '<?php //echo base_url('Shop/transQty_add/');?>'+a.data('uuid')+'/'+qty+'/'+id_add,
			success : function(hasil){*/
				a.closest('div').find('input[name^=qty2]').val(qty);
				a.closest('.col-md-12').find('.main-subtotal-cart').html("Rp."+subTotal);
				calculateadd();
				calculate();				
		/*	}
  })*/
})
    //minimum untuk additional
    $('.minusadd').on('click', function(e){
      var a = $(this);
		//var id_add = parseInt(a.closest('div').find('input[name^=id_add]').val());
		var qty = parseInt(a.closest('div').find('input[name^=qty2]').val());
		
		var harga = parseInt(a.closest('div').find('input[name=harga3]').val());
		var min = parseInt(a.closest('div').find('input[name^=qty2]').attr('min'));
		
		qty--;
		if(qty < min){
			swal({
				type : "info",
				text : "Minimal Order : " + min
			})
		}else{

			var subTotal = numeral(harga* qty).format('0,0');
			//alert(id_add+", "+qty+", "+harga+", "+subTotal)
            a.closest('div').find('input[name^=qty2]').val(qty);
            var subTotal = numeral(harga* qty).format('0,0');
				/*$.ajax({
					url : '<?php //echo base_url('Shop/transQty_add/');?>'+a.data('uuid')+'/'+qty+'/'+id_add,
					success : function(hasil){*/
						a.closest('div').find('input[name^=qty2]').val(qty);
						a.closest('.col-md-12').find('.subtotal-cart').html("Rp."+subTotal);
						calculate();
						calculateadd();
					/*}
				})*/
          }

      })
//akhir

</script>