<?php $this->load->view('header'); ?>
<!-- SUMMER SALE -->
<section >
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <label>text</label>
          <input class="form-control" type="text" name="">
        </div>
        <div class="form-group">
          <label>textarea</label>
          <textarea class="form-control" name=""></textarea>
        </div>
        <div class="form-group">
          <label>number</label>
          <input class="form-control" type="number" name="">
        </div>
        <div class="form-group">
          <label>dropdown</label>
          <select class="form-control">
            <option>1</option>
            <option>2</option>
            <option>3</option>
          </select>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <label>text</label>
          <input class="form-control datepicker" type="date" name="">
        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->load->view('footer'); ?>
