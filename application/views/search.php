<?php $this->load->view('header'); ?>


<!-- Inspiro Slider -->
<div id="slider" class="inspiro-slider arrows-large arrows-creative dots-creative" data-height-xs="360" data-autoplay-timeout="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    <!-- Slide 1 -->
    <div class="slide" style="background-image:url('<?php echo base_url('assets'); ?>/images/pages/slider-kategori/header1.jpg');"> 
        <!-- <div class="container">
            <div class="slide-captions text-right">
              
                <h2 class="text-large"">YEPS HADIR UNTUK<br/> MEMBANTU ACARA ANDA</h2>
                <a class="btn btn-light btn-outline" href="#kategori">Lihat Kategori</a>
                
            </div>
        </div> -->
    </div>
    <!-- end: Slide 1 -->
    <!-- Slide 2 -->
    <div class="slide" style="background-image:url('<?php echo base_url('assets'); ?>/images/pages/slider-kategori/header2.jpg');">
    </div>
    <!-- end: Slide 2 -->
</div>
<!--end: Inspiro Slider -->

<!-- Shop products -->
<section id="page-content" class="sidebar-left" style="background-color: #f7f7f7">
    <div class="container">
        <div class="row">
            <!-- Content-->
            <div class="content col-md-9">
                <div class="row m-b-20">
                    <div class="col-md-6 p-t-10 m-b-20">
                        <h3 class="m-b-20">YEPS Indonesia</h3>
                        <p>Hasil pencarian untuk : <b><?php echo $keyword;?></b></p>

                    </div>
                    <div class="col-md-6">
                        <div class="order-select">
                            <h6>Atur Berdasarkan</h6>
                            <!-- <p>Showing 1&ndash;12 of 25 results</p> -->

                            <form method="post" action="<?php echo base_url('Search')?>" class="form-inline">
                                <input type="hidden" name="keyword" value="<?php echo $keyword;?>">
                                <div class="form-group">
                                    <select name="sort">
                                        <option selected="selected" value="">Pengaturan Default</option>
                                        <option value="n">Nama: A - Z</option>
                                        <option value="nh">Nama: Z - A</option>
                                        <option value="ph">Harga Tinggi ke Rendah</option>
                                        <option value="p">Harga Rendah ke Tinggi</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-yeps btn-sm">Filter</button>
                            </form>
                        </div>
                    </div>
                    <!-- <div class="col-md-3">
                        <div class="order-select">
                            <h6>Atur Berdasarkan Harga</h6>
                            <form method="get">
                                <select>
                                    <option selected="selected" value="">< 10 Juta</option>
                                    <option value="">10 Juta - 50 Juta</option>
                                    <option value="">50 Juta - 70 Juta</option>
                                    <option value="">70 Juta - 100 Juta</option>
                                    <option value="">> 100 Juta</option>
                                </select>
                            </form>
                        </div>
                    </div>
                -->
            </div>
            <!--Product list-->
            <?php
            if($vendor != "" && count($vendor) > 0){
                ?>
                <div class="col-12">
                    <h5>Hasil Pencarian Vendor</h5>
                    <div class="carousel" data-items="3" data-margin="20"> 
                        <!-- VENDOR -->
                        <?php
                        foreach ($vendor as $key) {
                            $vendor_name = str_replace(" ", "_", $key['namaVendor'])."/page=1";
                            if($key['publicId'] == ""){
                                $img = base_url('./assets/images/profil.jpg');
                            }else{
                                $img = "https://res.cloudinary.com/yepsindo/image/upload//w_380,h_507,c_fill/q_auto:best/".$key['publicId'].".jpg";
                            }
                            ?>
                            <div class="vendor-box-search">
                                <table style="margin-bottom: -15px" class="">
                                    <tr>
                                        <td>
                                            <img alt="Vendor YEPS" class="vendor-img-search" src="<?php echo $img;?>">
                                        </td>
                                        <td><a class="capital vendor-name-cart" href="<?php echo base_url('jasa/'.$vendor_name);?>"><?php echo $key['namaVendor']?></a><br>
                                            <p style="font-size: 12px"><i class="fas fa-map-marker-alt">&nbsp;</i><?php echo $key['kabupaten']?></p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <?php } ?>

                            <!-- VENDOR -->
                            <!-- END -->
                        </div>
                    </div>

                    <?php } ?>

                    <hr style="height: 10px">

                    <div class="shop">
                        <h5>Hasil Pencarian Produk</h5>
                        <div class="grid-layout grid-3-columns" data-item="grid-item">
                            <?php 
                            function exist($url){
                                $file_headers = @get_headers($url);
                                if($file_headers[0] == "HTTP/1.0 200 OK"){
                                    return true;
                                }else{
                                    return false;
                                }
                            }
                            foreach ($produk['result'] as $key) {
                                $vendor = str_replace(" ","_",$key['nama vendor']);
                                $produk = $key['slug'];
                                $foto = array_filter(explode(";", $key['parameter']['Foto']));

                                foreach ($foto as $keyFoto) {
                                    $image = "https://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/q_auto:best/".$keyFoto.".jpg";
                                    if(! exist($image) || $key['parameter']['Foto'] == "" || $keyFoto == ""){
                                        $image = base_url('assets/images/pages/blank.jpg');
                                    }
                                }
                                ?>
                                <div class="grid-item">
                                    <div class="product yeps-vendor-card">
                                        <div class="product-image">
                                            <!--                                         <a href="<?php echo base_url('shop/detail/'.$key['uuid']); ?>"> -->
                                                <a href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>">

                                                    <img alt="Shop product image!" src="<?php echo $image; ?>"> 
                                                </a>
                                                <?php
                                                if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal']){
                                                    ?>
                                                    <div class="product-harga" style="display: none">
                                                    </div>
                                                    <?php
                                                }else{
                                                    ?>
                                                    <span class="product-sale-off"><?php echo ceil((((int)$key['parameter']['Harga Normal'] - (int)$key['parameter']['Harga Promosi'])/(int)$key['parameter']['Harga Normal'])*100); echo "%";?> Off</span>
                                                    <?php
                                                }
                                                ?>
                                                <div class="product-overlay">
                                                    <a href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>">Lihat Detail</a>
                                                </div>
                                            </div>

                                            <div class="product-description" style="padding: 10px">

                                                <div class="product-category">Jakarta</div>

                                                <div class="product-title product-height">
                                                    <h3><a href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>"><?php echo $key['parameter']['Nama']?></a></h3>
                                                </div>
                                                <?php
                                                if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal']){
                                                    ?>
                                                    <div class="product-harga">Rp. <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?>
                                                    </div>
                                                    <?php
                                                }else{

                                                    ?>
                                                    <div class="product-harga" style="color: #107ade">Rp. <?php echo number_format($key['parameter']['Harga Promosi'],0,",","."); ?>
                                                    </div>
                                                    <div class="diskon-harga">Rp. <strike> <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?></strike>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>

                            <hr>
                            <!-- Pagination -->
                            <center>
                                <nav class="text-center">
                                  <ul class="pagination pagination-fancy">
                                    <li> <a href="#" aria-label="Previous"> <span aria-hidden="true"><i class="fa fa-angle-left"></i></span> </a> </li>
                                    <?php
                                    $active = "";
                                    if($produk_count%12 > 0){
                                        $produk_count = ((int)($produk_count/12))+1;
                                    }else{
                                        $produk_count = ((int)($produk_count/12));
                                    }

                                    for ($i=1; $i <= $produk_count ; $i++) { 
                                        if($i == $page){
                                            $active = "class='active'";
                                        }
                                        echo "<li ".$active."><a href='".base_url('Search/produk/'.$keyword.'/'.$i)."'>".$i."</a></li>";
                                        $active = "";
                                    }
                                    ?>
                                    <li> <a href="#" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-angle-right"></i></span> </a> </li>
                                </ul>
                            </nav>
                        </center>
                        <!-- end: Pagination -->
                    </div>
                    <!--End: Product list-->
                </div>
                <!-- end: Content-->

                <!-- Sidebar-->
                <div class="sidebar col-md-3">
                    <!--widget newsletter-->
                    <?php $this->load->view('sider-category'); ?>

                    <!-- <div class="widget clearfix widget-tags">
                        <h4 class="widget-title">Tags</h4>
                        <div class="tags">
                            <a href="#">Wedding</a>
                            <a href="#">Event</a>
                            <a href="#">Venue</a>
                            <a href="#">Branding</a>
                            <a href="#">Photography</a>
                        </div>
                    </div>
                    <div class="widget clearfix widget-newsletter">
                        <form class="form-inline" method="get" action="#">
                            <h4 class="widget-title">Subscribe for Latest Offers</h4>
                            <small>Subscribe to our Newsletter to get Sales Offers &amp; Coupon Codes etc.</small>
                            <div class="input-group">

                                <input type="email" placeholder="Enter your Email" class="form-control required email" name="widget-subscribe-form-email" aria-required="true">
                                <span class="input-group-btn">
                                  <button type="submit" class="btn btn-yeps"><i class="fa fa-paper-plane"></i></button>
                              </span>
                          </div>
                      </form>
                  </div> -->


              </div>
              <!-- end: Sidebar-->
          </div>
      </div>
  </section>
  <!-- end: Shop products -->

  <?php $this->load->view('footer'); ?>