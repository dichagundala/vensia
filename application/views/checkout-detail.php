<div class="row mb20">
	<div class="col-sm-12">
		<!-- TABLE PESANAN -->
		<div class="table table-condensed table-striped table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th class="cart-product-thumbnail" style="text-align: center">Nama Vendor</th>
						<th class="cart-product-name" style="text-align: center">Kategori</th>
						<th class="cart-product-price" style="text-align: center">Biaya</th>
						<th class="cart-product-price" style="text-align: center">Kuantitas</th>
						<th class="cart-product-price" style="text-align: center">Catatan</th>
						<th class="cart-product-price" style="text-align: center">Tambahan</th>
						<th class="cart-product-price" style="text-align: center">Tanggal Event</th>
						<th class="cart-product-price" style="text-align: center">Sub Total</th>
						<th class="cart-product-price" style="text-align: center">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$biaya = 0;
					$sub = 0;
					$c = 0 ;
					foreach ($result as $key) {
						?>
						<tr>
							<td class="cart-product-thumbnail"  style="text-align: center">
								<div class="cart-product-thumbnail-name"><?php echo $key['vendor'];?></div>
							</td>
							<td class="cart-product-description" style="text-align: center">
								<p><span><?php echo $key['kategori'];?></span></p>
							</td>

							<td class="cart-product-price"  style="text-align: center">
								<span class="amount">Rp. 
									<?php 
									if($key['parameter']['Harga Promosi'] == "0"){
										echo number_format($key['parameter']['Harga Normal'],0,",",".");
										if(@$key['Status Transaksi'] == "ACCEPT" || @$tipe_detail == "checkout"){

											$biaya += (int)$key['parameter']['Harga Normal'];
										}
									}else{
										echo number_format($key['parameter']['Harga Promosi'],0,",",".");
										if(@$key['Status Transaksi'] == "ACCEPT" || @$tipe_detail == "checkout"){
											$biaya += (int)$key['parameter']['Harga Promosi'];
										}
									}
									?> 
								</span>
							</td>
							<td class="cart-product-price" style="text-align: center">
								<span class="amount">
									<?php 
									echo $key['qty'];
									?> 
								</span>
							</td>
							<td class="cart-product-price" style="text-align: center">
								<span class="amount">
									<?php 
									echo $key['Note'];
									?> 
								</span>
							</td>
							<td class="cart-product-price" style="text-align: center">
								<span class="amount">
									<?php echo $key['nama_add'][$c]; ?>&nbsp;(<?php echo $key['qty_add'][$c]; ?>)
								</span>
							</td>
							<td class="cart-product-price" style="text-align: center">
								<span class="amount">
									<?php 
									echo date_format(date_create($key['date']), "d M Y");
									?> 
								</span>
							</td>
							<td class="cart-product-price"  style="text-align: center">
								<span class="amount">Rp. 
									<?php 
									if($key['parameter']['Harga Promosi'] == "0"){
										// echo number_format($key['parameter']['Harga Normal']*$key['qty'],0,",",".");
										$add = (int)$key['harga_add'][$c] * (int)$key['qty_add'][$c];
										$sub1 = (int)$key['parameter']['Harga Normal']*$key['qty'];
										
									}else{
										// echo number_format($key['parameter']['Harga Promosi']*$key['qty'],0,",",".");
										$add = (int)$key['harga_add'][$c] * (int)$key['qty_add'][$c];
										$sub1 = (int)$key['parameter']['Harga Promosi']*$key['qty'];
										
									}
									/*untuk harga additional */
									/* awal */
									$id_produk = $key['id_add'];
									$nama_produk = $key['nama_add'];
									$qty_produk = $key['qty_add'];
									$harga_produk =$key['harga_add'];
									$total_add = count($nama_produk);
									
									$id_produk2 = $key['id_add2'];
									$nama_produk2 = $key['nama_add2'];
									$qty_produk2 = $key['qty_add2'];
									$harga_produk2 =$key['harga_add2'];
									$total_add2 = count($nama_produk2);
									// for ($x = 0; $x < $total_add; $x++){ 
									// 	$sub += (int) 	$qty_produk[$x]*	$harga_produk[$x];
									// }
									$sub += $sub1+$add;
									echo number_format($sub1+$add,0,",",".");
									?>

								</span>
							</td>
							<td class="cart-product-price" style="text-align: center">
								<span class="amount"><?php echo $key['Status Transaksi']; ?> 
								</span>
							</td>
					</tr>
					<?php $c++;
				}
				?>
			</tbody>
		</table>
		<table class="table">
			<tbody>
				<tr>
					<td class="cart-product-name">
						<strong>Sub Total</strong> 
					</td>

					<td class="cart-product-name text-right">
						<span class="amount" id="subTotal">Rp. <?php echo number_format($sub,0,",","."); ?></span>
					</td>
				</tr> 
<!-- 				<tr>
					<td class="cart-product-name">
						<strong>Pajak (10%)</strong>
					</td>

					<td class="cart-product-name  text-right">
						<span class="amount" id="pajak">Rp. <?php //echo number_format($biaya*0.1,0,",",".");?></span>
					</td>
				</tr> -->
				<tr>
					<td class="cart-product-name">
						<strong>Event Charge</strong>
					</td>

					<td class="cart-product-name  text-right">
						<span class="amount" id="eventCharge">Rp. <?php echo number_format(($sub*0.05),0,",",".");?></span>
					</td>
				</tr>
				<tr>
					<td class="cart-product-name">
						<strong>Total Pembayaran</strong>
					</td>

					<td class="cart-product-name text-right">
						<span class="amount color lead" id="grantTotal"><strong>Rp. <?php 
						echo number_format($sub+($sub*0.05),0,",",".");?></strong></span>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<!-- / TABLE PESANAN -->
</div>
</div>