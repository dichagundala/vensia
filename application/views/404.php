<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="YEPS" />
    <meta name="description" content="Your Event Partner Solution">
    <!-- Document title -->
    <title>YEPS - Not Found Product</title>
    <!-- Stylesheets & Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,800,700,600|Montserrat:400,500,600,700|Raleway:100,300,600,700,800" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets'); ?>/css/plugins.css" rel="stylesheet">
    <link href="<?php echo base_url('assets'); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url('assets'); ?>/css/responsive.css" rel="stylesheet"> 
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets'); ?>/images/favicon-16x16.png">
</head>
<style type="text/css" media="screen">
.btm-r{
    position: absolute;
    bottom: 30px;
    right: 40px;
}    
</style>
<body>
    <!-- Wrapper -->
    <div id="wrapper">
        <!-- Section -->
        <section class="fullscreen" style="background-image: url(<?php echo base_url('assets'); ?>/images/pages/error/404.jpg)">
            <div class="m-b-30" style="left: 70px">
                <a href="<?php echo base_url();?>" class="logo">
                    <img src="<?php echo base_url('assets'); ?>/images/logo-dark.png" alt="Polo Logo" style="max-height: 100px">
                </a>
            </div>
            <a href="<?php echo base_url(''); ?>" class="btn btn-default btm-r"><i class="fa fa-home"></i>Kembali ke Beranda</a>
        </section>
        <!-- end: Section -->
    </div>
    <!-- end: Wrapper -->

    <!-- Go to top button -->
    <a id="goToTop"><i class="fa fa-angle-up top-icon"></i><i class="fa fa-angle-up"></i></a>

    <!--Plugins-->
    <script src="<?php echo base_url('assets'); ?>/js/jquery.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/plugins.js"></script>

    <!--Template functions-->
    <script src="<?php echo base_url('assets'); ?>/js/functions.js"></script>
</body>

</html>
