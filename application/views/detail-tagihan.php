  
<div style="font-size: 16;text-align: center; line-height: 0px !important">

  <p>Jumlah Angsuran : <?php echo count($cicilan);?></p>
  <?php
  $lunas = 0;
  $bayar = 0;
  foreach ($cicilan as $key) {
    if($key['status'] == "LUNAS"){
      $lunas++;
    }elseif($key['status'] == "BELUM DIBAYAR"){
      $bayar++;
    }
  }
  ?>
  <p>Sudah Lunas : <?php echo $lunas;?>x</p>
  <p>Sisa Cicilan : <?php echo $bayar;?>x</p>
</div>
<table id="classTable" class="table table-bordered">
  <thead>
    <th>No</th>
    <th>Cicilan</th>
    <th>Tanggal</th>
    <th>Biaya</th>
    <th>Status</th>
    <th>Aksi</th>
  </thead>
  <tbody>
    <?php
    $i = 0;
    foreach ($cicilan as $key) {
      $i++;
      if($key['biaya_cicilan'] == "0"){
        $tanggal = "-";
        $status = "-";
        $biaya = "-";

      }else{
        $temp = new dateTime($key['tanggal']);
        $tanggal = $temp->format('d M Y');
        $status = $key['status'];
        $biaya = "Rp. ". number_format($key['biaya_cicilan'],0,",",".");
      }
      ?>
      <tr>
        <td><?php echo $i;?></td>
        <td><?php echo "Ke ".$i;?></td>
        <td><?php echo $tanggal;?></td>
        <td><?php echo $biaya;?></td>
        <td><?php echo $key['status'];?></td>
        <td><a href="<?php echo base_url('profil/konfirmasi/cicilan/'.$key['uuid']);?>" class="btn btn-primary">Konfirmasi</a></td>
      </tr>
      <?php
    }
    ?>
  </tbody>
</table>