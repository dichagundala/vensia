<?php $this->load->view('header'); ?>
<style type="text/css">

	input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
		/* display: none; <- Crashes Chrome on hover */
		-webkit-appearance: none;
		margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
	}
</style>

<section style="background-color: #f9fafb">
	<div style="padding: 0px 30px; margin: -40px 0px;">
		<div class="card" style="box-shadow: 0 1px 6px 0 rgba(0,0,0,.12);">
			<div class="card-body">
				<!-- content -->

				<!--Horizontal tabs default-->
				<div id="tabs-003" class="tabs simple justified">
					<ul class="tabs-navigation">
						<li class="active" style="text-align: center"><a href="#Riwayat"><i class="fa fa-cubes"></i>Riwayat Transaksi</a> </li>
						<li><a href="#Tagihan" style="text-align: center"><i class="icon-ecommerce-wallet"></i>Tagihanku</a> </li>
					</ul>
					<div class="tabs-content">
						<div class="tab-pane active" id="Riwayat">
							<?php $this->load->view('riwayat-detail'); ?>
						</div>
						<div class="tab-pane" id="Tagihan">
							<h4>Services</h4>
							<?php $this->load->view('tagihan-detail'); ?>
						</div>
					</div>
				</div>
				<!-- end:content -->
			</div>
		</div>
	</div>
</section>



<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<?php $this->load->view('footer'); ?>


<script>
	$(document).ready(function(){
		$("#details").hide();
		$("#lihat-detail").click(function(){
			$("#details").toggle();
		});
	});
</script>

<script>
	$(document).ready(function(){
		$("#tagihan").hide();
		$("#lihat-tagihan").click(function(){
			$("#tagihan").toggle();
		});
	});
</script>


