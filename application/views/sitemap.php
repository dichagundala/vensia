<?php '<?xml version="1.0" encoding="UTF-8" ?>' ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    <?php foreach($result as $url) { ?>
    <url>
        <loc><?php echo $url['link'];?></loc>
        <priority><?php echo $url['priority'];?></priority>
    </url>
    <?php } ?>
    
    <?php foreach($produk as $url) { ?>
    <url>
        <loc><?php echo $url['link'];?></loc>
        <priority><?php echo $url['priority'];?></priority>
    </url>
    <?php } ?>
</urlset>