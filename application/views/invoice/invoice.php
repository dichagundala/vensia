<!DOCTYPE html>
<html>
<head>
	<title>HTML to API - Invoice</title>
	<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet"> 
	<!-- <link rel="stylesheet" href="sass/main.css" media="screen" charset="utf-8"/> -->
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
	<meta http-equiv="content-type" content="text-html; charset=utf-8">
	<style type="text/css">
	html, body, div, span, applet, object, iframe,
	h1, h2, h3, h4, h5, h6, p, blockquote, pre,
	a, abbr, acronym, address, big, cite, code,
	del, dfn, em, img, ins, kbd, q, s, samp,
	small, strike, strong, sub, sup, tt, var,
	b, u, i, center,
	dl, dt, dd, ol, ul, li,
	fieldset, form, label, legend,
	table, caption, tbody, tfoot, thead, tr, th, td,
	article, aside, canvas, details, embed,
	figure, figcaption, footer, header, hgroup,
	menu, nav, output, ruby, section, summary,
	time, mark, audio, video {
		margin: 0;
		padding: 0;
		border: 0;
		font: inherit;
		font-size: 100%;
		vertical-align: baseline;
	}

	html {
		line-height: 1;
	}

	ol, ul {
		list-style: none;
	}

	table {
		border-collapse: collapse;
		border-spacing: 0;
	}

	caption, th, td {
		text-align: left;
		font-weight: normal;
		vertical-align: middle;
	}

	q, blockquote {
		quotes: none;
	}
	q:before, q:after, blockquote:before, blockquote:after {
		content: "";
		content: none;
	}

	a img {
		border: none;
	}

	article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary {
		display: block;
	}

	body {
		font-family: 'Rubik', sans-serif;
		font-weight: 300;
		font-size: 12px;
		margin: 0;
		padding: 0;
	}
	body a {
		text-decoration: none;
		color: inherit;
	}
	body a:hover {
		color: inherit;
		opacity: 0.7;
	}
	body .container {
		min-width: 500px;
		margin: 0 auto;
		padding: 0 20px;
	}
	body .clearfix:after {
		content: "";
		display: table;
		clear: both;
	}
	body .left {
		float: left;
	}
	body .center {
		float: center;
	}
	body .right {
		float: right;
	}
	body .helper {
		display: inline-block;
		height: 100%;
		vertical-align: middle;
	}
	body .no-break {
		page-break-inside: avoid;
	}

	header {
		margin-top: 20px;
		margin-bottom: 50px;
	}
	header figure {
		float: left;
			/*width: 60px;
			height: 60px;*/
			margin-right: 10px;/*
			background-color: #415b91;*/
			border-radius: 50%;
			text-align: center;
		}
		header figure img {
			margin-top: 13px;
		}
		header .company-address {
			float: left;
			max-width: 150px;
			line-height: 1.7em;
		}
		header .company-address .title {
			color: #415b91;
			font-weight: 400;
			font-size: 1.5em;
			text-transform: uppercase;
		}
		header .company-contact {
			float: right;
			height: 60px;
			padding: 0 10px;
			/*background-color: #415b91;*/
			color: white;
		}
		header .company-contact span {
			display: inline-block;
			vertical-align: middle;
		}
		header .company-contact .circle {
			width: 20px;
			height: 20px;
			background-color: white;
			border-radius: 50%;
			text-align: center;
		}
		header .company-contact .circle img {
			vertical-align: middle;
		}
		header .company-contact .phone {
			height: 100%;
			/*margin-right: 20px;*/
		}
		header .company-contact .email {
			height: 100%;
			min-width: 100px;
			text-align: right;
		}

		.mail{
			color: #1374d2;
		}

		section .details {
			margin-bottom: 55px;
		}
		section .details .client {
			width: 50%;
			line-height: 20px;
		}
		section .details .client p{
			padding-left: 10px;
		}

		section .details .client a{
			padding-left: 10px;
		}
		section .details .client .name {
			color: #415b91;
		}
		section .details .data {
			width: 50%;
			text-align: right;
		}
		section .details .data1 {
			width: 20%;
			text-align: right;
		}
		section .details .data2 {
			width: 30%;
			text-align: center;
		}
		section .details .data3 {
			width: 5%;
			text-align: right;
		}
		section .details .data4 {
			width: 45%;
			text-align: right;
		}
		section .details .title {
			margin-bottom: 15px;
			color: #415b91;
			font-size: 3em;
			font-weight: 400;
			text-transform: uppercase;
		}
		section table {
			width: 100%;
			border-collapse: collapse;
			border-spacing: 0;
			font-size: 0.9166em;
		}
		section table .qty, section table .unit, section table .total {
			width: 15%;
		}
		section table .desc {
			width: 55%;
		}
		section table .sub {
			width: 30%;
		}
		section table thead {
			display: table-header-group;
			vertical-align: middle;
			border-color: inherit;
		}
		section table thead th {
			padding: 5px 10px;
			background: #415b91;
			border-bottom: 5px solid #FFFFFF;
			border-right: 4px solid #FFFFFF;
			text-align: right;
			color: white;
			font-weight: 400;
			text-transform: uppercase;
		}
		section table thead th:last-child {
			border-right: none;
		}
		section table thead .desc {
			text-align: left;
		}
		section table thead .qty {
			text-align: center;
		}
		section table tbody td {
			padding: 10px;
			background: #f0f4f7;
			color: #777777;
			text-align: right;
			border-bottom: 5px solid #FFFFFF;
			border-right: 4px solid #f0f4f7;
		}

		.table-data {
			padding: 6px !important;
			background: #ffffff !important;
			color: black !important;
			text-align: right !important;
			border: 1px solid #777777 !important;
			/*border-right: 4px solid #777777 !important;*/
		}
		.table-list {
			padding: 6px !important;
			background: #ffffff !important;
			color: black !important;
			text-align: right !important;
			border: 1px solid #ffffff !important;
			/*border-right: 4px solid #777777 !important;*/
		}

		section table tbody td:last-child {
			border-right: none;
		}
		section table tbody h3 {
			margin-bottom: 5px;
			color: #415b91;
			font-weight: 600;
		}
		section table tbody .desc {
			text-align: left;
		}
		section table tbody .qty {
			text-align: center;
		}
		section table.grand-total {
			margin-bottom: 5px;
		}
		section table.grand-total td {
			padding: 5px 10px;
			border: none;
			color: #777777;
			text-align: right;
		}
		section table.grand-total .desc {
			background-color: transparent;
		}
		section table.grand-total tr:last-child td {
			font-weight: 600;
			color: #415b91;
			font-size: 1.18181818181818em;
		}

		section table.grand-total2 {
			margin-bottom: 5px;
		}
		section table.grand-total2 td {
			padding: 5px 10px;
			border: none;
			color: #777777;
			text-align: right;

			font-weight: 600;
			color: #415b91;
			font-size: 1.18181818181818em;
		}
		section table.grand-total2 .desc {
			background-color: transparent;
		}

		footer {
			margin-bottom: 20px;
		}
		footer .thanks {
			margin-bottom: 10px;
			color: #415b91;
			font-size: 1.16666666666667em;
			font-weight: 600;
		}
		footer .notice {
			margin-bottom: 25px;
		}
		footer .end {
			padding-top: 5px;
			border-top: 2px solid #415b91;
			text-align: center;
		}

		/*Add*/
		.inv-to{
			background-color: #415b91; color: #ffffff; padding-left: 10px; margin-bottom: 5px
		}
		.note{
			background-color: #415b91; color: #ffffff; padding-left: 10px;
		}
		.note-box{
			margin-bottom: 5px; border: 1px solid #415b91; padding-right: 10px;
		}

		.table-1{
			width: 55%;
		}
		.table-2{
			width: 45%;
		}
	</style>
</head>

<body style="background-color: white;">
	<header class="clearfix">
		<div class="container">
			<figure>
				<img class="logo" src="<?php echo base_url('assets'); ?>/images/logo.png" alt="" style="height: 70px; margin-top: 0px">
			</figure>

			<div class="company-contact">
				<div class="phone right">
					<span style="font-size: 30px; font-weight: bold; text-align: center; color: #0a5dab">INVOICE</span>
					<span class="helper"></span>
				</div>
			</div>
		</div>
	</header>

	<section style="margin-top: -30px">
		<div class="container">
			<div class="details clearfix">
				<div class="client left">
					<p style="font-size: 15px">YEPS INDONESIA</p>
					<p>Ariobimo Sentral Lantai 4. Jl. H. R. Rasuna Said Kuningan Timur, <br>Kecamatan Setiabudi, Jakarta Selatan, DKI Jakarta 12950 </p>
					<p>Phone: [021-3972-2372]</p>
					<a href="mailto:support@yepsindonesia.com" class="mail">support@yepsindonesia.com</a>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container">
			<div class="details clearfix">
				<div class="client left">
					<p class="inv-to">Kepada</p>
					<p>Nama Klien</p>
					<p>Phone / Whatsapp : 0857 xxx xxx</p>
					<p>Tanggal : 11 September 2018</p>
					<a href="mailto:john@example.com" class="mail">john@example.com</a>

				</div>
				<div class="data1 right">
					<table>
						<tbody>
							<tr>
								<td style="text-align: center !important;" class="table-data">10 Oktober 2018</td>
							</tr>
							<tr>
								<td style="text-align: center !important;" class="table-data">021-1/INV-YEPS/10/2018</td>
							</tr>
							<tr>
								<td style="text-align: center !important;" class="table-data">BLOCK 71</td>
							</tr>
							<tr>
								<td style="text-align: center !important; background: #b6d0fb !important" class="table-data">12 September 2018</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="data2 right">
					<table>
						<tbody>
							<tr>
								<td class="table-list">Tanggal Event</td>
							</tr>
							<tr>
								<td class="table-list">Invoice</td>
							</tr>
							<tr>
								<td class="table-list">Identitas Klien</td>
							</tr>
							<tr>
								<td class="table-list">Tanggal Jatuh Tempo</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<table border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th class="desc">Description</th>
						<th class="qty">Quantity</th>
						<th class="unit">Unit price</th>
						<th class="total">Total</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="desc"><h3>LNDS Catering</h3>LNDS Baso - Catering</td>
						<td class="qty">100</td>
						<td class="unit">Rp. 10.000</td>
						<td class="total">Rp. 1.000.000</td>
					</tr>
					<tr>
						<td class="desc"><h3>Es Kopi Susu Tante</h3>ArtiKopi Indonesia - Catering</td>
						<td class="qty">50</td>
						<td class="unit">Rp. 18.000</td>
						<td class="total">Rp. 900.000</td>
					</tr>
				</tbody>
			</table>
			<div class="details clearfix">
				<div class="client left"  style="margin-top: 30px">
					<p class="note">Catatan dan Saran</p>
					<div class="note-box">
						<p>1. Pembayaran pelunasan sebesar Rp. 1.519.500 (sisa pembayaran) </p>
						<p>2. Metode Pembayaran : Transfer </p>
						<p style="padding-left: 30px">Nama Bank : Bank Mandiri</p>
						<p style="padding-left: 30px">No. Rek : 1230007621875</p>
						<p style="padding-left: 30px">Atas Nama : Redho Octaviano ZP</p>
						<p style="padding-left: 30px">NPWP : 86.260.098.8-067.000</p>
						<p>3. Sisa pembayaran diselesaikan maksimal 3 hari setelah terbitnya invoice ini atas pelaksanaan event/pesanan yang telah selesai</p>
					</div>
				</div>
				<div class="data3 right"></div>
				<div class="data4 right">
					<div class="no-break">
						<table class="grand-total">
							<tbody>
								<tr>
									<td class="sub">SUBTOTAL:</td>
									<td class="total">Rp. 1.900.000</td>
								</tr>
								<!-- <tr>
									<td class="sub">PAJAK 10%:</td>
									<td class="total">Rp. 190.000</td>
								</tr> -->
								<tr>
									<td class="sub">EVENT CHARGE:</td>
									<td class="total">Rp. 95.000</td>
								</tr>
								<tr>
									<td class="sub">TOTAL PEMBAYARAN:</td>
									<td class="total">Rp. 2.185.000</td>
								</tr>
							</tbody>
						</table>
						<table class="grand-total2">
							<tbody>
								<tr>
									<td class="sub">TOTAL DP 30%:</td>
									<td class="total">Rp. 665.500</td>
								</tr>
								<tr>
									<td class="sub">SISA PEMBAYARAN:</td>
									<td class="total">Rp. 1.519.500</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>

	<footer>
		<div class="container">
			<div class="thanks">Thank You For Your Business!</div>
			<div class="notice">
				<div style="line-height: 15px">Jika Anda memiliki pertanyaan tentang invoice ini, silahkan hubungi<br>Rama, Phone | WhatsApp: +62 858 1321 5324, <span class="mail">rama@yepsindonesia.com</span> </div>
			</div>
			<div class="end">Invoice was created on a computer and is valid without the signature and seal.</div>
		</div>
	</footer>
</body>
<!-- <script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.0/jspdf.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/rasterizehtml/1.3.0/rasterizeHTML.allinone.js"></script> -->
<!-- <script>
	let doc = new jsPDF('p','cm','letter');

	doc.addHTML(document.body, 0, 0, function() {
		//doc.save('html.pdf');
		$("body").html("");
		var iframe = document.createElement('iframe');
		iframe.setAttribute('style','overflow:hidden;height:100%;width:100%;position:absolute;top:0px;left:0px;right:0px;bottom:0px;background-color:white;');
		iframe.name = "INVOICE-29";
		iframe.frameBorder = 0;
		iframe.style.width = 100 + "%";
		iframe.style.height = 100 + "%";
		iframe.src = doc.output('datauristring');
		document.body.appendChild(iframe); 
	});
</script> -->

</html>
