
<form class="form" action="<?php echo base_url('dummy/addDummy')?>" method="POST" id="form-param">
 <div class="col-sm-12">

    <div class="form-group">
        <label>Pilih Vendor</label>
        <select name="vendor">
         <?php
         foreach ($vendor as $key) {
            echo "<option value='".$key['uuid']."'>".$key['nama']."</option>";
        }
        ?>
    </select>
</div>
</div>
<h2 align="center">Parameter</h2>
<?php
foreach ($param as $key) {

    ?>
    <div class="col-sm-6">

        <div class="form-group">
            <label><?php echo $key['nama'];?></label>
            <input type="hidden" class="form-control" name="trx[]" value="<?php echo $key['id'];?>">
            <input type="text" class="form-control" name="value[]" autocomplete="off">
        </div>
    </div>
    <?php } ?>
    <input type="hidden" name="kategori" value="<?php echo $kategori;?>">
    <div class="col-sm-12">
        <button type="submit" class="btn btn-primary" id="submit-param" >Submit</button>

    </div>
</form>
