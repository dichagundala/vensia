<!-- Footer -->
<?php
//untuk get IP Address
function getUserIpAddr(){
  if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        //ip from share internet
    $ip = $_SERVER['HTTP_CLIENT_IP'];
  }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        //ip pass from proxy
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
  }else{
    $ip = $_SERVER['REMOTE_ADDR'];
  }
  return $ip;

}
//echo 'User Real IP - '.getUserIpAddr();
?>
<footer id="footer" class="footer-light">
  <div class="footer-content">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <!-- Footer widget area 1 -->
          <div class="widget clearfix widget-contact-us">
            <ul class="list-icon">
              <li>
                <i class="fa fa-map-marker"></i>Jalan Hj. Murtado XIII No 206 Kelurahan Paseban <br>Kecamatan Senen, Jakarta Pusat DKI Jakarta
              </li>
              <li><i class="fa fa-envelope"></i> <a href="mailto:halo@yepsindonesia.com">halo@vensia.id</a>
              </li>
            </ul>
          </div>
          <!-- end: Footer widget area 1 -->
        </div>
        <div class="col-md-2">
          <!-- Footer widget area 2 --> 
          <div class="widget">
            <ul class="list-icon list-icon-arrow">
              <li><a href="https://vensia.id/id/tentang-kami/">Tentang Kami</a></li>
              <li><a href="https://vensia.id/id/syarat-dan-ketentuan">Syarat dan Ketentuan</a></li>
            </ul>
          </div>
          <!-- end: Footer widget area 2 -->
        </div>
        <div class="col-md-2">
          <!-- Footer widget area 2 --> 
          <div class="widget">
            <ul class="list-icon list-icon-arrow">
                <li><a href="https://vensia.id/id/keamanan">Keamanan</a></li>
                <li><a href="https://vensia.id/id/event-management">Event Management</a></li>
              </ul>
          </div>
          <!-- end: Footer widget area 2 -->
        </div>
        
        <div class="col-md-3">
          <!-- Footer widget area 3 -->
          <div class="widget">
            <div class="widget">
              <!-- Social icons -->
              <div class="social-icons social-icons-border float-left m-t-20">
                <ul>
                  <li class="social-instagram"><a href="https://www.instagram.com/vensia/"><i class="fab fa-instagram"></i></a></li>
                  <li class="social-facebook"><a href="#"><i class="fab fa-facebook"></i></a></li>
                  <li class="social-twitter"><a href="#"><i class="fab fa-twitter"></i></a></li>
                  <li class="social-youtube"><a href="#"><i class="fab fa-youtube"></i></a></li>
                  <!-- <li class="social-gplus"><a href="#"><i class="fa fa-google-plus"></i></a></li> -->
                </ul>
              </div>
              <!-- end: Social icons -->
            </div>
          </div>
          <!-- end: Footer widget area 3 -->
        </div>
      </div>
    </div>

  </div>
  <div class="copyright-content" style="background-color: #ca1d69">
    <div class="container">
      <div class="copyright-text text-center">&copy; 2019 Vensia. All Rights Reserved.
      </div>
    </div>
  </div>
</footer>
<!-- end: Footer -->

</div>
<!-- end: Wrapper -->



<!-- Go to top button -->
<!--<a href="https://tawk.to/chat/5b3c386c6d961556373d623e/default" id="mybutton" target="_blank">
    <button class="btn btn-yeps">Live Chat</button>
  </a>-->

  <a id="goToTop"><i class="fa fa-angle-up top-icon"></i><i class="fa fa-angle-up"></i></a>
  <!-- uikit -->
  <script src="<?php echo base_url('assets'); ?>/uikit/js/uikit.js"></script>
  
  <!--Plugins-->
  <script src="<?php echo base_url('assets'); ?>/js/jquery.js"></script>
  <script src="<?php echo base_url('assets'); ?>/js/plugins.js"></script>

  <!--Template functions-->
  <script src="<?php echo base_url('assets'); ?>/js/functions.js"></script>

  <script src="<?php echo base_url('assets'); ?>/js/custom.js"></script>
  <script type="text/javascript" src="<?php echo base_url('assets'); ?>/timepicker/dist/bootstrap-clockpicker.min.js"></script>

  <script type="text/javascript">
    $('.clockpicker').clockpicker({
      placement: 'top',
      align: 'left',
      donetext: 'Done'
    });
  </script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.11/dist/sweetalert2.all.min.js"></script>

  <script type="text/javascript">
    $('form#form-login').on('submit', function(e){
      e.preventDefault();

      var mydata = $("form#form-login").serialize();
      $.ajax({
        type : "POST",
        url : "<?php echo base_url('Authss/cek_login')?>",
        data : mydata,
        datatype : "json",
        success : function(hasil){
          console.log(hasil);
          var rs = $.parseJSON(hasil);
          Swal.fire({
            position: 'center',
            type : rs['icon'],
            text : rs['text'],
            showConfirmButton: false,
            timer: 2500
          });
          location.replace(rs['direct']);
        }
      });
    });
  </script>
  <script type="text/javascript">
    $('form#form-search').on('submit', function(e){
      e.preventDefault();

      var keyword = $("#keyword").val();
      location.replace("<?php echo base_url('Search/produk/')?>"+keyword+"/1");

    });
  </script>
  <script type="text/javascript">
    $('#modal-3').on('show.bs.modal', function(e){
      var uuid = $(e.relatedTarget).data('uuid');
      var modal = $(this);

      modal.find('.modal-title').html("Pilihan Tanggal Keberangkatan : <u>" + name + "</u>");
      $('.modal-body').html("Getting Data...");
      $.ajax({
        type : 'post',
        url : '<?php echo base_url('Shop/detail_cart/')?>'+uuid,
        success : function(data){
          $('.modal-body').html(data);
        }
      });
    })
  </script>
  <script type="text/javascript">
   /*for show and hidden password*/
   $(".toggle-password").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
     input.attr("type", "text");
   } else {
     input.attr("type", "password");
   }
 });
</script>
<script type="text/javascript">
  /*setelah klik masuk*/
  $('#form-login').on('submit', function(e){
    e.preventDefault();

    var mydata = $('form#form-login').serialize();
    $.ajax({
      type        : "POST",
      url         : "<?php echo base_url('index.php/Authss/cek_login');?>",
      data        : mydata,
      datatype    : "json",
      success     : function(hasil){
        console.log(hasil);                       
        var rs = $.parseJSON(hasil); 
        swal({
          type : rs['icon'],
          text : rs['text']
        }).then( function() {
          if(rs['icon'] == "success"){
            location.replace(rs['direct']);
          }
        });
      } 
    });
  });
</script>
<!--Start of Tawk.to Script-->
<!-- <script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5b6bc6e4df040c3e9e0c6ddc/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script> -->
<!--End of Tawk.to Script-->

<!-- Partical Js files  -->
<!-- <script src="js/plugins/components/particles.js" type="text/javascript"></script>
<script src="js/plugins/components/particles-animation.js" type="text/javascript"></script>
-->

<script>
  function myFunction() {
    document.getElementById("dropdown-yeps").classList.toggle("show");
  }
</script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
  function alert1(){           
   swal("Pemberitahuan", "Pengajuan vendor anda belum diproses oleh Back Office, mohon untuk bersabar","success");
 }
 function alert2(){           
   swal("Pemberitahuan", "Pengajuan vendor sedang dalam proses oleh Back Office, mohon untuk bersabar","success");
 }
 function alert3(){
   swal("Pemberitahuan", "Pengajuan vendor anda ditolak", "error");
 }
 function alert4(){
   swal("Pemberitahuan", "Akun anda telah diblokir", "warning");
 }
 function alert5(){
  $.post("<?php echo base_url('vendors/auth/login'); ?>", $("#login_form").serialize(), function(data, status) {
    console.log(data);
    data = JSON.parse(data);
    
    if(data.status == "error"){
      swal({
        title: "Login gagal!",
        text: data.message,
        type: "error"
      }).then((result) => {
        location.reload();
      });
    }
    else{      
      localStorage.setItem("namaVendor", data[0].namaVendor);
      localStorage.setItem("url", data[0].secureUrl);
      localStorage.setItem("kategori", data[0].kategori);
      window.location.href = "<?php echo base_url('vendors/home');?>";   
    }
  });
}
</script>
</body>
</html>