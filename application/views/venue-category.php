<?php $this->load->view('header'); ?>


<!-- Inspiro Slider -->
<div id="slider" class="inspiro-slider arrows-large arrows-creative dots-creative" data-height-xs="360" data-autoplay-timeout="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    <!-- Slide 1 -->
    <div class="slide" style="background-image:url('<?php echo base_url('assets'); ?>/homepages/shop-v3/images/b1.jpg');"> 
        <div class="container">
            <div class="slide-captions text-right">
                <!-- Captions -->
                <h2 class="text-large"">YEPS HADIR UNTUK<br/> MEMBANTU ACARA ANDA</h2>
                <a class="btn btn-light btn-outline" href="#kategori">Lihat Kategori</a>
                <!-- end: Captions -->
            </div>
        </div>
    </div>
    <!-- end: Slide 1 -->
    <!-- Slide 2 -->
    <div class="slide" style="background-image:url('<?php echo base_url('assets'); ?>/homepages/shop-v3/images/b2.jpg');">
        <div class="container">
            <div class="slide-captions">
                <!-- Captions -->
                <h2 class="text-large text-dark">DISKON SAMPAI<br /> DENGAN 50%</h2>
                <a class="btn btn-dark" href="photography-category.html">Lihat Koleksi</a>
                <!-- end: Captions -->
            </div>
        </div>
    </div>
    <!-- end: Slide 2 -->
</div>
<!--end: Inspiro Slider -->

<!-- Shop products -->
<section id="page-content" class="sidebar-left">
    <div class="container">
        <div class="row">
            <!-- Content-->
            <div class="content col-md-9">
                <div class="row m-b-20">
                    <div class="col-md-6 p-t-10 m-b-20">
                        <h3 class="m-b-20">YEPS Venue</h3>
                        <p>Tentukan lokasi acara yang terbaik sesuai dengan keinginan anda.</p>
                       
                    </div>
                    <div class="col-md-3">
                        <div class="order-select">
                            <h6>Atur Berdasarkan Tanggal</h6>
                            <!-- <p>Showing 1&ndash;12 of 25 results</p> -->

                            <form method="get">
                                <input type="date" class="form-control required date" name="">
                            </form>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="order-select clockpicker">
                            <h6>Atur Berdasarkan Waktu</h6>
                            <!-- <p>Showing 1&ndash;12 of 25 results</p> -->

                            <form method="get">
                                <input type="text" name="waktu" class="form-control" placeholder="Select time">
                            </form>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="order-select">
                            <h6>Atur Berdasarkan</h6>
                            <!-- <p>Showing 1&ndash;12 of 25 results</p> -->

                            <form method="get">
                                <select>
                                    <option selected="selected" value="order">Pengaturan Default</option>
                                    <option value="popularity">Popularitas</option>
                                    <option value="asc">Nama: A - Z</option>
                                    <option value="dsc">Nama: Z - A</option>
                                    <option value="price">Harga Tinggi ke Rendah</option>
                                    <option value="price-desc">Harga Rendah ke Tinggi</option>
                                    <option value="disc">Diskon</option>
                                </select>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="order-select">
                            <h6>Atur Berdasarkan Harga</h6>
                            <form method="get">
                                <select>
                                    <option selected="selected" value="">< 10 Juta</option>
                                    <option value="">10 Juta - 50 Juta</option>
                                    <option value="">50 Juta - 70 Juta</option>
                                    <option value="">70 Juta - 100 Juta</option>
                                    <option value="">> 100 Juta</option>
                                </select>
                            </form>
                        </div>
                    </div>

                </div>
                <!--Product list-->
                <div class="shop">

                    <div class="grid-layout grid-3-columns" data-item="grid-item">
                        <div class="grid-item">
                            <?php 
                                foreach ($produk['result'] as $key) {
                            ?>
                            <div class="product">
                                <div class="product-image">
                                    <a href="<?php echo base_url('shop/detail/'.$key['uuid']); ?>"><img alt="Shop product image!" src="<?php echo $key['parameter']['Foto']; ?>"> 
                                    </a>
                                    <span class="product-new">NEW</span>
                                    <span class="product-wishlist">
                                        <a href="<?php echo base_url('venue/jadwal'); ?>" data-toggle="tooltip" data-placement="right" title="Lihat Jadwal"><i class="fa fa-calendar"></i></a>
                                    </span>
                                    <div class="product-overlay">
                                        <a href="<?php echo base_url('shop/detail/'.$key['uuid']); ?>">Quick View</a>
                                    </div>
                                </div>

                                <div class="product-description">
                                    <div class="product-category">Jakarta Selatan</div>
                                    <div class="product-title">
                                        <h3><a href="<?php echo base_url('shop/detail'); ?>"><?php ?></a></h3>
                                    </div>
                                    <?php
                                     if($key['parameter']['Harga Normal'] == "0"){

                                        
                                    ?>
                                    <div class="product-harga">Rp. <?php echo number_format($key['parameter']['Harga Normal'],0,".",","); ?>
                                    </div>
                                    <?php
                                        }else{

                                            ?>
                                            <div class="product-harga">Rp. <?php echo number_format($key['parameter']['Harga Promosi'],0,".",","); ?>
                                            </div>
                                            <div class="product-harga">Rp. <strike> <?php echo number_format($key['parameter']['Harga Normal'],0,".",","); ?></strike>
                                            </div>

                                            <?php

                                        }
                                    ?>
                                </div>
                            </div>
                            <?php
                                }
                            ?>
                        </div>
                        
                    </div>
                    <hr>
                    <!-- Pagination -->
                    <nav class="text-center">
                      <ul class="pagination pagination-fancy">
                        <li> <a href="#" aria-label="Previous"> <span aria-hidden="true"><i class="fa fa-angle-left"></i></span> </a> </li>
                        <li><a href="#">1</a> </li>
                        <li><a href="#">2</a> </li>
                        <li class="active"><a href="#">3</a> </li>
                        <li><a href="#">4</a> </li>
                        <li><a href="#">5</a> </li>
                        <li> <a href="#" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-angle-right"></i></span> </a> </li>
                    </ul>
                </nav>
                <!-- end: Pagination -->
            </div>
            <!--End: Product list-->
        </div>
        <!-- end: Content-->

        <!-- Sidebar-->
        <div class="sidebar col-md-3">
            <!--widget newsletter-->
            <?php $this->load->view('sider-category'); ?>

            <div class="widget clearfix widget-tags">
                <h4 class="widget-title">Tags</h4>
                <div class="tags">
                    <a href="#">Wedding</a>
                    <a href="#">Event</a>
                    <a href="#">Venue</a>
                    <a href="#">Branding</a>
                    <a href="#">Photography</a>
                </div>
            </div>
            <div class="widget clearfix widget-newsletter">
                <form class="form-inline" method="get" action="#">
                    <h4 class="widget-title">Subscribe for Latest Offers</h4>
                    <small>Subscribe to our Newsletter to get Sales Offers &amp; Coupon Codes etc.</small>
                    <div class="input-group">

                        <input type="email" placeholder="Enter your Email" class="form-control required email" name="widget-subscribe-form-email" aria-required="true">
                        <span class="input-group-btn">
                          <button type="submit" class="btn btn-default"><i class="fa fa-paper-plane"></i></button>
                      </span>
                  </div>
              </form>
          </div>


      </div>
      <!-- end: Sidebar-->
  </div>
</div>
</section>
<!-- end: Shop products -->

<?php $this->load->view('footer'); ?>