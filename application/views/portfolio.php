<?php $this->load->view('header'); ?>

<section class="section-pattern p-t-60 p-b-30 text-center" style="background: url(<?php echo base_url('assets'); ?>/images/pages/bg.jpg)">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 class="text-medium" style="color: white">YEPS Portfolio</h3>
            </div>
        </div>
    </div>
</section>

<section id="page-content">
    <div class="container">
        <?php
        if(count($data) <= 0){
            ?>
            <div class="row box-cart" style="background-color: #f9fafb;">
                <center>
                    <img src="<?php echo base_url('assets'); ?>/images/pages/icon/upload.svg" alt="" style="max-width: 25%; margin-bottom: 20px" data-animation="pulse infinite">
                    <h5 class="text-center">Team kami sedang menyiapkan Portfolio untuk di tampilkan</h5>
                </center>
            </div>

            <?php
        } else {
            ?>
            <!-- Portfolio YEPS -->
            <div id="blog" class="grid-layout post-2-columns m-b-30" data-item="post-item">

                <?php foreach ($data as $d ) { 
                    if ($d['link'] == null) {
                     $exfoto = explode(",",$d['foto']);
                     ?>
                     <!-- Post item-->
                     <div class="post-item border shadow">
                        <div class="post-item-wrap">
                            <div class="post-image">
                                <a href="<?php echo base_url('asset/images/portfolio/'.$d['foto']); ?>">
                                    <img alt="Foto Slider" src="<?php echo base_url('asset/images/portfolio/'.$exfoto[0]); ?>">
                                </a>
                                <!-- <span class="post-meta-category"><a href="">Lifestyle</a></span> -->
                            </div>
                            <div class="post-item-description">
                                <span class="post-meta-date"><i class="fa fa-calendar-o"></i><?php echo date_format(date_create($d['date_event']), 'd F Y');?></span>
                                <h2><a href="<?php echo base_url('Portfolio/detail/'.$d['id']) ?>"><?php echo $d['judul'] ?>
                            </a></h2>
                            <p><?php echo $d['brief'] ?></p>

                            <a href="<?php echo base_url('Portfolio/detail/'.$d['id']) ?>" class="item-link">Read More <i class="fa fa-arrow-right"></i></a>

                        </div>
                    </div>
                </div>
                <!-- end: Post item-->
            <?php } else { ?>
                <!-- Post item YouTube-->
                <div class="post-item border shadow">
                    <div class="post-item-wrap">
                        <div class="post-video">
                            <?php $idvid = explode("=",$d['link']) ?>
                            <iframe width="560" height="376" src="https://www.youtube.com/embed/<?php echo $idvid[1] ?>" frameborder="0" allowfullscreen></iframe>
                            <!-- <span class="post-meta-category"><a href="">Video</a></span> -->
                        </div>
                        <div class="post-item-description">
                            <span class="post-meta-date"><i class="fa fa-calendar-o"></i><?php echo date_format(date_create($d['date_event']), 'd F Y');?></span>

                            <h2><a href="<?php echo base_url('Portfolio/detail/'.$d['id']) ?>"><?php echo $d['judul'] ?></a></h2>
                            <p><?php echo $d['brief'] ?></p>

                            <a href="<?php echo base_url('Portfolio/detail/'.$d['id']) ?>" class="item-link">Read More <i class="fa fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- end: Post item YouTube-->
            <?php } } ?>
        </div>
        <!-- end: Portfolio -->
    <?php } ?>

</div>
</section>
<!-- end: Content -->


<?php $this->load->view('footer'); ?>