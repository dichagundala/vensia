<?php $this->load->view('header'); ?>
<link href="<?php echo base_url('assets'); ?>/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<!-- SUMMER SALE -->
<section class="section-pattern p-t-60 p-b-30 text-center text-light upper" text-light upper" style="background: url(<?php echo base_url('assets'); ?>/images/pages/bg_other.jpg)">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h3 class="text-medium">Profil Pengguna</h3>
      </div>
    </div>
  </div>
</section>
<!-- end: SUMMER SALE -->

<section >
  <div class="container">
    <!--Vertical tabs default -->
    <div id="tabs-09" class="tabs tabs-vertical">

      <div class="box tabs-navigation" style="padding-right: 5px">
        <form class="form" id="form-img">
          <center>
            <?php
            if($users['img'] == ""){
              $users['img'] = base_url('./assets/images/profil.jpg');
            }else{
              $users['img'] = base_url('./uploads/'.$users['img']);
            }
            ?>
            <img src="<?php echo $users['img'];?>" class="img-responsive img-thumbnail" alt="User Img YEPS" id="gambar">
          </center>
          <div class="form-group text-center">
            <input type="file" class="form-control" name="file" id="img"> 
          </div>
          <div class="form-group text-center">
            <button class="btn btn-vensia" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Simpan</button>
          </div>
        </form>

      </div>
      <div class="tabs-content" style="border-radius: 5px">  
        <div class="tab-pane active widget-archive">
          <h4 class="widget-title"><?php echo $users['namaDepan'];echo " "; echo $users['namaBelakang'];?></h4>
          <ul class="list list-lines list-icon">
            <li><i class="icon-mail"></i> <?php echo $users['email'];?>
          </li>
          <li><i class="icon-smartphone"></i> <?php echo $users['telp'];?>
        </li>
        <li><i class="icon-postcard-multiple"></i> 
          <?php 
          if($users['tglLahir'] != "0000-00-00"){
            $viewDate = $users['tglLahir'];
            echo date_format(date_create($users['tglLahir']), "d M Y");
          } else{
            $viewDate = "2000-01-01";
            echo "-";
          }
          ?>
        </li>
        <li><i class="icon-home"></i>
          <?php 
          if($users['address'] != ""){

            echo $users['address'];
          } else{
            echo "-";
          }
          ?> 
        </li>
      </ul>
    </div>
  </div>
</div>
<!--END: Vertical tabs default -->


<!--Horizontal tabs default-->
<div id="tabs-003" class="tabs simple">
  <ul class="tabs-navigation">
    <li class="active"><a href="#ubahprofil"><i class="fa fa-edit"></i>Ubah Profil</a> </li>
    <li><a href="#history"><i class="fa fa-history"></i>Riwayat Pesanan</a> </li>
    <li><a href="#tagihan"><i class="icon-ecommerce-wallet"></i>Tagihanku</a> </li>
  </ul>
  <div class="tabs-content">
    <div class="tab-pane active" id="ubahprofil">
      <!--Default Form-->
      <div class="hr-title hr-long center"><abbr>Ubah Biodata Diri</abbr> </div>
      <div class="row">
       <div class="col-md-10 col-md-offset-1">
        <form class="form-gray-fields" id="form-update">
         <div class="row">
          <div class="col-md-6">
           <div class="form-group">
            <label class="upper" for="name">Nama Depan</label>
            <input type="text" class="form-control required" name="namaDepan" placeholder="Masukkan Nama Depan" value="<?php echo $users['namaDepan'];?>" id="name3" aria-required="true">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label class="upper" for="name">Nama Belakang</label>
            <input type="text" class="form-control required" name="namaBelakang" placeholder="Masukkan Nama Belakang" value="<?php echo $users['namaBelakang'];?>" id="name32" aria-required="true">
          </div>
        </div>
        <div class="col-md-12">
         <div class="form-group">
          <label class="upper" for="email">Email</label>
          <input type="email" class="form-control required email" name="email" placeholder="Masukkan Email" value="<?php echo $users['email'];?>" id="email3" aria-required="true">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
       <div class="form-group">
        <label class="upper" for="phone">Nomor Handphone</label>
        <input type="text" class="form-control required" name="telp" placeholder="Masukkan Nomor Handphone" id="phone3" value="<?php echo $users['telp'];?>" aria-required="true">
      </div>
    </div>
    <div class="col-md-6">
     <div class="form-group">
      <label class="upper" for="company">Jenis Kelamin</label>
      <select id="dok_asli" name="kelamin" class="form-control" required="required" placeholder="--Pilih--" value=''>
        <!-- <option value="2" selected disabled>--Pilih--</option> -->
        <?php if($users['gender']=="male"){?>
          <option value="male">Pria</option>
          <option value="female">Wanita</option>
        <?php }if($users['gender']=="female"){ ?>
          <option value="female">Wanita</option>
          <option value="male">Pria</option>
        <?php } ?>
      </select>
    </div>
  </div>
  <div class="col-md-12">

   <div class="form-group">
    <label class="upper" for="company">Tanggal Lahir</label>
    <div class="input-group date form_datetime" data-date="<?php echo $viewDate;?>" data-date-format="dd MM yyyy" data-link-field="dtp_input1">
      <input class="form-control" size="12" type="text" readonly name="tgl_lahir" value="<?php if($users['tglLahir']=="0000-00-00"){echo "Masukkan Tanggal Lahir";}else{echo date_format(date_create($users['tglLahir']), "d M Y");}?>" required>
      <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
    </div>
    <input type="hidden" id="dtp_input1" value="" /><br/>
  </div>

</div>
</div>
<div class="row">
  <div class="col-md-12">
   <div class="form-group">
    <label class="upper" for="comment">Alamat</label>
    <textarea class="form-control required" name="alamat" rows="9" placeholder="Masukkan Alamat Anda" id="comment3" aria-required="true"><?php echo $users['address'];?></textarea>
  </div>
</div>
</div>
<div class="row">
  <div class="col-md-12">
   <div class="form-group text-center">
    <button class="btn btn-vensia" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Simpan</button>
  </div>
</div>
</div>
</form>
</div>
</div>
<!--END: Default Form-->
</div>


<div class="tab-pane" id="history">
  <div class="row">

    <!-- Section -->
    <section>
      <div class="container"> 
        <!--Table with Borders-->
        <div class="hr-title hr-long center" style="margin-top: -60px"><abbr>Riwayat Pesanan</abbr> </div>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>No</th>
                <th>Nomor Order</th>
                <th>Tanggal Pesan</th>
                <th>Status</th>
                <th>Refund</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $i = 0;
              foreach ($invoice as $key) {
                $i++;
                
                ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td>
                    <?php
                    if($key['Status Invoice'] == "MENUNGGU PEMBAYARAN" || $key['Status Invoice'] == "KONFIRMASI"){
                      ?>
                      <a href="<?php echo base_url('Shop/checkout_completed/'.$key['Invoice Uuid']); ?>"><?php echo $key['Kode Invoice']?></a>
                      <?php
                    }else{                      
                      echo $key['Kode Invoice'];
                    }
                    ?>
                  </td>
                  <td><?php echo date_format(date_create($key['Tgl Pesan']), 'd-m-Y');?></td>
                  <td><?php echo $key['Status Invoice'];?></td>
                  <td><a data-target="#modal" data-toggle="modal" href="#" class="btn btn-xs">Refund</a></td>
                </tr>
                <?php
              }
              ?>
            </tbody>
          </table>
        </div>

        <!--END: Table with Borders-->
      </div>
    </section>



  </div>
</div>
<div class="tab-pane" id="tagihan">
  <div class="hr-title hr-long center"><abbr>Tagihan Pesanan</abbr> </div>
  <div class="table-responsive">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>No</th>
          <th>Nomor Order</th>
          <th>Tanggal Pembayaran Selanjutnya</th>
          <th>Detail</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $i = 0;
        
        foreach ($invoice as $key) {
          if($key['Status Invoice'] == "MENUNGGU PEMBAYARAN"){
            $i++;

            ?>
            <tr>
              <td><?php echo $i;?></td>
              <td><?php echo $key['Kode Invoice'];?></td>
              <td>DP 30%</td>
              <td><a href="<?php echo base_url('Shop/checkout_completed/'.$key['Invoice Uuid']); ?>" class="btn btn-xs">Lihat Rincian</a></td>
            </tr>
            <?php 
          } }
          ?>

          <?php

          foreach ($tagihan as $key) {
            $i++;
            ?>
            <tr>
              <td><?php echo $i;?></td>
              <td><?php echo $key['Kode Invoice'];?></td>
              <td>Cicilan</td>
              <td><a href="#" data-uuid="<?php echo $key['Invoice Uuid'];?>" data-invoice="<?php echo $key['Kode Invoice'];?>" data-cicilan="<?php echo $key['cicilan'];?>" class="btn btn-xs rincian">Lihat Rincian</a></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
<!--END: Horizontal tabs default-->

<!-- --  Modal Auto Load -- -->
<?php if(($users['namaDepan'] == "")||($users['email'] == "")||($users['telp'] == "")||
  ($users['gender'] == "")||
  ($users['tglLahir'] == "0000-00-00")||($users['address'] == "")) { ?>
    <div id="modalText" class="modal modal-auto-open text-center" data-delay="3000">
      <h3 style="font-weight: bold; text-transform: uppercase;">Lengkapi Profil Anda</h3>
      <p>Lengkapi informasi anda dan segera terhubung lebih mudah dengan YEPS.</p>
      <a class="btn btn-xs modal-close" href="#">Close</a>
    </div>
  <?php } ?>
  <!-- --  end: Modal Auto Load -- -->

</div>
</section>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/datepicker/jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/datepicker/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/datepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/datepicker/js/locales/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>

<script type="text/javascript">
  $('.form_datetime').datetimepicker({
        //language:  'fr',
        format: "dd MM yyyy",
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0,
        pickerPosition: "bottom-left"
      });
  $('.form_date').datetimepicker({
    language:  'fr',
    weekStart: 1,
    todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
    forceParse: 0
  });
  $('.form_time').datetimepicker({
    language:  'fr',
    weekStart: 1,
    todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 1,
    minView: 0,
    maxView: 1,
    forceParse: 0
  });
</script>

<?php $this->load->view('footer'); ?>
<div class="modal fade" id="modal" tabindex="-1" role="modal" aria-labelledby="modal-label" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="modal-label">Refund Transaksi</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <p>Permintaan Refund anda akan di proses selama 2x24 jam oleh Management kami. Terima Kasih.</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-b" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

<div id="classModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="classInfo" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          ×
        </button>
        <h3 class="modal-title" id="classModalLabel" align="center">
          Detail Tagihan
        </h3>
      </div>
      <div class="modal-body" style="overflow-x: auto;">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">
          Close
        </button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('.rincian').on('click', function(e){
    e.preventDefault();
    var a = $(this);
    $.ajax({
      url : "<?php echo base_url('Profil/detailTagihan/')?>"+a.data('uuid'),
      success : function(e){
        $('#classModal .modal-body').html(e);
        $('#classModal .modal-title').html("Detail Tagihan "+a.data('invoice'));
        $('#classModal').modal({show:true});
      }
    })
  })

  $('form#form-update').on('submit', function(e){
    e.preventDefault();
    var mydata = $('form#form-update').serialize();
    console.log(mydata);
    $.ajax({
      type    : "POST",
      url     : "<?php echo base_url('index.php/profil/update_profil')?>",
      data: mydata,
      datatype: "json",
      success : function(hasil){
        console.log(hasil);
        var rs = $.parseJSON(hasil);
        swal({
          type : rs['icon'],
          text : rs['text']
        }).then( function() {
          if(rs['icon'] == "success"){
            location.replace(rs['direct']);
          }
        });
      }
    });
  });

  $('form#form-img').on('submit', function(e){
    e.preventDefault();
    $.ajax({
      type    : "POST",
      url     : "<?php echo base_url('index.php/profil/update_img')?>",
      data: new FormData(this),
      processData: false,
      contentType: false,
      cache:false,
      async:false,
      datatype: "json",
      success : function(hasil){
        console.log(hasil);
        var rs = $.parseJSON(hasil);
        swal({
          type : rs['icon'],
          text : rs['text']
        }).then( function() {
          if(rs['icon'] == "success"){
            location.replace(rs['direct']);
          }
        });
      }
    });
  });

  function readURL1(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#gambar').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#img").change(function(){
    readURL1(this);
  });
</script>
