<hr class="our-space">

<section id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1 style="font-size: 28px; font-weight: 700; margin: 0">Service yang terkait</h1>
                <p style="margin-top: 0px">Servis rekomendasi untuk Anda</p>
            </div>
            <div class="col-md-4 text-right">
                <a href="<?php echo base_url('kategori/tipe/Invitation'); ?>"
                    class="btn btn-outline btn-rounded btn-sm btn-shadow">Lihat Semua</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="shop">
                <div class="grid-layout grid-6-columns" data-item="grid-item">
                    <?php foreach (array_slice($Invitation['result'],0,6) as $key) {

                        $vendor = str_replace(" ","_",$key['nama vendor']);
                        $produk = $key['slug'];

                        $foto = array_filter(explode(";", $key['parameter']['Foto']));

                        if(count($foto) > 0){
                            foreach ($foto as $keyFoto) {
                                $image = "http://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/q_auto:best/".$keyFoto.".jpg";
                                if(empty($image) || $key['parameter']['Foto'] == "" || $keyFoto == ""){
                                    $image = base_url('assets/images/pages/blank.jpg');
                                }
                            }
                        }else{
                            $image = "http://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/q_auto:best/".$key['parameter']['Foto'].".jpg";
                            if(empty($image) || $key['parameter']['Foto'] == "" || $key['parameter']['Foto'] == ""){
                                $image = base_url('assets/images/pages/blank.jpg');
                            }
                        } ?>

                    <div class="grid-item">
                        <div class="product yeps-vendor-card">
                            <div class="product-image">
                                <a href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>">
                                    <img alt="Shop product image!" style="border-radius: 8px"
                                        src="<?php echo $image; ?>">
                                </a>
                                <?php
                                    if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal']){
                                ?>
                                <div class="product-harga" style="display: none"></div>
                                <?php }else{ ?>
                                <span
                                    class="product-sale-off"><?php echo ceil((((int)$key['parameter']['Harga Normal'] - (int)$key['parameter']['Harga Promosi'])/(int)$key['parameter']['Harga Normal'])*100); echo "%";?>
                                    Off</span>
                                <?php } ?>
                            </div>
                            <div class="product-description" style="padding: 10px">
                                <div class="product-category">
                                    <?php if($key['parameter']['Lokasi'] == ""){ echo "&nbsp;";}else{echo $key['parameter']['Lokasi'];}?>
                                </div>
                                <div class="product-title product-height">
                                    <h3>
                                        <a
                                            href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>"><?php echo $key['parameter']['Nama']?></a>
                                    </h3>
                                </div>
                                <?php
                                    if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal'])
                                    { ?>
                                <div class="product-harga">
                                    Rp. <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?>
                                </div>
                                <?php } else { ?>
                                <div class="product-harga" style="color: #FB9307">
                                    Rp. <?php echo number_format($key['parameter']['Harga Promosi'],0,",","."); ?>
                                </div>
                                <div class="diskon-harga">
                                    Rp. <strike>
                                        <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?></strike>
                                </div>
                                <?php } ?>
                                <!-- <div style="color: #ADAAAA; font-size: 13px"><i class="fa fa-users"></i> Nama Vendor Gans</div> -->
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>