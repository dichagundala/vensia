<?php $this->load->view('header'); ?>

<section class="section-pattern p-t-60 p-b-30 text-center" style="background: url(<?php echo base_url('assets'); ?>/images/pattern/pattern22.png)">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h3 class="text-medium">Riwayat Pesanan</h3>
      </div>
    </div>
  </div>
</section>

<!-- Section -->
<section>
  <div class="container">

    <!--Rectangle Countdown-->
    <div class="hr-title hr-long center"><abbr>Sisa Waktu Pembayaran</abbr> </div>
    <div class="row">
      <div class="countdown rectangle small" data-countdown="2018/3/31 24:00:00"></div>
    </div>
    <!--END: Rectangle Countdown-->
  </div>
</section>
<!-- end: Section -->

<!-- SHOP CART -->
<section id="shop-cart" style="margin-top: -70px">
 <div class="container">
  <div class="container">
    <div class="shop-cart">
      <div class="table table-condensed table-striped table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th class="cart-product-remove"></th>
              <th class="cart-product-thumbnail">Nama Vendor</th>
              <th class="cart-product-name">Kategori</th>
              <th class="cart-product-price">Biaya</th>
              <th class="cart-product-quantity">Detail</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="cart-product-remove">
                <a href="#"><i class="fa fa-close"></i></a>
              </td>
              <td class="cart-product-thumbnail">
                <div class="cart-product-thumbnail-name">Hotel Santika</div>
              </td>
              <td class="cart-product-description">
                <p><span>Venue</span></p>
              </td>

              <td class="cart-product-price">
                <span class="amount">Rp. 120.000.000</span>
              </td>

              <td class="cart-product-subtotal">
                <a href="#" class="btn btn-default btn-xs" data-target="#modal-3" data-toggle="modal">Lihat Detail</a>
              </td>
            </tr>
            <tr>
              <td class="cart-product-remove">
                <a href="#"><i class="fa fa-close"></i></a>
              </td>
              <td class="cart-product-thumbnail">
                <div class="cart-product-thumbnail-name">Alta Photo</div>
              </td>
              <td class="cart-product-description">
                <p><span>Photography</span></p>
              </td>

              <td class="cart-product-price">
                <span class="amount">Rp. 5.000.000</span>
              </td>

              <td class="cart-product-subtotal">
                <a href="#" class="btn btn-default btn-xs" data-target="#modal-3" data-toggle="modal">Lihat Detail</a>
              </td>
            </tr>
            <tr>
              <td class="cart-product-remove">
                <a href="#"><i class="fa fa-close"></i></a>
              </td>
              <td class="cart-product-thumbnail">
                <div class="cart-product-thumbnail-name">Sumba Catering</div>
              </td>
              <td class="cart-product-description">
                <p><span>Catering</span></p>
              </td>

              <td class="cart-product-price">
                <span class="amount">Rp. 10.000.000</span>
              </td>

              <td class="cart-product-subtotal">
                <a href="#" class="btn btn-default btn-xs" data-target="#modal-3" data-toggle="modal">Lihat Detail</a>
              </td>
            </tr>

          </tbody>

        </table>

      </div>

      <div class="row">
        <hr class="space">
        <div class="col-md-6">

        </div>
        <div class="col-md-6 p-r-10 ">
          <div class="table-responsive">
            <h4>Cart Subtotal</h4>

            <table class="table">
              <tbody>
                <tr>
                  <td class="cart-product-name">
                    <strong>Sub Total</strong> 
                  </td>

                  <td class="cart-product-name text-right">
                    <span class="amount">Rp. 135.000.000</span>
                  </td>
                </tr>
                <tr>
                  <td class="cart-product-name">
                    <strong>Pajak (10%)</strong>
                  </td>

                  <td class="cart-product-name  text-right">
                    <span class="amount">Rp. 13.500.000</span>
                  </td>
                </tr>
                <tr>
                  <td class="cart-product-name">
                    <strong>Event Charge</strong>
                  </td>

                  <td class="cart-product-name  text-right">
                    <span class="amount">Rp. 6.750.000</span>
                  </td>
                </tr>
                <td class="cart-product-name">
                  <strong>Total Pembayaran</strong>
                </td>

                <td class="cart-product-name text-right">
                  <span class="amount color lead"><strong>Rp. 155.250.000</strong></span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <a href="<?php echo base_url('#'); ?>/" class="btn btn-success icon-left float-right m-l-10"><span>Lihat Invoice</span></a>
        <a href="<?php echo base_url('#'); ?>/" class="btn btn-danger icon-left float-right"><span>Batalkan Pesanan</span></a>
      </div>
    </div>
  </div>
</div>
</div>
</section>
<!-- end: SHOP CART -->

<!--- MODAL --->
<div class="modal fade" id="modal-3" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 id="modal-label-3" class="modal-title">Swiss BelHotel Pondok Indah - Rp.328.888.000 </h4>
      </div>
      <div class="modal-body">
        <div class="row mb20">
          <div class="col-sm-12">
            <p>1. Buffet for 900 persons<br>
              2. Complimentary 700 portions of selected food stall<br>
              3. Complimentary 100 cups of ice cream stall<br>
              4. Complimentary soft drinks equal to buffet order<br>
              5. Food tasting for 8 persons after 50% down payment<br>
              6. Complimentary separate buffet for 30 persons inside the private room on the day of wedding<br>
              7. Free one night stay in Suite room for Bride and Groom including breakfast for 2 persons<br>
              8. Free one night stay in one Superior room for family including breakfast for 2 persons<br>
              9. Special rate for additional room for family or friends on the day of wedding<br>
              10. Free photo taking at bridal room, lobby and swimming pool during your stay<br>
              11. Red carpet from ballroom entrance to the bridal stage<br>
              12. Complimentary two bottles of sparkling fruit for the wedding toast<br>
              13. Free usage of private room for Tea Pai ceremony<br>
              14. Complimentary VIP parking for 5 cars including 20 free parking on the day of wedding<br>
              15. Complimentary 6 reception books<br>
              16. Free one night stay in Superior room on your 1st Wedding Anniversary including dinner voucher at Rp 500,000<br>
              17. Entertainment : MC, Singer, Baby Grand Piano, Saxophone, Bass and standard Sound System<br>
            18. Free decoration option for your choice from our vendor at Rp 30,000,000</p></p>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 xs-box">
            <div class="box-services-a">
              <h3>Description</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis maiores repudiandae, accusantium reiciendis!</p>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="box-services-a">
              <h3>Additional Info</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis maiores repudiandae, accusantium reiciendis!</p>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-b" type="button">Close</button>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('footer'); ?>