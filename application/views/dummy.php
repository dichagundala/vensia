<?php $this->load->view('header'); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">

        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">Add Data Dummy</div>
            <div class="panel-body">
                <form class="form">
                    <div class="form-group">

                        <div class="col-sm-2">
                            <label>Pilih Category</label>
                            <select id="category">
                                <?php
                                foreach ($kategori as $key) {
                                    echo "<option value='".$key['id']."'>".$key['nama']."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </form>
                <div class="col-sm-10" id="parameter" style="border-left: solid 5px blue; border-radius: 5px;">

                </div>
                
                
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>
<script type="text/javascript">
    $.ajax({
        url : '<?php echo base_url("dummy/getParam/1");?>',
        success : function(hasil){
            $("#parameter").html(hasil);
        }
    })
    $("#category").on('change', function(){
        var id = $("#category").val();
        $.ajax({
            url : '<?php echo base_url("dummy/getParam/");?>'+id,
            success : function(hasil){
                $("#parameter").html(hasil);
            }
        })
    });

    
</script>

<script type="text/javascript">
    var form = new FormData();
form.append("key", "RwcnGa7MC9A15jvJ2xufU50uQrtuU1");
form.append("action", "payment");
form.append("produk", "INV/001");
form.append("price", "10000");
form.append("quantity", "1");
form.append("comment", "test");
form.append("ureturn", "http://localhost/api15/return.php?q=return");
form.append("unotify", "http://localhost/api15/notify.php");
form.append("ucancel", "http://localhost/api15/cancel.php");
form.append("format", "json");

var settings = {
  "async": true,
  "crossDomain": true,
  "url": "https://my.ipaymu.com/payment.htm",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache",
    "Access-Control-Allow-Origin": "https://my.ipaymu.com/payment.htm",
    "postman-token": "19d5d0e9-5d47-835d-79ec-b60635951b8a"
  },
  "processData": false,
  "contentType": false,
  "mimeType": "multipart/form-data",
  "data": form
}

$.ajax(settings).done(function (response) {
  console.log(response);
});
</script>

