<?php $this->load->view('header'); ?>

<!-- Page title -->
<!-- <section id="page-title" data-parallax-image="<?php echo base_url('assets'); ?>/images/parallax/5.jpg">
	<div class="container">
		<div class="page-title">
			<h1>Checkout</h1>
			<span>Checkout details</span>
		</div>
		<div class="breadcrumb">
			<ul>
				<li><a href="#">Home</a>
				</li>
				<li class="active"><a href="#">Checkout</a>
				</li>
			</ul>
		</div>
	</div>
</section> -->
<!-- end: Page title -->

<section class="section-pattern p-t-60 p-b-30 text-center" style="background: url(<?php echo base_url('assets'); ?>/images/pattern/pattern22.png)">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h3 class="text-medium">Checkout</h3>
				<?php var_dump($result); ?>
			</div>
		</div>
	</div>
</section>

<!-- SHOP CHECKOUT -->
<section id="shop-checkout">
	<div class="container">
		<div class="shop-cart">
			<div class="hr-title hr-long center"><abbr>Pilih Metode Pembayaran dan Layanan Angsuran Pembayaran </abbr> </div>
			<div class="seperator"><span>Detail Pembelian</span></div>
			<center>
				<a href="#" class="btn btn-default icon-left" data-target="#modal-3" data-toggle="modal"><span>Lihat Pesanan Anda</span></a>
			</center>



			<div class="seperator"><i class="fa fa-credit-card"></i>Pembayaran Diawal Adalah 30% dari Total Pembayaran Anda<i class="fa fa-credit-card"></i>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="table-responsive">
						<h4>Detail Pembayaran</h4>
						<?php 
						$subTotal = 0;
						$dates = array();
						foreach ($result as $key) {
							if($key['parameter']['Harga Promosi'] == "0"){
								$subTotal += (int)$key['parameter']['Harga Normal']*$key['qty'];
							}else{
								$subTotal += (int)$key['parameter']['Harga Promosi']*$key['qty'];	
							}
							$dates[] = $key['date'];
						}

						$max = 0;
						$datetime1 = new DateTime('Now');
						$longestdate = new DateTime('Now');
						$interval = 0;
						foreach($dates as $date){
							$datetime2 = new DateTime($date);
							$interval = $datetime1->diff($datetime2);
							
							if((int)$interval->format("%R%a") > $max){
								$max = (int)$interval->format("%R%a");
							}
							
						}
						
						$grantTotal = $subTotal + $subTotal/10 + $subTotal/20;
						$angsuran = $grantTotal*0.7;
						?>
						<input type="hidden" name="angsuran" value="<?php echo $angsuran;?>">
						<input type="hidden" name="hari" value="<?php echo $max;?>">
						<table class="table">
							<tbody>
								<tr>
									<td class="cart-product-name">
										<strong>Sub Total</strong> 
									</td>

									<td class="cart-product-name text-right">
										<span class="amount">Rp. <?php echo number_format($subTotal,0,",","."); ?></span>
									</td>
								</tr>
								<tr>
									<td class="cart-product-name">
										<strong>Pajak (10%)</strong>
									</td>

									<td class="cart-product-name  text-right">
										<span class="amount">Rp. <?php echo number_format($subTotal/10,0,",","."); ?></span>
									</td>
								</tr>
								<tr>
									<td class="cart-product-name">
										<strong>Event Charge</strong>
									</td>

									<td class="cart-product-name  text-right">
										<span class="amount">Rp. <?php echo number_format($subTotal/20,0,",","."); ?></span>
									</td>
								</tr>
								<td class="cart-product-name">
									<strong>Total Pembayaran</strong>
								</td>

								<td class="cart-product-name text-right">
									<span class="amount color lead"><strong>Rp. <?php echo number_format($grantTotal,0,",","."); ?></strong></span>
								</td>
							</tr>
						</tbody>
					</table>
					<table class="table">
						<tbody>
							<tr>
								<td class="cart-product-name">
									<strong>Total DP (30%)</strong>
								</td>

								<td class="cart-product-name text-right">
									<span class="amount color lead"><strong>Rp. <?php echo number_format($grantTotal*0.3,0,",","."); ?></strong></span>
								</td>
							</tr>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-6">
			<div class="table-responsive">
				<h4>Jangka Waktu Pembayaran</h4>
				<select name="angsuran" id="angsuran">
					<option>Pilih Angsuran Bulan</option>
					<?php

					if($max < 14){
						echo "<option value='1'>1x Angsuran</option>";
					}
					if($max >= 14){
						echo "<option value='3'>3x Angsuran</option>";
					}
					if($max > 30){
						echo "<option value='4'>4x Angsuran</option>";
					}
					if($max > 45){
						echo "<option value='5'>5x Angsuran</option>";
					}

					?>
				</select>
				<table class="table" id="table-angsuran">
					<tbody>
						
					</tbody>
				</table>
				<br><br><br><br><br>
			</div>
		</div>

		<div class="seperator"><i class="fa fa-credit-card"></i> Pilih Metode Pembayaran <i class="fa fa-credit-card"></i>
		</div>

		<div class="col-md-12">
			<h4 class="upper">Payment Method</h4>

			<table class="payment-method table table-bordered table-condensed table-responsive">
				<tbody>
					<tr>
						<td>
							<div class="radio">
								<label>
									<input type="radio" name="optionsRadios" value=""><b class="dark">Transfer Bank</b>
									<br>
								</label>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="radio">
								<label>
									<input type="radio" name="optionsRadios" value=""><b class="dark">Virtual Account</b>
								</label>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<a class="btn btn-default icon-left float-right" style="margin-left: 10px" id="confirm">
				<span>Konfirmasi Pesanan</span>
			</a>
			<a class="btn btn-danger icon-left float-right" href="<?php echo base_url('shop'); ?>/"><span>Kembali ke Pemesanan</span></a>
		</div>
	</div>
</div>
</div>
</section>
<!-- end: SHOP CHECKOUT -->

<!-- DELIVERY INFO -->
<section class="background-grey p-t-40 p-b-0">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="icon-box medium fancy">
					<div class="icon" data-animation="pulse infinite"> <a href="#"><i class="fa fa-smile-o"></i></a> </div>
					<h3>Support 24/7</h3>
					<p>Kami siap melayani anda 24 jam setiap hari</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="icon-box medium fancy">
					<div class="icon" data-animation="pulse infinite"> <a href="#"><i class="fa fa-lock"></i></a> </div>
					<h3>Data Privacy</h3>
					<p>Sistem Kami menjamin data pelanggan agar tidak tersebar ke pihak lain.</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="icon-box medium fancy">
					<div class="icon" data-animation="pulse infinite"> <a href="#"><i class="fa fa-angellist"></i></a> </div>
					<h3>Friendly User</h3>
					<p>Sistem ini dibuat untuk memudahkan pelanggan untuk melakukan aktivitasnya</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end: DELIVERY INFO -->


<!-- / MODAL KONFIRMASI -->


<?php $this->load->view('footer'); ?>
<!--- MODAL -->
<div class="modal fade" id="modal-3" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
				<h4 id="modal-label-3" class="modal-title">Detail Pesanan Anda </h4>
			</div>
			<div class="modal-body">
				<?php $this->load->view('checkout-detail');?>
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-b" type="button">Close</button>
			</div>
		</div>
	</div>
</div>
<!--- MODAL KONFIRMASI -->
<div class="modal fade" id="konfirmasi" tabindex="-1" role="modal" aria-labelledby="modal-label" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="modal-label">Konfirmasi Pesanan</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<p>Minta konfirmasi ketersediaan pemesanan yang telah Anda pesan?</p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
				<a href="<?php echo base_url('shop/checkout_completed'); ?>/" type="button" class="btn btn-default">Ya</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#angsuran').on('change', function(e){
		var a = $(this);
		var isi = "";
		var angsuran = $('input[name=angsuran]').val();
		var max = $('input[name=hari]').val();

		var hari = parseInt(max/a.val());
		var add = hari;
		$("table#table-angsuran").empty();
		$('table#table-angsuran').append("<tr><td class='cart-product-name' align='center'><strong>Tanggal Angsuran</strong></td><td class='cart-product-name' align='center'><strong>Perkiraan Besar Angsuran</strong></td></tr>");
		for(var i = 1; i<= a.val(); i++){
			var date = new Date();
			var hasil = date.addDays(hari);
			var day = hasil.getDate()+"-"+(hasil.getMonth()+1)+"-"+hasil.getFullYear(); 
			$('table#table-angsuran').append("<tr><td align='center'>"+day+"</td><td align='center'>"+(angsuran/a.val())+"</td></tr>");
			hari += add;
		}	
	})

	Date.prototype.addDays = function(days) {
		var date = new Date(this.valueOf());
		date.setDate(date.getDate() + days);
		return date;
	}

</script>
<script type="text/javascript">
	$('a#confirm').on('click', function(e){
		e.preventDefault();
		swal({
			title: 'Konfirmasi Pesanan',
			text: "Minta konfirmasi ketersediaan pemesanan yang telah Anda pesan?",
			type: 'info',
			showCancelButton: true,
			confirmButtonColor: '#107ADE',
			cancelButtonColor: '#C30000',
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					type : 'POST',
					url : '<?php echo base_url('Shop/generateInvoice/')?>'+ a.data('uuid'),
					typedata : 'json',
					success : function(hasil){
						var rs = $.parseJSON(hasil);
						swal({
							type : rs['icon'],
							text : rs['text']
						}).then( function(e) {
							location.reload();
						});
					}
				});
			}
		})
	})
</script>