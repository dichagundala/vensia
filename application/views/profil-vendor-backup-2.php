<?php $this->load->view('header'); ?>

<!-- Our numbers -->
<section class="p-t-40 p-b-40" style="background-image: linear-gradient(to right, #05519a , #107ade, #4091de);">
    <div class="container xs-text-center sm-text-center">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
               <div class="equalize testimonial testimonial-box" style="box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2), 0 2px 10px 0 rgba(0, 0, 0, 0.19);">
                <div class="p-b-5 p-t-5 testimonial-item">

                    <?php
                    if($vendor['public_id'] == ""){
                        $img = base_url('./assets/images/profil.jpg');
                    }else{
                        $img = "https://res.cloudinary.com/yepsindo/image/upload/w_380,h_507/".$vendor['public_id'].".jpg";
                    }
                    ?>
                    <img alt="Vendor img" class="yeps-avatar m-t-10" src="<?php echo $img; ?>">
                    <span><h1 class="yeps-vendor-name">
                        <?php echo $vendor['nama'];?><br>
                        <?php echo $vendor['deskripsi'];?>
                        
                    </h1></span>
                    <p class="yeps-vendor-location">
                        <i class="fas fa-map-marker-alt" style="color: black"></i> 
                        &nbsp;<?php echo $vendor['kabupaten'];?>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
        <!-- <div class="col-md-6">
           <h4 class="m-b-10 text-light">Pilih Kategori</h4>

           <select class="selectpicker" multiple>
            <?php
            $kategori = explode(",", $vendor['kategori']);
            foreach ($kategori as $key) {
                echo "<option value='".$key."'>".$key."</option>";
            }
            ?>
        </select>
        <h4 class="m-b-10 text-light">Atur Berdasarkan</h4>

        <select class="selectpicker m-b-20">
            <option value="popularity">Popularitas</option>
            <option value="asc">Nama: A - Z</option>
            <option value="dsc">Nama: Z - A</option>
            <option value="price">Harga Tinggi ke Rendah</option>
            <option value="price-desc">Harga Rendah ke Tinggi</option>
            <option value="disc">Diskon</option>
        </select>

        <div class="btn-group bootstrap-select">
            <a href="" class="btn btn-danger" style="width: 100%">CARI</a>
        </div>
    </div>
    <div class="col-md-1"></div> -->
    
</div>
</div>
</section>
<!-- end: Our numbers -->

<!-- Content -->
<section id="page-content" style="background-color: #f7f7f7;">
    <div class="container clearfix container-fullscreen">
        <div class="row">
            <!-- post content -->
            <div id="tabs-04" class="tabs simple justified no-border" style="margin-top: -40px">
                <ul class="tabs-navigation">
                    <li class="active" style="text-align: center"><a href="#About4"><i class="fa fa-home"></i>Produk</a> </li>
                    <li><a href="#Choose4" style="text-align: center"><i class="fa fa-images"></i>Portfolio</a> </li>
                    <li><a href="#Assets4" style="text-align: center"><i class="fa fa-book"></i>Tentang Kami</a> </li>
                </ul>
                <div class="tabs-content" style="background-color: #f7f7f7">
                    <div class="tab-pane active" id="About4">
                        <div class="row m-b-20">
                            <div class="col-md-6 p-t-10 m-b-20"></div>
                            <div class="col-md-3 p-t-5" style="text-align: right;"><h5>Atur Berdasarkan</h5></div>
                            <div class="col-md-3">
                                <div class="order-select">
                                    <!-- <form action="<?php echo base_url('kategori/tipe/'.$kategori.'/'.$page)?>" method="post" class="form-inline"> -->
                                        <form action="#" method="post" class="form-inline">
                                            <div class="form-group">
                                                <select name="sort">
                                                    <option selected="selected" value="order">Pengaturan Default</option>
                                                    <option value="n">Nama: A - Z</option>
                                                    <option value="nh">Nama: Z - A</option>
                                                    <option value="ph">Harga Tinggi ke Rendah</option>
                                                    <option value="p">Harga Rendah ke Tinggi</option>
                                                </select>
                                            </div>
                                            <button type="submit" class="btn btn-default btn-sm">Filter</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--Product list-->
                            <div class="shop container">
                                <div class="grid-layout grid-5-columns" data-item="grid-item">
                                    <?php 
                                    function exist($url){
                                        $file_headers = @get_headers($url);
                                        if($file_headers[0] == "HTTP/1.0 200 OK"){
                                            return true;
                                        }else{
                                            return false;
                                        }
                                    }
                                    foreach ($produk['result'] as $key){
                                       $nama_vendor = str_replace(" ","_",$key['vendor']);
                                       $produk = $key['slug'];
                                       $foto = array_filter(explode(";", $key['parameter']['Foto']));
                                       if(count($foto) > 0){

                                        foreach ($foto as $keyFoto) {
                                            $image = "https://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/".$keyFoto.".jpg";
                                            if(!exist($image) || $key['parameter']['Foto'] == "" || $keyFoto == ""){
                                                $image = base_url('assets/images/pages/blank.jpg');
                                            }
                                        }
                                    }else{

                                        $image = "https://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/".$key['parameter']['Foto'].".jpg";

                                        if(!exist($image) || $key['parameter']['Foto'] == "" || $key['parameter']['Foto'] == ""){
                                            $image = base_url('assets/images/pages/blank.jpg');
                                        }
                                    }
                                    ?>
                                    <div class="grid-item">
                                        <div class="product yeps-vendor-card">
                                            <div class="product-image">
                                                <a href="<?php echo base_url('jasa/'.$nama_vendor.'/'.$produk); ?>">

                                                    <img alt="Shop product image!" src="<?php echo $image; ?>"> 
                                                </a>
                                                <?php
                                                if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal']){
                                                    ?>
                                                    <div class="product-harga" style="display: none">
                                                    </div>
                                                    <?php
                                                }else{
                                                    ?>
                                                    <span class="product-sale-off"><?php echo ceil((((int)$key['parameter']['Harga Normal'] - (int)$key['parameter']['Harga Promosi'])/(int)$key['parameter']['Harga Normal'])*100); echo "%";?> Off</span>
                                                    <?php
                                                }
                                                ?>
                                                <div class="product-overlay">
                                                    <a href="<?php echo base_url('jasa/'.$nama_vendor.'/'.$produk); ?>">Lihat Detail</a>
                                                </div>
                                            </div>

                                            <div class="product-description" style="padding: 10px">
                                                <div class="product-category"><?php $key['parameter']['Lokasi']; ?></div>

                                                <div class="product-title product-height">
                                                    <h3><a href="<?php echo base_url('jasa/'.$nama_vendor.'/'.$produk); ?>"><?php echo $key['parameter']['Nama']?></a></h3>
                                                </div>
                                                <?php
                                                if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal']){
                                                    ?>
                                                    <div class="product-harga">Rp. <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?>
                                                    </div>
                                                    <?php
                                                }else{

                                                    ?>
                                                    <div class="product-harga" style="color: #107ade">Rp. <?php echo number_format($key['parameter']['Harga Promosi'],0,",","."); ?>
                                                    </div>
                                                    <div class="diskon-harga">Rp. <strike> <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?></strike>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                            </div>
                            <hr>
                            <!-- Pagination -->
                            <center>
                                <nav class="text-center">
                                  <ul class="pagination pagination-fancy">
                                    <li> <a href="#" aria-label="Previous"> <span aria-hidden="true"><i class="fa fa-angle-left"></i></span> </a> </li>
                                    <?php
                                    $active = "";
                                    if($produk_count%10 > 0){
                                        $produk_count = ((int)($produk_count/10))+1;
                                    }else{
                                        $produk_count = ((int)($produk_count/10));
                                    }

                                    for ($i=1; $i <= $produk_count ; $i++) { 
                                        $nama_vendor = str_replace(" ", "_", $vendor['nama']);
                                        if($i == $page){
                                            $active = "class='active'";
                                        }
                                        echo "<li ".$active."><a href='".base_url('jasa/'.$nama_vendor.'/page='.$i)."'>".$i."</a></li>";
                                        $active = "";
                                    }
                                    ?>
                                    <li> <a href="#" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-angle-right"></i></span> </a> </li>
                                </ul>
                            </nav>
                        </center>
                        <!-- end: Pagination -->
                    </div>
                    <!--End: Product list-->
                </div>

                <div class="tab-pane" id="Choose4" style="height: 1000vh;">
                    <div class="container">
                        <div class="row">
                            <!-- Gallery -->
                            <div class="grid-layout grid-3-columns" data-margin="20" data-item="grid-item" id="gallery-album">

                            </div>
                            <!-- end: Gallery -->
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="Assets4">
                    <div class="container">
                        <div class="row">
                            <p>
                                <?php echo $vendor['deskripsi']?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: post content -->
    </div>
</div>
</section>
<!-- end: Content -->

<?php $this->load->view('footer'); ?>

<script type="text/javascript">
    $.ajax({
       url: 'http://localhost/api15/vendors/album_with_sampul',
       type: 'POST',
       data: {
         uuid_vendor : '<?php echo $vendor['uuid']; ?>'
     },
     dataType: 'json',
     success: function(result){
        console.log(result);
      for(var i = 0; i < result.length; i++){
        /*var template = "<div class='grid-item'>"+result[i].judul+"<br>"+result[i].total+"<br>"+result[i].deskripsi+"<br>"+result[i].tgl_kegiatan+"<br>"+result[i].uuid+"</div><div class='grid-item'>"+result[i].judul+"<br>"+result[i].total+"<br>"+result[i].deskripsi+"<br>"+result[i].tgl_kegiatan+"<br>"+result[i].uuid+"</div>";*/

        var template = "<div class='grid-item'><a class='image-hover-zoom' href='https://res.cloudinary.com/yepsindo/image/upload/w_380,h_507/"+result[i].public_id+"' data-lightbox='gallery-item'><img src='https://res.cloudinary.com/yepsindo/image/upload/w_380,h_507/"+result[i].public_id+"'></a></div>";

        $("#gallery-album").append(template);
    }
}
});
</script>