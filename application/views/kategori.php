<?php $this->load->view('header'); ?>

<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="min-height: 200; max-height: 300; autoplay: true; animation: push">

    <ul class="uk-slideshow-items">
        <li>
            <img src="<?php echo base_url('assets'); ?>/images/pages/slider-home/vensia-banner.jpg" alt="" uk-cover>
        </li>
        <li>
            <img src="<?php echo base_url('assets'); ?>/images/pages/slider-home/vensia-banner-1.jpg" alt="" uk-cover>
        </li>
        <li>
            <img src="<?php echo base_url('assets'); ?>/images/pages/slider-home/vensia-banner-2.jpg" alt="" uk-cover>
        </li>
    </ul>

    <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
    <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

</div>
<!-- Shop products -->
<section id="page-content" class="sidebar-left">
    <div class="lyt-vnsia">
        <div>
            <!-- Content-->
            <div class="content col-md-10">
                <div class="row m-b-20">
                    <div class="col-md-6 p-t-10 m-b-20">
                        <h3 class="m-b-20">Vensia <?php echo $kategori;?></h3>
                        <p>Temukan vendor <?php echo strtolower($kategori);?> terbaik sesuai dengan keinginan anda.</p>

                    </div>
                    <div class="col-md-6">
                        <div class="order-select">
                            <h6>Atur Berdasarkan</h6>
                            <!-- <p>Showing 1&ndash;12 of 25 results</p> -->

                            <form action="<?php echo base_url('kategori/tipe/'.$kategori.'/'.$page)?>" method="post" class="form-inline">
                                <div class="form-group">
                                    <select name="sort">
                                        <option selected="selected" value="order">Pengaturan Default</option>
                                        <option value="n">Nama: A - Z</option>
                                        <option value="nh">Nama: Z - A</option>
                                        <option value="ph">Harga Tinggi ke Rendah</option>
                                        <option value="p">Harga Rendah ke Tinggi</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-vensia btn-sm">Filter</button>
                                
                            </form>
                        </div>
                    </div>
                </div>
                <!--Product list-->
                <?php
                ?>
                <div class="shop">

                    <div class="grid-layout grid-4-columns" data-item="grid-item">
                        <?php
                        function exist($url){
                            $file_headers = @get_headers($url);
                            if($file_headers[0] == "HTTP/1.0 200 OK"){
                                return true;
                            }else{
                                return false;
                            }
                        }
                        foreach ($produk['result'] as $key) {

                            $vendor = str_replace(" ","_",$key['nama vendor']);
                            $produk = $key['slug'];
                            
                            $foto = array_filter(explode(";", $key['parameter']['Foto']));

                            if(count($foto) > 0){
                                foreach ($foto as $keyFoto) {
                                    $image = "http://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/q_auto:best/".$keyFoto.".jpg";
                                    if(empty($image) || $key['parameter']['Foto'] == "" || $keyFoto == ""){
                                        $image = base_url('assets/images/pages/blank.jpg');
                                    }
                                }
                            }else{
                                $image = "http://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/q_auto:best/".$key['parameter']['Foto'].".jpg";
                                if(empty($image) || $key['parameter']['Foto'] == "" || $key['parameter']['Foto'] == ""){
                                    $image = base_url('assets/images/pages/blank.jpg');
                                }
                            } ?>
                            <div class="grid-item">
                                <div class="product yeps-vendor-card">
                                    <div class="product-image">
                                        <a href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>">
                                            <img alt="Shop product image!" src="<?php echo $image; ?>"> 
                                        </a>
                                        <?php
                                        if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal']){
                                            ?>
                                            <div class="product-harga" style="display: none">
                                            </div>
                                            <?php
                                        }else{
                                            ?>
                                            <span class="product-sale-off"><?php echo ceil((((int)$key['parameter']['Harga Normal'] - (int)$key['parameter']['Harga Promosi'])/(int)$key['parameter']['Harga Normal'])*100); echo "%";?> Off</span>
                                            <?php
                                        }
                                        ?>
                                    </div>

                                    <div class="product-description" style="padding: 10px">

                                        <div class="product-category"><?php if($key['parameter']['Lokasi'] == ""){ echo "&nbsp;";}else{echo $key['parameter']['Lokasi'];}?></div>

                                        <div class="product-title product-height">
                                            <h3><a href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>"><?php echo $key['parameter']['Nama']?></a></h3>
                                        </div>
                                        <?php
                                        if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal']){
                                            ?>

                                            <div class="product-harga">Rp. <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?>
                                        </div>
                                        <?php
                                    }else{

                                        ?>
                                        <div class="product-harga" style="color: #FB9307">Rp. <?php echo number_format($key['parameter']['Harga Promosi'],0,",","."); ?>
                                    </div>
                                    <div class="diskon-harga">Rp. <strike> <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?></strike>
                                    </div>
                                    <?php
                                }
                                ?>

                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>

            <hr>
            <!-- Pagination -->
            <center>
                <nav class="text-center">
                  <ul class="pagination pagination-fancy">
                    <li> <a href="#" aria-label="Previous"> <span aria-hidden="true"><i class="fa fa-angle-left"></i></span> </a> </li>
                    <?php
                    $active = "";
                    if($produk_count%12 > 0){
                        $produk_count = ((int)($produk_count/12))+1;
                    }else{
                        $produk_count = ((int)($produk_count/12));
                    }

                    for ($i=1; $i <= $produk_count ; $i++) { 
                        if($i == $page){
                            $active = "class='active'";
                        }
                        echo "<li ".$active."><a href='".base_url('kategori/tipe/'.$kategori.'/'.$i)."'>".$i."</a></li>";
                        $active = "";
                    }
                    ?>
                    <li> <a href="#" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-angle-right"></i></span> </a> </li>
                </ul>
            </nav>
        </center>
        <!-- end: Pagination -->
    </div>
    <!--End: Product list-->
</div>
<!-- end: Content-->

<!-- Sidebar-->
<div class="col-md-2">
    <!--widget newsletter-->
    <?php $this->load->view('sider-category'); ?>
</div>
<!-- end: Sidebar-->
</div>
</div>
</section>
<!-- end: Shop products -->

<?php $this->load->view('footer'); ?>