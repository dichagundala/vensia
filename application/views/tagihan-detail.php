<!-- 				<section>
					<div class="container">
						<div class="row box-cart" style="background-color: #f9fafb; margin-top: -50px">
							<center>
								<img src="<?php echo base_url('assets'); ?>/images/pages/icon/shopping.svg" alt="" style="max-width: 15%; margin-bottom: 20px" data-animation="pulse infinite">
								<h5 class="text-center">Belum Ada Pesanan</h5>
								<a href="<?php echo base_url('');?>" class="btn btn-yeps">Buat Pesanan Anda</a>
							</center>
						</div>
					</div>	
				</section>
			-->
			<section id="shop-cart" style="margin: -60px 0px;">
				<div class="container">
					<div class="shop-cart">
						<div class="col-md-12">
							<!-- Content -->
							<div class="col-md-6" style="padding: 0px 20px">
								<div class="row box-cart">
									<div class="col-md-12">
										<div class="col-md-2">
											<img src="<?php echo base_url('assets/images/pages/icon/wallet.svg');?>" class="img-cart" style="width: 70px; margin-top: -10px">	
										</div>
										<div class="col-md-7" style="padding-left: 50px">
											<h4 class="h-inv-yeps">Nomor Invoice</h4>
											<h5 class="l-h-15">Rp. 10.000.000 (Total Cicilan 1)</h5>
											<p class="h-ctn-yeps">Tanggal Pembayaran: 20 Mei 2019</p>
										</div>
										<div class="col-md-3" align="center">
											<h4 class="h-t-txt-yeps">Status</h4>
											<h4 class="h-t-text-ctn-yeps" id="main-harga">Lunas</h4>
											<a href="#" data-uuid="" class="btn btn-success btn-xs"></i>Konfirmasi</a>
										</div>

										<div class="col-md-12">
											<a href="#" id="lihat-tagihan" style="font-weight: bold; font-size: 12px">Detail Cicilan</a>
										</div>
										<div id="tagihan">

											<br><hr>

											<div class="col-md-12 card">
												<div class="card-body">
													<p class="add-service-yeps">Cicilan Ke 1</p>
													<p class="add-desc-yeps">20 April 2019 - <span class="add-price-yeps">Rp. 20.000</span></p>
													<hr class="m-t-20">
													<p class="add-service-yeps">Cicilan Ke 1</p>
													<p class="add-desc-yeps">20 April 2019 - <span class="add-price-yeps">Rp. 20.000</span></p>
													<hr class="m-t-20">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- end: Content -->
						</div>
					</div>
				</div>
			</section>


