<?php $this->load->view('header'); ?>

<!-- Content -->
<section id="page-content">
    <div class="container">
        <!-- post content -->

        <!-- Page title -->
        <div class="page-title" style="margin-bottom: 20px">
            <h2>Our Blog</h2>
            <hr style="background-color: #107ade; height: 4px; margin-top: -15px">
        </div>
        <!-- end: Page title -->

        <!-- Blog -->
        <div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">

            <!-- Post item-->
            <div class="post-item border">
                <div class="post-item-wrap">
                    <div class="post-image">
                        <a href="#">
                           <img alt="" src="<?php echo base_url('assets/images/karir/1.jpg'); ?>">
                        </a>
                        <span class="post-meta-category"><a href="">Lifestyle</a></span>
                    </div>
                    <div class="post-item-description">
                        <span class="post-meta-date"><i class="fa fa-calendar-o"></i>Jan 21, 2017</span>
                        <span class="post-meta-comments"><a href=""><i class="fa fa-comments-o"></i>33 Comments</a></span>
                        <h2><a href="#">Lighthouse, standard post with a single image
                        </a></h2>
                        <p>Curabitur pulvinar euismod ante, ac sagittis ante posuere ac. Vivamus luctus commodo dolor porta feugiat. Fusce at velit id ligula pharetra laoreet.</p>

                        <a href="<?php echo base_url('blog/blog_detail'); ?>" class="item-link">Read More <i class="fa fa-arrow-right"></i></a>

                    </div>
                </div>
            </div>
            <!-- end: Post item-->

            <!-- Post item-->
            <div class="post-item border">
                <div class="post-item-wrap">
                    <div class="post-image">
                        <a href="#">
                            <img alt="" src="<?php echo base_url('assets/images/karir/1.jpg'); ?>">
                        </a>
                        <span class="post-meta-category"><a href="">Lifestyle</a></span>
                    </div>
                    <div class="post-item-description">
                        <span class="post-meta-date"><i class="fa fa-calendar-o"></i>Jan 21, 2017</span>
                        <span class="post-meta-comments"><a href=""><i class="fa fa-comments-o"></i>33 Comments</a></span>
                        <h2><a href="#">Lighthouse, standard post with a single image
                        </a></h2>
                        <p>Curabitur pulvinar euismod ante, ac sagittis ante posuere ac. Vivamus luctus commodo dolor porta feugiat. Fusce at velit id ligula pharetra laoreet.</p>

                        <a href="<?php echo base_url('blog/blog_detail'); ?>" class="item-link">Read More <i class="fa fa-arrow-right"></i></a>

                    </div>
                </div>
            </div>
            <!-- end: Post item-->

            <!-- Post item-->
            <div class="post-item border">
                <div class="post-item-wrap">
                    <div class="post-image">
                        <a href="#">
                            <img alt="" src="<?php echo base_url('assets/images/karir/1.jpg'); ?>">
                        </a>
                        <span class="post-meta-category"><a href="">Lifestyle</a></span>
                    </div>
                    <div class="post-item-description">
                        <span class="post-meta-date"><i class="fa fa-calendar-o"></i>Jan 21, 2017</span>
                        <span class="post-meta-comments"><a href=""><i class="fa fa-comments-o"></i>33 Comments</a></span>
                        <h2><a href="#">Lighthouse, standard post with a single image
                        </a></h2>
                        <p>Curabitur pulvinar euismod ante, ac sagittis ante posuere ac. Vivamus luctus commodo dolor porta feugiat. Fusce at velit id ligula pharetra laoreet.</p>

                        <a href="<?php echo base_url('blog/blog_detail'); ?>" class="item-link">Read More <i class="fa fa-arrow-right"></i></a>

                    </div>
                </div>
            </div>
            <!-- end: Post item-->

            <!-- Post item-->
            <div class="post-item border">
                <div class="post-item-wrap">
                    <div class="post-image">
                        <a href="#">
                            <img alt="" src="<?php echo base_url('assets/images/karir/1.jpg'); ?>">
                        </a>
                        <span class="post-meta-category"><a href="">Lifestyle</a></span>
                    </div>
                    <div class="post-item-description">
                        <span class="post-meta-date"><i class="fa fa-calendar-o"></i>Jan 21, 2017</span>
                        <span class="post-meta-comments"><a href=""><i class="fa fa-comments-o"></i>33 Comments</a></span>
                        <h2><a href="#">Lighthouse, standard post with a single image
                        </a></h2>
                        <p>Curabitur pulvinar euismod ante, ac sagittis ante posuere ac. Vivamus luctus commodo dolor porta feugiat. Fusce at velit id ligula pharetra laoreet.</p>

                        <a href="<?php echo base_url('blog/blog_detail'); ?>" class="item-link">Read More <i class="fa fa-arrow-right"></i></a>

                    </div>
                </div>
            </div>
            <!-- end: Post item-->



        </div>
        <!-- end: Blog -->

        <!-- Pagination -->
        <div class="pagination pagination-simple">
            <ul>
                <li>
                    <a href="#" aria-label="Previous"> <span aria-hidden="true"><i class="fa fa-angle-left"></i></span> </a>
                </li>
                <li><a href="#">1</a> </li>
                <li><a href="#">2</a> </li>
                <li class="active"><a href="#">3</a> </li>
                <li><a href="#">4</a> </li>
                <li><a href="#">5</a> </li>
                <li>
                    <a href="#" aria-label="Next"> <span aria-hidden="true"><i class="fa fa-angle-right"></i></span> </a>
                </li>
            </ul>
        </div>
        <!-- end: Pagination -->

    </div>
    <!-- end: post content -->

</section>
<!-- end: Content -->

<?php $this->load->view('footer'); ?>
