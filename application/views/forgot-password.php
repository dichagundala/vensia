<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="YEPS" />
    <meta name="description" content="Your Event Partner Solution">
    <!-- Document title -->
    <title>YEPS - Lupa Password</title>
    <!-- Stylesheets & Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,800,700,600|Montserrat:400,500,600,700|Raleway:100,300,600,700,800" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets'); ?>/css/plugins.css" rel="stylesheet">
    <link href="<?php echo base_url('assets'); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url('assets'); ?>/css/responsive.css" rel="stylesheet"> 
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets'); ?>/images/favicon-16x16.png">
</head>
<body>
    <div id="loading" class="loader" style="position: absolute; margin: auto; height: auto; width: 100%; z-index: 9999; margin-top: 300px; display: none" align="center">
        <div class="loader-inner line-scale">
           <div></div>
           <div></div>
           <div></div>
           <div></div>
           <div></div>
       </div>
   </div>
   <!-- Wrapper -->
   <div id="wrapper">
       <!-- Section -->
       <section class="fullscreen" style="background-image: url(<?php echo base_url('assets'); ?>/images/pages/backg-forgot.jpg); background-repeat: no-repeat;">
        <div class="container container-fullscreen">
            <div class="text-middle">
                <div class="text-center m-b-30">
                    <a href="<?php echo base_url();?>" class="logo">
                        <img src="<?php echo base_url('assets'); ?>/images/logo-dark.png" alt="Polo Logo" style="max-height: 100px">
                    </a>
                </div>
                <div class="row">
                   <div class="col-md-3 center p-30 background-white b-r-6">
                    <h3>Forgot Password?</h3>
                    <form id="form-validation">
                        <div class="form-group">
                            <p class="center">To receive a new password, enter your email address below.</p>
                            <input type="email" name="email" class="form-control form-white placeholder" placeholder="Enter your email..." required>
                        </div>
                        <div class="text-center">
                            <button type="submit" id="submit" class="btn btn-default">Recover your Password</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end: Section -->
</div>
<!-- end: Wrapper -->

<!-- Go to top button -->
<a id="goToTop"><i class="fa fa-angle-up top-icon"></i><i class="fa fa-angle-up"></i></a>

<!--Plugins-->
<script src="<?php echo base_url('assets'); ?>/js/jquery.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/plugins.js"></script>

<!--Template functions-->
<script src="<?php echo base_url('assets'); ?>/js/functions.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.11/dist/sweetalert2.all.min.js"></script>
<script type="text/javascript">
    $('form#form-validation').on('submit', function(e){
        e.preventDefault();

        var mydata = $('form#form-validation').serialize();
        $.ajax({
            type        : "POST",
            url         : "<?php echo base_url('index.php/Authss/generateForgot');?>",
            data        : mydata,
            datatype    : "json",
            beforeSend  : function(){
                $("#submit").prop("disabled", true);
                $("#loading").show();
                $("#wrapper").css('opacity','0.5');  
            },
            success     : function(hasil){
                console.log(hasil);                       
                var rs = $.parseJSON(hasil); 
                $("#loading").hide();
                swal({
                    type : rs['icon'],
                    text : rs['text']
                }).then( function() {
                        $("#submit").prop("disabled", false);
                        $("#wrapper").css('opacity','1.0');

                    
                });
            } 
        });
    });
</script>
</body>

</html>
