<?php $this->load->view('header'); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">

        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">Add Data Sitemap</div>
            <div class="panel-body">
                <div class="form-group">

                    <div class="col-sm-3">
                        &nbsp;
                    </div>
                </div>
                <div class="col-sm-6" style="border-left: solid 5px blue; border-radius: 5px;border-right: solid 5px blue; border-radius: 5px;">
                    <form class="form" id="sitemap" action="<?php echo base_url('sitemap/add')?>" method="POST">
                        <div class="form-group">
                            <label>Link</label>
                            <input type="text" name="link" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Priority</label>
                            <select class="form-control" name="prior">
                                <option value="1.0">1.0</option>
                                <option value="0.5">0.5</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>


