<?php $this->load->view('header'); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">

        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">Konfirmasi Pembayaran</div>
            <div class="panel-body">
                <form class="form" id="konfirmasi" action="<?php echo base_url('Profil/addKonfirmasi')?>" enctype="multipart/form-data" method="POST">

                    <div class="col-sm-6">
                        <h3>Data Bank Anda</h3>
                        <div class="form-group">
                            <label>Data bank Anda</label>
                            <input type="text" name="bank_name_fr" class="form-control" required>
                            <input type="hidden" name="tipe" class="form-control" value="<?php echo $tipe;?>" required>
                            <input type="hidden" name="uuid" class="form-control" value="<?php echo $uuid?>" required>
                        </div>
                        <div class="form-group">
                            <label>No. Rekening</label>
                            <input type="text" name="bank_no_fr" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Atas Nama</label>
                            <input type="text" name="bank_account_fr" class="form-control" required>
                        </div>

                    </div>
                    <div class="col-sm-6" >
                        <h3>Data Pembayaran</h3>
                        <div class="form-group">
                            <label>No. Pemesanan/Invoice</label>
                            <input type="text" name="invoice" class="form-control" value="<?php echo $nama;?>" readonly>
                        </div>
                        <div class="form-group">
                            <label>Rekening Tujuan</label>
                            <input type="text" name="bank_name_to" class="form-control" value="<?php echo $bank['bank_name'];?>" readonly>
                        </div>
                        <div class="form-group">
                            <label>Jumlah Pembayaran</label>
                            <input type="number" name="transfer_price" class="form-control" value="<?php echo $price;?>" readonly>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Pembayaran</label>
                            <input type="date" name="transfer_date" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Berita/Keterangan</label>
                            <input type="text" name="transfer_note" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Upload File</label>
                            <input type="file" name="file" id="img" class="form-control" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer'); ?>
<script type="text/javascript" src="jquery.ui.widget.js"></script>
<script type="text/javascript" src="jquery.iframe-transport.js"></script>
<script type="text/javascript" src="jquery.fileupload.js"></script>
<script type="text/javascript" src="jquery.cloudinary.js"></script>
<script type="text/javascript">
   $('#konfirmasi').on('submit', function(e){
    e.preventDefault();
    var mydata = $("form#konfirmasi")[0];
    var data = new FormData(mydata);

    $.ajax({
        url : '<?php echo base_url('Profil/addKonfirmasi')?>',
        data : data,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        type : 'POST',
        success : function(hasil){
            console.log(hasil);
            var rs = $.parseJSON(hasil);
            swal({
              type : rs['icon'],
              text : rs['text']
          }).then( function(e) {
            if(rs['icon'] == "success"){
                location.replace(rs['direct']);
            }
        });
      },
  })
})
</script>