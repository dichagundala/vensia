<?php $this->load->view('header'); ?>
<script type="text/javascript"
src="https://app.sandbox.midtrans.com/snap/snap.js"
data-client-key="SB-Mid-client-gampCCrldTZVOghQ"></script>


<!-- SHOP CHECKOUT COMPLETED -->
<section id="shop-checkout-completed">
 <form id="payment-form" method="post" action="<?=site_url()?>/snap/finish">
  <input type="hidden" name="result_type" id="result-type" value=""></div>
  <input type="hidden" name="result_data" id="result-data" value=""></div>
</form>


<div class="container">
  <div class="hr-title hr-long center"><abbr>Konfirmasi Pesanan dan Pembayaran </abbr> </div>
  <?php
  if($invoice[0]['Status'] == "MENUNGGU PEMBAYARAN"){
    ?>
    <center>
      <a href="#" class="btn btn-success icon-left" data-target="#status" data-toggle="modal"><span>Lihat Status Konfirmasi Pesanan Anda</span></a>
    </center>
    <?php } ?>
    <div class="p-t-10 m-b-20 text-center">

      <?php
      $date =  new dateTime($invoice[0]['Tgl Pemesanan']);
      if($invoice[0]['Status'] == "MENUNGGU PEMBAYARAN"){
        $date =  new dateTime($invoice[0]['Tgl Pembayaran']);
        ?>
        <div style="padding: 0 160px 0 160px">
         <div class="panel panel-success">
          <div class="panel-heading">
            <h3 class="panel-title">Silahkan Melakukan Pembayaran</h3>
          </div>
          <div class="panel-body">
            Lakukan Pembayaran Sebelum:<br>
            <div class="countdown small" data-countdown="<?php echo $date->modify('+2 day')->format('Y/m/d H:i:s');?>"></div>
          </div>
        </div>
      </div>
      <?php
    }elseif($invoice[0]['Status'] == "KONFIRMASI"){
      ?>
      <div style="padding: 0 160px 0 160px">
       <div class="panel panel-danger">
        <div class="panel-heading">
          <h3 class="panel-title">Tunggu konfirmasi pemesanan sebelum pembayaran</h3>
        </div>
        <div class="panel-body">
          Silahkan menunggu konfirmasi pesanan<br>
          <div class="countdown small" data-countdown="<?php echo $date->modify('+1 day')->format('Y/m/d H:i:s');?>"></div>
        </div>
      </div>
    </div>
    <?php
  }
  ?>
  <div style="padding: 0 160px 0 160px; text-align: left !important;">
    <div class="panel panel-default">
      <!-- <div class="panel-heading">Panel heading without title</div> -->
      <div class="panel-body">
        <div class="col-md-6">
          <p><b>Payment Detail</b></p>
          <table>
            <tr>
              <td width="35%">Nama</td>
              <td width="15%">:</td>
              <td width="50%"><?php echo $invoice[0]['Nama'];?></td>
            </tr>
            <tr>
              <td width="35%">No Tlp</td>
              <td width="15%">:</td>
              <td width="50%"><?php echo $invoice[0]['No Telp'];?></td>
            </tr>
            <tr>
              <td width="35%">Email</td>
              <td width="15%">:</td>
              <td width="50%"><?php echo $invoice[0]['email'];?></td>
            </tr>
          </table>
        </div>

        <div class="col-md-6">
          <p><b>Customer Detail</b></p>
          <table>
            <tr>
              <td width="45%">No. Booking</td>
              <td width="5%">:</td>
              <td width="50%"><?php echo $invoice[0]['Invoice'];?></td>
            </tr>
            <tr>
              <td width="45%">Metode Pembayaran</td>
              <td width="5%">:</td>
              <td width="50%"><?php echo $invoice[0]['Metode Pembayaran'];?></td>
            </tr>
            <tr>
              <td width="45%">Status</td>
              <td width="5%">:</td>
              <td width="50%"><?php echo $invoice[0]['Status'];?></td>
            </tr>
            <tr>
              <td width="45%">Term Pembayaran</td>
              <td width="5%">:</td>
              <td width="50%"><?php echo $invoice[0]['Angsuran'];?>x Angsuran</td>
            </tr>
            <tr>
              <td width="45%">Tgl Pemesanan</td>
              <td width="5%">:</td>
              <td width="50%"><?php echo date_format(date_create($invoice[0]['Tgl Pemesanan']),'d M Y');?></td>
            </tr>
            <tr>
              <td width="45%">Tgl Event</td>
              <td width="5%">:</td>
              <td width="50%"><?php echo date_format(date_create($invoice[0]['Tgl Event']),'d M Y');?></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>

  <?php
  if($invoice[0]['Status'] == "MENUNGGU PEMBAYARAN"){
    ?>
    <button id="pay-button">Pay!</button>
    <h5>Please transfer to :</h5>
    <img class="bank-icon" src="<?php echo base_url('assets'); ?>/images/bank/mandiri.png"><br>
    <div class="row" style="padding-bottom: 20px">
      <div class="col-md-4"></div>
      <div class="col-md-2">Account Number<br>Account Holder Number<br>Total Amount</div>
      <div class="col-md-2"><b>1234567<br>YEPS Indonesia<br>Rp. <?php echo number_format($invoice[0]['DP'],0,",",".");?></b></div>
      <div class="col-md-4"></div>
    </div>
    <hr>

    <?php
  }

  ?>
  <hr>

  <div>
    <?php
    if($invoice[0]['Status'] == "MENUNGGU PEMBAYARAN"){
      ?>
      <h5>Complete your payment?</h5>
      <?php 
    } 
    ?>
    <a href="#" class="btn btn-danger icon-left" id="cancel"><span>Batalkan Pesanan</span></a>
    <?php
    if($invoice[0]['Status'] == "MENUNGGU PEMBAYARAN"){
      ?>
      <a class="btn btn-dark icon-left" href="<?php echo base_url(''); ?>"><span>Konfirmasi Pembayaran</span></a>
      <?php 
    } 
    ?>
    <br>
    <a href="#" class="btn btn-warning icon-left"><span>Help</span></a>
    <a class="btn btn-default icon-left" href="<?php echo base_url('profil'); ?>/"><span>Go to my Profile</span></a>
  </div>
</div>
</div>
</section>
<!-- end: SHOP CHECKOUT COMPLETED -->

<!-- DELIVERY INFO -->
<section class="background-grey p-t-40 p-b-0">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="icon-box medium fancy">
          <div class="icon" data-animation="pulse infinite"> <a href="#"><i class="fa fa-smile-o"></i></a> </div>
          <h3>Support 24/7</h3>
          <p>Kami siap melayani anda 24 jam setiap hari</p>
        </div>
      </div>
      <div class="col-md-4">
        <div class="icon-box medium fancy">
          <div class="icon" data-animation="pulse infinite"> <a href="#"><i class="fa fa-lock"></i></a> </div>
          <h3>Data Privacy</h3>
          <p>Sistem Kami menjamin data pelanggan agar tidak tersebar ke pihak lain.</p>
        </div>
      </div>
      <div class="col-md-4">
        <div class="icon-box medium fancy">
          <div class="icon" data-animation="pulse infinite"> <a href="#"><i class="fa fa-angellist"></i></a> </div>
          <h3>Friendly User</h3>
          <p>Sistem ini dibuat untuk memudahkan pelanggan untuk melakukan aktivitasnya</p>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end: DELIVERY INFO -->

<!-- MODAL KONFIRMASI -->
<div class="modal fade" id="konfirmasi" tabindex="-1" role="modal" aria-labelledby="modal-label" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="modal-label">Batalkan Pesanan</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <p>Apakah Anda yakin akan membatalkan semua pesanan Anda?</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
        <a href="<?php echo base_url(); ?>" type="button" class="btn btn-default">Ya</a>
      </div>
    </div>
  </div>
</div>
<!-- / MODAL KONFIRMASI -->

<?php $this->load->view('footer'); ?>
<!-- MODAL -->
<div class="modal fade" id="status" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 id="modal-label-3" class="modal-title">Status Pesanan Anda </h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('checkout-detail');?>
      </div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-b" type="button">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('a#cancel').on('click', function(e){
    e.preventDefault();
    swal({
      title: 'Batalkan Pesanan',
      text: "Apakah Anda yakin akan membatalkan pesanan ini?",
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#107ADE',
      cancelButtonColor: '#C30000',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then((result) => {
      if (result.value) {

        $.ajax({
          type : 'POST',
          data : mydata,
          url : '<?php echo base_url('Shop/cancel/')?>',
          typedata : 'json',
          success : function(hasil){
            console.log(hasil);
            var rs = $.parseJSON(hasil);
            swal({
              type : rs['icon'],
              text : rs['text']
            }).then( function(e) {
              if(rs['icon'] == "success"){

                location.replace(rs['direct']);
              }
            });
          }
        });
      }
    })
  })
</script>
<script type="text/javascript">
  
    $('#pay-button').click(function (event) {
      event.preventDefault();
      $(this).attr("disabled", "disabled");
    
    $.ajax({
      url: '<?=site_url()?>/snap/token',
      cache: false,

      success: function(data) {
        //location = data;

        console.log('token = '+data);
        
        var resultType = document.getElementById('result-type');
        var resultData = document.getElementById('result-data');

        function changeResult(type,data){
          $("#result-type").val(type);
          $("#result-data").val(JSON.stringify(data));
          //resultType.innerHTML = type;
          //resultData.innerHTML = JSON.stringify(data);
        }

        snap.pay(data, {
          
          onSuccess: function(result){
            changeResult('success', result);
            console.log(result.status_message);
            console.log(result);
            $("#payment-form").submit();
          },
          onPending: function(result){
            changeResult('pending', result);
            console.log(result.status_message);
            $("#payment-form").submit();
          },
          onError: function(result){
            changeResult('error', result);
            console.log(result.status_message);
            $("#payment-form").submit();
          }
        });
      }
    });
  });

  </script>