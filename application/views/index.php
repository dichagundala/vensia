<?php $this->load->view('header'); ?>
<style type="text/css">


</style>

<div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider="center: true; autoplay: true">
    <ul class="uk-slider-items uk-grid">
        <li class="uk-width-3-4 slider-vensia">
            <div class="uk-panel">
                <img src="<?php echo base_url('assets'); ?>/images/pages/slider-home/vensia-banner.jpg" alt=""
                    class="img-slider-vensia">
            </div>
        </li>
        <li class="uk-width-3-4 slider-vensia">
            <div class="uk-panel">
                <a href="<?php echo base_url('kategori/tipe/Decoration'); ?>">
                    <img src="<?php echo base_url('assets'); ?>/images/pages/slider-home/vensia-banner-1.jpg" alt=""
                        class="img-slider-vensia">
                </a>
            </div>
        </li>
        <li class="uk-width-3-4 slider-vensia">
            <div class="uk-panel">
                <img src="<?php echo base_url('assets'); ?>/images/pages/slider-home/vensia-banner.jpg" alt=""
                    class="img-slider-vensia">
            </div>
        </li>
        <li class="uk-width-3-4 slider-vensia">
            <div class="uk-panel">
                <a href="<?php echo base_url('kategori/tipe/Photography'); ?>">
                    <img src="<?php echo base_url('assets'); ?>/images/pages/slider-home/vensia-banner-2.jpg" alt=""
                        class="img-slider-vensia">
                </a>
            </div>
        </li>
    </ul>

    <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
        uk-slider-item="previous"></a>
    <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
        uk-slider-item="next"></a>
</div>

<section class="section-pattern p-t-10 p-b-10 text-center">
    <div class="scrollmenu">
        <span style="font-weight: 500">Explore : </span>
        <a href="<?php echo base_url('kategori/tipe/Venue'); ?>">Venue</a>
        <a href="<?php echo base_url('kategori/tipe/Photography'); ?>">Photography</a>
        <a href="<?php echo base_url('kategori/tipe/Wardrobe'); ?>">Wardrobe</a>
        <a href="<?php echo base_url('kategori/tipe/Decoration'); ?>">Decoration</a>
        <a href="<?php echo base_url('kategori/tipe/Souvenir'); ?>">Souvenir</a>
        <a href="<?php echo base_url('kategori/tipe/Catering'); ?>">Catering</a>
        <a href="<?php echo base_url('kategori/tipe/Make Up'); ?>">Make up</a>
        <a href="<?php echo base_url('kategori/tipe/Invitation'); ?>">Invitation</a>
        <a href="<?php echo base_url('kategori/tipe/Entertainment'); ?>">Entertainment</a>
        <a href="<?php echo base_url('kategori/tipe/Transportation'); ?>">Transportation</a>
        <a href="<?php echo base_url('kategori/tipe/Event Cake'); ?>">Event Cake</a>
        <a href="<?php echo base_url('kategori/tipe/Videography'); ?>">Videography</a>
    </div>
</section>

<section id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1 style="font-size: 28px; font-weight: 700; margin: 0">Photography</h1>
                <p style="margin-top: 0px">Showing recommendations with all budgets</p>
            </div>
            <div class="col-md-4 leright">
                <a href="<?php echo base_url('kategori/tipe/Photography'); ?>"
                    class="btn btn-outline btn-rounded btn-sm btn-shadow">Lihat Selengkapnya</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="shop">
                <div class="grid-layout grid-4-columns" data-item="grid-item">
                    <?php foreach (array_slice($Photography['result'],0,4) as $key) {

                        $vendor = str_replace(" ","_",$key['nama vendor']);
                        $produk = $key['slug'];

                        $foto = array_filter(explode(";", $key['parameter']['Foto']));

                        if(count($foto) > 0){
                            foreach ($foto as $keyFoto) {
                                $image = "http://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/q_auto:best/".$keyFoto.".jpg";
                                if(empty($image) || $key['parameter']['Foto'] == "" || $keyFoto == ""){
                                    $image = base_url('assets/images/pages/blank.jpg');
                                }
                            }
                        }else{
                            $image = "http://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/q_auto:best/".$key['parameter']['Foto'].".jpg";
                            if(empty($image) || $key['parameter']['Foto'] == "" || $key['parameter']['Foto'] == ""){
                                $image = base_url('assets/images/pages/blank.jpg');
                            }
                        } ?>

                    <div class="grid-item">
                        <div class="product yeps-vendor-card">
                            <div class="product-image">
                                <a href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>">
                                    <img alt="Shop product image!" style="border-radius: 8px"
                                        src="<?php echo $image; ?>">
                                </a>
                                <?php
                                    if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal']){
                                ?>
                                <div class="product-harga" style="display: none"></div>
                                <?php }else{ ?>
                                <span
                                    class="product-sale-off"><?php echo ceil((((int)$key['parameter']['Harga Normal'] - (int)$key['parameter']['Harga Promosi'])/(int)$key['parameter']['Harga Normal'])*100); echo "%";?>
                                    Off</span>
                                <?php } ?>
                            </div>
                            <div class="product-description" style="padding: 10px">
                                <div class="product-category">
                                    <?php if($key['parameter']['Lokasi'] == ""){ echo "&nbsp;";}else{echo $key['parameter']['Lokasi'];}?>
                                </div>
                                <div class="product-title product-height">
                                    <h3>
                                        <a
                                            href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>"><?php echo $key['parameter']['Nama']?></a>
                                    </h3>
                                </div>
                                <?php
                                    if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal'])
                                    { ?>
                                <div class="product-harga">
                                    Rp. <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?>
                                </div>
                                <?php } else { ?>
                                <div class="product-harga" style="color: #FB9307">
                                    Rp. <?php echo number_format($key['parameter']['Harga Promosi'],0,",","."); ?>
                                </div>
                                <div class="diskon-harga">
                                    Rp. <strike>
                                        <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?></strike>
                                </div>
                                <?php } ?>
                                <!-- <div style="color: #ADAAAA; font-size: 13px"><i class="fa fa-users"></i> Nama Vendor Gans</div> -->
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<hr class="our-space"> 

<section id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1 style="font-size: 28px; font-weight: 700; margin: 0">Decoration</h1>
                <p style="margin-top: 0px">Showing recommendations with all budgets</p>
            </div>
            <div class="col-md-4 leright">
                <a href="<?php echo base_url('kategori/tipe/Decoration'); ?>"
                    class="btn btn-outline btn-rounded btn-sm btn-shadow">Lihat Selengkapnya</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="shop">
                <div class="grid-layout grid-4-columns" data-item="grid-item">
                    <?php foreach (array_slice($Decoration['result'],0,4) as $key) {

                        $vendor = str_replace(" ","_",$key['nama vendor']);
                        $produk = $key['slug'];

                        $foto = array_filter(explode(";", $key['parameter']['Foto']));

                        if(count($foto) > 0){
                            foreach ($foto as $keyFoto) {
                                $image = "http://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/q_auto:best/".$keyFoto.".jpg";
                                if(empty($image) || $key['parameter']['Foto'] == "" || $keyFoto == ""){
                                    $image = base_url('assets/images/pages/blank.jpg');
                                }
                            }
                        }else{
                            $image = "http://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/q_auto:best/".$key['parameter']['Foto'].".jpg";
                            if(empty($image) || $key['parameter']['Foto'] == "" || $key['parameter']['Foto'] == ""){
                                $image = base_url('assets/images/pages/blank.jpg');
                            }
                        } ?>

                    <div class="grid-item">
                        <div class="product yeps-vendor-card">
                            <div class="product-image">
                                <a href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>">
                                    <img alt="Shop product image!" style="border-radius: 8px"
                                        src="<?php echo $image; ?>">
                                </a>
                                <?php
                                    if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal']){
                                ?>
                                <div class="product-harga" style="display: none"></div>
                                <?php }else{ ?>
                                <span
                                    class="product-sale-off"><?php echo ceil((((int)$key['parameter']['Harga Normal'] - (int)$key['parameter']['Harga Promosi'])/(int)$key['parameter']['Harga Normal'])*100); echo "%";?>
                                    Off</span>
                                <?php } ?>
                            </div>
                            <div class="product-description" style="padding: 10px">
                                <div class="product-category">
                                    <?php if($key['parameter']['Lokasi'] == ""){ echo "&nbsp;";}else{echo $key['parameter']['Lokasi'];}?>
                                </div>
                                <div class="product-title product-height">
                                    <h3>
                                        <a
                                            href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>"><?php echo $key['parameter']['Nama']?></a>
                                    </h3>
                                </div>
                                <?php
                                    if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal'])
                                    { ?>
                                <div class="product-harga">
                                    Rp. <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?>
                                </div>
                                <?php } else { ?>
                                <div class="product-harga" style="color: #FB9307">
                                    Rp. <?php echo number_format($key['parameter']['Harga Promosi'],0,",","."); ?>
                                </div>
                                <div class="diskon-harga">
                                    Rp. <strike>
                                        <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?></strike>
                                </div>
                                <?php } ?>
                                <!-- <div style="color: #ADAAAA; font-size: 13px"><i class="fa fa-users"></i> Nama Vendor Gans</div> -->
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<hr class="our-space">

<section id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1 style="font-size: 28px; font-weight: 700; margin: 0">Catering</h1>
                <p style="margin-top: 0px">Showing recommendations with all budgets</p>
            </div>
            <div class="col-md-4 leright">
                <a href="<?php echo base_url('kategori/tipe/Catering'); ?>"
                    class="btn btn-outline btn-rounded btn-sm btn-shadow">Lihat Selengkapnya</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="shop">
                <div class="grid-layout grid-4-columns" data-item="grid-item">
                    <?php foreach (array_slice($Catering['result'],0,4) as $key) {

                        $vendor = str_replace(" ","_",$key['nama vendor']);
                        $produk = $key['slug'];

                        $foto = array_filter(explode(";", $key['parameter']['Foto']));

                        if(count($foto) > 0){
                            foreach ($foto as $keyFoto) {
                                $image = "http://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/q_auto:best/".$keyFoto.".jpg";
                                if(empty($image) || $key['parameter']['Foto'] == "" || $keyFoto == ""){
                                    $image = base_url('assets/images/pages/blank.jpg');
                                }
                            }
                        }else{
                            $image = "http://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/q_auto:best/".$key['parameter']['Foto'].".jpg";
                            if(empty($image) || $key['parameter']['Foto'] == "" || $key['parameter']['Foto'] == ""){
                                $image = base_url('assets/images/pages/blank.jpg');
                            }
                        } ?>

                    <div class="grid-item">
                        <div class="product yeps-vendor-card">
                            <div class="product-image">
                                <a href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>">
                                    <img alt="Shop product image!" style="border-radius: 8px"
                                        src="<?php echo $image; ?>">
                                </a>
                                <?php
                                    if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal']){
                                ?>
                                <div class="product-harga" style="display: none"></div>
                                <?php }else{ ?>
                                <span
                                    class="product-sale-off"><?php echo ceil((((int)$key['parameter']['Harga Normal'] - (int)$key['parameter']['Harga Promosi'])/(int)$key['parameter']['Harga Normal'])*100); echo "%";?>
                                    Off</span>
                                <?php } ?>
                            </div>
                            <div class="product-description" style="padding: 10px">
                                <div class="product-category">
                                    <?php if($key['parameter']['Lokasi'] == ""){ echo "&nbsp;";}else{echo $key['parameter']['Lokasi'];}?>
                                </div>
                                <div class="product-title product-height">
                                    <h3>
                                        <a
                                            href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>"><?php echo $key['parameter']['Nama']?></a>
                                    </h3>
                                </div>
                                <?php
                                    if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal'])
                                    { ?>
                                <div class="product-harga">
                                    Rp. <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?>
                                </div>
                                <?php } else { ?>
                                <div class="product-harga" style="color: #FB9307">
                                    Rp. <?php echo number_format($key['parameter']['Harga Promosi'],0,",","."); ?>
                                </div>
                                <div class="diskon-harga">
                                    Rp. <strike>
                                        <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?></strike>
                                </div>
                                <?php } ?>
                                <!-- <div style="color: #ADAAAA; font-size: 13px"><i class="fa fa-users"></i> Nama Vendor Gans</div> -->
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<hr class="our-space">

<section id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1 style="font-size: 28px; font-weight: 700; margin: 0">Venue</h1>
                <p style="margin-top: 0px">Showing recommendations with all budgets</p>
            </div>
            <div class="col-md-4 leright">
                <a href="<?php echo base_url('kategori/tipe/Venue'); ?>"
                    class="btn btn-outline btn-rounded btn-sm btn-shadow">Lihat Selengkapnya</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="shop">
                <div class="grid-layout grid-4-columns" data-item="grid-item">
                    <?php foreach (array_slice($Venue['result'],0,4) as $key) {

                        $vendor = str_replace(" ","_",$key['nama vendor']);
                        $produk = $key['slug'];

                        $foto = array_filter(explode(";", $key['parameter']['Foto']));

                        if(count($foto) > 0){
                            foreach ($foto as $keyFoto) {
                                $image = "http://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/q_auto:best/".$keyFoto.".jpg";
                                if(empty($image) || $key['parameter']['Foto'] == "" || $keyFoto == ""){
                                    $image = base_url('assets/images/pages/blank.jpg');
                                }
                            }
                        }else{
                            $image = "http://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/q_auto:best/".$key['parameter']['Foto'].".jpg";
                            if(empty($image) || $key['parameter']['Foto'] == "" || $key['parameter']['Foto'] == ""){
                                $image = base_url('assets/images/pages/blank.jpg');
                            }
                        } ?>

                    <div class="grid-item">
                        <div class="product yeps-vendor-card">
                            <div class="product-image">
                                <a href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>">
                                    <img alt="Shop product image!" style="border-radius: 8px"
                                        src="<?php echo $image; ?>">
                                </a>
                                <?php
                                    if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal']){
                                ?>
                                <div class="product-harga" style="display: none"></div>
                                <?php }else{ ?>
                                <span
                                    class="product-sale-off"><?php echo ceil((((int)$key['parameter']['Harga Normal'] - (int)$key['parameter']['Harga Promosi'])/(int)$key['parameter']['Harga Normal'])*100); echo "%";?>
                                    Off</span>
                                <?php } ?>
                            </div>
                            <div class="product-description" style="padding: 10px">
                                <div class="product-category">
                                    <?php if($key['parameter']['Lokasi'] == ""){ echo "&nbsp;";}else{echo $key['parameter']['Lokasi'];}?>
                                </div>
                                <div class="product-title product-height">
                                    <h3>
                                        <a
                                            href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>"><?php echo $key['parameter']['Nama']?></a>
                                    </h3>
                                </div>
                                <?php
                                    if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal'])
                                    { ?>
                                <div class="product-harga">
                                    Rp. <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?>
                                </div>
                                <?php } else { ?>
                                <div class="product-harga" style="color: #FB9307">
                                    Rp. <?php echo number_format($key['parameter']['Harga Promosi'],0,",","."); ?>
                                </div>
                                <div class="diskon-harga">
                                    Rp. <strike>
                                        <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?></strike>
                                </div>
                                <?php } ?>
                                <!-- <div style="color: #ADAAAA; font-size: 13px"><i class="fa fa-users"></i> Nama Vendor Gans</div> -->
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<hr class="our-space">

<section id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1 style="font-size: 28px; font-weight: 700; margin: 0">Invitation</h1>
                <p style="margin-top: 0px">Showing recommendations with all budgets</p>
            </div>
            <div class="col-md-4 leright">
                <a href="<?php echo base_url('kategori/tipe/Invitation'); ?>"
                    class="btn btn-outline btn-rounded btn-sm btn-shadow">Lihat Selengkapnya</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="shop">
                <div class="grid-layout grid-4-columns" data-item="grid-item">
                    <?php foreach (array_slice($Invitation['result'],0,4) as $key) {

                        $vendor = str_replace(" ","_",$key['nama vendor']);
                        $produk = $key['slug'];

                        $foto = array_filter(explode(";", $key['parameter']['Foto']));

                        if(count($foto) > 0){
                            foreach ($foto as $keyFoto) {
                                $image = "http://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/q_auto:best/".$keyFoto.".jpg";
                                if(empty($image) || $key['parameter']['Foto'] == "" || $keyFoto == ""){
                                    $image = base_url('assets/images/pages/blank.jpg');
                                }
                            }
                        }else{
                            $image = "http://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/q_auto:best/".$key['parameter']['Foto'].".jpg";
                            if(empty($image) || $key['parameter']['Foto'] == "" || $key['parameter']['Foto'] == ""){
                                $image = base_url('assets/images/pages/blank.jpg');
                            }
                        } ?>

                    <div class="grid-item">
                        <div class="product yeps-vendor-card">
                            <div class="product-image">
                                <a href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>">
                                    <img alt="Shop product image!" style="border-radius: 8px"
                                        src="<?php echo $image; ?>">
                                </a>
                                <?php
                                    if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal']){
                                ?>
                                <div class="product-harga" style="display: none"></div>
                                <?php }else{ ?>
                                <span
                                    class="product-sale-off"><?php echo ceil((((int)$key['parameter']['Harga Normal'] - (int)$key['parameter']['Harga Promosi'])/(int)$key['parameter']['Harga Normal'])*100); echo "%";?>
                                    Off</span>
                                <?php } ?>
                            </div>
                            <div class="product-description" style="padding: 10px">
                                <div class="product-category">
                                    <?php if($key['parameter']['Lokasi'] == ""){ echo "&nbsp;";}else{echo $key['parameter']['Lokasi'];}?>
                                </div>
                                <div class="product-title product-height">
                                    <h3>
                                        <a
                                            href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>"><?php echo $key['parameter']['Nama']?></a>
                                    </h3>
                                </div>
                                <?php
                                    if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal'])
                                    { ?>
                                <div class="product-harga">
                                    Rp. <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?>
                                </div>
                                <?php } else { ?>
                                <div class="product-harga" style="color: #FB9307">
                                    Rp. <?php echo number_format($key['parameter']['Harga Promosi'],0,",","."); ?>
                                </div>
                                <div class="diskon-harga">
                                    Rp. <strike>
                                        <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?></strike>
                                </div>
                                <?php } ?>
                                <!-- <div style="color: #ADAAAA; font-size: 13px"><i class="fa fa-users"></i> Nama Vendor Gans</div> -->
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<hr class="our-space">

<section id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1 style="font-size: 28px; font-weight: 700; margin: 0">Service yang terkait</h1>
                <p style="margin-top: 0px">Servis rekomendasi untuk Anda</p>
            </div>
            <div class="col-md-4 leright">
                <a href="<?php echo base_url('kategori/tipe/Invitation'); ?>"
                    class="btn btn-outline btn-rounded btn-sm btn-shadow">Lihat Selengkapnya</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="shop">
                <div class="grid-layout grid-6-columns" data-item="grid-item">
                    <?php foreach (array_slice($Invitation['result'],0,6) as $key) {

                        $vendor = str_replace(" ","_",$key['nama vendor']);
                        $produk = $key['slug'];

                        $foto = array_filter(explode(";", $key['parameter']['Foto']));

                        if(count($foto) > 0){
                            foreach ($foto as $keyFoto) {
                                $image = "http://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/q_auto:best/".$keyFoto.".jpg";
                                if(empty($image) || $key['parameter']['Foto'] == "" || $keyFoto == ""){
                                    $image = base_url('assets/images/pages/blank.jpg');
                                }
                            }
                        }else{
                            $image = "http://res.cloudinary.com/yepsindo/image/upload/w_380,h_507,c_fill/q_auto:best/".$key['parameter']['Foto'].".jpg";
                            if(empty($image) || $key['parameter']['Foto'] == "" || $key['parameter']['Foto'] == ""){
                                $image = base_url('assets/images/pages/blank.jpg');
                            }
                        } ?>

                    <div class="grid-item">
                        <div class="product yeps-vendor-card">
                            <div class="product-image">
                                <a href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>">
                                    <img alt="Shop product image!" style="border-radius: 8px"
                                        src="<?php echo $image; ?>">
                                </a>
                                <?php
                                    if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal']){
                                ?>
                                <div class="product-harga" style="display: none"></div>
                                <?php }else{ ?>
                                <span
                                    class="product-sale-off"><?php echo ceil((((int)$key['parameter']['Harga Normal'] - (int)$key['parameter']['Harga Promosi'])/(int)$key['parameter']['Harga Normal'])*100); echo "%";?>
                                    Off</span>
                                <?php } ?>
                            </div>
                            <div class="product-description" style="padding: 10px">
                                <div class="product-category">
                                    <?php if($key['parameter']['Lokasi'] == ""){ echo "&nbsp;";}else{echo $key['parameter']['Lokasi'];}?>
                                </div>
                                <div class="product-title product-height">
                                    <h3>
                                        <a
                                            href="<?php echo base_url('jasa/'.$vendor.'/'.$produk); ?>"><?php echo $key['parameter']['Nama']?></a>
                                    </h3>
                                </div>
                                <?php
                                    if($key['parameter']['Harga Promosi'] == "0" || $key['parameter']['Harga Promosi'] == $key['parameter']['Harga Normal'])
                                    { ?>
                                <div class="product-harga">
                                    Rp. <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?>
                                </div>
                                <?php } else { ?>
                                <div class="product-harga" style="color: #FB9307">
                                    Rp. <?php echo number_format($key['parameter']['Harga Promosi'],0,",","."); ?>
                                </div>
                                <div class="diskon-harga">
                                    Rp. <strike>
                                        <?php echo number_format($key['parameter']['Harga Normal'],0,",","."); ?></strike>
                                </div>
                                <?php } ?>
                                <!-- <div style="color: #ADAAAA; font-size: 13px"><i class="fa fa-users"></i> Nama Vendor Gans</div> -->
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="https://unpkg.com/scrollreveal"></script>
<?php $this->load->view('footer'); ?>
<script type="text/javascript">
    ScrollReveal().reveal('.animated_card', {
        delay: 100,
        duration: 2000,
        reset: true
    });

</script>

<!-- Smartsupp Live Chat script -->
<script type="text/javascript">
    /*var _smartsupp = _smartsupp || {};
_smartsupp.key = '0a9f484e31f882f8ccafbe1c0552ee3bbbe2eaed';
window.smartsupp||(function(d) {
  var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
  s=d.getElementsByTagName('script')[0];c=d.createElement('script');
  c.type='text/javascript';c.charset='utf-8';c.async=true;
  c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
})(document);*/

</script>
