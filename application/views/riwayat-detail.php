<!-- 				<section>
					<div class="container">
						<div class="row box-cart" style="background-color: #f9fafb; margin-top: -50px">
							<center>
								<img src="<?php echo base_url('assets'); ?>/images/pages/icon/shopping.svg" alt="" style="max-width: 15%; margin-bottom: 20px" data-animation="pulse infinite">
								<h5 class="text-center">Belum Ada Pesanan</h5>
								<a href="<?php echo base_url('');?>" class="btn btn-yeps">Buat Pesanan Anda</a>
							</center>
						</div>
					</div>	
				</section>
 -->
				<section id="shop-cart" style="margin: -60px 0px;">
					<div class="container">
						<div class="shop-cart">

							<div class="row box-cart">
								<div class="col-md-12">
									<div class="col-md-2">
										<img src="<?php echo base_url('assets/images/pages/icon/invoice_cart.svg');?>" class="img-cart" style="height: 100px; margin-top: -10px">	
									</div>
									<div class="col-md-6">
										<h4 class="h-inv-yeps">Nomor Invoice</h4>
										<h5 class="l-h-15">Rp. 10.000.000 (Total Harga)</h5>
										<p class="h-ctn-yeps">20 Mei 2019</p>
									</div>
									<div class="col-md-3">
										<h4 class="h-t-txt-yeps">Status</h4>
										<h4 class="h-t-text-ctn-yeps" id="main-harga">Menunggu Konfirmasi</h4>
									</div>
									<div class="col-md-1" align="center">
										<a href="#" data-uuid="" class="btn btn-default btn-xs"></i>Tagihan</a>
										<a href="#" data-uuid="" class="btn btn-danger btn-xs"></i>Refund</a>
									</div>

									<div class="col-md-12">
										<a href="#" id="lihat-detail" style="font-weight: bold">Lihat Selengkapnya</a>
									</div>
									<div id="details">
										<hr>
										<!-- Service Details -->
										<div class="col-md-12">
											<div class="col-md-2">
												<img src="" class="img-cart">
											</div>
											<div class="col-md-6">
												<h5 class="l-h-15">Nama Vendor - Nama Produk</h5>
												<h5 class="l-h-15">Rp. 10.000
							<!-- <?php 
							//if($key['parameter']['Harga Promosi'] == "0"){
								// echo number_format($key['parameter']['Harga Normal'],0,",",".");
							// }else{
								// echo number_format($key['parameter']['Harga Promosi'],0,",",".");
							// }
							?> -->
						</h5>
						<p class="h-ctn-yeps">Tanggal : <?php echo date_format(date_create($key['date']),'d-m-Y');
						if($key['time'] != ""){
							echo "<br> Jam ". $key['time']; 
						}
						?></p>
						<p class="h-ctn-yeps">Qty : 100</p>
						<p class="h-ctn-yeps">Kategori : Catering</p>
					</div>
					<div class="col-md-3">
						<h4 class="h-t-txt-yeps" style="padding-top: 3px">Sub Total</h4>
						<h4 class="h-t-text-ctn-yeps" id="main-harga">Rp. 2.000.000</h4>
					</div>
					<div class="col-md-1">
						<h4 class="h-t-txt-yeps" style="padding-top: 3px; font-size: 13px">Status</h4>
						<h4 class="h-t-text-ctn-yeps" id="main-harga" style="font-size: 13px">Diterima</h4>
					</div>
				</div>
				<div class="col-md-12" style="margin-bottom: 20px">
					<div class="col-md-4">
						<p class="m-b-0 variant-cart f-w-6">Additional Service</p>
						<p class="add-service-yeps">Nama Service</p>
						<p class="add-desc-yeps">Qty : 5 - <span class="add-price-yeps">Rp. 20.000</span></p>
						<hr class="m-t-20">
						<p class="add-service-yeps">Nama Service</p>
						<p class="add-desc-yeps">Qty : 5 - <span class="add-price-yeps">Rp. 20.000</span></p>
						<hr class="m-t-20">
						<p class="add-service-yeps">Nama Service</p>
						<p class="add-desc-yeps">Qty : 5 - <span class="add-price-yeps">Rp. 20.000</span></p>
					</div>
					<div class="col-md-4">
						<p class="m-b-0 variant-cart f-w-6">Catatan Untuk Vendor</p>
						<p class="h-t-text-sub-yeps">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer id diam id quam luctus vestibulum. Nunc a lorem pellentesque, consectetur sapien ut, venenatis augue. Phasellus a convallis massa. Donec tristique ultrices ligula sed sagittis. Etiam quis lacus nec ex rhoncus blandit ut eget mi</p>
					</div>
					<div class="col-md-4">
						<p class="m-b-0 variant-cart f-w-6">Alamat Event</p>
						<p class="h-t-text-sub-yeps">Jl. Kramat Sentiong II No. G62 RT 004/05 Kelurahan Kramat Kecamatan Senen Jakarta Pusat 10450</p>
					</div>
				</div>
				<br><hr>
				<!-- end: Service Details -->

				<div class="col-md-12 card">
					<div class="card-body">
						<div class="col-md-6">
							<p class="m-b-0 variant-cart f-w-6">Detail Pesanan</p>
							<table class="det-ord-yeps">
								<tr>
									<td width="45%">Term Pembayaran</td>
									<td width="5%">:</td>
									<td width="50%">2x Angsuran</td>
								</tr>
								<tr>
									<td width="45%">Metode Pembayaran</td>
									<td width="5%">:</td>
									<td width="50%">Virtual Account</td>
								</tr>
								<tr>
									<td width="45%">Sub Total</td>
									<td width="5%">:</td>
									<td width="50%">Rp. 1.000.000</td>
								</tr>
								<tr>
									<td width="45%">Event Charge</td>
									<td width="5%">:</td>
									<td width="50%">Rp. 50.000</td>
								</tr>
								<tr>
									<td width="45%">Pembayaran DP</td>
									<td width="5%">:</td>
									<td width="50%">Rp. 320.000</td>
								</tr>
								<tr>
									<td width="45%">Total Pesanan</td>
									<td width="5%">:</td>
									<td width="50%" class="add-price-yeps">Rp. 1.050.000</td>
								</tr>
							</table>
						</div>
						<div class="col-md-6">
							<p class="m-b-0 variant-cart f-w-6">Status Pesanan</p>
							
							<div class="timeline-centered">
								<article class="timeline-entry">
									<div class="timeline-entry-inner">
										<div class="timeline-icon bg-success">
											<i class="entypo-feather"></i>
										</div>
										<div class="timeline-label">
											<h2>Menunggu Konfirmasi <span>Jumat, 1 Jan 2019 - 01.00 WIB</span></h2>
											<p>Meminta konfirmasi pesanan ke Vendor</p>
										</div>
									</div>
								</article>
								<article class="timeline-entry">
									<div class="timeline-entry-inner">
										<div class="timeline-icon bg-success">
											<i class="entypo-feather"></i>
										</div>
										<div class="timeline-label">
											<h2>Menunggu Pembayaran <span>Jumat, 2 Jan 2019 - 01.00 WIB</span></h2>
											<p>Pesanan di Konfirmasi dan Menunggu Pembayaran DP 30%</p>
										</div>
									</div>
								</article>
								<article class="timeline-entry">
									<div class="timeline-entry-inner">
										<div class="timeline-icon bg-success">
											<i class="entypo-feather"></i>
										</div>
										<div class="timeline-label">
											<h2>Pembayaran Terverifikasi <span>Jumat, 2 Jan 2019 - 03.00 WIB</span></h2>
											<p>Pembayaran DP Anda sudah di verifikasi</p>
										</div>
									</div>
								</article>
								<article class="timeline-entry">
									<div class="timeline-entry-inner">
										<div class="timeline-icon bg-success">
											<i class="entypo-feather"></i>
										</div>
										<div class="timeline-label">
											<h2>Pesanan di Proses <span>Jumat, 15 Jan 2019 - 03.00 WIB</span></h2>
											<p>YEPS sedang melakukan persiapan Event</p>
										</div>
									</div>
								</article>
								<article class="timeline-entry">
									<div class="timeline-entry-inner">
										<div class="timeline-icon bg-success">
											<i class="entypo-feather"></i>
										</div>
										<div class="timeline-label">
											<h2>Pesanan Selesai <span>Jumat, 29 Jan 2019 - 03.00 WIB</span></h2>
											<p>Event Anda sudah selesai di laksanakan</p>
										</div>
									</div>
								</article>

								<article class="timeline-entry">
									<div class="timeline-entry-inner">
										<div class="timeline-icon bg-secondary">
											<i class="entypo-suitcase"></i>
										</div>
										<div class="timeline-label">
											<h2>Pesanan Batal <span>Jumat, 03 Jan 2019 - 03.00 WIB</span></h2>
											<p>Vendor menolak pesanan Anda / Anda melewati batas waktu pembayaran</p>
										</div>
									</div>
								</article>
								<article class="timeline-entry">
									<div class="timeline-entry-inner">
										<div class="timeline-icon bg-secondary">
											<i class="entypo-suitcase"></i>
										</div>
										<div class="timeline-label">
											<h2>Refund <span>Jumat, 03 Jan 2019 - 03.00 WIB</span></h2>
											<p>Anda meminta untuk mengembalikan uang muka</p>
										</div>
									</div>
								</article>
								<article class="timeline-entry">
									<div class="timeline-entry-inner">
										<div class="timeline-icon bg-secondary">
											<i class="entypo-suitcase"></i>
										</div>
										<div class="timeline-label">
											<h2>Refund Berhasil <span>Jumat, 10 Jan 2019 - 03.00 WIB</span></h2>
											<p>YEPS sudah berhasil mengembalikan uang muka yang telah di bayarkan</p>
										</div>
									</div>
								</article>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</section>


