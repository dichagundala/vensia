<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="Vensia" />
    <meta name="description" content="Your Event Partner Solution">
    <!-- Document title -->
    <title>Vensia - Masuk</title>
    <!-- Stylesheets & Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,800,700,600|Montserrat:400,500,600,700|Raleway:100,300,600,700,800" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets'); ?>/css/plugins.css" rel="stylesheet">
    <link href="<?php echo base_url('assets'); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url('assets'); ?>/css/responsive.css" rel="stylesheet"> 
    <link href="<?php echo base_url('assets'); ?>/css/custom.css" rel="stylesheet"> 
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets'); ?>/images/favicon-16x16.png">
</head>
<body>
    <!-- Wrapper -->
    <div id="wrapper">
        <!-- Section -->
        <section class="fullscreen" style="background-image: url(<?php echo base_url('assets'); ?>/images/pages/backg-login.gif);background-repeat: no-repeat;background-size: cover;">
            <div class="container container-fullscreen">
                <div class="text-middle">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-2"></div>
                        <div class="col-md-3 text-center m-b-30">
                            <a href="<?php echo base_url(); ?>" class="logo hidden-xs">
                                <img src="<?php echo base_url('assets'); ?>/images/logo.png" alt="Vensia logo" style="max-height: 100px">
                            </a>
                            <a href="<?php echo base_url(); ?>" class="logo hidden-lg hidden-md hidden-sm">
                                <img src="<?php echo base_url('assets'); ?>/images/logo-dark.png" alt="Vensia logo" style="max-height: 100px;">
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-2"></div>
                        <div class="col-md-3 p-30 background-white b-r-6" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                            <h3>Masuk ke Vensia</h3>
                            <form class="form-transparent-grey" id="form-login">
                                <div class="form-group">
                                    <label class="sr-only">Email</label>
                                    <input type="text" class="form-control" name="email" placeholder="Email" required>
                                </div>
                                <div class="form-group m-b-5">
                                    <label class="sr-only">Kata Sandi</label>
                                    <input type="password" class="form-control" name="password" placeholder="Password" required id="password-field">
                                    <span toggle="#password-field" class="field-icon fa fa-fw fa-eye toggle-password"></span>
                                </div>
                                <div class="form-group form-inline text-left ">

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"><small> Remember me</small>
                                        </label>
                                    </div>
                                    <a href="<?php echo base_url('authss/forgotpassword'); ?>/" class="right" style="margin-top: 8px">
                                        <small>Lupa Kata Sandi?</small></a>
                                    </div>
                                    <div class="text-left form-group">
                                        <input type="submit" name="submit" class="btn btn-block btn-vensia" value="Masuk ke Vensia">
                                    </div>
                                </form>
                                <p class="small" style="text-align: center;">Belum punya akun Vensia? Daftar <a href="<?php echo base_url('authss/register'); ?>/" style="color: #dc2977; font-weight: bold;">disini</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end: Section -->
        </div>
        <!-- end: Wrapper -->

        <!-- Go to top button -->
        <a id="goToTop"><i class="fa fa-angle-up top-icon"></i><i class="fa fa-angle-up"></i></a>

        <!--Plugins-->
        <script src="<?php echo base_url('assets'); ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('assets'); ?>/js/plugins.js"></script>

        <!--Template functions-->
        <script src="<?php echo base_url('assets'); ?>/js/functions.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.11/dist/sweetalert2.all.min.js"></script>
        <script type="text/javascript">
            $('#form-login').on('submit', function(e){
                e.preventDefault();

                var mydata = $('form#form-login').serialize();
                $.ajax({
                    type        : "POST",
                    url         : "<?php echo base_url('index.php/Authss/cek_login');?>",
                    data        : mydata,
                    datatype    : "json",
                    success     : function(hasil){
                        console.log(hasil);                       
                        var rs = $.parseJSON(hasil); 
                        swal({
                            type : rs['icon'],
                            text : rs['text']
                        }).then( function() {
                            if(rs['icon'] == "success"){
                                
                                location.replace(rs['direct']);
                            }
                        });
                    } 
                });
            });
        </script>
        <script type="text/javascript">
            $(".toggle-password").click(function() {

              $(this).toggleClass("fa-eye fa-eye-slash");
              var input = $($(this).attr("toggle"));
              if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    </script>
</body>

</html>
