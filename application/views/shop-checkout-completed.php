<?php $this->load->view('header'); ?>



<!-- SHOP CHECKOUT COMPLETED -->
<section id="shop-checkout-completed">
	<div class="container">
    <div class="hr-title hr-long center"><abbr>Konfirmasi Pesanan dan Pembayaran </abbr> </div>

    <center>
      <a href="#" class="btn btn-vensia icon-left" data-target="#status" data-toggle="modal"><span>Lihat Status Konfirmasi Pesanan Anda</span></a>
    </center>
    <div class="p-t-10 m-b-20 text-center">

      <?php
      if($invoice[0]['Status'] == "MENUNGGU PEMBAYARAN"){
        $date =  new dateTime($invoice[0]['Tgl Konfirm']);
        ?>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8">
           <div class="panel panel-success">
            <div class="panel-heading">
              <h3 class="panel-title">Silahkan Melakukan Pembayaran</h3>
            </div>
            <div class="panel-body">
              Lakukan Pembayaran Sebelum:<br>
              <div class="countdown small" data-countdown="<?php echo $date->modify('+1 day')->format('Y/m/d H:i:s');?>"></div>
            </div>
          </div>
        </div>
        <div class="col-md-2"></div>
      </div>
      <?php
    }elseif($invoice[0]['Status'] == "KONFIRMASI"){
      $date =  new dateTime($invoice[0]['Tgl Pemesanan']);
      ?>
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
         <div class="panel panel-danger">
          <div class="panel-heading">
            <h3 class="panel-title">Tunggu konfirmasi pemesanan sebelum pembayaran</h3>
          </div>
          <div class="panel-body">
            Silahkan menunggu konfirmasi pesanan<br>
            <div class="countdown small" data-countdown="<?php echo $date->modify('+1 day')->format('Y/m/d H:i:s');?>"></div>
            
          </div>
          <?php 
          //mengecek jika waktu lebih dari 24 jam 
          /* awal */
          //tanggal pemesanan
          $nil=$invoice[0]['Tgl Pemesanan'];
          // Menambah jam di php
          if(function_exists('date_default_timezone_set')) date_default_timezone_set('Asia/Jakarta');
          $date2 = date_create($nil);
          //menambahkan 24 jam
          date_add($date2, date_interval_create_from_date_string('24 hours'));
          /*echo 'Ditambahkan 24 jam: '.date_format($date2, 'Y/m/d H:i:s').'';
          echo "<br/>";
         // $nil=$invoice[0]['Tgl Pemesanan']->format('Y/m/d H:i:s');
          echo date('Y/m/d H:i:s');
          echo "<br/>";
          echo $nil;
          echo "<br/>";
          echo date_format($date2, 'Y/m/d H:i:s');
          echo "<br/>";
          echo $date->modify('+1 day')->format('Y/m/d H:i:s');*/
                 
          ?>
          <input type="hidden" id="tgl_awal" value="<?php echo date('Y/m/d H:i:s')?>">
          <input type="hidden" id="tgl_akhir" value="<?php echo date_format($date2, 'Y/m/d H:i:s') ?>">
          <input type="hidden" id="invoce_uuid" value="<?php echo $invoice_uuid ?>"/>
          <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->

          <script type="text/javascript">
          tgl_awal = document.getElementById("tgl_awal").value;
          tgl_akhir = document.getElementById("tgl_akhir").value;
          tgl_awal2=new Date(Date.parse(tgl_awal));
          tgl_akhir2 = new Date(Date.parse(tgl_akhir));
          uuid = document.getElementById("invoce_uuid").value;
          //alert(tgl_awal+" - "+tgl_akhir);
          if(tgl_awal2 > tgl_akhir2){
            
            //alert(uuid);
            
              $.ajax({
                type : "POST",
                data : {uuid :  uuid, proses : "false"},
                url : "http://localhost/api15/vendors/proses_pesanan_time",
                success : function(hasil){
                  hasil = JSON.parse(hasil);
                  swal({
                    title: hasil.status,
                    text: hasil.message,
                    type: hasil.status
                  }).then((result) => {
                    location.reload();
                  });
                }
              });  
            
          }
          </script>
          <?php 
          
          /* akhir */?>
        </div>
      </div>
      <div class="col-md-2"></div>
    </div>
    <?php
  }
  ?>
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8" style="text-align: left !important;">
      <div class="panel panel-default">
        <!-- <div class="panel-heading">Panel heading without title</div> -->
        <div class="panel-body">
          <div class="col-md-6">
            <p><b>Customer Detail</b></p>
            <table>
              <tr>
                <td width="35%">Nama</td>
                <td width="15%">:</td>
                <td width="50%"><?php echo $invoice[0]['Nama'];?></td>
              </tr>
              <tr>
                <td width="35%">No Tlp</td>
                <td width="15%">:</td>
                <td width="50%"><?php echo $invoice[0]['No Telp'];?></td>
              </tr>
              <tr>
                <td width="35%">Email</td>
                <td width="15%">:</td>
                <td width="50%"><?php echo $invoice[0]['email'];?></td>
              </tr>
            </table>
          </div>

          <div class="col-md-6">
            <p><b>Payment Detail</b></p>
            <table>
              <tr>
                <td width="45%">No. Booking</td>
                <td width="5%">:</td>
                <td width="50%"><?php echo $invoice[0]['Invoice'];?></td>
              </tr>
              <tr>
                <td width="45%">Metode Pembayaran</td>
                <td width="5%">:</td>
                <td width="50%"><?php echo $invoice[0]['Metode Pembayaran'];?></td>
              </tr>
              <tr>
                <td width="45%">Status</td>
                <td width="5%">:</td>
                <td width="50%"><?php echo $invoice[0]['Status'];?></td>
              </tr>
              <tr>
                <td width="45%">Term Pembayaran</td>
                <td width="5%">:</td>
                <td width="50%"><?php echo $invoice[0]['Angsuran'];?>x Angsuran</td>
              </tr>
              <tr>
                <td width="45%">Tgl Pemesanan</td>
                <td width="5%">:</td>
                <td width="50%"><?php echo date_format(date_create($invoice[0]['Tgl Pemesanan']),'d M Y');?></td>
              </tr>
              <tr>
                <td width="45%">Tgl Event</td>
                <td width="5%">:</td>
                <td width="50%"><?php echo date_format(date_create($invoice[0]['Tgl Event']),'d M Y');?></td>
              </tr>
            <tr>
                <td width="45%">Alamat Event</td>
                <td width="5%">:</td>
                <td width="50%"><?php echo $invoice[0]['Alamat'];?></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-2"></div>
  </div>

  <div id="section-payment">

    <?php
    if($invoice[0]['Status'] == "MENUNGGU PEMBAYARAN"){
      if($invoice[0]['Metode Pembayaran'] == "VA" || $invoice[0]['Metode Pembayaran'] == "transfer-CIMB"){
        echo "<h2>No. Virtual Account : ".$invoice[0]['Ipaymu']."</h2>";
        ?>
        <?php
      }else{
        ?>
        <h5>Mohon Transfer untuk pembayaran DP ke :</h5>
        <img class="bank-icon" src="<?php echo base_url('assets'); ?>/images/bank/mandiri.png"><br>
        <div class="row" style="padding-bottom: 20px">
          <div class="col-md-4"></div>
          <div class="col-md-2">Account Number<br>Account Holder Number<br>Total Amount</div>
          <div class="col-md-2"><b><?php echo $bank['bank_number'];?><br><?php echo $bank['account_name'];?><br>Rp. <?php echo number_format($invoice[0]['DP'],0,",",".");?></b></div>
          <div class="col-md-4"></div>
        </div>
        <hr>

        <?php
      }
    }

    ?>
    <hr>
  </div>

  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8" style="text-align: left !important;">
      <h5>Catatan</h5> 
      <ul>
        <li class="p-t-10">Klien akan mendapatkan konfirmasi atas layanan/produk Vendor yang telah dipesan
          maksimal dalam waktu <span style="color: #1374d2; font-weight: 500">1x24 jam</span>. Lebih dari itu pesanan Klien dianggap telah ditolak
        oleh Vendor.</li>
        <li class="p-t-10">Setelah Vendor mengkonfirmasi pesanan, Anda akan mendapatkan <a href="#" style="font-weight: 500">Invoice</a> pembayaran.</li>
        <li class="p-t-10">Pesanan <span style="color: #1374d2; font-weight: 500">otomatis dibatalkan</span> apabila tidak melakukan pembayaran lebih dari <span style="color: #1374d2; font-weight: 500">1x24 jam</span> setelah kode pembayaran diberikan</li>
        <li class="p-t-10"><span style="font-weight: 500">Simpan bukti pembayaran</span> Anda apabila sewaktu-waktu diperlukan jika terjadi kendala dalam melakukan transaksi</li>
      </ul> 
    </div>
    <div class="col-md-2"></div>
  </div>

  <div>
  
    <?php
    if($invoice[0]['Status'] == "MENUNGGU PEMBAYARAN"){
      ?>
      <h5>Complete your payment?</h5>
      <?php 
    } 
    ?>
    <!-- <a href="#" class="btn btn-danger icon-left" id="cancel"><span>Batalkan Pesanan</span></a> -->
    <?php
    if($invoice[0]['Status'] == "MENUNGGU PEMBAYARAN" && $invoice[0]['Metode Pembayaran'] != "VA"){
      ?>
      <a class="btn btn-dark icon-left" href="<?php echo base_url('profil/konfirmasi/invoice/'.$invoice_uuid);?>"><span>Konfirmasi Pembayaran</span></a>
      <?php 
    } 
    ?>
    <!-- <br>
    <a href="#" class="btn btn-warning icon-left"><span>Help</span></a>
    <a class="btn btn-yeps icon-left" href="<?php echo base_url('profil'); ?>/"><span>Go to my Profile</span></a> -->
  </div>
</div>
</div>
</section>
<!-- end: SHOP CHECKOUT COMPLETED -->

<?php $this->load->view('info-yeps'); ?>

<!-- MODAL KONFIRMASI -->
<div class="modal fade" id="konfirmasi" tabindex="-1" role="modal" aria-labelledby="modal-label" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="modal-label">Batalkan Pesanan</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <p>Apakah Anda yakin akan membatalkan semua pesanan Anda?</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
        <a href="<?php echo base_url(); ?>" type="button" class="btn btn-yeps">Ya</a>
      </div>
    </div>
  </div>
</div>
<!-- / MODAL KONFIRMASI -->

<?php $this->load->view('footer'); ?>
<!-- MODAL -->
<div class="modal fade" id="status" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 id="modal-label-3" class="modal-title">Status Pesanan Anda </h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('checkout-detail');?>
      </div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-b" type="button">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('a#cancel').on('click', function(e){
    e.preventDefault();
    swal({
      title: 'Batalkan Pesanan',
      text: "Apakah Anda yakin akan membatalkan pesanan ini?",
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#107ADE',
      cancelButtonColor: '#C30000',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then((result) => {
      if (result.value) {

        $.ajax({
          type : 'POST',
          data : mydata,
          url : '<?php echo base_url('Shop/cancel/')?>',
          typedata : 'json',
          success : function(hasil){
            console.log(hasil);
            var rs = $.parseJSON(hasil);
            swal({
              type : rs['icon'],
              text : rs['text']
            }).then( function(e) {
              if(rs['icon'] == "success"){

                location.replace(rs['direct']);
              }
            });
          }
        });
      }
    })
  })
</script>

<script type="text/javascript">

  $('#getVA').on('click', function(e){
    e.preventDefault();
    var mydata = $('form#payment').serialize();
    $.ajax({
      url : "<?php echo base_url('Shop/va');?>",
      data : mydata,
      type : 'POST',
      success : function(hasil){
        $('#section-payment').html(hasil);
      }
    })

  });
</script>