<?php $this->load->view('header'); ?>
<style type="text/css">
	input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
		/* display: none; <- Crashes Chrome on hover */
		-webkit-appearance: none;
		margin: 0;
		/* <-- Apparently some margin are still there even though it's hidden */
	}

	.lihat-detail {
		cursor: pointer;
	}

</style>

<div class="lyt-vnsia p-t-30">
	<h3>Keranjang Pesanan</h3>
</div>

<!-- SHOP CART -->
<?php
if(count($result) <= 0){
	?>
	<section>
		<div class="container">
			<div class="row box-cart" style="background-color: #f9fafb; margin-top: -50px">
				<center>
					<img src="<?php echo base_url('assets'); ?>/images/pages/icon/empty-cart.png" alt=""
					style="max-width: 15%; margin-bottom: 20px" data-animation="pulse infinite">
					<h5 class="text-center">Keranjang Pesanan Anda Kosong</h5>
					<a href="<?php echo base_url('');?>" class="btn btn-vensia">Cari Vendor Sekarang</a>
				</center>
			</div>

		</div>
	</section>
	<?php
} else {
	?>
	<section id="shop-cart" style="padding-top: 40px">
		<div class="lyt-vnsia">
			<div class="shop-cart row">
				<div class="col-lg-8">
					<div class="col-md-12 box-cart">
						<div class="col-md-8 ">
							<div class="form-check">
								<input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
								<label class="form-check-label" for="defaultCheck1" style="margin-left: 17px">
									Pilih Semua
								</label>
							</div>
						</div>
						<div class="col-md-4 ">
							<a href="#" data-uuid="<?php echo isset($key['uuid']) ? $key['uuid'] : '';?>"
								class="float-right btn btn-putih btn-xs deleteTrans"><i class="fa fa-trash"></i>Hapus
							</a>
						</div>
					</div>
					<?php
					$subTotal = 0;
					function exist($url){
						$file_headers = @get_headers($url);
						if($file_headers[0] == "HTTP/1.0 200 OK"){
							return true;
						}else{
							return false;
						}
					}
					foreach ($result as $key) {
						
						$nama_vendor = str_replace(" ","_",$key['vendor']);
						$produk = $key['slug'];
						if($key['parameter']['Harga Promosi'] == "0"){
							$subTotal += (int)$key['parameter']['Harga Normal']*$key['qty'];
						}else{
							$subTotal += (int)$key['parameter']['Harga Promosi']*$key['qty'];	
						}
						$foto = array_filter(explode(";", $key['parameter']['Foto']));

						foreach ($foto as $keyFoto) {
							$image = "https://res.cloudinary.com/yepsindo/image/upload/w_150,h_200,c_fill/".$keyFoto.".jpg";
							if(empty($image) || $key['parameter']['Foto'] == "" || $keyFoto == ""){
								$image = base_url('assets/images/pages/blank.jpg');
							}
						}
						?>
						<div class="col-md-12 box-cart">
							<div class="col-md-8">
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
									<label class="form-check-label" for="defaultCheck1" style="margin-left: 17px">
										<a href="<?php echo base_url('jasa/'.$nama_vendor.'/page=1');?>">
											<?php echo $key['vendor'];?>
										</a>
									</label>
								</div>
								
							</div>
							<div class="col-md-4 ">
								<a href="#" data-uuid="<?php echo $key['uuid'];?>"
									class="float-right btn btn-noline btn-xs deleteTrans"><i class="fa fa-trash"></i>Hapus
								</a>
							</div>
							<hr>
							<div class="col-md-12">
								<div class="col-md-2">
									<img src="<?php echo $image;?>" class="img-cart">
								</div>
								<div class="col-md-4">
									<a href="<?php echo base_url('jasa/'.$nama_vendor.'/'.$produk);?>"
										class="product-name"><?php echo $key['parameter']['Nama'];?></a>
										<h6 class="m-b-0 m-t-5 vnsia-clr">Tanggal & Waktu</h6>
										<p class="variant-cart"><i class='fa fa-calendar' style='margin: 0px 10px 0px 0px;'></i> <?php echo date_format(date_create($key['date']),'d-m-Y');
										if($key['time'] != ""){
											echo "<i class='fa fa-clock' style='margin: 0px 10px'></i> ". $key['time']; 
										}
										?></p>
									</div>
									<div class="col-md-3">
										<h4 class="price-cart">Rp. <?php 
										if($key['parameter']['Harga Promosi'] == "0"){
											echo number_format($key['parameter']['Harga Normal'],0,",",".");
										}else{
											echo number_format($key['parameter']['Harga Promosi'],0,",",".");
										} ?>
									</h4>
								</div>
								<div class="col-md-3 cart-product-quantity">
									<div class="quantity m-b-10">
										<?php
										if($key['parameter']['Harga Promosi'] == "0"){
											echo "<input type='hidden' name='harga' value='".$key['parameter']['Harga Normal']."'>";
										}else{
											echo "<input type='hidden' name='harga' value='".$key['parameter']['Harga Promosi']."'>";
										}
										?>
										<input type="button" class="minus" value="-" data-uuid="<?php echo $key['uuid'];?>">
										<input type="number" class="qty" data-uuid="<?php echo $key['uuid'];?>" <?php if (isset($key['parameter']['Minimal Order'])) {
											echo "min='".$key['parameter']['Minimal Order']."'";
										}
										?> value="<?php echo $key['qty'];?>" name="qty;<?php echo $key['uuid'];?>">
										<?php
										if($key['parameter']['Harga Promosi'] == "0"){ ?>
											<input type='hidden' name='total_per_transaksi' value='<?php echo $key['parameter']['Harga Normal']*$key['qty']+$key['total_add']; ?>'>
										<?php }else{ ?>
											<input type='hidden' name='total_per_transaksi' value='<?php echo $key['parameter']['Harga Promosi']*$key['qty']+$key['total_add']; ?>'>
										<?php }
										?>
										<input type='hidden' name='harga_add' value='<?php echo $key['total_add']; ?>'>
										<input type="button" class="plus" value="+" data-uuid="<?php echo $key['uuid'];?>">
									</div>
								</div>
								<hr>
								<!-- <h5 class="lihat-detail">Lihat Selengkapnya</h5> -->
								<div class="details">
									<div class="col-md-12">
										<h6 class="m-b-0 m-t-5 vnsia-clr">Additional Produk</h6>
									</div>
									<?php $result2 = $this->produk->get_cart_add($key['uuid']); ?>
									<?php foreach($result2 as $key2){ ?>
										<div class="col-md-2">
											<p class="add-name"><?php echo $key2['nama_add'];?></p>
										</div>
										<div class="col-md-4">
											<p class="add-name"><span class="add-price-satuan">Rp.
												<?php echo number_format($key2['harga_add'],0,",","."); ?></span></p>
											</div>
											<div class="col-md-3">
												<h5 class="add-subtotal-cart" style="font-size: 15px">Rp. <?php echo number_format($key2['total_add'],0,",","."); ?>
											</h5>
										</div>
										<div class="col-md-3 cart-product-quantity">
											<div class="quantity m-b-10">
												<?php echo "<input type='hidden' name='harga2' value='".$key2['harga_add']."'>"; ?>
												<?php echo "<input type='hidden' name='id_add' value='".$key2['id']."'>"; ?>
												<?php echo "<input type='hidden' name='total_at' value='".$key2['total_add']."'>"; ?>
												<input type="button" class="minusadd" value="-" data-uuid="<?php echo $key['uuid'];?>">
												<input type="number" class="qty2" data-uuid="<?php echo $key['uuid'];?>" <?php echo "min='". $key2['minimal']."'"; ?> value="<?php echo $key2['qty_add'];?>" name="qty2" data-uuid="<?php echo $key['uuid'];?>">
												<input type="button" class="plusadd" value="+" data-uuid="<?php echo $key['uuid'];?>">
											</div>
										</div>
									<?php  } ?>
									<br>
								</div>

								<hr>
								<div class="col-md-2"></div>
								<div class="col-md-4"></div>
								<div class="col-md-3">
									<p class="variant-cart">Sub Total</p>
								</div>
								<div class="col-md-3 m-b-20">
									<h4 class="main-subtotal-cart" style="font-size: 15px" id="main-harga">Rp. <?php 
									if($key['parameter']['Harga Promosi'] == "0"){
										echo number_format($key['parameter']['Harga Normal']*$key['qty']+$key['total_add'],0,",",".");
									}else{
										echo number_format($key['parameter']['Harga Promosi']*$key['qty']+$key['total_add'],0,",",".");
									}
									?>
								</h4>
							</div>
							<hr>
							<div class="col-md-6">
								<div class="form-group">
									<h6 class="m-b-10 m-t-5 vnsia-clr">Catatan untuk Vendor</h6>
									<textarea type="text" class="form-control noteInput"
									name="note"><?php echo $key['note'];?></textarea>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<h6 class="m-b-10 m-t-5 vnsia-clr">Alamat Vendor</h6>
									<textarea type="text" class="form-control noteInput"
									name="alamat"><?php echo $key['address'];?></textarea>
								</div>
							</div>
						</div>
					</div>

					<?php
				}
				$eventCharge = $subTotal*0.05;
				$grantTotal = $subTotal + $eventCharge;
				?>
			</div>
			<!-- <div class="col-md-1" style="width: 3%"></div> -->
			<div class="col-md-4">
				<div class="box-cart p-r-10 ">
					<div class="table-responsive">
						<h4>Ringkasan Pesanan</h4>
						<hr>
						<table class="p-5">
							<tbody>
								<tr>
									<td class="cart-product-name">
										<strong>Sub Total</strong>
									</td>

									<td class="cart-product-name text-right">
										<span class="amount" id="subTotal">Rp.
											<?php echo number_format($subTotal,0,",",".");?></span>
										</td>
									</tr>
									<tr>
									</tr>
									<tr>
										<td class="cart-product-name">
											<strong>Event Charge</strong>
										</td>

										<td class="cart-product-name  text-right">
											<span class="amount" id="eventCharge">Rp.
												<?php echo number_format($eventCharge,0,",",".");?></span>
											</td>
										</tr>
									</tbody>
								</table>
								<hr>
								<h5 class="txt-total-harga">
									<strong>Total Pembayaran</strong>
								</h5>

								<h5 class="total-harga">
									<span style="color: #FB9307" class="amount color lead" id="grantTotal"><strong>Rp.
										<?php echo number_format($grantTotal,0,",",".");?></strong></span>
									</h5>
									<div>
										<a href="#" class="btn btn-vensia icon-left btn-block checkoutBtn"><span>Lanjutkan ke Pembayaran</span></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<?php
			}
			?>
			<!-- end: SHOP CART -->

			<!-- <?php $this->load->view('info-yeps'); ?> -->

			<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js" type="text/javascript"></script>

			<?php $this->load->view('footer'); ?>
			<script type="text/javascript">

				$('.alamat-reset').hide();
				$('.alamat-profil').on('click', function(e){

					$('textarea[name=alamat]').val('<?php echo $alamat;?>');

				})
				$('textarea[name=alamat]').on('change', function(e){
					$('.alamat-reset').show();
					$('.alamat-profil').hide();

				})
				$('.alamat-reset').on('click', function(e){

					$('textarea[name=alamat]').val('');
					$('.alamat-profil').show();
					$('.alamat-reset').hide();
				})

			</script>

			<script type="text/javascript">
				$('.checkoutBtn').on('click', function(e){
					e.preventDefault();
					var hasil = $('.noteInput'); 
					$('.noteInput').each(function(i, obj){
						if(this.value == ""){
							$(hasil[i]).val("none");
						}
					})

					var mydata = $('input[name^=note], input[name^=uuid]').serialize();


					var alamat = $('textarea[name^=alamat]').val();
					if(alamat == "none"){
						alamat = "<?php echo $alamat;?>";
					}
					console.log(mydata);
					$.ajax({
						data  : mydata,
						type : 'post',
						url : '<?php echo base_url('Shop/note_save');?>',
						success : function(hasil){
							location.replace('<?php echo base_url('Shop/checkout/');?>'+alamat);
						}
					});

				})
			</script>

			<script type="text/javascript">
				$('.deleteTrans').click(function(e){
					e.preventDefault();
					var a = $(this);
					swal({
						title: 'Hapus Jasa',
						text: "Yakin akan hapus jasa ini dari Cart?",
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Ya Hapus'
					}).then((result) => {
// 			if (result.value) {

	$.ajax({
		type : 'POST',
		url : '<?php echo base_url('Shop/delete_cart/')?>'+ a.data('uuid'),
		typedata : 'json',
		success : function(hasil){
			var rs = $.parseJSON(hasil);
			swal({
				type : rs['icon'],
				text : rs['text']
			}).then( function(e) {
				location.reload();
			});
		}
	});
			// }
		})

				})
			</script>
			<script type="text/javascript">
				numeral.register('locale', 'id', {
					delimiters: {
						thousands: '.',
						decimal: ','
					},
					abbreviations: {
						thousand: 'k',
						million: 'm',
						billion: 'b',
						trillion: 't'
					},
					ordinal : function (number) {
						return number === 1 ? 'er' : 'ème';
					},
					currency: {
						symbol: '€'
					}
				});
				numeral.locale('id');
			</script>
			<script type="text/javascript">
				$('.qty').on('change', function(e){
					var a = $(this);
					var min = parseInt(a.closest('div').find('input[name^=qty]').attr('min'));
		//var min2 = parseInt(a.closest('div').find('input[name^=qty2]').attr('min'));
		
		var qty = a.val();
		if(qty < min){
			a.val(min);
			var qty = a.val();
			var harga = parseInt(a.closest('div').find('input[name=harga]').val());
			var total_add = parseInt(a.closest('div').find('input[name=harga_add]').val());
			var subTotal = numeral((harga* qty)+total_add).format('0,0');
			a.closest('.col-md-12').find('.main-subtotal-cart').html("Rp."+subTotal);
			swal({
				type : "info",
				text : "Minimal Order : " + min
			})
			$.ajax({
				url : '<?php echo base_url('Shop/transQty/');?>'+a.data('uuid')+'/'+qty,
				success : function(hasil){
					a.closest('.row').find('input[name=total_per_transaksi]').val((harga* qty)+total_add);
					a.closest('.col-md-12').find('.main-subtotal-cart').html("Rp."+subTotal);
					calculateadd();	
				}
			})
		}
		/* untuk menghapus jasa, untuk sementara ini di komen 
		else if(qty <= 0){
			swal({
				text : "Hapus jasa isi?",
				icon : "warning",
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Ya Hapus'
			}).then((result) => {
				if (result.value) {
					$.ajax({
						type : 'POST',
						url : '<?php //echo base_url('Shop/delete_cart/')?>'+ a.data('uuid'),
						typedata : 'json',
						success : function(hasil){
							var rs = $.parseJSON(hasil);
							swal({
								type : rs['icon'],
								text : rs['text']
							}).then( function(e) {
								location.reload();
							});
						}
					});
				}
			})
		} */
		else{
			var harga = parseInt(a.closest('div').find('input[name=harga]').val());
			var total_add = parseInt(a.closest('div').find('input[name=harga_add]').val());
			var subTotal = numeral((harga* qty)+total_add).format('0,0');
			a.closest('.col-md-12').find('.main-subtotal-cart').html("Rp."+subTotal);
			$.ajax({
				url : '<?php echo base_url('Shop/transQty/');?>'+a.data('uuid')+'/'+qty,
				success : function(hasil){
					a.closest('div').find('input[name^=qty]').val(qty);
					a.closest('.row').find('input[name=total_per_transaksi]').val((harga* qty)+total_add);
					a.closest('.col-md-12').find('.main-subtotal-cart').html("Rp."+subTotal);
					calculateadd();	
				}
			})
		}
	});

				$('.qty2').on('change', function(e){
					var a = $(this);
					var min = parseInt(a.closest('div').find('input[name^=qty2]').attr('min'));
					var id_add = parseInt(a.closest('div').find('input[name^=id_add]').val());
					var qty = a.val();
					if(qty < min){
						a.val(min);
						var qty = a.val();
						var harga = parseInt(a.closest('div').find('input[name=harga2]').val());
						var harga_at = parseInt(a.closest('div').find('input[name=total_at]').val());
						var total = parseInt(a.closest('.row').find('input[name=total_per_transaksi]').val());
						var harga_tmp = total-harga_at;
						var subTotal = numeral((harga*qty)+harga_tmp).format('0,0');
						var addTotal = numeral(harga*qty).format('0,0');
						a.closest('.col-md-12').find('.add-subtotal-cart').html("Rp."+subTotal);
						a.closest('.row').find('#main-harga').html("Rp. "+subTotal);
						swal({
							type : "info",
							text : "Minimal Order : " + min
						})
						$.ajax({
							url : '<?php echo base_url('Shop/transQty_add/');?>'+a.data('uuid')+'/'+qty+'/'+id_add,
							success : function(hasil){
								a.closest('.row').find('input[name=total_per_transaksi]').val((harga*qty)+harga_tmp);
								a.closest('.col-md-12').find('.add-subtotal-cart').html("Rp."+addTotal);
								a.closest('.row').find('#main-harga').html("Rp. "+subTotal);
								calculateadd();	
							}
						})
					}
					else{
						var harga = parseInt(a.closest('div').find('input[name=harga2]').val());
						var harga_at = parseInt(a.closest('div').find('input[name=total_at]').val());
						var total = parseInt(a.closest('.row').find('input[name=total_per_transaksi]').val());
						var harga_tmp = total-harga_at;
						var harga_add = parseInt(a.closest('.row').find('input[name=harga_add]').val());
						var harga_add_tmp = harga_add - harga_at;
						var subTotal = numeral((harga*qty)+harga_tmp).format('0,0');
						var addTotal = numeral(harga*qty).format('0,0');
						$.ajax({
							url : '<?php echo base_url('Shop/transQty_add/');?>'+a.data('uuid')+'/'+qty+'/'+id_add,
							success : function(hasil){
								a.closest('div').find('input[name^=qty2]').val(qty);
								a.closest('div').find('input[name^=total_at]').val(harga*qty);
								a.closest('.row').find('input[name^=harga_add]').val(harga_add_tmp+(harga*qty));
								a.closest('.row').find('input[name=total_per_transaksi]').val((harga*qty)+harga_tmp);
								a.closest('.col-md-12').find('.add-subtotal-cart').html("Rp."+addTotal);
								a.closest('.row').find('#main-harga').html("Rp. "+subTotal);
								calculateadd();	
							}
						})
					}
				});	

				function calculate(){
					var subTotal = numeral('0');
					var qty = [];
					var i = 0;
					$('input.qty').each(function(){
						qty.push($(this).val());

					});
					$('input.qty2').each(function(){
						qty2.push($(this).val());

					});
					$('input[name=harga]').each(function(){
						subTotal.add(parseInt($(this).val()) * qty[i]);
						i++;
					});
					$('input[name=harga2]').each(function(){
						subTotal.add(parseInt($(this).val()) * qty[i]);
						i++;
					});

		//var pajak = numeral(subTotal.value()/10);
		var event = numeral(subTotal.value()/20);
		//var grant = numeral(subTotal.value()+pajak.value()+event.value());
		var grant = numeral(subTotal.value()+event.value());
		$('#subTotal').html("Rp. "+subTotal.format('0,0'));
		//$('#pajak').html("Rp. "+pajak.format('0,0'));
		$('#eventCharge').html("Rp. "+event.format('0,0'));
		$('#grantTotal').html("<strong>Rp. "+grant.format('0,0')+"</strong>");

	}
	
	function calculateadd(){
		var subTotal = numeral('0');
		var qty = [];
		var i = 0;
		$('input.qty').each(function(){
			qty.push($(this).val());

		});
		$('input.qty2').each(function(){
			qty.push($(this).val());

		});
		$('input[name=harga]').each(function(){
			subTotal.add(parseInt($(this).val()) * qty[i]);
			i++;
		});
		$('input[name=harga2]').each(function(){
			subTotal.add(parseInt($(this).val()) * qty[i]);
			i++;
		});
		var event = numeral(subTotal.value()/20);
		//var grant = numeral(subTotal.value()+pajak.value()+event.value());
		var grant = numeral(subTotal.value()+event.value());
		$('#subTotal').html("Rp. "+subTotal.format('0,0'));
		//$('#pajak').html("Rp. "+pajak.format('0,0'));
		$('#eventCharge').html("Rp. "+event.format('0,0'));
		$('#grantTotal').html("<strong>Rp. "+grant.format('0,0')+"</strong>");
		

	} 

	$('.plus').on('click', function(e){
		numeral.locale('id');
		var a = $(this);
		var qty = parseInt(a.closest('div').find('input[name^=qty]').val());
		var harga = parseInt(a.closest('div').find('input[name=harga]').val());
		var total = parseInt(a.closest('div').find('input[name=total_per_transaksi]').val());
		qty++; 
		var subTotal = numeral(harga+total).format('0,0');
		$.ajax({
			url : '<?php echo base_url('Shop/transQty/');?>'+a.data('uuid')+'/'+qty,
			success : function(hasil){
				a.closest('div').find('input[name^=qty]').val(qty);
				a.closest('.row').find('input[name=total_per_transaksi]').val(harga+total);
				a.closest('.col-md-12').find('.main-subtotal-cart').html("Rp. "+subTotal);
				calculateadd();	
			}
		})
	})
	// untuk tomol plus additional produk
	$('.plusadd').on('click', function(e){
		numeral.locale('id');
		var a = $(this);
		var id_add = parseInt(a.closest('div').find('input[name^=id_add]').val());
		var qty = parseInt(a.closest('div').find('input[name^=qty2]').val());
		var harga = parseInt(a.closest('div').find('input[name=harga2]').val());
		var total = parseInt(a.closest('.row').find('input[name=total_per_transaksi]').val());
		qty++; 
		var subTotal = numeral(harga+total).format('0,0');
		var addTotal = numeral(harga*qty).format('0,0');
		$.ajax({
			url : '<?php echo base_url('Shop/transQty_add/');?>'+a.data('uuid')+'/'+qty+'/'+id_add,
			success : function(hasil){
				a.closest('div').find('input[name^=qty2]').val(qty);
				a.closest('.row').find('input[name=total_per_transaksi]').val(harga+total);
				a.closest('.col-md-12').find('.add-subtotal-cart').html("Rp. "+addTotal);
				a.closest('.row').find('#main-harga').html("Rp. "+subTotal);
				calculateadd();				
			}
		})
	})

	$('.minus').on('click', function(e){
		var a = $(this);
		
		var qty = parseInt(a.closest('div').find('input[name^=qty]').val());
		var harga = parseInt(a.closest('div').find('input[name=harga]').val());
		var min = parseInt(a.closest('div').find('input[name^=qty]').attr('min'));
		var total = parseInt(a.closest('div').find('input[name=total_per_transaksi]').val());
		qty--;
		if(qty < min){
			swal({
				type : "info",
				text : "Minimal Order : " + min
			})
		}else{
			a.closest('div').find('input[name^=qty]').val(qty);
			var subTotal = numeral(total-harga).format('0,0');
			$.ajax({
				url : '<?php echo base_url('Shop/transQty/');?>'+a.data('uuid')+'/'+qty,
				success : function(hasil){
					a.closest('div').find('input[name^=qty]').val(qty);
					a.closest('.row').find('input[name=total_per_transaksi]').val(total-harga);
					a.closest('.col-md-12').find('.main-subtotal-cart').html("Rp. "+subTotal);
					calculateadd();	
				}
			})
		}
		
	})

	//minum untuk additional
	$('.minusadd').on('click', function(e){
		var a = $(this);
		var id_add = parseInt(a.closest('div').find('input[name^=id_add]').val());
		var qty = parseInt(a.closest('div').find('input[name^=qty2]').val());
		var harga = parseInt(a.closest('div').find('input[name=harga2]').val());
		var min = parseInt(a.closest('div').find('input[name^=qty2]').attr('min'));
		var total = parseInt(a.closest('.row').find('input[name=total_per_transaksi]').val());
		qty--;
		if(qty < min){
			swal({
				type : "info",
				text : "Minimal Order : " + min
			})
		}else{
			a.closest('div').find('input[name^=qty2]').val(qty);
			var subTotal = numeral(total-harga).format('0,0');
			var addTotal = numeral(harga*qty).format('0,0');
			$.ajax({
				url : '<?php echo base_url('Shop/transQty_add/');?>'+a.data('uuid')+'/'+qty+'/'+id_add,
				success : function(hasil){
					a.closest('div').find('input[name^=qty2]').val(qty);
					a.closest('.row').find('input[name=total_per_transaksi]').val(total-harga);
					a.closest('.col-md-12').find('.add-subtotal-cart').html("Rp."+addTotal);
					a.closest('.row').find('#main-harga').html("Rp. "+subTotal);
					calculateadd();
				}
			})
		}
		
	})
</script>

<script> 
	$(document).ready(function(){
		calculateadd();
		calculate();

	}); 
</script>
<script>
	function toggleDocs(event) {
		if (event.target && event.target.className == 'lihat-detail') {
			var next = event.target.nextElementSibling;
			if (next.style.display == "none") {
				next.style.display = "block";
			} else {
				next.style.display = "none";
			}
		}
	}
	document.addEventListener('click', toggleDocs, true);
</script>

