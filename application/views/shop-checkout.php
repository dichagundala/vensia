<?php $this->load->view('header'); ?>

<div class="lyt-vnsia p-t-30">
	<h3>Pembayaran</h3>
</div>

<!-- SHOP CHECKOUT -->
<section id="shop-checkout">
	<div class="container">
		<div class="shop-cart">
			<div class="hr-title hr-long center"><abbr>Pilih Metode Pembayaran dan Layanan Angsuran Pembayaran </abbr> </div>
			<div class="seperator"><span>Detail Pembelian</span></div>
			<center>
				<a href="#" class="btn btn-vensia icon-left" data-target="#modal-3" data-toggle="modal"><span>Lihat Pesanan Anda</span></a>
			</center>

			<div class="seperator"><i class="fa fa-credit-card hidden-xs"></i>Pembayaran Diawal Adalah 30% dari Total Pembayaran Anda<i class="fa fa-credit-card hidden-xs"></i>
			</div>

			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="table-responsive">
						<h4>Detail Pembayaran</h4>
						<?php 
						$tipe_detail = "checkout";
						$subTotal = 0;
						$dates = array();
						foreach ($result as $key) {
							if($key['parameter']['Harga Promosi'] == "0"){
								$subTotal += (int)$key['parameter']['Harga Normal']*$key['qty'];
							}else{
								$subTotal += (int)$key['parameter']['Harga Promosi']*$key['qty'];	
							}
							$dates[] = $key['date'];
						}
						/*untuk harga additional */
						/* awal */
						$id_produk = $key['id_add'];
						$nama_produk = $key['nama_add'];
						$qty_produk = $key['qty_add'];
						$harga_produk =$key['harga_add'];
						$total_add = count($nama_produk);
						
						$id_produk2 = $key['id_add2'];
						$nama_produk2 = $key['nama_add2'];
						$qty_produk2 = $key['qty_add2'];
						$harga_produk2 =$key['harga_add2'];
						$total_add2 = count($nama_produk2);
						for ($x = 0; $x < $total_add; $x++){ 
							$subTotal += (int) 	$qty_produk[$x]*	$harga_produk[$x];
						}
						/* akhir */
						$max = 0;
						$datetime1 = new DateTime('Now');
						$longestdate = new DateTime('Now');
						$interval = 0;
						foreach($dates as $date){
							$datetime2 = new DateTime($date);
							$interval = $datetime1->diff($datetime2);
							
							if((int)$interval->format("%R%a") > $max){
								$max = (int)$interval->format("%R%a");
							}	
						}

//						$grantTotal = $subTotal + $subTotal/10 + $subTotal/20;
						$grantTotal = $subTotal + ($subTotal*0.05);
						$angsuran = $grantTotal*0.7;
						?>
						<input type="hidden" name="angsuran" value="<?php echo $angsuran;?>">
						<input type="hidden" name="hari" value="<?php echo $max;?>">
						<table class="table">
							<tbody>
								<tr>
									<td class="cart-product-name">
										<strong>Sub Total</strong> 
									</td>

									<td class="cart-product-name text-right">
										<span class="amount">Rp. <?php $tot = number_format($subTotal,0,",","."); echo number_format($subTotal,0,",","."); ?></span>
									</td>
								</tr>
								<tr>
									<td class="cart-product-name">
										<strong>Pajak (10%)</strong>
									</td>

									<td class="cart-product-name  text-right">
										<span class="amount">Rp. <?php echo number_format($subTotal/10,0,",","."); ?></span>
									</td>
								</tr>
								<tr>
									<td class="cart-product-name">
										<strong>Event Charge</strong>
									</td>

									<td class="cart-product-name  text-right">
										<span class="amount">Rp. <?php echo number_format(($subTotal*0.05),0,",","."); ?></span>
									</td>
								</tr>
								<td class="cart-product-name">
									<strong>Total Pembayaran</strong>
								</td>

								<td class="cart-product-name text-right">
									<span class="amount color lead"><strong>Rp. <?php echo number_format($grantTotal,0,",","."); ?></strong></span>
								</td>
							</tr>
						</tbody>
					</table>
					<table class="table">
						<tbody>
							<tr>
								<td class="cart-product-name">
									<strong style="color: #dc2977">Total DP (30%)</strong>
								</td>

								<td class="cart-product-name text-right">
									<span class="amount color lead"><strong style="color: #dc2977">Rp. <?php echo number_format($grantTotal*0.3,0,",","."); ?></strong></span>
								</td>
							</tr>
							<tr>
								<td class="cart-product-name" colspan="2" style="text-align: center; background-color: #f6f6f6">
									<strong>Harga di atas tidak termasuk Pajak</strong>
								</td>
							</tr>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<form id="checkout">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="table-responsive">
					<h4>Jangka Waktu Pembayaran</h4>
					<!-- <select name="angsuran" id="angsuran" required>
						<option value="">Pilih Angsuran Bulan</option>
						<?php

						if($max < 14){
							echo "<option value='1'>1x Angsuran</option>";
						}
						if($max >= 14){
							echo "<option value='2'>2x Angsuran</option>";
						}
						if($max > 30){
							echo "<option value='3'>3x Angsuran</option>";
						}
						if($max > 45){
							echo "<option value='4'>4x Angsuran</option>";
						}
						if($max > 60){
							echo "<option value='5'>5x Angsuran</option>";
						}
						?>
					</select>
					<div id="angsuran-note" class="help-txt-angsuran">
						<p>Anda telah memilih <span id="angsuran-nilai"></span> angsuran untuk melaksanakan event ini. Untuk waktu pembayaran angsuran akan di informasikan kembali oleh tim kami</p>
					</div> -->
					<!--<h4>Alamat Pemesanan</h4>
						<textarea type="text" class="form-control noteInput" placeholder="Masukkan alamat lengkap event anda" name="alamat" value="<?php// echo $alamat;?>"><?php// echo $alamat;?></textarea>-->

						<br><br><br><br><br>
					</div>
				</div>

				<div class="seperator"><i class="fa fa-credit-card"></i> Pilih Metode Pembayaran <i class="fa fa-credit-card"></i>
				</div>

				<div class="col-md-12">
					<h4 class="upper">Payment Method</h4>

					<table class="payment-method table table-bordered table-condensed table-responsive">
						<tbody>
							<tr>
								<td>
									<div class="radio">
										<label>
											<input type="radio" id="transfer" name="metode" value="" required><b class="dark">Transfer Bank</b>
											<br>
										</label>
									</div>
									<div id="bank" class="m-l-20">
										<div class="radio">
											<label>
												<input type="radio" name="metode" value="transfer-Mandiri Syariah" required style="margin-top: 20px">
												<img class="img-h-50 m-l-10" src="<?php echo base_url('assets'); ?>/images/bank/mandiri-syariah.png">
											</label>
											<hr>
										</div>
										<div class="radio">
											<label>
												<input type="radio" name="metode" value="transfer-Mandiri" required style="margin-top: 13px">
												<img class="img-h-50 m-l-10" src="<?php echo base_url('assets'); ?>/images/bank/mandiri.png">
											</label>
											<hr>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="radio">
										<label>
											<input type="radio" id="VA" name="metode" value="VA" required><b class="dark">Virtual Account</b>
										</label>
									</div>
									<div class="target">
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<a class="btn btn-vensia icon-left float-right" style="margin-left: 10px" id="confirm">
						<span>Konfirmasi Pesanan</span>
					</a>
					<a class="btn btn-success icon-left float-right" href="<?php echo base_url('shop'); ?>/"><span>Kembali ke Pemesanan</span></a>
				</div>
			</form>

		</div>
	</div>
</div>
</section>
<!-- end: SHOP CHECKOUT -->

<!-- / MODAL KONFIRMASI -->



<section id="shop-cart" style="padding-top: 40px">
	<div class="lyt-vnsia">
		<div class="shop-cart row">
			<div class="col-lg-7">
				<?php
				$subTotal = 0;
				function exist($url){
					$file_headers = @get_headers($url);
					if($file_headers[0] == "HTTP/1.0 200 OK"){
						return true;
					}else{
						return false;
					}
				}
				
				foreach ($result as $key) {

					$nama_vendor = str_replace(" ","_",$key['vendor']);
					$produk = isset($key['slug']) ? $key['slug'] : '';
					if($key['parameter']['Harga Promosi'] == "0"){
						$subTotal += (int)$key['parameter']['Harga Normal']*$key['qty'];
					}else{
						$subTotal += (int)$key['parameter']['Harga Promosi']*$key['qty'];	
					}
					$foto = array_filter(explode(";", $key['parameter']['Foto']));

					foreach ($foto as $keyFoto) {
						$image = "https://res.cloudinary.com/yepsindo/image/upload/w_150,h_200,c_fill/".$keyFoto.".jpg";
						if(empty($image) || $key['parameter']['Foto'] == "" || $keyFoto == ""){
							$image = base_url('assets/images/pages/blank.jpg');
						}
					}
					?>
					<div class="col-md-12 box-cart">
						<div class="col-md-8">
							<a href="<?php echo base_url('jasa/'.$nama_vendor.'/page=1');?>">
								<h3 class="vendor-name"><?php echo $key['vendor'];?></h3>
							</a>
						</div>
						<hr>
						<div class="col-md-12">
							<div class="col-md-2">
								<img src="<?php echo $image;?>" class="img-cart">
							</div>
							<div class="col-md-4">
								<a href="<?php echo base_url('jasa/'.$nama_vendor.'/'.$produk);?>"
									class="product-name"><?php echo $key['parameter']['Nama'];?></a>
									<p class="variant-cart">Kuantitas : <?php echo $key['qty'];?></p>
								</div>
								<div class="col-md-3">
									<h6 class="m-b-0 m-t-5 vnsia-clr">Tanggal & Waktu</h6>
									<p class="variant-cart"><i class='fa fa-calendar' style='margin: 0px 10px 0px 0px;'></i> <?php echo date_format(date_create($key['date']),'d-m-Y');
									if($key['time'] != ""){
										echo "<br><i class='fa fa-clock' style='margin: 0px 10px 0px 0px'></i> ". $key['time']; 
									}
									?></p>
								</div>
								<div class="col-md-3">
									<h4 class="price-cart">
										Rp. <span class="kanan"><?php 
										if($key['parameter']['Harga Promosi'] == "0"){
											echo number_format($key['parameter']['Harga Normal'],0,",",".");
										}else{
											echo number_format($key['parameter']['Harga Promosi'],0,",",".");
										} ?></span>
									</h4>
								</div>
								<!-- <h5 class="lihat-detail">Lihat Selengkapnya</h5> -->
								<div class="details">
									<div class="col-md-2">
									</div>
									<div class="col-md-4">
									</div>
									<div class="col-md-6">
										<h6 class="m-b-0 m-t-5 vnsia-clr">Additional Produk</h6>
									</div>
									<?php $result2 = $this->produk->get_cart_add($key['uuid']); ?>
									<?php foreach($result2 as $key2){ ?>
										<div class="col-md-2">
										</div>
										<div class="col-md-4">
										</div>
										<div class="col-md-3">
											<p class="variant-cart"><?php echo $key2['qty_add'];?> <?php echo $key2['nama_add'];?></p>

										</div>
										<div class="col-md-3">
											<h5 class="add-subtotal-cart" style="font-size: 15px">
												Rp. <span class="kanan"><?php echo number_format($key2['total_add'],0,",","."); ?></span>
											</h5>
										</div>
									<?php  } ?>
									<br>
								</div>

								<hr class="our-space2">
								<div class="col-md-2"></div>
								<div class="col-md-4"></div>
								<div class="col-md-3">
									<p class="variant-cart">Sub Total</p>
								</div>
								<div class="col-md-3 m-b-20">
									<h4 class="main-subtotal-cart" style="font-size: 15px" id="main-harga">
										Rp. <span class="kanan"><?php 
										if($key['parameter']['Harga Promosi'] == "0"){
											echo number_format($key['parameter']['Harga Normal']*$key['qty']+$key2['total_add'],0,",",".");
										}else{
											echo number_format($key['parameter']['Harga Promosi']*$key['qty']+$key2['total_add'],0,",",".");
										}
										?></span>
									</h4>
								</div>
								<hr>
								<div class="col-md-6">
									<div class="form-group">
										<h6 class="m-b-10 m-t-5 vnsia-clr">Catatan untuk Vendor</h6>
										<p class="variant-cart"><?php echo isset($key['Note']) ? $key['Note'] : ''; ?></p>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<h6 class="m-b-10 m-t-5 vnsia-clr">Alamat Vendor</h6>
										<p class="variant-cart"><?php echo isset($key['Alamat']) ? $key['Alamat'] : '';?></p>
									</div>
								</div>
							</div>
						</div>

						<?php
					}
					$eventCharge = $subTotal*0.05;
					$grantTotal = $subTotal + $eventCharge;
					?>
				</div>
				<!-- <div class="col-md-1" style="width: 3%"></div> -->
				<div class="col-md-5">
					<div class="box-cart p-r-10 ">
						<div class="table-responsive">
							<h4>Ringkasan Pemesanan</h4>
							<hr>
							<table class="p-5" style="width: 100%">
								<tbody>
									<tr>
										<td class="cart-product-name">
											<strong>Sub Total</strong>
										</td>

										<td class="cart-product-name text-right">
											<span class="amount" id="subTotal">
												Rp. <?php echo $tot; ?>
											</span>
										</td>
									</tr>
									<tr>
									</tr>
									<tr>
										<td class="cart-product-name">
											<strong>Event Charge</strong>
										</td>

										<td class="cart-product-name  text-right">
											<span class="amount" id="eventCharge">
												Rp. <?php echo number_format($eventCharge,0,",",".");?>
											</span>
										</td>
									</tr>
								</tbody>
							</table>
							<hr>
							<table class="p-5" style="width: 100%">
								<tbody>
									<tr>
										<td class="cart-product-name">
											<h5>
												<strong>Total Harga</strong>
											</h5>
										</td>

										<td class="cart-product-name  text-right">
											<h5 class="total-harga">
												<span style="color: #FB9307" class="amount color lead" id="grantTotal">
													<strong>Rp. <?php echo number_format($grantTotal,0,",",".");?></strong>
												</span>
											</h5>
										</td>
									</tr>
								</tbody>
							</table>
							<div class="pembayaran-card">
								<div class="tabs">
									<ul class="nav nav-tabs outline" id="myTab" role="tablist">
										<li class="nav-item active">
											<a class="nav-link active" id="uang-muka" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Uang Muka</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" id="bayar-full" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Bayar Full</a>
										</li>
									</ul>
									<div class="tab-content" id="myTabContent">
										<div class="tab-pane fade active in" id="" role="tabpanel" aria-labelledby="uang-muka">
											<center>
												<h5 style="margin: 20px 0px">
													Uang Muka 30% 
													<span class="amount color lead"><strong style="color: #dc2977">Rp. <?php echo number_format($grantTotal*0.3,0,",","."); ?></strong></span>
												</h5>
												<select name="angsuran" id="angsuran" required>
													<option value="">Pilih Angsuran Bulan</option>
													<?php

													if($max < 14){
														echo "<option value='1'>1x Angsuran</option>";
													}
													if($max >= 14){
														echo "<option value='2'>2x Angsuran</option>";
													}
													if($max > 30){
														echo "<option value='3'>3x Angsuran</option>";
													}
													if($max > 45){
														echo "<option value='4'>4x Angsuran</option>";
													}
													if($max > 60){
														echo "<option value='5'>5x Angsuran</option>";
													}
													?>
												</select>
												<div id="angsuran-note" class="help-txt-angsuran">
													<p>Anda telah memilih <span id="angsuran-nilai"></span> angsuran untuk melaksanakan event ini. Untuk waktu pembayaran angsuran akan di informasikan kembali oleh tim kami</p>
												</div>
											</center>
										</div>
										<div class="tab-pane fade" id="" role="tabpanel" aria-labelledby="bayar-full">
											<p>Omnis </p>
										</div>
									</div>
								</div>
								<button class="btn btn-vensia icon-left btn-block" data-toggle="modal" data-target="#pilihMetode" style="margin-top: 20px">Pilih Metode Pembayaran</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


		<?php $this->load->view('footer'); ?>

		<!-- Modal -->
		<div class="modal fade" id="pilihMetode" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 id="modal-label-3" class="modal-title">Metode Pembayaran</h4>
					</div>
					<div class="modal-body">
						<div class="col-lg-3">
							Pilih Bank
						</div>
						<div class="col-lg-9">
							<table class="payment-method" style="width: 100%">
								<tbody>
									<tr>
										<td>
											<div class="m-l-20">
												<div class="row">
													<div class="col-lg-6" style="margin-top: -10px">
														<div class="radio">
															<label>
																<input type="radio" name="metode" value="transfer-Mandiri Syariah" required style="margin-top: 20px">
																<img class="img-h-50 m-l-10" src="<?php echo base_url('assets'); ?>/images/bank/mandiri-syariah.png">
															</label>
														</div>
													</div>
													<div class="col-lg-6">
														<h5>(Dicek Otomatis)</h5>
													</div>
												</div>
												<div class="row">
													<div class="col-lg-6" style="margin-top: -10px">
														<div class="radio">
															<label>
																<input type="radio" name="metode" value="transfer-Mandiri" required style="margin-top: 13px">
																<img class="img-h-50 m-l-10" src="<?php echo base_url('assets'); ?>/images/bank/mandiri.png">
															</label>
														</div>
													</div>
													<div class="col-lg-6">
														<h5>(Dicek Otomatis)</h5>
													</div>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
							<a class="btn btn-vensia icon-left btn-block" style="margin-left: 10px" id="confirm">
								<span>Konfirmasi Pesanan</span>
							</a>
						</div>
					</div>
					<div class="modal-footer">
						<!-- <button data-dismiss="modal" class="btn btn-b" type="button">Close</button>
 -->					</div>
				</div>
			</div>
		</div>

		<!--- MODAL -->
		<div class="modal fade" id="modal-3" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 id="modal-label-3" class="modal-title">Detail Pesanan Andaaaaa </h4>
					</div>
					<div class="modal-body">
						<?php 
						$data['tipe_detail'] = $tipe_detail;
						$this->load->view('checkout-detail');?>
					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" class="btn btn-b" type="button">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!--- MODAL KONFIRMASI -->
		<div class="modal fade" id="konfirmasi" tabindex="-1" role="modal" aria-labelledby="modal-label" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="modal-label">Konfirmasi Pesanan</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-sm-12">
								<p>Minta konfirmasi ketersediaan pemesanan yang telah Anda pesan?</p>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
						<a href="<?php echo base_url('shop/checkout_completed'); ?>/" type="button" class="btn btn-yeps">Ya</a>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript"
		src="https://app.sandbox.midtrans.com/snap/snap.js"
		data-client-key="SB-Mid-client-gampCCrldTZVOghQ"></script>

		<script>
			$(document).ready(function(){
				$("#bank").hide();
				$("#transfer").click(function(){
					$("#bank").show();
				});

				$('#VA').on('click',function(){
        //var aID = $(this).attr('href');
        //var elem = $(''+aID).html();
        var kode_invoice="INV/27122018/C/1/007";
        <?php	foreach ($result as $key) {

        	$id1=$key['uuid'];
        	$first1=$key['namaDepan'];
        	$last1=$key['namaBelakang'];
        	$telp1=$key['telp'];
        	$email1=$key['email'];

        	$first1 = $this->session->userdata('namaDepan');
        	$produk1=$key['parameter']['Nama'];
        	$qty1=$key['qty'];
        	if($key['parameter']['Harga Promosi'] == "0"){
        		$subTotal1 += (int)$key['parameter']['Harga Normal'];
        	}else{
        		$subTotal1 += (int)$key['parameter']['Harga Promosi'];	
        	}
        }?>
        var produk1="<?php echo $produk1 ; ?>";
				//var subTotal1 =1234;
				//var qty1=1;
				var id1="<?php echo $id1 ; ?>";
				var first1="<?php echo $first1 ; ?>";
				var last1="<?php echo $last1 ; ?>";
				var telp1="<?php echo $telp1 ; ?>";
				var email1="<?php echo $email1 ; ?>";
				var subTotal=<?= json_encode($subTotal); ?>;
				var subTotal1=<?= json_encode($subTotal1); ?>;
				var qty1=<?= json_encode($qty1) ; ?>;
				//alert(produk1+','+first1+','+last1+','+telp1+','+email1+','+subTotal+','+subTotal1+','+qty1);
				var requestBody = 
				{
					"transaction_details": {
						"order_id": kode_invoice,
						"gross_amount": subTotal
					},
					"item_details": [
					{
						"id": id1,
						"price": subTotal1,
						"quantity": qty1,
						"name": produk1
					}
					],
					"customer_details": [ {
						"email": "tester@example.com",
						"first_name": first1,
						"last_name": last1,
						"phone": "628112341234"
					}]
				}
				getSnapToken(requestBody, function(response){
					var response = JSON.parse(response);
					console.log("new token response", response);
      // Open SNAP payment popup, please refer to docs for all available options: https://snap-docs.midtrans.com/#snap-js
      //snap.pay(response.token);
			//$('.target').load("snap.pay(response.token)");
		})

			});
		/*$("#VA").click(function(){
			$("#bank").hide();
		});*/
	});
	/**
  * Send AJAX POST request to checkout.php, then call callback with the API response
  * @param {object} requestBody: request body to be sent to SNAP API
  * @param {function} callback: callback function to pass the response
  */
  function getSnapToken(requestBody, callback) {
  	var xmlHttp = new XMLHttpRequest();
  	xmlHttp.onreadystatechange = function() {
  		if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
  			callback(xmlHttp.responseText);
  		}
  	}
  	xmlHttp.open("post", "http://localhost/midtrans/checkout.php");
  	xmlHttp.send(JSON.stringify(requestBody));
  }
</script>

<script type="text/javascript">
	$('#angsuran-note').hide();
	$('#angsuran').on('change', function(e){
		if($('#angsuran').val() == ""){
			$('#angsuran-note').hide("slow");
		}else{
			$('#angsuran-note').show("slow");
		}
		$('#angsuran-nilai').html($('#angsuran').val()+'x');
	})
</script>
<script type="text/javascript">

	$('a#confirm').on('click', function(e){
		// alert("Angsuran "+$('#angsuran option:selected').val()+" Payment "+document.querySelector('input[name="metode"]:checked').value); untuk ngecek
		e.preventDefault();
		swal({
			title: 'Konfirmasi Pesanan',
			text: "Minta konfirmasi ketersediaan pemesanan yang telah Anda pesan?",
			type: 'info',
			showCancelButton: true,
			confirmButtonColor: '#107ADE',
			cancelButtonColor: '#C30000',
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then((result) => {
			// if (result.value) {
				if($('#angsuran option:selected').val() != "" && document.querySelector('input[name="metode"]:checked').value != "" ){

					var mydata = $('form#checkout').serialize();

					$.ajax({
						type : "POST",
						data : mydata,
						url : '<?php echo base_url('Shop/generateInvoice/')?>',
						typedata : 'json',
						beforeSend  : function(){
							$("a#confirm").prop("disabled", true);
							$("#loading").show();
							$("#wrapper").css('opacity','0.5');  
						},
						success : function(hasil){
							console.log(hasil);
							var rs = $.parseJSON(hasil);
							swal({
								type : rs['icon'],
								text : rs['text']
							}).then( function(e) {
								$("a#confirm").prop("disabled", false);
								$("#wrapper").css('opacity','1.0');
								if(rs['icon'] == "success"){
									location.replace(rs['direct']);
								}
							});
						} 
					});
				}else{
					swal({
						type : "info",
						text : "Mohon pilih angsuran dan metode pembayaran terlebih dahulu"
					});
				}
			// }
		})
	})
</script>
<script type="text/javascript">

	$('#pay-button').click(function (event) {
		event.preventDefault();
		$(this).attr("disabled", "disabled");

		$.ajax({
			url: '<?=site_url()?>/snap/token',
			cache: false,
			success: function(data) {
        //location = data;
        console.log('token = '+data);
        
        var resultType = document.getElementById('result-type');
        var resultData = document.getElementById('result-data');
        function changeResult(type,data){
        	$("#result-type").val(type);
        	$("#result-data").val(JSON.stringify(data));
          //resultType.innerHTML = type;
          //resultData.innerHTML = JSON.stringify(data);
      }
      snap.pay(data, {

      	onSuccess: function(result){
      		changeResult('success', result);
      		console.log(result.status_message);
      		console.log(result);
      		$("#payment-form").submit();
      	},
      	onPending: function(result){
      		changeResult('pending', result);
      		console.log(result.status_message);
      		$("#payment-form").submit();
      	},
      	onError: function(result){
      		changeResult('error', result);
      		console.log(result.status_message);
      		$("#payment-form").submit();
      	}
      });
  }
});
	});
</script>
<script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="SB-Mid-client-gampCCrldTZVOghQ"></script>